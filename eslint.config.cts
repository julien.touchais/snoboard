const js = require('@eslint/js')
const pluginVue = require('eslint-plugin-vue')
const skipFormattingConfig = require('@vue/eslint-config-prettier/skip-formatting')
const {
  vueTsConfigs,
  defineConfigWithVueTs
} = require('@vue/eslint-config-typescript')

module.exports = defineConfigWithVueTs(
  {
    name: 'app/files-to-lint',
    files: ['**/*.{js,mjs,jsx,vue}']
  },
  {
    name: 'app/files-to-ignore',
    ignores: ['**/dist/**', 'src/gql/codegen/**']
  },
  pluginVue.configs['flat/recommended'],
  vueTsConfigs.recommended,
  skipFormattingConfig,
  {
    languageOptions: {
      ecmaVersion: 'latest',
      sourceType: 'script'
    }
  }
)
