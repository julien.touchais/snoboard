import type { CodegenConfig } from '@graphql-codegen/cli'
import 'dotenv/config'

const config: CodegenConfig = {
  schema: process.env.VITE_API_URL,
  documents: ['src/**/*.vue', 'src/gql/queries.ts'],
  ignoreNoDocuments: true, // for better experience with the watcher
  generates: {
    './src/gql/codegen/': {
      preset: 'client',
      config: {
        useTypeImports: true
      },
      plugins: []
    }
  }
}

export default config
