/// <reference types="vite/client" />
/// <reference types="unplugin-icons/types/vue" />

interface ImportMetaEnv {
  /** URL of the GraphQL API endpoint */
  readonly VITE_API_URL: string
  /**
   * URL where static files can be found (e.g. target secondary structure,
   * sequences...)
   */
  readonly VITE_STATIC_URL: string
}

interface ImportMeta {
  readonly env: ImportMetaEnv
}
