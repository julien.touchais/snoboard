import TypographyPlugin from '@tailwindcss/typography'
/** @type {import('tailwindcss').Config} */
export default {
  content: ['./index.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
  theme: {
    extend: {
      keyframes: {
        spinY: {
          '0%,100%': { transform: 'rotateY(0)' },
          '50%': { transform: 'rotateY(.5turn)' }
        },
        dnaSpin: {
          '0%,100%': { transform: 'translateY(0)' },
          '100%': { transform: 'translateY(-100%)' }
        }
      }
    }
  },
  plugins: [TypographyPlugin],
  safelist: [
    {
      // BaseDescribedChip, BaseLinkListCard, SequenceBoard
      pattern: /^!border-[a-z]+-600$/
    },
    {
      // BaseLinkListCard, DataTableView, ModificationView
      pattern: /^border-[a-z]+-600$/,
      variants: ['hover']
    },
    {
      // BaseDescribedChip, SequenceBoard, DataTableView, ModificationView
      pattern: /^!?bg-[a-z]+-100$/
    },
    {
      // BaseDescribedChip, SequenceBoard, DataTableView, ModificationView
      pattern: /^!?text-[a-z]+-600$/
    },
    {
      // DataTableView
      pattern: /^text-[a-z]+-600$/,
      variants: ['hover']
    },
    {
      // InteractionCardCD, InteractionCardHACA, BaseLegendButtonOverlay
      pattern: /^fill-[a-z]+-100$/
    },
    {
      // InteractionCardCD, InteractionCardHACA
      pattern: /^fill-[a-z]+-600$/
    },
    {
      // InteractionCardCD, InteractionCardHACA, BaseLegendButtonOverlay
      pattern: /^stroke-[a-z]+-600$/
    },
    'brightness-90', // ChromosomeMagnify
    'cursor-pointer', // ChromosomeMagnify
    'text-xs', // SecondaryStructure, SequenceAlignment (tooltip)
    'text-center' // SecondaryStructure, SequenceAlignment (tooltip)
  ]
}
