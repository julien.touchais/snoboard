/* eslint-disable */
import type { TypedDocumentNode as DocumentNode } from '@graphql-typed-document-node/core';
export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
export type MakeEmpty<T extends { [key: string]: unknown }, K extends keyof T> = { [_ in K]?: never };
export type Incremental<T> = T | { [P in keyof T]?: P extends ' $fragmentName' | '__typename' ? T[P] : never };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: { input: string; output: string; }
  String: { input: string; output: string; }
  Boolean: { input: boolean; output: boolean; }
  Int: { input: number; output: number; }
  Float: { input: number; output: number; }
};

export type AlignGroup = SequenceGroup & {
  __typename?: 'AlignGroup';
  /** The description of the group (Optional) */
  description?: Maybe<Scalars['String']['output']>;
  /** The group id */
  id: Scalars['ID']['output'];
  /** The name of the group (Optional) */
  name?: Maybe<Scalars['String']['output']>;
  /** The sequences that are part of group */
  sequences: Array<Sequence>;
  /** The type of group */
  type?: Maybe<AlignGroupType>;
};

export type AlignGroupAggregateSelection = {
  __typename?: 'AlignGroupAggregateSelection';
  count: Scalars['Int']['output'];
  description: StringAggregateSelection;
  id: IdAggregateSelection;
  name: StringAggregateSelection;
};

export type AlignGroupEdge = {
  __typename?: 'AlignGroupEdge';
  cursor: Scalars['String']['output'];
  node: AlignGroup;
};

export type AlignGroupOptions = {
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  /** Specify one or more AlignGroupSort objects to sort AlignGroups by. The sorts will be applied in the order in which they are arranged in the array. */
  sort?: InputMaybe<Array<AlignGroupSort>>;
};

/** Fields to sort AlignGroups by. The order in which sorts are applied is not guaranteed when specifying many fields in one AlignGroupSort object. */
export type AlignGroupSort = {
  description?: InputMaybe<SortDirection>;
  id?: InputMaybe<SortDirection>;
  name?: InputMaybe<SortDirection>;
  type?: InputMaybe<SortDirection>;
};

export enum AlignGroupType {
  /** A group of Guides */
  Guides = 'GUIDES',
  /** A group of Targets */
  Targets = 'TARGETS'
}

export type AlignGroupWhere = {
  AND?: InputMaybe<Array<AlignGroupWhere>>;
  NOT?: InputMaybe<AlignGroupWhere>;
  OR?: InputMaybe<Array<AlignGroupWhere>>;
  description?: InputMaybe<Scalars['String']['input']>;
  description_CONTAINS?: InputMaybe<Scalars['String']['input']>;
  description_ENDS_WITH?: InputMaybe<Scalars['String']['input']>;
  description_IN?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  description_STARTS_WITH?: InputMaybe<Scalars['String']['input']>;
  id?: InputMaybe<Scalars['ID']['input']>;
  id_CONTAINS?: InputMaybe<Scalars['ID']['input']>;
  id_ENDS_WITH?: InputMaybe<Scalars['ID']['input']>;
  id_IN?: InputMaybe<Array<Scalars['ID']['input']>>;
  id_STARTS_WITH?: InputMaybe<Scalars['ID']['input']>;
  name?: InputMaybe<Scalars['String']['input']>;
  name_CONTAINS?: InputMaybe<Scalars['String']['input']>;
  name_ENDS_WITH?: InputMaybe<Scalars['String']['input']>;
  name_IN?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  name_STARTS_WITH?: InputMaybe<Scalars['String']['input']>;
  type?: InputMaybe<AlignGroupType>;
  type_IN?: InputMaybe<Array<InputMaybe<AlignGroupType>>>;
};

export type AlignGroupsConnection = {
  __typename?: 'AlignGroupsConnection';
  edges: Array<AlignGroupEdge>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int']['output'];
};

export enum AlignType {
  Guide = 'GUIDE',
  Target = 'TARGET'
}

export type AlignedModification = {
  __typename?: 'AlignedModification';
  /** The position corrected for the alignement */
  position: Scalars['Int']['output'];
  /** The modification */
  ref: Modification;
};

export type AlignedModificationAggregateSelection = {
  __typename?: 'AlignedModificationAggregateSelection';
  count: Scalars['Int']['output'];
  position: IntAggregateSelection;
};

export type AlignedModificationEdge = {
  __typename?: 'AlignedModificationEdge';
  cursor: Scalars['String']['output'];
  node: AlignedModification;
};

export type AlignedModificationOptions = {
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  /** Specify one or more AlignedModificationSort objects to sort AlignedModifications by. The sorts will be applied in the order in which they are arranged in the array. */
  sort?: InputMaybe<Array<AlignedModificationSort>>;
};

/** Fields to sort AlignedModifications by. The order in which sorts are applied is not guaranteed when specifying many fields in one AlignedModificationSort object. */
export type AlignedModificationSort = {
  position?: InputMaybe<SortDirection>;
};

export type AlignedModificationWhere = {
  AND?: InputMaybe<Array<AlignedModificationWhere>>;
  NOT?: InputMaybe<AlignedModificationWhere>;
  OR?: InputMaybe<Array<AlignedModificationWhere>>;
  position?: InputMaybe<Scalars['Int']['input']>;
  position_GT?: InputMaybe<Scalars['Int']['input']>;
  position_GTE?: InputMaybe<Scalars['Int']['input']>;
  position_IN?: InputMaybe<Array<Scalars['Int']['input']>>;
  position_LT?: InputMaybe<Scalars['Int']['input']>;
  position_LTE?: InputMaybe<Scalars['Int']['input']>;
};

export type AlignedModificationsConnection = {
  __typename?: 'AlignedModificationsConnection';
  edges: Array<AlignedModificationEdge>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int']['output'];
};

export type Alignment = {
  __typename?: 'Alignment';
  /** The track id */
  id: Scalars['ID']['output'];
  /** A track is a sequence of something */
  tracks: Array<Track>;
  /** The type of tracks, etither GUIDE or TARGET */
  type: AlignType;
};

export type AlignmentAggregateSelection = {
  __typename?: 'AlignmentAggregateSelection';
  count: Scalars['Int']['output'];
  id: IdAggregateSelection;
};

export type AlignmentEdge = {
  __typename?: 'AlignmentEdge';
  cursor: Scalars['String']['output'];
  node: Alignment;
};

export type AlignmentOptions = {
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  /** Specify one or more AlignmentSort objects to sort Alignments by. The sorts will be applied in the order in which they are arranged in the array. */
  sort?: InputMaybe<Array<AlignmentSort>>;
};

/** Fields to sort Alignments by. The order in which sorts are applied is not guaranteed when specifying many fields in one AlignmentSort object. */
export type AlignmentSort = {
  id?: InputMaybe<SortDirection>;
  type?: InputMaybe<SortDirection>;
};

export type AlignmentWhere = {
  AND?: InputMaybe<Array<AlignmentWhere>>;
  NOT?: InputMaybe<AlignmentWhere>;
  OR?: InputMaybe<Array<AlignmentWhere>>;
  id?: InputMaybe<Scalars['ID']['input']>;
  id_CONTAINS?: InputMaybe<Scalars['ID']['input']>;
  id_ENDS_WITH?: InputMaybe<Scalars['ID']['input']>;
  id_IN?: InputMaybe<Array<Scalars['ID']['input']>>;
  id_STARTS_WITH?: InputMaybe<Scalars['ID']['input']>;
  type?: InputMaybe<AlignType>;
  type_IN?: InputMaybe<Array<AlignType>>;
};

export type AlignmentsConnection = {
  __typename?: 'AlignmentsConnection';
  edges: Array<AlignmentEdge>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int']['output'];
};

export enum AnnotationType {
  /** ACA_Box */
  AcaBox = 'ACA_Box',
  /** C Box */
  CBox = 'C_Box',
  /** D_Box */
  DBox = 'D_Box',
  /** H_Box */
  HBox = 'H_Box',
  /** Anything else */
  Other = 'Other'
}

/**
 * The edge properties for the following fields:
 * * Duplex.strands
 */
export type BindIn = {
  __typename?: 'BindIn';
  /** The end position, where start < end (following 5'->3' direction) */
  end: Scalars['Int']['output'];
  /** The primary sequence of the binding in duplex */
  primary?: Maybe<Scalars['Boolean']['output']>;
  /** The start position of sequence part, where start < end (following 5'->3' direction) */
  start: Scalars['Int']['output'];
};

export type BindInAggregationWhereInput = {
  AND?: InputMaybe<Array<BindInAggregationWhereInput>>;
  NOT?: InputMaybe<BindInAggregationWhereInput>;
  OR?: InputMaybe<Array<BindInAggregationWhereInput>>;
  end_AVERAGE_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  end_AVERAGE_GT?: InputMaybe<Scalars['Float']['input']>;
  end_AVERAGE_GTE?: InputMaybe<Scalars['Float']['input']>;
  end_AVERAGE_LT?: InputMaybe<Scalars['Float']['input']>;
  end_AVERAGE_LTE?: InputMaybe<Scalars['Float']['input']>;
  end_MAX_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  end_MAX_GT?: InputMaybe<Scalars['Int']['input']>;
  end_MAX_GTE?: InputMaybe<Scalars['Int']['input']>;
  end_MAX_LT?: InputMaybe<Scalars['Int']['input']>;
  end_MAX_LTE?: InputMaybe<Scalars['Int']['input']>;
  end_MIN_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  end_MIN_GT?: InputMaybe<Scalars['Int']['input']>;
  end_MIN_GTE?: InputMaybe<Scalars['Int']['input']>;
  end_MIN_LT?: InputMaybe<Scalars['Int']['input']>;
  end_MIN_LTE?: InputMaybe<Scalars['Int']['input']>;
  end_SUM_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  end_SUM_GT?: InputMaybe<Scalars['Int']['input']>;
  end_SUM_GTE?: InputMaybe<Scalars['Int']['input']>;
  end_SUM_LT?: InputMaybe<Scalars['Int']['input']>;
  end_SUM_LTE?: InputMaybe<Scalars['Int']['input']>;
  start_AVERAGE_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  start_AVERAGE_GT?: InputMaybe<Scalars['Float']['input']>;
  start_AVERAGE_GTE?: InputMaybe<Scalars['Float']['input']>;
  start_AVERAGE_LT?: InputMaybe<Scalars['Float']['input']>;
  start_AVERAGE_LTE?: InputMaybe<Scalars['Float']['input']>;
  start_MAX_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  start_MAX_GT?: InputMaybe<Scalars['Int']['input']>;
  start_MAX_GTE?: InputMaybe<Scalars['Int']['input']>;
  start_MAX_LT?: InputMaybe<Scalars['Int']['input']>;
  start_MAX_LTE?: InputMaybe<Scalars['Int']['input']>;
  start_MIN_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  start_MIN_GT?: InputMaybe<Scalars['Int']['input']>;
  start_MIN_GTE?: InputMaybe<Scalars['Int']['input']>;
  start_MIN_LT?: InputMaybe<Scalars['Int']['input']>;
  start_MIN_LTE?: InputMaybe<Scalars['Int']['input']>;
  start_SUM_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  start_SUM_GT?: InputMaybe<Scalars['Int']['input']>;
  start_SUM_GTE?: InputMaybe<Scalars['Int']['input']>;
  start_SUM_LT?: InputMaybe<Scalars['Int']['input']>;
  start_SUM_LTE?: InputMaybe<Scalars['Int']['input']>;
};

export type BindInSort = {
  end?: InputMaybe<SortDirection>;
  primary?: InputMaybe<SortDirection>;
  start?: InputMaybe<SortDirection>;
};

export type BindInWhere = {
  AND?: InputMaybe<Array<BindInWhere>>;
  NOT?: InputMaybe<BindInWhere>;
  OR?: InputMaybe<Array<BindInWhere>>;
  end?: InputMaybe<Scalars['Int']['input']>;
  end_GT?: InputMaybe<Scalars['Int']['input']>;
  end_GTE?: InputMaybe<Scalars['Int']['input']>;
  end_IN?: InputMaybe<Array<Scalars['Int']['input']>>;
  end_LT?: InputMaybe<Scalars['Int']['input']>;
  end_LTE?: InputMaybe<Scalars['Int']['input']>;
  primary?: InputMaybe<Scalars['Boolean']['input']>;
  start?: InputMaybe<Scalars['Int']['input']>;
  start_GT?: InputMaybe<Scalars['Int']['input']>;
  start_GTE?: InputMaybe<Scalars['Int']['input']>;
  start_IN?: InputMaybe<Array<Scalars['Int']['input']>>;
  start_LT?: InputMaybe<Scalars['Int']['input']>;
  start_LTE?: InputMaybe<Scalars['Int']['input']>;
};

/** Description of a Chromosome */
export type Chromosome = Sequence & {
  __typename?: 'Chromosome';
  /** ALternative chromosome names */
  altnames?: Maybe<Array<Scalars['String']['output']>>;
  /** The annotation of the sequence if any */
  annotation?: Maybe<AnnotationType>;
  /** The Chemical Entities of Biological Interest Ontology (ChEBI) id */
  chebi_id?: Maybe<Scalars['String']['output']>;
  /** The class of the sequence */
  class: SequenceClass;
  /** The chromosome description (i.e. the comment part in fasta file) */
  description?: Maybe<Scalars['String']['output']>;
  /** The features associated with the chromosome */
  features: Array<Sequence>;
  featuresAggregate?: Maybe<ChromosomeSequenceFeaturesAggregationSelection>;
  featuresConnection: SequenceFeaturesConnection;
  /** The reference genome the sequence comes from */
  genome?: Maybe<Genome>;
  genomeAggregate?: Maybe<ChromosomeGenomeGenomeAggregationSelection>;
  genomeConnection: SequenceGenomeConnection;
  /** The type of GraphQL object */
  graphql_type: GraphQlType;
  /** The sequence ID (i.e. id part in fasta file) */
  id: Scalars['ID']['output'];
  /** The length of sequence in number of nucleotide */
  length: Scalars['Int']['output'];
  /** Common chromosome name */
  name?: Maybe<Scalars['String']['output']>;
  /** The sequence itself (url can be used instead for huge sequence) */
  seq?: Maybe<Scalars['String']['output']>;
  /** The Sequence Ontology (so) id */
  so_id?: Maybe<Scalars['String']['output']>;
  /** The type of the sequence (DNA or RNA) */
  type: SequenceType;
  /** The url to get the file containning the sequence */
  url?: Maybe<Scalars['String']['output']>;
};


/** Description of a Chromosome */
export type ChromosomeFeaturesArgs = {
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  options?: InputMaybe<SequenceOptions>;
  where?: InputMaybe<SequenceWhere>;
};


/** Description of a Chromosome */
export type ChromosomeFeaturesAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  where?: InputMaybe<SequenceWhere>;
};


/** Description of a Chromosome */
export type ChromosomeFeaturesConnectionArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  sort?: InputMaybe<Array<SequenceFeaturesConnectionSort>>;
  where?: InputMaybe<SequenceFeaturesConnectionWhere>;
};


/** Description of a Chromosome */
export type ChromosomeGenomeArgs = {
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  options?: InputMaybe<GenomeOptions>;
  where?: InputMaybe<GenomeWhere>;
};


/** Description of a Chromosome */
export type ChromosomeGenomeAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  where?: InputMaybe<GenomeWhere>;
};


/** Description of a Chromosome */
export type ChromosomeGenomeConnectionArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  sort?: InputMaybe<Array<SequenceGenomeConnectionSort>>;
  where?: InputMaybe<SequenceGenomeConnectionWhere>;
};

export type ChromosomeAggregateSelection = {
  __typename?: 'ChromosomeAggregateSelection';
  chebi_id: StringAggregateSelection;
  count: Scalars['Int']['output'];
  description: StringAggregateSelection;
  id: IdAggregateSelection;
  length: IntAggregateSelection;
  name: StringAggregateSelection;
  seq: StringAggregateSelection;
  so_id: StringAggregateSelection;
  url: StringAggregateSelection;
};

export type ChromosomeEdge = {
  __typename?: 'ChromosomeEdge';
  cursor: Scalars['String']['output'];
  node: Chromosome;
};

export type ChromosomeFeaturesAggregateInput = {
  AND?: InputMaybe<Array<ChromosomeFeaturesAggregateInput>>;
  NOT?: InputMaybe<ChromosomeFeaturesAggregateInput>;
  OR?: InputMaybe<Array<ChromosomeFeaturesAggregateInput>>;
  count?: InputMaybe<Scalars['Int']['input']>;
  count_GT?: InputMaybe<Scalars['Int']['input']>;
  count_GTE?: InputMaybe<Scalars['Int']['input']>;
  count_LT?: InputMaybe<Scalars['Int']['input']>;
  count_LTE?: InputMaybe<Scalars['Int']['input']>;
  edge?: InputMaybe<HasFeatureAggregationWhereInput>;
  node?: InputMaybe<ChromosomeFeaturesNodeAggregationWhereInput>;
};

export type ChromosomeFeaturesNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<ChromosomeFeaturesNodeAggregationWhereInput>>;
  NOT?: InputMaybe<ChromosomeFeaturesNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<ChromosomeFeaturesNodeAggregationWhereInput>>;
  chebi_id_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  chebi_id_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  chebi_id_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  chebi_id_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  chebi_id_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  chebi_id_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  description_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  description_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  length_AVERAGE_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  length_AVERAGE_GT?: InputMaybe<Scalars['Float']['input']>;
  length_AVERAGE_GTE?: InputMaybe<Scalars['Float']['input']>;
  length_AVERAGE_LT?: InputMaybe<Scalars['Float']['input']>;
  length_AVERAGE_LTE?: InputMaybe<Scalars['Float']['input']>;
  length_MAX_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  length_MAX_GT?: InputMaybe<Scalars['Int']['input']>;
  length_MAX_GTE?: InputMaybe<Scalars['Int']['input']>;
  length_MAX_LT?: InputMaybe<Scalars['Int']['input']>;
  length_MAX_LTE?: InputMaybe<Scalars['Int']['input']>;
  length_MIN_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  length_MIN_GT?: InputMaybe<Scalars['Int']['input']>;
  length_MIN_GTE?: InputMaybe<Scalars['Int']['input']>;
  length_MIN_LT?: InputMaybe<Scalars['Int']['input']>;
  length_MIN_LTE?: InputMaybe<Scalars['Int']['input']>;
  length_SUM_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  length_SUM_GT?: InputMaybe<Scalars['Int']['input']>;
  length_SUM_GTE?: InputMaybe<Scalars['Int']['input']>;
  length_SUM_LT?: InputMaybe<Scalars['Int']['input']>;
  length_SUM_LTE?: InputMaybe<Scalars['Int']['input']>;
  name_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  name_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  seq_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  seq_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  seq_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  seq_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  seq_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  seq_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  seq_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  seq_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  seq_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  seq_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  seq_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  seq_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  seq_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  seq_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  seq_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  so_id_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  url_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  url_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
};

export type ChromosomeGenomeAggregateInput = {
  AND?: InputMaybe<Array<ChromosomeGenomeAggregateInput>>;
  NOT?: InputMaybe<ChromosomeGenomeAggregateInput>;
  OR?: InputMaybe<Array<ChromosomeGenomeAggregateInput>>;
  count?: InputMaybe<Scalars['Int']['input']>;
  count_GT?: InputMaybe<Scalars['Int']['input']>;
  count_GTE?: InputMaybe<Scalars['Int']['input']>;
  count_LT?: InputMaybe<Scalars['Int']['input']>;
  count_LTE?: InputMaybe<Scalars['Int']['input']>;
  node?: InputMaybe<ChromosomeGenomeNodeAggregationWhereInput>;
};

export type ChromosomeGenomeGenomeAggregationSelection = {
  __typename?: 'ChromosomeGenomeGenomeAggregationSelection';
  count: Scalars['Int']['output'];
  node?: Maybe<ChromosomeGenomeGenomeNodeAggregateSelection>;
};

export type ChromosomeGenomeGenomeNodeAggregateSelection = {
  __typename?: 'ChromosomeGenomeGenomeNodeAggregateSelection';
  description: StringAggregateSelection;
  id: IdAggregateSelection;
  so_id: StringAggregateSelection;
  source: StringAggregateSelection;
  url: StringAggregateSelection;
  version: StringAggregateSelection;
};

export type ChromosomeGenomeNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<ChromosomeGenomeNodeAggregationWhereInput>>;
  NOT?: InputMaybe<ChromosomeGenomeNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<ChromosomeGenomeNodeAggregationWhereInput>>;
  description_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  description_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  so_id_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  source_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  source_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  source_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  source_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  source_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  source_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  source_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  source_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  source_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  source_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  source_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  source_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  source_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  source_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  source_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  url_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  url_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  version_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  version_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  version_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  version_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  version_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  version_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  version_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  version_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  version_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  version_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  version_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  version_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  version_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  version_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  version_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
};

export type ChromosomeOptions = {
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  /** Specify one or more ChromosomeSort objects to sort Chromosomes by. The sorts will be applied in the order in which they are arranged in the array. */
  sort?: InputMaybe<Array<ChromosomeSort>>;
};

export type ChromosomeSequenceFeaturesAggregationSelection = {
  __typename?: 'ChromosomeSequenceFeaturesAggregationSelection';
  count: Scalars['Int']['output'];
  edge?: Maybe<ChromosomeSequenceFeaturesEdgeAggregateSelection>;
  node?: Maybe<ChromosomeSequenceFeaturesNodeAggregateSelection>;
};

export type ChromosomeSequenceFeaturesEdgeAggregateSelection = {
  __typename?: 'ChromosomeSequenceFeaturesEdgeAggregateSelection';
  end: IntAggregateSelection;
  start: IntAggregateSelection;
};

export type ChromosomeSequenceFeaturesNodeAggregateSelection = {
  __typename?: 'ChromosomeSequenceFeaturesNodeAggregateSelection';
  chebi_id: StringAggregateSelection;
  description: StringAggregateSelection;
  id: IdAggregateSelection;
  length: IntAggregateSelection;
  name: StringAggregateSelection;
  seq: StringAggregateSelection;
  so_id: StringAggregateSelection;
  url: StringAggregateSelection;
};

/** Fields to sort Chromosomes by. The order in which sorts are applied is not guaranteed when specifying many fields in one ChromosomeSort object. */
export type ChromosomeSort = {
  annotation?: InputMaybe<SortDirection>;
  chebi_id?: InputMaybe<SortDirection>;
  class?: InputMaybe<SortDirection>;
  description?: InputMaybe<SortDirection>;
  graphql_type?: InputMaybe<SortDirection>;
  id?: InputMaybe<SortDirection>;
  length?: InputMaybe<SortDirection>;
  name?: InputMaybe<SortDirection>;
  seq?: InputMaybe<SortDirection>;
  so_id?: InputMaybe<SortDirection>;
  type?: InputMaybe<SortDirection>;
  url?: InputMaybe<SortDirection>;
};

export type ChromosomeWhere = {
  AND?: InputMaybe<Array<ChromosomeWhere>>;
  NOT?: InputMaybe<ChromosomeWhere>;
  OR?: InputMaybe<Array<ChromosomeWhere>>;
  altnames?: InputMaybe<Array<Scalars['String']['input']>>;
  altnames_INCLUDES?: InputMaybe<Scalars['String']['input']>;
  annotation?: InputMaybe<AnnotationType>;
  annotation_IN?: InputMaybe<Array<InputMaybe<AnnotationType>>>;
  chebi_id?: InputMaybe<Scalars['String']['input']>;
  chebi_id_CONTAINS?: InputMaybe<Scalars['String']['input']>;
  chebi_id_ENDS_WITH?: InputMaybe<Scalars['String']['input']>;
  chebi_id_IN?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  chebi_id_STARTS_WITH?: InputMaybe<Scalars['String']['input']>;
  class?: InputMaybe<SequenceClass>;
  class_IN?: InputMaybe<Array<SequenceClass>>;
  description?: InputMaybe<Scalars['String']['input']>;
  description_CONTAINS?: InputMaybe<Scalars['String']['input']>;
  description_ENDS_WITH?: InputMaybe<Scalars['String']['input']>;
  description_IN?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  description_STARTS_WITH?: InputMaybe<Scalars['String']['input']>;
  featuresAggregate?: InputMaybe<ChromosomeFeaturesAggregateInput>;
  /** Return Chromosomes where all of the related SequenceFeaturesConnections match this filter */
  featuresConnection_ALL?: InputMaybe<SequenceFeaturesConnectionWhere>;
  /** Return Chromosomes where none of the related SequenceFeaturesConnections match this filter */
  featuresConnection_NONE?: InputMaybe<SequenceFeaturesConnectionWhere>;
  /** Return Chromosomes where one of the related SequenceFeaturesConnections match this filter */
  featuresConnection_SINGLE?: InputMaybe<SequenceFeaturesConnectionWhere>;
  /** Return Chromosomes where some of the related SequenceFeaturesConnections match this filter */
  featuresConnection_SOME?: InputMaybe<SequenceFeaturesConnectionWhere>;
  /** Return Chromosomes where all of the related Sequences match this filter */
  features_ALL?: InputMaybe<SequenceWhere>;
  /** Return Chromosomes where none of the related Sequences match this filter */
  features_NONE?: InputMaybe<SequenceWhere>;
  /** Return Chromosomes where one of the related Sequences match this filter */
  features_SINGLE?: InputMaybe<SequenceWhere>;
  /** Return Chromosomes where some of the related Sequences match this filter */
  features_SOME?: InputMaybe<SequenceWhere>;
  genome?: InputMaybe<GenomeWhere>;
  genomeAggregate?: InputMaybe<ChromosomeGenomeAggregateInput>;
  genomeConnection?: InputMaybe<SequenceGenomeConnectionWhere>;
  genomeConnection_NOT?: InputMaybe<SequenceGenomeConnectionWhere>;
  genome_NOT?: InputMaybe<GenomeWhere>;
  graphql_type?: InputMaybe<GraphQlType>;
  graphql_type_IN?: InputMaybe<Array<GraphQlType>>;
  id?: InputMaybe<Scalars['ID']['input']>;
  id_CONTAINS?: InputMaybe<Scalars['ID']['input']>;
  id_ENDS_WITH?: InputMaybe<Scalars['ID']['input']>;
  id_IN?: InputMaybe<Array<Scalars['ID']['input']>>;
  id_STARTS_WITH?: InputMaybe<Scalars['ID']['input']>;
  length?: InputMaybe<Scalars['Int']['input']>;
  length_GT?: InputMaybe<Scalars['Int']['input']>;
  length_GTE?: InputMaybe<Scalars['Int']['input']>;
  length_IN?: InputMaybe<Array<Scalars['Int']['input']>>;
  length_LT?: InputMaybe<Scalars['Int']['input']>;
  length_LTE?: InputMaybe<Scalars['Int']['input']>;
  name?: InputMaybe<Scalars['String']['input']>;
  name_CONTAINS?: InputMaybe<Scalars['String']['input']>;
  name_ENDS_WITH?: InputMaybe<Scalars['String']['input']>;
  name_IN?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  name_STARTS_WITH?: InputMaybe<Scalars['String']['input']>;
  seq?: InputMaybe<Scalars['String']['input']>;
  seq_CONTAINS?: InputMaybe<Scalars['String']['input']>;
  seq_ENDS_WITH?: InputMaybe<Scalars['String']['input']>;
  seq_IN?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  seq_STARTS_WITH?: InputMaybe<Scalars['String']['input']>;
  so_id?: InputMaybe<Scalars['String']['input']>;
  so_id_CONTAINS?: InputMaybe<Scalars['String']['input']>;
  so_id_ENDS_WITH?: InputMaybe<Scalars['String']['input']>;
  so_id_IN?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  so_id_STARTS_WITH?: InputMaybe<Scalars['String']['input']>;
  type?: InputMaybe<SequenceType>;
  type_IN?: InputMaybe<Array<SequenceType>>;
  url?: InputMaybe<Scalars['String']['input']>;
  url_CONTAINS?: InputMaybe<Scalars['String']['input']>;
  url_ENDS_WITH?: InputMaybe<Scalars['String']['input']>;
  url_IN?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  url_STARTS_WITH?: InputMaybe<Scalars['String']['input']>;
};

export type ChromosomesConnection = {
  __typename?: 'ChromosomesConnection';
  edges: Array<ChromosomeEdge>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int']['output'];
};

/** A cluster of SnoRNAs */
export type Cluster = GuideGroup & {
  __typename?: 'Cluster';
  /** The guides that are part of cluster */
  guides: Array<Guide>;
  guidesAggregate?: Maybe<ClusterGuideGuidesAggregationSelection>;
  guidesConnection: GuideGroupGuidesConnection;
  /** The cluster id */
  id: Scalars['ID']['output'];
};


/** A cluster of SnoRNAs */
export type ClusterGuidesArgs = {
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  options?: InputMaybe<GuideOptions>;
  where?: InputMaybe<GuideWhere>;
};


/** A cluster of SnoRNAs */
export type ClusterGuidesAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  where?: InputMaybe<GuideWhere>;
};


/** A cluster of SnoRNAs */
export type ClusterGuidesConnectionArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  sort?: InputMaybe<Array<GuideGroupGuidesConnectionSort>>;
  where?: InputMaybe<GuideGroupGuidesConnectionWhere>;
};

export type ClusterAggregateSelection = {
  __typename?: 'ClusterAggregateSelection';
  count: Scalars['Int']['output'];
  id: IdAggregateSelection;
};

export type ClusterEdge = {
  __typename?: 'ClusterEdge';
  cursor: Scalars['String']['output'];
  node: Cluster;
};

export type ClusterGuideGuidesAggregationSelection = {
  __typename?: 'ClusterGuideGuidesAggregationSelection';
  count: Scalars['Int']['output'];
  node?: Maybe<ClusterGuideGuidesNodeAggregateSelection>;
};

export type ClusterGuideGuidesNodeAggregateSelection = {
  __typename?: 'ClusterGuideGuidesNodeAggregateSelection';
  chebi_id: StringAggregateSelection;
  description: StringAggregateSelection;
  id: IdAggregateSelection;
  length: IntAggregateSelection;
  name: StringAggregateSelection;
  seq: StringAggregateSelection;
  so_id: StringAggregateSelection;
  url: StringAggregateSelection;
};

export type ClusterGuidesAggregateInput = {
  AND?: InputMaybe<Array<ClusterGuidesAggregateInput>>;
  NOT?: InputMaybe<ClusterGuidesAggregateInput>;
  OR?: InputMaybe<Array<ClusterGuidesAggregateInput>>;
  count?: InputMaybe<Scalars['Int']['input']>;
  count_GT?: InputMaybe<Scalars['Int']['input']>;
  count_GTE?: InputMaybe<Scalars['Int']['input']>;
  count_LT?: InputMaybe<Scalars['Int']['input']>;
  count_LTE?: InputMaybe<Scalars['Int']['input']>;
  node?: InputMaybe<ClusterGuidesNodeAggregationWhereInput>;
};

export type ClusterGuidesNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<ClusterGuidesNodeAggregationWhereInput>>;
  NOT?: InputMaybe<ClusterGuidesNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<ClusterGuidesNodeAggregationWhereInput>>;
  chebi_id_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  chebi_id_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  chebi_id_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  chebi_id_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  chebi_id_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  chebi_id_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  description_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  description_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  length_AVERAGE_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  length_AVERAGE_GT?: InputMaybe<Scalars['Float']['input']>;
  length_AVERAGE_GTE?: InputMaybe<Scalars['Float']['input']>;
  length_AVERAGE_LT?: InputMaybe<Scalars['Float']['input']>;
  length_AVERAGE_LTE?: InputMaybe<Scalars['Float']['input']>;
  length_MAX_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  length_MAX_GT?: InputMaybe<Scalars['Int']['input']>;
  length_MAX_GTE?: InputMaybe<Scalars['Int']['input']>;
  length_MAX_LT?: InputMaybe<Scalars['Int']['input']>;
  length_MAX_LTE?: InputMaybe<Scalars['Int']['input']>;
  length_MIN_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  length_MIN_GT?: InputMaybe<Scalars['Int']['input']>;
  length_MIN_GTE?: InputMaybe<Scalars['Int']['input']>;
  length_MIN_LT?: InputMaybe<Scalars['Int']['input']>;
  length_MIN_LTE?: InputMaybe<Scalars['Int']['input']>;
  length_SUM_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  length_SUM_GT?: InputMaybe<Scalars['Int']['input']>;
  length_SUM_GTE?: InputMaybe<Scalars['Int']['input']>;
  length_SUM_LT?: InputMaybe<Scalars['Int']['input']>;
  length_SUM_LTE?: InputMaybe<Scalars['Int']['input']>;
  name_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  name_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  seq_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  seq_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  seq_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  seq_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  seq_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  seq_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  seq_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  seq_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  seq_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  seq_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  seq_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  seq_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  seq_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  seq_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  seq_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  so_id_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  url_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  url_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
};

export type ClusterOptions = {
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  /** Specify one or more ClusterSort objects to sort Clusters by. The sorts will be applied in the order in which they are arranged in the array. */
  sort?: InputMaybe<Array<ClusterSort>>;
};

/** Fields to sort Clusters by. The order in which sorts are applied is not guaranteed when specifying many fields in one ClusterSort object. */
export type ClusterSort = {
  id?: InputMaybe<SortDirection>;
};

export type ClusterWhere = {
  AND?: InputMaybe<Array<ClusterWhere>>;
  NOT?: InputMaybe<ClusterWhere>;
  OR?: InputMaybe<Array<ClusterWhere>>;
  guidesAggregate?: InputMaybe<ClusterGuidesAggregateInput>;
  /** Return Clusters where all of the related GuideGroupGuidesConnections match this filter */
  guidesConnection_ALL?: InputMaybe<GuideGroupGuidesConnectionWhere>;
  /** Return Clusters where none of the related GuideGroupGuidesConnections match this filter */
  guidesConnection_NONE?: InputMaybe<GuideGroupGuidesConnectionWhere>;
  /** Return Clusters where one of the related GuideGroupGuidesConnections match this filter */
  guidesConnection_SINGLE?: InputMaybe<GuideGroupGuidesConnectionWhere>;
  /** Return Clusters where some of the related GuideGroupGuidesConnections match this filter */
  guidesConnection_SOME?: InputMaybe<GuideGroupGuidesConnectionWhere>;
  /** Return Clusters where all of the related Guides match this filter */
  guides_ALL?: InputMaybe<GuideWhere>;
  /** Return Clusters where none of the related Guides match this filter */
  guides_NONE?: InputMaybe<GuideWhere>;
  /** Return Clusters where one of the related Guides match this filter */
  guides_SINGLE?: InputMaybe<GuideWhere>;
  /** Return Clusters where some of the related Guides match this filter */
  guides_SOME?: InputMaybe<GuideWhere>;
  id?: InputMaybe<Scalars['ID']['input']>;
  id_CONTAINS?: InputMaybe<Scalars['ID']['input']>;
  id_ENDS_WITH?: InputMaybe<Scalars['ID']['input']>;
  id_IN?: InputMaybe<Array<Scalars['ID']['input']>>;
  id_STARTS_WITH?: InputMaybe<Scalars['ID']['input']>;
};

export type ClustersConnection = {
  __typename?: 'ClustersConnection';
  edges: Array<ClusterEdge>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int']['output'];
};

/** A Document in Markdown Format */
export type Document = {
  __typename?: 'Document';
  /** The page content */
  content: Scalars['String']['output'];
  id: Scalars['ID']['output'];
  /** The label for menu */
  menu_label: Scalars['String']['output'];
  /** The type/group/kind of document */
  types?: Maybe<Array<Maybe<Scalars['String']['output']>>>;
};

export type DocumentAggregateSelection = {
  __typename?: 'DocumentAggregateSelection';
  content: StringAggregateSelection;
  count: Scalars['Int']['output'];
  id: IdAggregateSelection;
  menu_label: StringAggregateSelection;
};

export type DocumentEdge = {
  __typename?: 'DocumentEdge';
  cursor: Scalars['String']['output'];
  node: Document;
};

export type DocumentOptions = {
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  /** Specify one or more DocumentSort objects to sort Documents by. The sorts will be applied in the order in which they are arranged in the array. */
  sort?: InputMaybe<Array<DocumentSort>>;
};

/** Fields to sort Documents by. The order in which sorts are applied is not guaranteed when specifying many fields in one DocumentSort object. */
export type DocumentSort = {
  content?: InputMaybe<SortDirection>;
  id?: InputMaybe<SortDirection>;
  menu_label?: InputMaybe<SortDirection>;
};

export type DocumentWhere = {
  AND?: InputMaybe<Array<DocumentWhere>>;
  NOT?: InputMaybe<DocumentWhere>;
  OR?: InputMaybe<Array<DocumentWhere>>;
  content?: InputMaybe<Scalars['String']['input']>;
  content_CONTAINS?: InputMaybe<Scalars['String']['input']>;
  content_ENDS_WITH?: InputMaybe<Scalars['String']['input']>;
  content_IN?: InputMaybe<Array<Scalars['String']['input']>>;
  content_STARTS_WITH?: InputMaybe<Scalars['String']['input']>;
  id?: InputMaybe<Scalars['ID']['input']>;
  id_CONTAINS?: InputMaybe<Scalars['ID']['input']>;
  id_ENDS_WITH?: InputMaybe<Scalars['ID']['input']>;
  id_IN?: InputMaybe<Array<Scalars['ID']['input']>>;
  id_STARTS_WITH?: InputMaybe<Scalars['ID']['input']>;
  menu_label?: InputMaybe<Scalars['String']['input']>;
  menu_label_CONTAINS?: InputMaybe<Scalars['String']['input']>;
  menu_label_ENDS_WITH?: InputMaybe<Scalars['String']['input']>;
  menu_label_IN?: InputMaybe<Array<Scalars['String']['input']>>;
  menu_label_STARTS_WITH?: InputMaybe<Scalars['String']['input']>;
  types?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  types_INCLUDES?: InputMaybe<Scalars['String']['input']>;
};

export type DocumentsConnection = {
  __typename?: 'DocumentsConnection';
  edges: Array<DocumentEdge>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int']['output'];
};

/** A duplex describes the pairing of two nucleotide sequences (both DNA or RNA) during an interaction */
export type Duplex = {
  __typename?: 'Duplex';
  /** The Modification id */
  id: Scalars['ID']['output'];
  /** Index in interaction (A duplex is associated to only one interaction). */
  index: Scalars['Int']['output'];
  /** The interation associated with the duplex */
  interaction: Interaction;
  interactionAggregate?: Maybe<DuplexInteractionInteractionAggregationSelection>;
  interactionConnection: DuplexInteractionConnection;
  /** The strands binding in the duplex */
  strands: Array<GenericSequence>;
  strandsAggregate?: Maybe<DuplexGenericSequenceStrandsAggregationSelection>;
  strandsConnection: DuplexStrandsConnection;
};


/** A duplex describes the pairing of two nucleotide sequences (both DNA or RNA) during an interaction */
export type DuplexInteractionArgs = {
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  options?: InputMaybe<InteractionOptions>;
  where?: InputMaybe<InteractionWhere>;
};


/** A duplex describes the pairing of two nucleotide sequences (both DNA or RNA) during an interaction */
export type DuplexInteractionAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  where?: InputMaybe<InteractionWhere>;
};


/** A duplex describes the pairing of two nucleotide sequences (both DNA or RNA) during an interaction */
export type DuplexInteractionConnectionArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  sort?: InputMaybe<Array<DuplexInteractionConnectionSort>>;
  where?: InputMaybe<DuplexInteractionConnectionWhere>;
};


/** A duplex describes the pairing of two nucleotide sequences (both DNA or RNA) during an interaction */
export type DuplexStrandsArgs = {
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  options?: InputMaybe<GenericSequenceOptions>;
  where?: InputMaybe<GenericSequenceWhere>;
};


/** A duplex describes the pairing of two nucleotide sequences (both DNA or RNA) during an interaction */
export type DuplexStrandsAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  where?: InputMaybe<GenericSequenceWhere>;
};


/** A duplex describes the pairing of two nucleotide sequences (both DNA or RNA) during an interaction */
export type DuplexStrandsConnectionArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  sort?: InputMaybe<Array<DuplexStrandsConnectionSort>>;
  where?: InputMaybe<DuplexStrandsConnectionWhere>;
};

export type DuplexAggregateSelection = {
  __typename?: 'DuplexAggregateSelection';
  count: Scalars['Int']['output'];
  id: IdAggregateSelection;
  index: IntAggregateSelection;
};

export type DuplexEdge = {
  __typename?: 'DuplexEdge';
  cursor: Scalars['String']['output'];
  node: Duplex;
};

export type DuplexGenericSequenceStrandsAggregationSelection = {
  __typename?: 'DuplexGenericSequenceStrandsAggregationSelection';
  count: Scalars['Int']['output'];
  edge?: Maybe<DuplexGenericSequenceStrandsEdgeAggregateSelection>;
  node?: Maybe<DuplexGenericSequenceStrandsNodeAggregateSelection>;
};

export type DuplexGenericSequenceStrandsEdgeAggregateSelection = {
  __typename?: 'DuplexGenericSequenceStrandsEdgeAggregateSelection';
  end: IntAggregateSelection;
  start: IntAggregateSelection;
};

export type DuplexGenericSequenceStrandsNodeAggregateSelection = {
  __typename?: 'DuplexGenericSequenceStrandsNodeAggregateSelection';
  chebi_id: StringAggregateSelection;
  description: StringAggregateSelection;
  id: IdAggregateSelection;
  length: IntAggregateSelection;
  name: StringAggregateSelection;
  seq: StringAggregateSelection;
  so_id: StringAggregateSelection;
  url: StringAggregateSelection;
};

export type DuplexInteractionAggregateInput = {
  AND?: InputMaybe<Array<DuplexInteractionAggregateInput>>;
  NOT?: InputMaybe<DuplexInteractionAggregateInput>;
  OR?: InputMaybe<Array<DuplexInteractionAggregateInput>>;
  count?: InputMaybe<Scalars['Int']['input']>;
  count_GT?: InputMaybe<Scalars['Int']['input']>;
  count_GTE?: InputMaybe<Scalars['Int']['input']>;
  count_LT?: InputMaybe<Scalars['Int']['input']>;
  count_LTE?: InputMaybe<Scalars['Int']['input']>;
  node?: InputMaybe<DuplexInteractionNodeAggregationWhereInput>;
};

export type DuplexInteractionConnection = {
  __typename?: 'DuplexInteractionConnection';
  edges: Array<DuplexInteractionRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int']['output'];
};

export type DuplexInteractionConnectionSort = {
  node?: InputMaybe<InteractionSort>;
};

export type DuplexInteractionConnectionWhere = {
  AND?: InputMaybe<Array<DuplexInteractionConnectionWhere>>;
  NOT?: InputMaybe<DuplexInteractionConnectionWhere>;
  OR?: InputMaybe<Array<DuplexInteractionConnectionWhere>>;
  node?: InputMaybe<InteractionWhere>;
};

export type DuplexInteractionInteractionAggregationSelection = {
  __typename?: 'DuplexInteractionInteractionAggregationSelection';
  count: Scalars['Int']['output'];
  node?: Maybe<DuplexInteractionInteractionNodeAggregateSelection>;
};

export type DuplexInteractionInteractionNodeAggregateSelection = {
  __typename?: 'DuplexInteractionInteractionNodeAggregateSelection';
  id: IdAggregateSelection;
};

export type DuplexInteractionNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<DuplexInteractionNodeAggregationWhereInput>>;
  NOT?: InputMaybe<DuplexInteractionNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<DuplexInteractionNodeAggregationWhereInput>>;
};

export type DuplexInteractionRelationship = {
  __typename?: 'DuplexInteractionRelationship';
  cursor: Scalars['String']['output'];
  node: Interaction;
};

export type DuplexOptions = {
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  /** Specify one or more DuplexSort objects to sort Duplexes by. The sorts will be applied in the order in which they are arranged in the array. */
  sort?: InputMaybe<Array<DuplexSort>>;
};

/** Fields to sort Duplexes by. The order in which sorts are applied is not guaranteed when specifying many fields in one DuplexSort object. */
export type DuplexSort = {
  id?: InputMaybe<SortDirection>;
  index?: InputMaybe<SortDirection>;
};

export type DuplexStrandsAggregateInput = {
  AND?: InputMaybe<Array<DuplexStrandsAggregateInput>>;
  NOT?: InputMaybe<DuplexStrandsAggregateInput>;
  OR?: InputMaybe<Array<DuplexStrandsAggregateInput>>;
  count?: InputMaybe<Scalars['Int']['input']>;
  count_GT?: InputMaybe<Scalars['Int']['input']>;
  count_GTE?: InputMaybe<Scalars['Int']['input']>;
  count_LT?: InputMaybe<Scalars['Int']['input']>;
  count_LTE?: InputMaybe<Scalars['Int']['input']>;
  edge?: InputMaybe<BindInAggregationWhereInput>;
  node?: InputMaybe<DuplexStrandsNodeAggregationWhereInput>;
};

export type DuplexStrandsConnection = {
  __typename?: 'DuplexStrandsConnection';
  edges: Array<DuplexStrandsRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int']['output'];
};

export type DuplexStrandsConnectionSort = {
  edge?: InputMaybe<BindInSort>;
  node?: InputMaybe<GenericSequenceSort>;
};

export type DuplexStrandsConnectionWhere = {
  AND?: InputMaybe<Array<DuplexStrandsConnectionWhere>>;
  NOT?: InputMaybe<DuplexStrandsConnectionWhere>;
  OR?: InputMaybe<Array<DuplexStrandsConnectionWhere>>;
  edge?: InputMaybe<BindInWhere>;
  node?: InputMaybe<GenericSequenceWhere>;
};

export type DuplexStrandsNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<DuplexStrandsNodeAggregationWhereInput>>;
  NOT?: InputMaybe<DuplexStrandsNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<DuplexStrandsNodeAggregationWhereInput>>;
  chebi_id_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  chebi_id_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  chebi_id_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  chebi_id_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  chebi_id_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  chebi_id_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  description_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  description_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  length_AVERAGE_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  length_AVERAGE_GT?: InputMaybe<Scalars['Float']['input']>;
  length_AVERAGE_GTE?: InputMaybe<Scalars['Float']['input']>;
  length_AVERAGE_LT?: InputMaybe<Scalars['Float']['input']>;
  length_AVERAGE_LTE?: InputMaybe<Scalars['Float']['input']>;
  length_MAX_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  length_MAX_GT?: InputMaybe<Scalars['Int']['input']>;
  length_MAX_GTE?: InputMaybe<Scalars['Int']['input']>;
  length_MAX_LT?: InputMaybe<Scalars['Int']['input']>;
  length_MAX_LTE?: InputMaybe<Scalars['Int']['input']>;
  length_MIN_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  length_MIN_GT?: InputMaybe<Scalars['Int']['input']>;
  length_MIN_GTE?: InputMaybe<Scalars['Int']['input']>;
  length_MIN_LT?: InputMaybe<Scalars['Int']['input']>;
  length_MIN_LTE?: InputMaybe<Scalars['Int']['input']>;
  length_SUM_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  length_SUM_GT?: InputMaybe<Scalars['Int']['input']>;
  length_SUM_GTE?: InputMaybe<Scalars['Int']['input']>;
  length_SUM_LT?: InputMaybe<Scalars['Int']['input']>;
  length_SUM_LTE?: InputMaybe<Scalars['Int']['input']>;
  name_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  name_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  seq_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  seq_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  seq_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  seq_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  seq_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  seq_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  seq_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  seq_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  seq_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  seq_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  seq_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  seq_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  seq_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  seq_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  seq_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  so_id_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  url_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  url_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
};

export type DuplexStrandsRelationship = {
  __typename?: 'DuplexStrandsRelationship';
  cursor: Scalars['String']['output'];
  node: GenericSequence;
  properties: BindIn;
};

export type DuplexWhere = {
  AND?: InputMaybe<Array<DuplexWhere>>;
  NOT?: InputMaybe<DuplexWhere>;
  OR?: InputMaybe<Array<DuplexWhere>>;
  id?: InputMaybe<Scalars['ID']['input']>;
  id_CONTAINS?: InputMaybe<Scalars['ID']['input']>;
  id_ENDS_WITH?: InputMaybe<Scalars['ID']['input']>;
  id_IN?: InputMaybe<Array<Scalars['ID']['input']>>;
  id_STARTS_WITH?: InputMaybe<Scalars['ID']['input']>;
  index?: InputMaybe<Scalars['Int']['input']>;
  index_GT?: InputMaybe<Scalars['Int']['input']>;
  index_GTE?: InputMaybe<Scalars['Int']['input']>;
  index_IN?: InputMaybe<Array<Scalars['Int']['input']>>;
  index_LT?: InputMaybe<Scalars['Int']['input']>;
  index_LTE?: InputMaybe<Scalars['Int']['input']>;
  interaction?: InputMaybe<InteractionWhere>;
  interactionAggregate?: InputMaybe<DuplexInteractionAggregateInput>;
  interactionConnection?: InputMaybe<DuplexInteractionConnectionWhere>;
  interactionConnection_NOT?: InputMaybe<DuplexInteractionConnectionWhere>;
  interaction_NOT?: InputMaybe<InteractionWhere>;
  strandsAggregate?: InputMaybe<DuplexStrandsAggregateInput>;
  /** Return Duplexes where all of the related DuplexStrandsConnections match this filter */
  strandsConnection_ALL?: InputMaybe<DuplexStrandsConnectionWhere>;
  /** Return Duplexes where none of the related DuplexStrandsConnections match this filter */
  strandsConnection_NONE?: InputMaybe<DuplexStrandsConnectionWhere>;
  /** Return Duplexes where one of the related DuplexStrandsConnections match this filter */
  strandsConnection_SINGLE?: InputMaybe<DuplexStrandsConnectionWhere>;
  /** Return Duplexes where some of the related DuplexStrandsConnections match this filter */
  strandsConnection_SOME?: InputMaybe<DuplexStrandsConnectionWhere>;
  /** Return Duplexes where all of the related GenericSequences match this filter */
  strands_ALL?: InputMaybe<GenericSequenceWhere>;
  /** Return Duplexes where none of the related GenericSequences match this filter */
  strands_NONE?: InputMaybe<GenericSequenceWhere>;
  /** Return Duplexes where one of the related GenericSequences match this filter */
  strands_SINGLE?: InputMaybe<GenericSequenceWhere>;
  /** Return Duplexes where some of the related GenericSequences match this filter */
  strands_SOME?: InputMaybe<GenericSequenceWhere>;
};

export type DuplexesConnection = {
  __typename?: 'DuplexesConnection';
  edges: Array<DuplexEdge>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int']['output'];
};

/** A Feature Sequence */
export type GenericSequence = Sequence & {
  __typename?: 'GenericSequence';
  /** ALternative sequence names */
  altnames?: Maybe<Array<Scalars['String']['output']>>;
  /** The annotation of the sequence if any */
  annotation?: Maybe<AnnotationType>;
  /** The Chemical Entities of Biological Interest Ontology (ChEBI) id. Default value is CHEBI_33696: nucleic acid */
  chebi_id?: Maybe<Scalars['String']['output']>;
  /** The class of the sequence */
  class: SequenceClass;
  /** The sequence description (i.e. the comment part in fasta file) */
  description?: Maybe<Scalars['String']['output']>;
  /** The features associated with the sequence */
  features: Array<Sequence>;
  featuresAggregate?: Maybe<GenericSequenceSequenceFeaturesAggregationSelection>;
  featuresConnection: SequenceFeaturesConnection;
  /** The reference genome the sequence comes from */
  genome?: Maybe<Genome>;
  genomeAggregate?: Maybe<GenericSequenceGenomeGenomeAggregationSelection>;
  genomeConnection: SequenceGenomeConnection;
  /** The type of GraphQL object */
  graphql_type: GraphQlType;
  /** The sequence ID (i.e. id part in fasta file) */
  id: Scalars['ID']['output'];
  /** The length of sequence in number of nucleotide */
  length: Scalars['Int']['output'];
  /** Common sequence name */
  name?: Maybe<Scalars['String']['output']>;
  /** The parent sequence if any */
  parent?: Maybe<Sequence>;
  parentAggregate?: Maybe<GenericSequenceSequenceParentAggregationSelection>;
  parentConnection: GenericSequenceParentConnection;
  /** The sequence itself (url can be used instead for huge sequence). Sequence is alway from 5' to 3' */
  seq?: Maybe<Scalars['String']['output']>;
  /** The Sequence Ontology (so) id */
  so_id?: Maybe<Scalars['String']['output']>;
  /** The type of the sequence (DNA or RNA) */
  type: SequenceType;
  /** The url to get the file containning the sequence */
  url?: Maybe<Scalars['String']['output']>;
};


/** A Feature Sequence */
export type GenericSequenceFeaturesArgs = {
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  options?: InputMaybe<SequenceOptions>;
  where?: InputMaybe<SequenceWhere>;
};


/** A Feature Sequence */
export type GenericSequenceFeaturesAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  where?: InputMaybe<SequenceWhere>;
};


/** A Feature Sequence */
export type GenericSequenceFeaturesConnectionArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  sort?: InputMaybe<Array<SequenceFeaturesConnectionSort>>;
  where?: InputMaybe<SequenceFeaturesConnectionWhere>;
};


/** A Feature Sequence */
export type GenericSequenceGenomeArgs = {
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  options?: InputMaybe<GenomeOptions>;
  where?: InputMaybe<GenomeWhere>;
};


/** A Feature Sequence */
export type GenericSequenceGenomeAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  where?: InputMaybe<GenomeWhere>;
};


/** A Feature Sequence */
export type GenericSequenceGenomeConnectionArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  sort?: InputMaybe<Array<SequenceGenomeConnectionSort>>;
  where?: InputMaybe<SequenceGenomeConnectionWhere>;
};


/** A Feature Sequence */
export type GenericSequenceParentArgs = {
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  options?: InputMaybe<SequenceOptions>;
  where?: InputMaybe<SequenceWhere>;
};


/** A Feature Sequence */
export type GenericSequenceParentAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  where?: InputMaybe<SequenceWhere>;
};


/** A Feature Sequence */
export type GenericSequenceParentConnectionArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  sort?: InputMaybe<Array<GenericSequenceParentConnectionSort>>;
  where?: InputMaybe<GenericSequenceParentConnectionWhere>;
};

export type GenericSequenceAggregateSelection = {
  __typename?: 'GenericSequenceAggregateSelection';
  chebi_id: StringAggregateSelection;
  count: Scalars['Int']['output'];
  description: StringAggregateSelection;
  id: IdAggregateSelection;
  length: IntAggregateSelection;
  name: StringAggregateSelection;
  seq: StringAggregateSelection;
  so_id: StringAggregateSelection;
  url: StringAggregateSelection;
};

export type GenericSequenceEdge = {
  __typename?: 'GenericSequenceEdge';
  cursor: Scalars['String']['output'];
  node: GenericSequence;
};

export type GenericSequenceFeaturesAggregateInput = {
  AND?: InputMaybe<Array<GenericSequenceFeaturesAggregateInput>>;
  NOT?: InputMaybe<GenericSequenceFeaturesAggregateInput>;
  OR?: InputMaybe<Array<GenericSequenceFeaturesAggregateInput>>;
  count?: InputMaybe<Scalars['Int']['input']>;
  count_GT?: InputMaybe<Scalars['Int']['input']>;
  count_GTE?: InputMaybe<Scalars['Int']['input']>;
  count_LT?: InputMaybe<Scalars['Int']['input']>;
  count_LTE?: InputMaybe<Scalars['Int']['input']>;
  edge?: InputMaybe<HasFeatureAggregationWhereInput>;
  node?: InputMaybe<GenericSequenceFeaturesNodeAggregationWhereInput>;
};

export type GenericSequenceFeaturesNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<GenericSequenceFeaturesNodeAggregationWhereInput>>;
  NOT?: InputMaybe<GenericSequenceFeaturesNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<GenericSequenceFeaturesNodeAggregationWhereInput>>;
  chebi_id_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  chebi_id_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  chebi_id_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  chebi_id_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  chebi_id_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  chebi_id_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  description_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  description_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  length_AVERAGE_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  length_AVERAGE_GT?: InputMaybe<Scalars['Float']['input']>;
  length_AVERAGE_GTE?: InputMaybe<Scalars['Float']['input']>;
  length_AVERAGE_LT?: InputMaybe<Scalars['Float']['input']>;
  length_AVERAGE_LTE?: InputMaybe<Scalars['Float']['input']>;
  length_MAX_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  length_MAX_GT?: InputMaybe<Scalars['Int']['input']>;
  length_MAX_GTE?: InputMaybe<Scalars['Int']['input']>;
  length_MAX_LT?: InputMaybe<Scalars['Int']['input']>;
  length_MAX_LTE?: InputMaybe<Scalars['Int']['input']>;
  length_MIN_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  length_MIN_GT?: InputMaybe<Scalars['Int']['input']>;
  length_MIN_GTE?: InputMaybe<Scalars['Int']['input']>;
  length_MIN_LT?: InputMaybe<Scalars['Int']['input']>;
  length_MIN_LTE?: InputMaybe<Scalars['Int']['input']>;
  length_SUM_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  length_SUM_GT?: InputMaybe<Scalars['Int']['input']>;
  length_SUM_GTE?: InputMaybe<Scalars['Int']['input']>;
  length_SUM_LT?: InputMaybe<Scalars['Int']['input']>;
  length_SUM_LTE?: InputMaybe<Scalars['Int']['input']>;
  name_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  name_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  seq_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  seq_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  seq_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  seq_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  seq_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  seq_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  seq_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  seq_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  seq_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  seq_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  seq_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  seq_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  seq_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  seq_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  seq_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  so_id_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  url_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  url_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
};

export type GenericSequenceGenomeAggregateInput = {
  AND?: InputMaybe<Array<GenericSequenceGenomeAggregateInput>>;
  NOT?: InputMaybe<GenericSequenceGenomeAggregateInput>;
  OR?: InputMaybe<Array<GenericSequenceGenomeAggregateInput>>;
  count?: InputMaybe<Scalars['Int']['input']>;
  count_GT?: InputMaybe<Scalars['Int']['input']>;
  count_GTE?: InputMaybe<Scalars['Int']['input']>;
  count_LT?: InputMaybe<Scalars['Int']['input']>;
  count_LTE?: InputMaybe<Scalars['Int']['input']>;
  node?: InputMaybe<GenericSequenceGenomeNodeAggregationWhereInput>;
};

export type GenericSequenceGenomeGenomeAggregationSelection = {
  __typename?: 'GenericSequenceGenomeGenomeAggregationSelection';
  count: Scalars['Int']['output'];
  node?: Maybe<GenericSequenceGenomeGenomeNodeAggregateSelection>;
};

export type GenericSequenceGenomeGenomeNodeAggregateSelection = {
  __typename?: 'GenericSequenceGenomeGenomeNodeAggregateSelection';
  description: StringAggregateSelection;
  id: IdAggregateSelection;
  so_id: StringAggregateSelection;
  source: StringAggregateSelection;
  url: StringAggregateSelection;
  version: StringAggregateSelection;
};

export type GenericSequenceGenomeNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<GenericSequenceGenomeNodeAggregationWhereInput>>;
  NOT?: InputMaybe<GenericSequenceGenomeNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<GenericSequenceGenomeNodeAggregationWhereInput>>;
  description_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  description_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  so_id_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  source_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  source_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  source_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  source_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  source_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  source_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  source_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  source_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  source_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  source_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  source_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  source_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  source_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  source_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  source_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  url_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  url_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  version_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  version_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  version_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  version_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  version_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  version_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  version_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  version_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  version_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  version_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  version_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  version_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  version_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  version_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  version_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
};

export type GenericSequenceOptions = {
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  /** Specify one or more GenericSequenceSort objects to sort GenericSequences by. The sorts will be applied in the order in which they are arranged in the array. */
  sort?: InputMaybe<Array<GenericSequenceSort>>;
};

export type GenericSequenceParentAggregateInput = {
  AND?: InputMaybe<Array<GenericSequenceParentAggregateInput>>;
  NOT?: InputMaybe<GenericSequenceParentAggregateInput>;
  OR?: InputMaybe<Array<GenericSequenceParentAggregateInput>>;
  count?: InputMaybe<Scalars['Int']['input']>;
  count_GT?: InputMaybe<Scalars['Int']['input']>;
  count_GTE?: InputMaybe<Scalars['Int']['input']>;
  count_LT?: InputMaybe<Scalars['Int']['input']>;
  count_LTE?: InputMaybe<Scalars['Int']['input']>;
  edge?: InputMaybe<HasFeatureAggregationWhereInput>;
  node?: InputMaybe<GenericSequenceParentNodeAggregationWhereInput>;
};

export type GenericSequenceParentConnection = {
  __typename?: 'GenericSequenceParentConnection';
  edges: Array<GenericSequenceParentRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int']['output'];
};

export type GenericSequenceParentConnectionSort = {
  edge?: InputMaybe<HasFeatureSort>;
  node?: InputMaybe<SequenceSort>;
};

export type GenericSequenceParentConnectionWhere = {
  AND?: InputMaybe<Array<GenericSequenceParentConnectionWhere>>;
  NOT?: InputMaybe<GenericSequenceParentConnectionWhere>;
  OR?: InputMaybe<Array<GenericSequenceParentConnectionWhere>>;
  edge?: InputMaybe<HasFeatureWhere>;
  node?: InputMaybe<SequenceWhere>;
};

export type GenericSequenceParentNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<GenericSequenceParentNodeAggregationWhereInput>>;
  NOT?: InputMaybe<GenericSequenceParentNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<GenericSequenceParentNodeAggregationWhereInput>>;
  chebi_id_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  chebi_id_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  chebi_id_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  chebi_id_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  chebi_id_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  chebi_id_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  description_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  description_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  length_AVERAGE_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  length_AVERAGE_GT?: InputMaybe<Scalars['Float']['input']>;
  length_AVERAGE_GTE?: InputMaybe<Scalars['Float']['input']>;
  length_AVERAGE_LT?: InputMaybe<Scalars['Float']['input']>;
  length_AVERAGE_LTE?: InputMaybe<Scalars['Float']['input']>;
  length_MAX_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  length_MAX_GT?: InputMaybe<Scalars['Int']['input']>;
  length_MAX_GTE?: InputMaybe<Scalars['Int']['input']>;
  length_MAX_LT?: InputMaybe<Scalars['Int']['input']>;
  length_MAX_LTE?: InputMaybe<Scalars['Int']['input']>;
  length_MIN_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  length_MIN_GT?: InputMaybe<Scalars['Int']['input']>;
  length_MIN_GTE?: InputMaybe<Scalars['Int']['input']>;
  length_MIN_LT?: InputMaybe<Scalars['Int']['input']>;
  length_MIN_LTE?: InputMaybe<Scalars['Int']['input']>;
  length_SUM_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  length_SUM_GT?: InputMaybe<Scalars['Int']['input']>;
  length_SUM_GTE?: InputMaybe<Scalars['Int']['input']>;
  length_SUM_LT?: InputMaybe<Scalars['Int']['input']>;
  length_SUM_LTE?: InputMaybe<Scalars['Int']['input']>;
  name_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  name_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  seq_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  seq_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  seq_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  seq_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  seq_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  seq_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  seq_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  seq_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  seq_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  seq_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  seq_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  seq_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  seq_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  seq_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  seq_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  so_id_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  url_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  url_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
};

export type GenericSequenceParentRelationship = {
  __typename?: 'GenericSequenceParentRelationship';
  cursor: Scalars['String']['output'];
  node: Sequence;
  properties: HasFeature;
};

export type GenericSequenceSequenceFeaturesAggregationSelection = {
  __typename?: 'GenericSequenceSequenceFeaturesAggregationSelection';
  count: Scalars['Int']['output'];
  edge?: Maybe<GenericSequenceSequenceFeaturesEdgeAggregateSelection>;
  node?: Maybe<GenericSequenceSequenceFeaturesNodeAggregateSelection>;
};

export type GenericSequenceSequenceFeaturesEdgeAggregateSelection = {
  __typename?: 'GenericSequenceSequenceFeaturesEdgeAggregateSelection';
  end: IntAggregateSelection;
  start: IntAggregateSelection;
};

export type GenericSequenceSequenceFeaturesNodeAggregateSelection = {
  __typename?: 'GenericSequenceSequenceFeaturesNodeAggregateSelection';
  chebi_id: StringAggregateSelection;
  description: StringAggregateSelection;
  id: IdAggregateSelection;
  length: IntAggregateSelection;
  name: StringAggregateSelection;
  seq: StringAggregateSelection;
  so_id: StringAggregateSelection;
  url: StringAggregateSelection;
};

export type GenericSequenceSequenceParentAggregationSelection = {
  __typename?: 'GenericSequenceSequenceParentAggregationSelection';
  count: Scalars['Int']['output'];
  edge?: Maybe<GenericSequenceSequenceParentEdgeAggregateSelection>;
  node?: Maybe<GenericSequenceSequenceParentNodeAggregateSelection>;
};

export type GenericSequenceSequenceParentEdgeAggregateSelection = {
  __typename?: 'GenericSequenceSequenceParentEdgeAggregateSelection';
  end: IntAggregateSelection;
  start: IntAggregateSelection;
};

export type GenericSequenceSequenceParentNodeAggregateSelection = {
  __typename?: 'GenericSequenceSequenceParentNodeAggregateSelection';
  chebi_id: StringAggregateSelection;
  description: StringAggregateSelection;
  id: IdAggregateSelection;
  length: IntAggregateSelection;
  name: StringAggregateSelection;
  seq: StringAggregateSelection;
  so_id: StringAggregateSelection;
  url: StringAggregateSelection;
};

/** Fields to sort GenericSequences by. The order in which sorts are applied is not guaranteed when specifying many fields in one GenericSequenceSort object. */
export type GenericSequenceSort = {
  annotation?: InputMaybe<SortDirection>;
  chebi_id?: InputMaybe<SortDirection>;
  class?: InputMaybe<SortDirection>;
  description?: InputMaybe<SortDirection>;
  graphql_type?: InputMaybe<SortDirection>;
  id?: InputMaybe<SortDirection>;
  length?: InputMaybe<SortDirection>;
  name?: InputMaybe<SortDirection>;
  seq?: InputMaybe<SortDirection>;
  so_id?: InputMaybe<SortDirection>;
  type?: InputMaybe<SortDirection>;
  url?: InputMaybe<SortDirection>;
};

export type GenericSequenceWhere = {
  AND?: InputMaybe<Array<GenericSequenceWhere>>;
  NOT?: InputMaybe<GenericSequenceWhere>;
  OR?: InputMaybe<Array<GenericSequenceWhere>>;
  altnames?: InputMaybe<Array<Scalars['String']['input']>>;
  altnames_INCLUDES?: InputMaybe<Scalars['String']['input']>;
  annotation?: InputMaybe<AnnotationType>;
  annotation_IN?: InputMaybe<Array<InputMaybe<AnnotationType>>>;
  chebi_id?: InputMaybe<Scalars['String']['input']>;
  chebi_id_CONTAINS?: InputMaybe<Scalars['String']['input']>;
  chebi_id_ENDS_WITH?: InputMaybe<Scalars['String']['input']>;
  chebi_id_IN?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  chebi_id_STARTS_WITH?: InputMaybe<Scalars['String']['input']>;
  class?: InputMaybe<SequenceClass>;
  class_IN?: InputMaybe<Array<SequenceClass>>;
  description?: InputMaybe<Scalars['String']['input']>;
  description_CONTAINS?: InputMaybe<Scalars['String']['input']>;
  description_ENDS_WITH?: InputMaybe<Scalars['String']['input']>;
  description_IN?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  description_STARTS_WITH?: InputMaybe<Scalars['String']['input']>;
  featuresAggregate?: InputMaybe<GenericSequenceFeaturesAggregateInput>;
  /** Return GenericSequences where all of the related SequenceFeaturesConnections match this filter */
  featuresConnection_ALL?: InputMaybe<SequenceFeaturesConnectionWhere>;
  /** Return GenericSequences where none of the related SequenceFeaturesConnections match this filter */
  featuresConnection_NONE?: InputMaybe<SequenceFeaturesConnectionWhere>;
  /** Return GenericSequences where one of the related SequenceFeaturesConnections match this filter */
  featuresConnection_SINGLE?: InputMaybe<SequenceFeaturesConnectionWhere>;
  /** Return GenericSequences where some of the related SequenceFeaturesConnections match this filter */
  featuresConnection_SOME?: InputMaybe<SequenceFeaturesConnectionWhere>;
  /** Return GenericSequences where all of the related Sequences match this filter */
  features_ALL?: InputMaybe<SequenceWhere>;
  /** Return GenericSequences where none of the related Sequences match this filter */
  features_NONE?: InputMaybe<SequenceWhere>;
  /** Return GenericSequences where one of the related Sequences match this filter */
  features_SINGLE?: InputMaybe<SequenceWhere>;
  /** Return GenericSequences where some of the related Sequences match this filter */
  features_SOME?: InputMaybe<SequenceWhere>;
  genome?: InputMaybe<GenomeWhere>;
  genomeAggregate?: InputMaybe<GenericSequenceGenomeAggregateInput>;
  genomeConnection?: InputMaybe<SequenceGenomeConnectionWhere>;
  genomeConnection_NOT?: InputMaybe<SequenceGenomeConnectionWhere>;
  genome_NOT?: InputMaybe<GenomeWhere>;
  graphql_type?: InputMaybe<GraphQlType>;
  graphql_type_IN?: InputMaybe<Array<GraphQlType>>;
  id?: InputMaybe<Scalars['ID']['input']>;
  id_CONTAINS?: InputMaybe<Scalars['ID']['input']>;
  id_ENDS_WITH?: InputMaybe<Scalars['ID']['input']>;
  id_IN?: InputMaybe<Array<Scalars['ID']['input']>>;
  id_STARTS_WITH?: InputMaybe<Scalars['ID']['input']>;
  length?: InputMaybe<Scalars['Int']['input']>;
  length_GT?: InputMaybe<Scalars['Int']['input']>;
  length_GTE?: InputMaybe<Scalars['Int']['input']>;
  length_IN?: InputMaybe<Array<Scalars['Int']['input']>>;
  length_LT?: InputMaybe<Scalars['Int']['input']>;
  length_LTE?: InputMaybe<Scalars['Int']['input']>;
  name?: InputMaybe<Scalars['String']['input']>;
  name_CONTAINS?: InputMaybe<Scalars['String']['input']>;
  name_ENDS_WITH?: InputMaybe<Scalars['String']['input']>;
  name_IN?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  name_STARTS_WITH?: InputMaybe<Scalars['String']['input']>;
  parent?: InputMaybe<SequenceWhere>;
  parentAggregate?: InputMaybe<GenericSequenceParentAggregateInput>;
  parentConnection?: InputMaybe<GenericSequenceParentConnectionWhere>;
  parentConnection_NOT?: InputMaybe<GenericSequenceParentConnectionWhere>;
  parent_NOT?: InputMaybe<SequenceWhere>;
  seq?: InputMaybe<Scalars['String']['input']>;
  seq_CONTAINS?: InputMaybe<Scalars['String']['input']>;
  seq_ENDS_WITH?: InputMaybe<Scalars['String']['input']>;
  seq_IN?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  seq_STARTS_WITH?: InputMaybe<Scalars['String']['input']>;
  so_id?: InputMaybe<Scalars['String']['input']>;
  so_id_CONTAINS?: InputMaybe<Scalars['String']['input']>;
  so_id_ENDS_WITH?: InputMaybe<Scalars['String']['input']>;
  so_id_IN?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  so_id_STARTS_WITH?: InputMaybe<Scalars['String']['input']>;
  type?: InputMaybe<SequenceType>;
  type_IN?: InputMaybe<Array<SequenceType>>;
  url?: InputMaybe<Scalars['String']['input']>;
  url_CONTAINS?: InputMaybe<Scalars['String']['input']>;
  url_ENDS_WITH?: InputMaybe<Scalars['String']['input']>;
  url_IN?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  url_STARTS_WITH?: InputMaybe<Scalars['String']['input']>;
};

export type GenericSequencesConnection = {
  __typename?: 'GenericSequencesConnection';
  edges: Array<GenericSequenceEdge>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int']['output'];
};

/** A genome is a collection of sequences */
export type Genome = {
  __typename?: 'Genome';
  /** Genome description */
  description: Scalars['String']['output'];
  /** The genome ID */
  id: Scalars['ID']['output'];
  /** Organism the genome comes from */
  organism?: Maybe<Organism>;
  organismAggregate?: Maybe<GenomeOrganismOrganismAggregationSelection>;
  organismConnection: GenomeOrganismConnection;
  /** Sequences in genome */
  sequences: Array<Sequence>;
  sequencesAggregate?: Maybe<GenomeSequenceSequencesAggregationSelection>;
  sequencesConnection: GenomeSequencesConnection;
  /** The genome type in Sequence Ontology (so) */
  so_id?: Maybe<Scalars['String']['output']>;
  /** The genome source */
  source?: Maybe<Scalars['String']['output']>;
  /** The genomes fetch url */
  url?: Maybe<Scalars['String']['output']>;
  /** The genome version */
  version?: Maybe<Scalars['String']['output']>;
};


/** A genome is a collection of sequences */
export type GenomeOrganismArgs = {
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  options?: InputMaybe<OrganismOptions>;
  where?: InputMaybe<OrganismWhere>;
};


/** A genome is a collection of sequences */
export type GenomeOrganismAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  where?: InputMaybe<OrganismWhere>;
};


/** A genome is a collection of sequences */
export type GenomeOrganismConnectionArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  sort?: InputMaybe<Array<GenomeOrganismConnectionSort>>;
  where?: InputMaybe<GenomeOrganismConnectionWhere>;
};


/** A genome is a collection of sequences */
export type GenomeSequencesArgs = {
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  options?: InputMaybe<SequenceOptions>;
  where?: InputMaybe<SequenceWhere>;
};


/** A genome is a collection of sequences */
export type GenomeSequencesAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  where?: InputMaybe<SequenceWhere>;
};


/** A genome is a collection of sequences */
export type GenomeSequencesConnectionArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  sort?: InputMaybe<Array<GenomeSequencesConnectionSort>>;
  where?: InputMaybe<GenomeSequencesConnectionWhere>;
};

export type GenomeAggregateSelection = {
  __typename?: 'GenomeAggregateSelection';
  count: Scalars['Int']['output'];
  description: StringAggregateSelection;
  id: IdAggregateSelection;
  so_id: StringAggregateSelection;
  source: StringAggregateSelection;
  url: StringAggregateSelection;
  version: StringAggregateSelection;
};

export type GenomeEdge = {
  __typename?: 'GenomeEdge';
  cursor: Scalars['String']['output'];
  node: Genome;
};

export type GenomeOptions = {
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  /** Specify one or more GenomeSort objects to sort Genomes by. The sorts will be applied in the order in which they are arranged in the array. */
  sort?: InputMaybe<Array<GenomeSort>>;
};

export type GenomeOrganismAggregateInput = {
  AND?: InputMaybe<Array<GenomeOrganismAggregateInput>>;
  NOT?: InputMaybe<GenomeOrganismAggregateInput>;
  OR?: InputMaybe<Array<GenomeOrganismAggregateInput>>;
  count?: InputMaybe<Scalars['Int']['input']>;
  count_GT?: InputMaybe<Scalars['Int']['input']>;
  count_GTE?: InputMaybe<Scalars['Int']['input']>;
  count_LT?: InputMaybe<Scalars['Int']['input']>;
  count_LTE?: InputMaybe<Scalars['Int']['input']>;
  node?: InputMaybe<GenomeOrganismNodeAggregationWhereInput>;
};

export type GenomeOrganismConnection = {
  __typename?: 'GenomeOrganismConnection';
  edges: Array<GenomeOrganismRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int']['output'];
};

export type GenomeOrganismConnectionSort = {
  node?: InputMaybe<OrganismSort>;
};

export type GenomeOrganismConnectionWhere = {
  AND?: InputMaybe<Array<GenomeOrganismConnectionWhere>>;
  NOT?: InputMaybe<GenomeOrganismConnectionWhere>;
  OR?: InputMaybe<Array<GenomeOrganismConnectionWhere>>;
  node?: InputMaybe<OrganismWhere>;
};

export type GenomeOrganismNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<GenomeOrganismNodeAggregationWhereInput>>;
  NOT?: InputMaybe<GenomeOrganismNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<GenomeOrganismNodeAggregationWhereInput>>;
  id_AVERAGE_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  id_AVERAGE_GT?: InputMaybe<Scalars['Float']['input']>;
  id_AVERAGE_GTE?: InputMaybe<Scalars['Float']['input']>;
  id_AVERAGE_LT?: InputMaybe<Scalars['Float']['input']>;
  id_AVERAGE_LTE?: InputMaybe<Scalars['Float']['input']>;
  id_MAX_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  id_MAX_GT?: InputMaybe<Scalars['Int']['input']>;
  id_MAX_GTE?: InputMaybe<Scalars['Int']['input']>;
  id_MAX_LT?: InputMaybe<Scalars['Int']['input']>;
  id_MAX_LTE?: InputMaybe<Scalars['Int']['input']>;
  id_MIN_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  id_MIN_GT?: InputMaybe<Scalars['Int']['input']>;
  id_MIN_GTE?: InputMaybe<Scalars['Int']['input']>;
  id_MIN_LT?: InputMaybe<Scalars['Int']['input']>;
  id_MIN_LTE?: InputMaybe<Scalars['Int']['input']>;
  id_SUM_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  id_SUM_GT?: InputMaybe<Scalars['Int']['input']>;
  id_SUM_GTE?: InputMaybe<Scalars['Int']['input']>;
  id_SUM_LT?: InputMaybe<Scalars['Int']['input']>;
  id_SUM_LTE?: InputMaybe<Scalars['Int']['input']>;
  label_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  label_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  label_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  label_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  label_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  label_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  label_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  label_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  label_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  label_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  label_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  label_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  label_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  label_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  label_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  name_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  name_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  shortlabel_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  shortlabel_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  shortlabel_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  shortlabel_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  shortlabel_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  shortlabel_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  shortlabel_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  shortlabel_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  shortlabel_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  shortlabel_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  shortlabel_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  shortlabel_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  shortlabel_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  shortlabel_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  shortlabel_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  shortname_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  shortname_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  shortname_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  shortname_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  shortname_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  shortname_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  shortname_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  shortname_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  shortname_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  shortname_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  shortname_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  shortname_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  shortname_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  shortname_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  shortname_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
};

export type GenomeOrganismOrganismAggregationSelection = {
  __typename?: 'GenomeOrganismOrganismAggregationSelection';
  count: Scalars['Int']['output'];
  node?: Maybe<GenomeOrganismOrganismNodeAggregateSelection>;
};

export type GenomeOrganismOrganismNodeAggregateSelection = {
  __typename?: 'GenomeOrganismOrganismNodeAggregateSelection';
  id: IntAggregateSelection;
  label: StringAggregateSelection;
  name: StringAggregateSelection;
  shortlabel: StringAggregateSelection;
  shortname: StringAggregateSelection;
};

export type GenomeOrganismRelationship = {
  __typename?: 'GenomeOrganismRelationship';
  cursor: Scalars['String']['output'];
  node: Organism;
};

export type GenomeSequenceSequencesAggregationSelection = {
  __typename?: 'GenomeSequenceSequencesAggregationSelection';
  count: Scalars['Int']['output'];
  node?: Maybe<GenomeSequenceSequencesNodeAggregateSelection>;
};

export type GenomeSequenceSequencesNodeAggregateSelection = {
  __typename?: 'GenomeSequenceSequencesNodeAggregateSelection';
  chebi_id: StringAggregateSelection;
  description: StringAggregateSelection;
  id: IdAggregateSelection;
  length: IntAggregateSelection;
  name: StringAggregateSelection;
  seq: StringAggregateSelection;
  so_id: StringAggregateSelection;
  url: StringAggregateSelection;
};

export type GenomeSequencesAggregateInput = {
  AND?: InputMaybe<Array<GenomeSequencesAggregateInput>>;
  NOT?: InputMaybe<GenomeSequencesAggregateInput>;
  OR?: InputMaybe<Array<GenomeSequencesAggregateInput>>;
  count?: InputMaybe<Scalars['Int']['input']>;
  count_GT?: InputMaybe<Scalars['Int']['input']>;
  count_GTE?: InputMaybe<Scalars['Int']['input']>;
  count_LT?: InputMaybe<Scalars['Int']['input']>;
  count_LTE?: InputMaybe<Scalars['Int']['input']>;
  node?: InputMaybe<GenomeSequencesNodeAggregationWhereInput>;
};

export type GenomeSequencesConnection = {
  __typename?: 'GenomeSequencesConnection';
  edges: Array<GenomeSequencesRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int']['output'];
};

export type GenomeSequencesConnectionSort = {
  node?: InputMaybe<SequenceSort>;
};

export type GenomeSequencesConnectionWhere = {
  AND?: InputMaybe<Array<GenomeSequencesConnectionWhere>>;
  NOT?: InputMaybe<GenomeSequencesConnectionWhere>;
  OR?: InputMaybe<Array<GenomeSequencesConnectionWhere>>;
  node?: InputMaybe<SequenceWhere>;
};

export type GenomeSequencesNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<GenomeSequencesNodeAggregationWhereInput>>;
  NOT?: InputMaybe<GenomeSequencesNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<GenomeSequencesNodeAggregationWhereInput>>;
  chebi_id_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  chebi_id_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  chebi_id_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  chebi_id_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  chebi_id_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  chebi_id_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  description_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  description_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  length_AVERAGE_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  length_AVERAGE_GT?: InputMaybe<Scalars['Float']['input']>;
  length_AVERAGE_GTE?: InputMaybe<Scalars['Float']['input']>;
  length_AVERAGE_LT?: InputMaybe<Scalars['Float']['input']>;
  length_AVERAGE_LTE?: InputMaybe<Scalars['Float']['input']>;
  length_MAX_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  length_MAX_GT?: InputMaybe<Scalars['Int']['input']>;
  length_MAX_GTE?: InputMaybe<Scalars['Int']['input']>;
  length_MAX_LT?: InputMaybe<Scalars['Int']['input']>;
  length_MAX_LTE?: InputMaybe<Scalars['Int']['input']>;
  length_MIN_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  length_MIN_GT?: InputMaybe<Scalars['Int']['input']>;
  length_MIN_GTE?: InputMaybe<Scalars['Int']['input']>;
  length_MIN_LT?: InputMaybe<Scalars['Int']['input']>;
  length_MIN_LTE?: InputMaybe<Scalars['Int']['input']>;
  length_SUM_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  length_SUM_GT?: InputMaybe<Scalars['Int']['input']>;
  length_SUM_GTE?: InputMaybe<Scalars['Int']['input']>;
  length_SUM_LT?: InputMaybe<Scalars['Int']['input']>;
  length_SUM_LTE?: InputMaybe<Scalars['Int']['input']>;
  name_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  name_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  seq_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  seq_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  seq_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  seq_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  seq_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  seq_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  seq_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  seq_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  seq_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  seq_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  seq_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  seq_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  seq_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  seq_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  seq_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  so_id_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  url_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  url_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
};

export type GenomeSequencesRelationship = {
  __typename?: 'GenomeSequencesRelationship';
  cursor: Scalars['String']['output'];
  node: Sequence;
};

/** Fields to sort Genomes by. The order in which sorts are applied is not guaranteed when specifying many fields in one GenomeSort object. */
export type GenomeSort = {
  description?: InputMaybe<SortDirection>;
  id?: InputMaybe<SortDirection>;
  so_id?: InputMaybe<SortDirection>;
  source?: InputMaybe<SortDirection>;
  url?: InputMaybe<SortDirection>;
  version?: InputMaybe<SortDirection>;
};

export type GenomeWhere = {
  AND?: InputMaybe<Array<GenomeWhere>>;
  NOT?: InputMaybe<GenomeWhere>;
  OR?: InputMaybe<Array<GenomeWhere>>;
  description?: InputMaybe<Scalars['String']['input']>;
  description_CONTAINS?: InputMaybe<Scalars['String']['input']>;
  description_ENDS_WITH?: InputMaybe<Scalars['String']['input']>;
  description_IN?: InputMaybe<Array<Scalars['String']['input']>>;
  description_STARTS_WITH?: InputMaybe<Scalars['String']['input']>;
  id?: InputMaybe<Scalars['ID']['input']>;
  id_CONTAINS?: InputMaybe<Scalars['ID']['input']>;
  id_ENDS_WITH?: InputMaybe<Scalars['ID']['input']>;
  id_IN?: InputMaybe<Array<Scalars['ID']['input']>>;
  id_STARTS_WITH?: InputMaybe<Scalars['ID']['input']>;
  organism?: InputMaybe<OrganismWhere>;
  organismAggregate?: InputMaybe<GenomeOrganismAggregateInput>;
  organismConnection?: InputMaybe<GenomeOrganismConnectionWhere>;
  organismConnection_NOT?: InputMaybe<GenomeOrganismConnectionWhere>;
  organism_NOT?: InputMaybe<OrganismWhere>;
  sequencesAggregate?: InputMaybe<GenomeSequencesAggregateInput>;
  /** Return Genomes where all of the related GenomeSequencesConnections match this filter */
  sequencesConnection_ALL?: InputMaybe<GenomeSequencesConnectionWhere>;
  /** Return Genomes where none of the related GenomeSequencesConnections match this filter */
  sequencesConnection_NONE?: InputMaybe<GenomeSequencesConnectionWhere>;
  /** Return Genomes where one of the related GenomeSequencesConnections match this filter */
  sequencesConnection_SINGLE?: InputMaybe<GenomeSequencesConnectionWhere>;
  /** Return Genomes where some of the related GenomeSequencesConnections match this filter */
  sequencesConnection_SOME?: InputMaybe<GenomeSequencesConnectionWhere>;
  /** Return Genomes where all of the related Sequences match this filter */
  sequences_ALL?: InputMaybe<SequenceWhere>;
  /** Return Genomes where none of the related Sequences match this filter */
  sequences_NONE?: InputMaybe<SequenceWhere>;
  /** Return Genomes where one of the related Sequences match this filter */
  sequences_SINGLE?: InputMaybe<SequenceWhere>;
  /** Return Genomes where some of the related Sequences match this filter */
  sequences_SOME?: InputMaybe<SequenceWhere>;
  so_id?: InputMaybe<Scalars['String']['input']>;
  so_id_CONTAINS?: InputMaybe<Scalars['String']['input']>;
  so_id_ENDS_WITH?: InputMaybe<Scalars['String']['input']>;
  so_id_IN?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  so_id_STARTS_WITH?: InputMaybe<Scalars['String']['input']>;
  source?: InputMaybe<Scalars['String']['input']>;
  source_CONTAINS?: InputMaybe<Scalars['String']['input']>;
  source_ENDS_WITH?: InputMaybe<Scalars['String']['input']>;
  source_IN?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  source_STARTS_WITH?: InputMaybe<Scalars['String']['input']>;
  url?: InputMaybe<Scalars['String']['input']>;
  url_CONTAINS?: InputMaybe<Scalars['String']['input']>;
  url_ENDS_WITH?: InputMaybe<Scalars['String']['input']>;
  url_IN?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  url_STARTS_WITH?: InputMaybe<Scalars['String']['input']>;
  version?: InputMaybe<Scalars['String']['input']>;
  version_CONTAINS?: InputMaybe<Scalars['String']['input']>;
  version_ENDS_WITH?: InputMaybe<Scalars['String']['input']>;
  version_IN?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  version_STARTS_WITH?: InputMaybe<Scalars['String']['input']>;
};

export type GenomesConnection = {
  __typename?: 'GenomesConnection';
  edges: Array<GenomeEdge>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int']['output'];
};

export enum GraphQlType {
  /** A chromosome */
  Chromosome = 'Chromosome',
  /** A Generic Feature Sequence */
  GenericSequence = 'GenericSequence',
  /** A Guide */
  Guide = 'Guide',
  /** A Target */
  Target = 'Target'
}

/** A Guide guides some modifications */
export type Guide = Sequence & {
  __typename?: 'Guide';
  /** Alternative guide names */
  altnames?: Maybe<Array<Scalars['String']['output']>>;
  /** The annotation of the sequence if any */
  annotation?: Maybe<AnnotationType>;
  /** The Chemical Entities of Biological Interest Ontology (ChEBI) id */
  chebi_id?: Maybe<Scalars['String']['output']>;
  /** The class of the guide (snoRNA) */
  class: SequenceClass;
  /** Cluster the guide is in */
  cluster?: Maybe<Cluster>;
  clusterAggregate?: Maybe<GuideClusterClusterAggregationSelection>;
  clusterConnection: GuideClusterConnection;
  /** Guide description */
  description?: Maybe<Scalars['String']['output']>;
  /** Table entries associated to the guide */
  entries: Array<TableEntry>;
  entriesConnection: GuideEntriesConnection;
  /** The features associated with the guide, no deep traversal needed */
  features: Array<Sequence>;
  featuresAggregate?: Maybe<GuideSequenceFeaturesAggregationSelection>;
  featuresConnection: SequenceFeaturesConnection;
  /** The reference genome the sequence comes from */
  genome?: Maybe<Genome>;
  genomeAggregate?: Maybe<GuideGenomeGenomeAggregationSelection>;
  genomeConnection: SequenceGenomeConnection;
  /** The type of GraphQL object */
  graphql_type: GraphQlType;
  /** Host genes */
  host_genes: Array<Scalars['String']['output']>;
  /** The guide ID */
  id: Scalars['ID']['output'];
  /** The interactions the guide acts in */
  interactions: Array<Interaction>;
  interactionsConnection: GuideInteractionsConnection;
  /** Isoform the guide is in */
  isoform?: Maybe<Isoform>;
  isoformAggregate?: Maybe<GuideIsoformIsoformAggregationSelection>;
  isoformConnection: GuideIsoformConnection;
  /** The length of sequence in number of nucleotide */
  length: Scalars['Int']['output'];
  /** The modifications guided by the guide */
  modifications: Array<Modification>;
  modificationsAggregate?: Maybe<GuideModificationModificationsAggregationSelection>;
  modificationsConnection: GuideModificationsConnection;
  /** Common guide name */
  name?: Maybe<Scalars['String']['output']>;
  /** The guide is a feature from parent */
  parent?: Maybe<Sequence>;
  parentAggregate?: Maybe<GuideSequenceParentAggregationSelection>;
  parentConnection: GuideParentConnection;
  /** The sequence itself (url can be used instead for huge sequence) */
  seq?: Maybe<Scalars['String']['output']>;
  /** The Sequence Ontology (so) id */
  so_id?: Maybe<Scalars['String']['output']>;
  /** The subclass of the guide (CD Box, H/ACA Box, ScaRNA) */
  subclass: GuideClass;
  /** The subclass of the guide (CD Box, H/ACA Box, ScaRNA) */
  subclass_label: Scalars['String']['output'];
  /** The type of the sequence */
  type: SequenceType;
  /** The url to get the file containning the sequence */
  url?: Maybe<Scalars['String']['output']>;
};


/** A Guide guides some modifications */
export type GuideClusterArgs = {
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  options?: InputMaybe<ClusterOptions>;
  where?: InputMaybe<ClusterWhere>;
};


/** A Guide guides some modifications */
export type GuideClusterAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  where?: InputMaybe<ClusterWhere>;
};


/** A Guide guides some modifications */
export type GuideClusterConnectionArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  sort?: InputMaybe<Array<GuideClusterConnectionSort>>;
  where?: InputMaybe<GuideClusterConnectionWhere>;
};


/** A Guide guides some modifications */
export type GuideEntriesArgs = {
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  options?: InputMaybe<TableEntryOptions>;
  where?: InputMaybe<TableEntryWhere>;
};


/** A Guide guides some modifications */
export type GuideEntriesConnectionArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  sort?: InputMaybe<Array<GuideEntriesConnectionSort>>;
  where?: InputMaybe<GuideEntriesConnectionWhere>;
};


/** A Guide guides some modifications */
export type GuideFeaturesArgs = {
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  options?: InputMaybe<SequenceOptions>;
  where?: InputMaybe<SequenceWhere>;
};


/** A Guide guides some modifications */
export type GuideFeaturesAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  where?: InputMaybe<SequenceWhere>;
};


/** A Guide guides some modifications */
export type GuideFeaturesConnectionArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  sort?: InputMaybe<Array<SequenceFeaturesConnectionSort>>;
  where?: InputMaybe<SequenceFeaturesConnectionWhere>;
};


/** A Guide guides some modifications */
export type GuideGenomeArgs = {
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  options?: InputMaybe<GenomeOptions>;
  where?: InputMaybe<GenomeWhere>;
};


/** A Guide guides some modifications */
export type GuideGenomeAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  where?: InputMaybe<GenomeWhere>;
};


/** A Guide guides some modifications */
export type GuideGenomeConnectionArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  sort?: InputMaybe<Array<SequenceGenomeConnectionSort>>;
  where?: InputMaybe<SequenceGenomeConnectionWhere>;
};


/** A Guide guides some modifications */
export type GuideInteractionsArgs = {
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  options?: InputMaybe<InteractionOptions>;
  where?: InputMaybe<InteractionWhere>;
};


/** A Guide guides some modifications */
export type GuideInteractionsConnectionArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  sort?: InputMaybe<Array<GuideInteractionsConnectionSort>>;
  where?: InputMaybe<GuideInteractionsConnectionWhere>;
};


/** A Guide guides some modifications */
export type GuideIsoformArgs = {
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  options?: InputMaybe<IsoformOptions>;
  where?: InputMaybe<IsoformWhere>;
};


/** A Guide guides some modifications */
export type GuideIsoformAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  where?: InputMaybe<IsoformWhere>;
};


/** A Guide guides some modifications */
export type GuideIsoformConnectionArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  sort?: InputMaybe<Array<GuideIsoformConnectionSort>>;
  where?: InputMaybe<GuideIsoformConnectionWhere>;
};


/** A Guide guides some modifications */
export type GuideModificationsArgs = {
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  options?: InputMaybe<ModificationOptions>;
  where?: InputMaybe<ModificationWhere>;
};


/** A Guide guides some modifications */
export type GuideModificationsAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  where?: InputMaybe<ModificationWhere>;
};


/** A Guide guides some modifications */
export type GuideModificationsConnectionArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  sort?: InputMaybe<Array<GuideModificationsConnectionSort>>;
  where?: InputMaybe<GuideModificationsConnectionWhere>;
};


/** A Guide guides some modifications */
export type GuideParentArgs = {
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  options?: InputMaybe<SequenceOptions>;
  where?: InputMaybe<SequenceWhere>;
};


/** A Guide guides some modifications */
export type GuideParentAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  where?: InputMaybe<SequenceWhere>;
};


/** A Guide guides some modifications */
export type GuideParentConnectionArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  sort?: InputMaybe<Array<GuideParentConnectionSort>>;
  where?: InputMaybe<GuideParentConnectionWhere>;
};

export type GuideAggregateSelection = {
  __typename?: 'GuideAggregateSelection';
  chebi_id: StringAggregateSelection;
  count: Scalars['Int']['output'];
  description: StringAggregateSelection;
  id: IdAggregateSelection;
  length: IntAggregateSelection;
  name: StringAggregateSelection;
  seq: StringAggregateSelection;
  so_id: StringAggregateSelection;
  url: StringAggregateSelection;
};

/** Type of Guide */
export enum GuideClass {
  /** CD Box snoRNA */
  Cd = 'CD',
  /** H/ACA Box snoRNA */
  Haca = 'HACA',
  /** Other */
  Other = 'Other',
  /** ScaRNA */
  ScaRna = 'ScaRNA'
}

export type GuideClusterAggregateInput = {
  AND?: InputMaybe<Array<GuideClusterAggregateInput>>;
  NOT?: InputMaybe<GuideClusterAggregateInput>;
  OR?: InputMaybe<Array<GuideClusterAggregateInput>>;
  count?: InputMaybe<Scalars['Int']['input']>;
  count_GT?: InputMaybe<Scalars['Int']['input']>;
  count_GTE?: InputMaybe<Scalars['Int']['input']>;
  count_LT?: InputMaybe<Scalars['Int']['input']>;
  count_LTE?: InputMaybe<Scalars['Int']['input']>;
  node?: InputMaybe<GuideClusterNodeAggregationWhereInput>;
};

export type GuideClusterClusterAggregationSelection = {
  __typename?: 'GuideClusterClusterAggregationSelection';
  count: Scalars['Int']['output'];
  node?: Maybe<GuideClusterClusterNodeAggregateSelection>;
};

export type GuideClusterClusterNodeAggregateSelection = {
  __typename?: 'GuideClusterClusterNodeAggregateSelection';
  id: IdAggregateSelection;
};

export type GuideClusterConnection = {
  __typename?: 'GuideClusterConnection';
  edges: Array<GuideClusterRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int']['output'];
};

export type GuideClusterConnectionSort = {
  node?: InputMaybe<ClusterSort>;
};

export type GuideClusterConnectionWhere = {
  AND?: InputMaybe<Array<GuideClusterConnectionWhere>>;
  NOT?: InputMaybe<GuideClusterConnectionWhere>;
  OR?: InputMaybe<Array<GuideClusterConnectionWhere>>;
  node?: InputMaybe<ClusterWhere>;
};

export type GuideClusterNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<GuideClusterNodeAggregationWhereInput>>;
  NOT?: InputMaybe<GuideClusterNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<GuideClusterNodeAggregationWhereInput>>;
};

export type GuideClusterRelationship = {
  __typename?: 'GuideClusterRelationship';
  cursor: Scalars['String']['output'];
  node: Cluster;
};

export type GuideEdge = {
  __typename?: 'GuideEdge';
  cursor: Scalars['String']['output'];
  node: Guide;
};

export type GuideEntriesAggregateInput = {
  AND?: InputMaybe<Array<GuideEntriesAggregateInput>>;
  NOT?: InputMaybe<GuideEntriesAggregateInput>;
  OR?: InputMaybe<Array<GuideEntriesAggregateInput>>;
  count?: InputMaybe<Scalars['Int']['input']>;
  count_GT?: InputMaybe<Scalars['Int']['input']>;
  count_GTE?: InputMaybe<Scalars['Int']['input']>;
  count_LT?: InputMaybe<Scalars['Int']['input']>;
  count_LTE?: InputMaybe<Scalars['Int']['input']>;
  node?: InputMaybe<GuideEntriesNodeAggregationWhereInput>;
};

export type GuideEntriesConnection = {
  __typename?: 'GuideEntriesConnection';
  edges: Array<GuideEntriesRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int']['output'];
};

export type GuideEntriesConnectionSort = {
  node?: InputMaybe<TableEntrySort>;
};

export type GuideEntriesConnectionWhere = {
  AND?: InputMaybe<Array<GuideEntriesConnectionWhere>>;
  NOT?: InputMaybe<GuideEntriesConnectionWhere>;
  OR?: InputMaybe<Array<GuideEntriesConnectionWhere>>;
  node?: InputMaybe<TableEntryWhere>;
};

export type GuideEntriesNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<GuideEntriesNodeAggregationWhereInput>>;
  NOT?: InputMaybe<GuideEntriesNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<GuideEntriesNodeAggregationWhereInput>>;
};

export type GuideEntriesRelationship = {
  __typename?: 'GuideEntriesRelationship';
  cursor: Scalars['String']['output'];
  node: TableEntry;
};

export type GuideFeaturesAggregateInput = {
  AND?: InputMaybe<Array<GuideFeaturesAggregateInput>>;
  NOT?: InputMaybe<GuideFeaturesAggregateInput>;
  OR?: InputMaybe<Array<GuideFeaturesAggregateInput>>;
  count?: InputMaybe<Scalars['Int']['input']>;
  count_GT?: InputMaybe<Scalars['Int']['input']>;
  count_GTE?: InputMaybe<Scalars['Int']['input']>;
  count_LT?: InputMaybe<Scalars['Int']['input']>;
  count_LTE?: InputMaybe<Scalars['Int']['input']>;
  edge?: InputMaybe<HasFeatureAggregationWhereInput>;
  node?: InputMaybe<GuideFeaturesNodeAggregationWhereInput>;
};

export type GuideFeaturesNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<GuideFeaturesNodeAggregationWhereInput>>;
  NOT?: InputMaybe<GuideFeaturesNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<GuideFeaturesNodeAggregationWhereInput>>;
  chebi_id_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  chebi_id_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  chebi_id_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  chebi_id_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  chebi_id_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  chebi_id_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  description_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  description_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  length_AVERAGE_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  length_AVERAGE_GT?: InputMaybe<Scalars['Float']['input']>;
  length_AVERAGE_GTE?: InputMaybe<Scalars['Float']['input']>;
  length_AVERAGE_LT?: InputMaybe<Scalars['Float']['input']>;
  length_AVERAGE_LTE?: InputMaybe<Scalars['Float']['input']>;
  length_MAX_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  length_MAX_GT?: InputMaybe<Scalars['Int']['input']>;
  length_MAX_GTE?: InputMaybe<Scalars['Int']['input']>;
  length_MAX_LT?: InputMaybe<Scalars['Int']['input']>;
  length_MAX_LTE?: InputMaybe<Scalars['Int']['input']>;
  length_MIN_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  length_MIN_GT?: InputMaybe<Scalars['Int']['input']>;
  length_MIN_GTE?: InputMaybe<Scalars['Int']['input']>;
  length_MIN_LT?: InputMaybe<Scalars['Int']['input']>;
  length_MIN_LTE?: InputMaybe<Scalars['Int']['input']>;
  length_SUM_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  length_SUM_GT?: InputMaybe<Scalars['Int']['input']>;
  length_SUM_GTE?: InputMaybe<Scalars['Int']['input']>;
  length_SUM_LT?: InputMaybe<Scalars['Int']['input']>;
  length_SUM_LTE?: InputMaybe<Scalars['Int']['input']>;
  name_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  name_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  seq_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  seq_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  seq_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  seq_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  seq_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  seq_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  seq_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  seq_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  seq_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  seq_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  seq_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  seq_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  seq_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  seq_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  seq_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  so_id_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  url_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  url_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
};

export type GuideGenomeAggregateInput = {
  AND?: InputMaybe<Array<GuideGenomeAggregateInput>>;
  NOT?: InputMaybe<GuideGenomeAggregateInput>;
  OR?: InputMaybe<Array<GuideGenomeAggregateInput>>;
  count?: InputMaybe<Scalars['Int']['input']>;
  count_GT?: InputMaybe<Scalars['Int']['input']>;
  count_GTE?: InputMaybe<Scalars['Int']['input']>;
  count_LT?: InputMaybe<Scalars['Int']['input']>;
  count_LTE?: InputMaybe<Scalars['Int']['input']>;
  node?: InputMaybe<GuideGenomeNodeAggregationWhereInput>;
};

export type GuideGenomeGenomeAggregationSelection = {
  __typename?: 'GuideGenomeGenomeAggregationSelection';
  count: Scalars['Int']['output'];
  node?: Maybe<GuideGenomeGenomeNodeAggregateSelection>;
};

export type GuideGenomeGenomeNodeAggregateSelection = {
  __typename?: 'GuideGenomeGenomeNodeAggregateSelection';
  description: StringAggregateSelection;
  id: IdAggregateSelection;
  so_id: StringAggregateSelection;
  source: StringAggregateSelection;
  url: StringAggregateSelection;
  version: StringAggregateSelection;
};

export type GuideGenomeNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<GuideGenomeNodeAggregationWhereInput>>;
  NOT?: InputMaybe<GuideGenomeNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<GuideGenomeNodeAggregationWhereInput>>;
  description_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  description_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  so_id_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  source_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  source_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  source_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  source_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  source_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  source_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  source_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  source_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  source_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  source_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  source_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  source_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  source_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  source_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  source_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  url_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  url_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  version_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  version_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  version_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  version_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  version_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  version_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  version_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  version_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  version_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  version_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  version_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  version_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  version_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  version_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  version_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
};

export type GuideGroup = {
  /** The guides that are part of group */
  guides: Array<Guide>;
  guidesConnection: GuideGroupGuidesConnection;
  /** The group id */
  id: Scalars['ID']['output'];
};


export type GuideGroupGuidesArgs = {
  options?: InputMaybe<GuideOptions>;
  where?: InputMaybe<GuideWhere>;
};


export type GuideGroupGuidesConnectionArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  sort?: InputMaybe<Array<GuideGroupGuidesConnectionSort>>;
  where?: InputMaybe<GuideGroupGuidesConnectionWhere>;
};

export type GuideGroupAggregateSelection = {
  __typename?: 'GuideGroupAggregateSelection';
  count: Scalars['Int']['output'];
  id: IdAggregateSelection;
};

export type GuideGroupEdge = {
  __typename?: 'GuideGroupEdge';
  cursor: Scalars['String']['output'];
  node: GuideGroup;
};

export type GuideGroupGuidesAggregateInput = {
  AND?: InputMaybe<Array<GuideGroupGuidesAggregateInput>>;
  NOT?: InputMaybe<GuideGroupGuidesAggregateInput>;
  OR?: InputMaybe<Array<GuideGroupGuidesAggregateInput>>;
  count?: InputMaybe<Scalars['Int']['input']>;
  count_GT?: InputMaybe<Scalars['Int']['input']>;
  count_GTE?: InputMaybe<Scalars['Int']['input']>;
  count_LT?: InputMaybe<Scalars['Int']['input']>;
  count_LTE?: InputMaybe<Scalars['Int']['input']>;
  node?: InputMaybe<GuideGroupGuidesNodeAggregationWhereInput>;
};

export type GuideGroupGuidesConnection = {
  __typename?: 'GuideGroupGuidesConnection';
  edges: Array<GuideGroupGuidesRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int']['output'];
};

export type GuideGroupGuidesConnectionSort = {
  node?: InputMaybe<GuideSort>;
};

export type GuideGroupGuidesConnectionWhere = {
  AND?: InputMaybe<Array<GuideGroupGuidesConnectionWhere>>;
  NOT?: InputMaybe<GuideGroupGuidesConnectionWhere>;
  OR?: InputMaybe<Array<GuideGroupGuidesConnectionWhere>>;
  node?: InputMaybe<GuideWhere>;
};

export type GuideGroupGuidesNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<GuideGroupGuidesNodeAggregationWhereInput>>;
  NOT?: InputMaybe<GuideGroupGuidesNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<GuideGroupGuidesNodeAggregationWhereInput>>;
  chebi_id_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  chebi_id_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  chebi_id_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  chebi_id_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  chebi_id_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  chebi_id_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  description_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  description_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  length_AVERAGE_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  length_AVERAGE_GT?: InputMaybe<Scalars['Float']['input']>;
  length_AVERAGE_GTE?: InputMaybe<Scalars['Float']['input']>;
  length_AVERAGE_LT?: InputMaybe<Scalars['Float']['input']>;
  length_AVERAGE_LTE?: InputMaybe<Scalars['Float']['input']>;
  length_MAX_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  length_MAX_GT?: InputMaybe<Scalars['Int']['input']>;
  length_MAX_GTE?: InputMaybe<Scalars['Int']['input']>;
  length_MAX_LT?: InputMaybe<Scalars['Int']['input']>;
  length_MAX_LTE?: InputMaybe<Scalars['Int']['input']>;
  length_MIN_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  length_MIN_GT?: InputMaybe<Scalars['Int']['input']>;
  length_MIN_GTE?: InputMaybe<Scalars['Int']['input']>;
  length_MIN_LT?: InputMaybe<Scalars['Int']['input']>;
  length_MIN_LTE?: InputMaybe<Scalars['Int']['input']>;
  length_SUM_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  length_SUM_GT?: InputMaybe<Scalars['Int']['input']>;
  length_SUM_GTE?: InputMaybe<Scalars['Int']['input']>;
  length_SUM_LT?: InputMaybe<Scalars['Int']['input']>;
  length_SUM_LTE?: InputMaybe<Scalars['Int']['input']>;
  name_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  name_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  seq_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  seq_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  seq_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  seq_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  seq_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  seq_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  seq_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  seq_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  seq_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  seq_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  seq_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  seq_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  seq_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  seq_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  seq_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  so_id_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  url_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  url_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
};

export type GuideGroupGuidesRelationship = {
  __typename?: 'GuideGroupGuidesRelationship';
  cursor: Scalars['String']['output'];
  node: Guide;
};

export enum GuideGroupImplementation {
  Cluster = 'Cluster',
  Isoform = 'Isoform'
}

export type GuideGroupOptions = {
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  /** Specify one or more GuideGroupSort objects to sort GuideGroups by. The sorts will be applied in the order in which they are arranged in the array. */
  sort?: InputMaybe<Array<InputMaybe<GuideGroupSort>>>;
};

/** Fields to sort GuideGroups by. The order in which sorts are applied is not guaranteed when specifying many fields in one GuideGroupSort object. */
export type GuideGroupSort = {
  id?: InputMaybe<SortDirection>;
};

export type GuideGroupWhere = {
  AND?: InputMaybe<Array<GuideGroupWhere>>;
  NOT?: InputMaybe<GuideGroupWhere>;
  OR?: InputMaybe<Array<GuideGroupWhere>>;
  guidesAggregate?: InputMaybe<GuideGroupGuidesAggregateInput>;
  /** Return GuideGroups where all of the related GuideGroupGuidesConnections match this filter */
  guidesConnection_ALL?: InputMaybe<GuideGroupGuidesConnectionWhere>;
  /** Return GuideGroups where none of the related GuideGroupGuidesConnections match this filter */
  guidesConnection_NONE?: InputMaybe<GuideGroupGuidesConnectionWhere>;
  /** Return GuideGroups where one of the related GuideGroupGuidesConnections match this filter */
  guidesConnection_SINGLE?: InputMaybe<GuideGroupGuidesConnectionWhere>;
  /** Return GuideGroups where some of the related GuideGroupGuidesConnections match this filter */
  guidesConnection_SOME?: InputMaybe<GuideGroupGuidesConnectionWhere>;
  /** Return GuideGroups where all of the related Guides match this filter */
  guides_ALL?: InputMaybe<GuideWhere>;
  /** Return GuideGroups where none of the related Guides match this filter */
  guides_NONE?: InputMaybe<GuideWhere>;
  /** Return GuideGroups where one of the related Guides match this filter */
  guides_SINGLE?: InputMaybe<GuideWhere>;
  /** Return GuideGroups where some of the related Guides match this filter */
  guides_SOME?: InputMaybe<GuideWhere>;
  id?: InputMaybe<Scalars['ID']['input']>;
  id_CONTAINS?: InputMaybe<Scalars['ID']['input']>;
  id_ENDS_WITH?: InputMaybe<Scalars['ID']['input']>;
  id_IN?: InputMaybe<Array<Scalars['ID']['input']>>;
  id_STARTS_WITH?: InputMaybe<Scalars['ID']['input']>;
  typename_IN?: InputMaybe<Array<GuideGroupImplementation>>;
};

export type GuideGroupsConnection = {
  __typename?: 'GuideGroupsConnection';
  edges: Array<GuideGroupEdge>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int']['output'];
};

export type GuideInteractionsAggregateInput = {
  AND?: InputMaybe<Array<GuideInteractionsAggregateInput>>;
  NOT?: InputMaybe<GuideInteractionsAggregateInput>;
  OR?: InputMaybe<Array<GuideInteractionsAggregateInput>>;
  count?: InputMaybe<Scalars['Int']['input']>;
  count_GT?: InputMaybe<Scalars['Int']['input']>;
  count_GTE?: InputMaybe<Scalars['Int']['input']>;
  count_LT?: InputMaybe<Scalars['Int']['input']>;
  count_LTE?: InputMaybe<Scalars['Int']['input']>;
  node?: InputMaybe<GuideInteractionsNodeAggregationWhereInput>;
};

export type GuideInteractionsConnection = {
  __typename?: 'GuideInteractionsConnection';
  edges: Array<GuideInteractionsRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int']['output'];
};

export type GuideInteractionsConnectionSort = {
  node?: InputMaybe<InteractionSort>;
};

export type GuideInteractionsConnectionWhere = {
  AND?: InputMaybe<Array<GuideInteractionsConnectionWhere>>;
  NOT?: InputMaybe<GuideInteractionsConnectionWhere>;
  OR?: InputMaybe<Array<GuideInteractionsConnectionWhere>>;
  node?: InputMaybe<InteractionWhere>;
};

export type GuideInteractionsNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<GuideInteractionsNodeAggregationWhereInput>>;
  NOT?: InputMaybe<GuideInteractionsNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<GuideInteractionsNodeAggregationWhereInput>>;
};

export type GuideInteractionsRelationship = {
  __typename?: 'GuideInteractionsRelationship';
  cursor: Scalars['String']['output'];
  node: Interaction;
};

export type GuideIsoformAggregateInput = {
  AND?: InputMaybe<Array<GuideIsoformAggregateInput>>;
  NOT?: InputMaybe<GuideIsoformAggregateInput>;
  OR?: InputMaybe<Array<GuideIsoformAggregateInput>>;
  count?: InputMaybe<Scalars['Int']['input']>;
  count_GT?: InputMaybe<Scalars['Int']['input']>;
  count_GTE?: InputMaybe<Scalars['Int']['input']>;
  count_LT?: InputMaybe<Scalars['Int']['input']>;
  count_LTE?: InputMaybe<Scalars['Int']['input']>;
  node?: InputMaybe<GuideIsoformNodeAggregationWhereInput>;
};

export type GuideIsoformConnection = {
  __typename?: 'GuideIsoformConnection';
  edges: Array<GuideIsoformRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int']['output'];
};

export type GuideIsoformConnectionSort = {
  node?: InputMaybe<IsoformSort>;
};

export type GuideIsoformConnectionWhere = {
  AND?: InputMaybe<Array<GuideIsoformConnectionWhere>>;
  NOT?: InputMaybe<GuideIsoformConnectionWhere>;
  OR?: InputMaybe<Array<GuideIsoformConnectionWhere>>;
  node?: InputMaybe<IsoformWhere>;
};

export type GuideIsoformIsoformAggregationSelection = {
  __typename?: 'GuideIsoformIsoformAggregationSelection';
  count: Scalars['Int']['output'];
  node?: Maybe<GuideIsoformIsoformNodeAggregateSelection>;
};

export type GuideIsoformIsoformNodeAggregateSelection = {
  __typename?: 'GuideIsoformIsoformNodeAggregateSelection';
  id: IdAggregateSelection;
};

export type GuideIsoformNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<GuideIsoformNodeAggregationWhereInput>>;
  NOT?: InputMaybe<GuideIsoformNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<GuideIsoformNodeAggregationWhereInput>>;
};

export type GuideIsoformRelationship = {
  __typename?: 'GuideIsoformRelationship';
  cursor: Scalars['String']['output'];
  node: Isoform;
};

export type GuideModificationModificationsAggregationSelection = {
  __typename?: 'GuideModificationModificationsAggregationSelection';
  count: Scalars['Int']['output'];
  node?: Maybe<GuideModificationModificationsNodeAggregateSelection>;
};

export type GuideModificationModificationsNodeAggregateSelection = {
  __typename?: 'GuideModificationModificationsNodeAggregateSelection';
  id: IdAggregateSelection;
  name: StringAggregateSelection;
  nucleotide: StringAggregateSelection;
  position: IntAggregateSelection;
  symbol: StringAggregateSelection;
  symbol_label: StringAggregateSelection;
  type_label: StringAggregateSelection;
  type_short_label: StringAggregateSelection;
};

export type GuideModificationsAggregateInput = {
  AND?: InputMaybe<Array<GuideModificationsAggregateInput>>;
  NOT?: InputMaybe<GuideModificationsAggregateInput>;
  OR?: InputMaybe<Array<GuideModificationsAggregateInput>>;
  count?: InputMaybe<Scalars['Int']['input']>;
  count_GT?: InputMaybe<Scalars['Int']['input']>;
  count_GTE?: InputMaybe<Scalars['Int']['input']>;
  count_LT?: InputMaybe<Scalars['Int']['input']>;
  count_LTE?: InputMaybe<Scalars['Int']['input']>;
  node?: InputMaybe<GuideModificationsNodeAggregationWhereInput>;
};

export type GuideModificationsConnection = {
  __typename?: 'GuideModificationsConnection';
  edges: Array<GuideModificationsRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int']['output'];
};

export type GuideModificationsConnectionSort = {
  node?: InputMaybe<ModificationSort>;
};

export type GuideModificationsConnectionWhere = {
  AND?: InputMaybe<Array<GuideModificationsConnectionWhere>>;
  NOT?: InputMaybe<GuideModificationsConnectionWhere>;
  OR?: InputMaybe<Array<GuideModificationsConnectionWhere>>;
  node?: InputMaybe<ModificationWhere>;
};

export type GuideModificationsNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<GuideModificationsNodeAggregationWhereInput>>;
  NOT?: InputMaybe<GuideModificationsNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<GuideModificationsNodeAggregationWhereInput>>;
  name_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  name_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  nucleotide_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  nucleotide_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  nucleotide_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  nucleotide_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  nucleotide_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  nucleotide_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  nucleotide_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  nucleotide_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  nucleotide_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  nucleotide_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  nucleotide_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  nucleotide_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  nucleotide_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  nucleotide_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  nucleotide_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  position_AVERAGE_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  position_AVERAGE_GT?: InputMaybe<Scalars['Float']['input']>;
  position_AVERAGE_GTE?: InputMaybe<Scalars['Float']['input']>;
  position_AVERAGE_LT?: InputMaybe<Scalars['Float']['input']>;
  position_AVERAGE_LTE?: InputMaybe<Scalars['Float']['input']>;
  position_MAX_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  position_MAX_GT?: InputMaybe<Scalars['Int']['input']>;
  position_MAX_GTE?: InputMaybe<Scalars['Int']['input']>;
  position_MAX_LT?: InputMaybe<Scalars['Int']['input']>;
  position_MAX_LTE?: InputMaybe<Scalars['Int']['input']>;
  position_MIN_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  position_MIN_GT?: InputMaybe<Scalars['Int']['input']>;
  position_MIN_GTE?: InputMaybe<Scalars['Int']['input']>;
  position_MIN_LT?: InputMaybe<Scalars['Int']['input']>;
  position_MIN_LTE?: InputMaybe<Scalars['Int']['input']>;
  position_SUM_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  position_SUM_GT?: InputMaybe<Scalars['Int']['input']>;
  position_SUM_GTE?: InputMaybe<Scalars['Int']['input']>;
  position_SUM_LT?: InputMaybe<Scalars['Int']['input']>;
  position_SUM_LTE?: InputMaybe<Scalars['Int']['input']>;
  symbol_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  symbol_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  symbol_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  symbol_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  symbol_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  symbol_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  symbol_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  symbol_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  symbol_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  symbol_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  symbol_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  symbol_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  symbol_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  symbol_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  symbol_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  symbol_label_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  symbol_label_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  symbol_label_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  symbol_label_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  symbol_label_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  symbol_label_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  symbol_label_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  symbol_label_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  symbol_label_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  symbol_label_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  symbol_label_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  symbol_label_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  symbol_label_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  symbol_label_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  symbol_label_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  type_label_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  type_label_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  type_label_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  type_label_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  type_label_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  type_label_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  type_label_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  type_label_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  type_label_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  type_label_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  type_label_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  type_label_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  type_label_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  type_label_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  type_label_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  type_short_label_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  type_short_label_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  type_short_label_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  type_short_label_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  type_short_label_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  type_short_label_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  type_short_label_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  type_short_label_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  type_short_label_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  type_short_label_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  type_short_label_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  type_short_label_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  type_short_label_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  type_short_label_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  type_short_label_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
};

export type GuideModificationsRelationship = {
  __typename?: 'GuideModificationsRelationship';
  cursor: Scalars['String']['output'];
  node: Modification;
};

export type GuideOptions = {
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  /** Specify one or more GuideSort objects to sort Guides by. The sorts will be applied in the order in which they are arranged in the array. */
  sort?: InputMaybe<Array<GuideSort>>;
};

export type GuideParentAggregateInput = {
  AND?: InputMaybe<Array<GuideParentAggregateInput>>;
  NOT?: InputMaybe<GuideParentAggregateInput>;
  OR?: InputMaybe<Array<GuideParentAggregateInput>>;
  count?: InputMaybe<Scalars['Int']['input']>;
  count_GT?: InputMaybe<Scalars['Int']['input']>;
  count_GTE?: InputMaybe<Scalars['Int']['input']>;
  count_LT?: InputMaybe<Scalars['Int']['input']>;
  count_LTE?: InputMaybe<Scalars['Int']['input']>;
  edge?: InputMaybe<HasFeatureAggregationWhereInput>;
  node?: InputMaybe<GuideParentNodeAggregationWhereInput>;
};

export type GuideParentConnection = {
  __typename?: 'GuideParentConnection';
  edges: Array<GuideParentRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int']['output'];
};

export type GuideParentConnectionSort = {
  edge?: InputMaybe<HasFeatureSort>;
  node?: InputMaybe<SequenceSort>;
};

export type GuideParentConnectionWhere = {
  AND?: InputMaybe<Array<GuideParentConnectionWhere>>;
  NOT?: InputMaybe<GuideParentConnectionWhere>;
  OR?: InputMaybe<Array<GuideParentConnectionWhere>>;
  edge?: InputMaybe<HasFeatureWhere>;
  node?: InputMaybe<SequenceWhere>;
};

export type GuideParentNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<GuideParentNodeAggregationWhereInput>>;
  NOT?: InputMaybe<GuideParentNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<GuideParentNodeAggregationWhereInput>>;
  chebi_id_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  chebi_id_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  chebi_id_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  chebi_id_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  chebi_id_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  chebi_id_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  description_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  description_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  length_AVERAGE_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  length_AVERAGE_GT?: InputMaybe<Scalars['Float']['input']>;
  length_AVERAGE_GTE?: InputMaybe<Scalars['Float']['input']>;
  length_AVERAGE_LT?: InputMaybe<Scalars['Float']['input']>;
  length_AVERAGE_LTE?: InputMaybe<Scalars['Float']['input']>;
  length_MAX_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  length_MAX_GT?: InputMaybe<Scalars['Int']['input']>;
  length_MAX_GTE?: InputMaybe<Scalars['Int']['input']>;
  length_MAX_LT?: InputMaybe<Scalars['Int']['input']>;
  length_MAX_LTE?: InputMaybe<Scalars['Int']['input']>;
  length_MIN_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  length_MIN_GT?: InputMaybe<Scalars['Int']['input']>;
  length_MIN_GTE?: InputMaybe<Scalars['Int']['input']>;
  length_MIN_LT?: InputMaybe<Scalars['Int']['input']>;
  length_MIN_LTE?: InputMaybe<Scalars['Int']['input']>;
  length_SUM_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  length_SUM_GT?: InputMaybe<Scalars['Int']['input']>;
  length_SUM_GTE?: InputMaybe<Scalars['Int']['input']>;
  length_SUM_LT?: InputMaybe<Scalars['Int']['input']>;
  length_SUM_LTE?: InputMaybe<Scalars['Int']['input']>;
  name_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  name_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  seq_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  seq_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  seq_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  seq_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  seq_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  seq_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  seq_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  seq_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  seq_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  seq_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  seq_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  seq_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  seq_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  seq_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  seq_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  so_id_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  url_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  url_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
};

export type GuideParentRelationship = {
  __typename?: 'GuideParentRelationship';
  cursor: Scalars['String']['output'];
  node: Sequence;
  properties: HasFeature;
};

export type GuideSequenceFeaturesAggregationSelection = {
  __typename?: 'GuideSequenceFeaturesAggregationSelection';
  count: Scalars['Int']['output'];
  edge?: Maybe<GuideSequenceFeaturesEdgeAggregateSelection>;
  node?: Maybe<GuideSequenceFeaturesNodeAggregateSelection>;
};

export type GuideSequenceFeaturesEdgeAggregateSelection = {
  __typename?: 'GuideSequenceFeaturesEdgeAggregateSelection';
  end: IntAggregateSelection;
  start: IntAggregateSelection;
};

export type GuideSequenceFeaturesNodeAggregateSelection = {
  __typename?: 'GuideSequenceFeaturesNodeAggregateSelection';
  chebi_id: StringAggregateSelection;
  description: StringAggregateSelection;
  id: IdAggregateSelection;
  length: IntAggregateSelection;
  name: StringAggregateSelection;
  seq: StringAggregateSelection;
  so_id: StringAggregateSelection;
  url: StringAggregateSelection;
};

export type GuideSequenceParentAggregationSelection = {
  __typename?: 'GuideSequenceParentAggregationSelection';
  count: Scalars['Int']['output'];
  edge?: Maybe<GuideSequenceParentEdgeAggregateSelection>;
  node?: Maybe<GuideSequenceParentNodeAggregateSelection>;
};

export type GuideSequenceParentEdgeAggregateSelection = {
  __typename?: 'GuideSequenceParentEdgeAggregateSelection';
  end: IntAggregateSelection;
  start: IntAggregateSelection;
};

export type GuideSequenceParentNodeAggregateSelection = {
  __typename?: 'GuideSequenceParentNodeAggregateSelection';
  chebi_id: StringAggregateSelection;
  description: StringAggregateSelection;
  id: IdAggregateSelection;
  length: IntAggregateSelection;
  name: StringAggregateSelection;
  seq: StringAggregateSelection;
  so_id: StringAggregateSelection;
  url: StringAggregateSelection;
};

/** Fields to sort Guides by. The order in which sorts are applied is not guaranteed when specifying many fields in one GuideSort object. */
export type GuideSort = {
  annotation?: InputMaybe<SortDirection>;
  chebi_id?: InputMaybe<SortDirection>;
  class?: InputMaybe<SortDirection>;
  description?: InputMaybe<SortDirection>;
  graphql_type?: InputMaybe<SortDirection>;
  id?: InputMaybe<SortDirection>;
  length?: InputMaybe<SortDirection>;
  name?: InputMaybe<SortDirection>;
  seq?: InputMaybe<SortDirection>;
  so_id?: InputMaybe<SortDirection>;
  subclass?: InputMaybe<SortDirection>;
  type?: InputMaybe<SortDirection>;
  url?: InputMaybe<SortDirection>;
};

export type GuideWhere = {
  AND?: InputMaybe<Array<GuideWhere>>;
  NOT?: InputMaybe<GuideWhere>;
  OR?: InputMaybe<Array<GuideWhere>>;
  altnames?: InputMaybe<Array<Scalars['String']['input']>>;
  altnames_INCLUDES?: InputMaybe<Scalars['String']['input']>;
  annotation?: InputMaybe<AnnotationType>;
  annotation_IN?: InputMaybe<Array<InputMaybe<AnnotationType>>>;
  chebi_id?: InputMaybe<Scalars['String']['input']>;
  chebi_id_CONTAINS?: InputMaybe<Scalars['String']['input']>;
  chebi_id_ENDS_WITH?: InputMaybe<Scalars['String']['input']>;
  chebi_id_IN?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  chebi_id_STARTS_WITH?: InputMaybe<Scalars['String']['input']>;
  class?: InputMaybe<SequenceClass>;
  class_IN?: InputMaybe<Array<SequenceClass>>;
  cluster?: InputMaybe<ClusterWhere>;
  clusterAggregate?: InputMaybe<GuideClusterAggregateInput>;
  clusterConnection?: InputMaybe<GuideClusterConnectionWhere>;
  clusterConnection_NOT?: InputMaybe<GuideClusterConnectionWhere>;
  cluster_NOT?: InputMaybe<ClusterWhere>;
  description?: InputMaybe<Scalars['String']['input']>;
  description_CONTAINS?: InputMaybe<Scalars['String']['input']>;
  description_ENDS_WITH?: InputMaybe<Scalars['String']['input']>;
  description_IN?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  description_STARTS_WITH?: InputMaybe<Scalars['String']['input']>;
  entriesAggregate?: InputMaybe<GuideEntriesAggregateInput>;
  /** Return Guides where all of the related GuideEntriesConnections match this filter */
  entriesConnection_ALL?: InputMaybe<GuideEntriesConnectionWhere>;
  /** Return Guides where none of the related GuideEntriesConnections match this filter */
  entriesConnection_NONE?: InputMaybe<GuideEntriesConnectionWhere>;
  /** Return Guides where one of the related GuideEntriesConnections match this filter */
  entriesConnection_SINGLE?: InputMaybe<GuideEntriesConnectionWhere>;
  /** Return Guides where some of the related GuideEntriesConnections match this filter */
  entriesConnection_SOME?: InputMaybe<GuideEntriesConnectionWhere>;
  /** Return Guides where all of the related TableEntries match this filter */
  entries_ALL?: InputMaybe<TableEntryWhere>;
  /** Return Guides where none of the related TableEntries match this filter */
  entries_NONE?: InputMaybe<TableEntryWhere>;
  /** Return Guides where one of the related TableEntries match this filter */
  entries_SINGLE?: InputMaybe<TableEntryWhere>;
  /** Return Guides where some of the related TableEntries match this filter */
  entries_SOME?: InputMaybe<TableEntryWhere>;
  featuresAggregate?: InputMaybe<GuideFeaturesAggregateInput>;
  /** Return Guides where all of the related SequenceFeaturesConnections match this filter */
  featuresConnection_ALL?: InputMaybe<SequenceFeaturesConnectionWhere>;
  /** Return Guides where none of the related SequenceFeaturesConnections match this filter */
  featuresConnection_NONE?: InputMaybe<SequenceFeaturesConnectionWhere>;
  /** Return Guides where one of the related SequenceFeaturesConnections match this filter */
  featuresConnection_SINGLE?: InputMaybe<SequenceFeaturesConnectionWhere>;
  /** Return Guides where some of the related SequenceFeaturesConnections match this filter */
  featuresConnection_SOME?: InputMaybe<SequenceFeaturesConnectionWhere>;
  /** Return Guides where all of the related Sequences match this filter */
  features_ALL?: InputMaybe<SequenceWhere>;
  /** Return Guides where none of the related Sequences match this filter */
  features_NONE?: InputMaybe<SequenceWhere>;
  /** Return Guides where one of the related Sequences match this filter */
  features_SINGLE?: InputMaybe<SequenceWhere>;
  /** Return Guides where some of the related Sequences match this filter */
  features_SOME?: InputMaybe<SequenceWhere>;
  genome?: InputMaybe<GenomeWhere>;
  genomeAggregate?: InputMaybe<GuideGenomeAggregateInput>;
  genomeConnection?: InputMaybe<SequenceGenomeConnectionWhere>;
  genomeConnection_NOT?: InputMaybe<SequenceGenomeConnectionWhere>;
  genome_NOT?: InputMaybe<GenomeWhere>;
  graphql_type?: InputMaybe<GraphQlType>;
  graphql_type_IN?: InputMaybe<Array<GraphQlType>>;
  host_genes?: InputMaybe<Array<Scalars['String']['input']>>;
  host_genes_INCLUDES?: InputMaybe<Scalars['String']['input']>;
  id?: InputMaybe<Scalars['ID']['input']>;
  id_CONTAINS?: InputMaybe<Scalars['ID']['input']>;
  id_ENDS_WITH?: InputMaybe<Scalars['ID']['input']>;
  id_IN?: InputMaybe<Array<Scalars['ID']['input']>>;
  id_STARTS_WITH?: InputMaybe<Scalars['ID']['input']>;
  interactionsAggregate?: InputMaybe<GuideInteractionsAggregateInput>;
  /** Return Guides where all of the related GuideInteractionsConnections match this filter */
  interactionsConnection_ALL?: InputMaybe<GuideInteractionsConnectionWhere>;
  /** Return Guides where none of the related GuideInteractionsConnections match this filter */
  interactionsConnection_NONE?: InputMaybe<GuideInteractionsConnectionWhere>;
  /** Return Guides where one of the related GuideInteractionsConnections match this filter */
  interactionsConnection_SINGLE?: InputMaybe<GuideInteractionsConnectionWhere>;
  /** Return Guides where some of the related GuideInteractionsConnections match this filter */
  interactionsConnection_SOME?: InputMaybe<GuideInteractionsConnectionWhere>;
  /** Return Guides where all of the related Interactions match this filter */
  interactions_ALL?: InputMaybe<InteractionWhere>;
  /** Return Guides where none of the related Interactions match this filter */
  interactions_NONE?: InputMaybe<InteractionWhere>;
  /** Return Guides where one of the related Interactions match this filter */
  interactions_SINGLE?: InputMaybe<InteractionWhere>;
  /** Return Guides where some of the related Interactions match this filter */
  interactions_SOME?: InputMaybe<InteractionWhere>;
  isoform?: InputMaybe<IsoformWhere>;
  isoformAggregate?: InputMaybe<GuideIsoformAggregateInput>;
  isoformConnection?: InputMaybe<GuideIsoformConnectionWhere>;
  isoformConnection_NOT?: InputMaybe<GuideIsoformConnectionWhere>;
  isoform_NOT?: InputMaybe<IsoformWhere>;
  length?: InputMaybe<Scalars['Int']['input']>;
  length_GT?: InputMaybe<Scalars['Int']['input']>;
  length_GTE?: InputMaybe<Scalars['Int']['input']>;
  length_IN?: InputMaybe<Array<Scalars['Int']['input']>>;
  length_LT?: InputMaybe<Scalars['Int']['input']>;
  length_LTE?: InputMaybe<Scalars['Int']['input']>;
  modificationsAggregate?: InputMaybe<GuideModificationsAggregateInput>;
  /** Return Guides where all of the related GuideModificationsConnections match this filter */
  modificationsConnection_ALL?: InputMaybe<GuideModificationsConnectionWhere>;
  /** Return Guides where none of the related GuideModificationsConnections match this filter */
  modificationsConnection_NONE?: InputMaybe<GuideModificationsConnectionWhere>;
  /** Return Guides where one of the related GuideModificationsConnections match this filter */
  modificationsConnection_SINGLE?: InputMaybe<GuideModificationsConnectionWhere>;
  /** Return Guides where some of the related GuideModificationsConnections match this filter */
  modificationsConnection_SOME?: InputMaybe<GuideModificationsConnectionWhere>;
  /** Return Guides where all of the related Modifications match this filter */
  modifications_ALL?: InputMaybe<ModificationWhere>;
  /** Return Guides where none of the related Modifications match this filter */
  modifications_NONE?: InputMaybe<ModificationWhere>;
  /** Return Guides where one of the related Modifications match this filter */
  modifications_SINGLE?: InputMaybe<ModificationWhere>;
  /** Return Guides where some of the related Modifications match this filter */
  modifications_SOME?: InputMaybe<ModificationWhere>;
  name?: InputMaybe<Scalars['String']['input']>;
  name_CONTAINS?: InputMaybe<Scalars['String']['input']>;
  name_ENDS_WITH?: InputMaybe<Scalars['String']['input']>;
  name_IN?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  name_STARTS_WITH?: InputMaybe<Scalars['String']['input']>;
  parent?: InputMaybe<SequenceWhere>;
  parentAggregate?: InputMaybe<GuideParentAggregateInput>;
  parentConnection?: InputMaybe<GuideParentConnectionWhere>;
  parentConnection_NOT?: InputMaybe<GuideParentConnectionWhere>;
  parent_NOT?: InputMaybe<SequenceWhere>;
  seq?: InputMaybe<Scalars['String']['input']>;
  seq_CONTAINS?: InputMaybe<Scalars['String']['input']>;
  seq_ENDS_WITH?: InputMaybe<Scalars['String']['input']>;
  seq_IN?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  seq_STARTS_WITH?: InputMaybe<Scalars['String']['input']>;
  so_id?: InputMaybe<Scalars['String']['input']>;
  so_id_CONTAINS?: InputMaybe<Scalars['String']['input']>;
  so_id_ENDS_WITH?: InputMaybe<Scalars['String']['input']>;
  so_id_IN?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  so_id_STARTS_WITH?: InputMaybe<Scalars['String']['input']>;
  subclass?: InputMaybe<GuideClass>;
  subclass_IN?: InputMaybe<Array<GuideClass>>;
  type?: InputMaybe<SequenceType>;
  type_IN?: InputMaybe<Array<SequenceType>>;
  url?: InputMaybe<Scalars['String']['input']>;
  url_CONTAINS?: InputMaybe<Scalars['String']['input']>;
  url_ENDS_WITH?: InputMaybe<Scalars['String']['input']>;
  url_IN?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  url_STARTS_WITH?: InputMaybe<Scalars['String']['input']>;
};

export type GuidesConnection = {
  __typename?: 'GuidesConnection';
  edges: Array<GuideEdge>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int']['output'];
};

/**
 * The edge properties for the following fields:
 * * Chromosome.features
 * * Guide.features
 * * Guide.parent
 * * Target.features
 * * Target.parent
 * * GenericSequence.features
 * * GenericSequence.parent
 */
export type HasFeature = {
  __typename?: 'HasFeature';
  /** The end position, where start < end */
  end: Scalars['Int']['output'];
  /** The start position, where start < end */
  start: Scalars['Int']['output'];
  /** The strand of the feature, UNKNOWN by default */
  strand?: Maybe<Strand>;
};

export type HasFeatureAggregationWhereInput = {
  AND?: InputMaybe<Array<HasFeatureAggregationWhereInput>>;
  NOT?: InputMaybe<HasFeatureAggregationWhereInput>;
  OR?: InputMaybe<Array<HasFeatureAggregationWhereInput>>;
  end_AVERAGE_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  end_AVERAGE_GT?: InputMaybe<Scalars['Float']['input']>;
  end_AVERAGE_GTE?: InputMaybe<Scalars['Float']['input']>;
  end_AVERAGE_LT?: InputMaybe<Scalars['Float']['input']>;
  end_AVERAGE_LTE?: InputMaybe<Scalars['Float']['input']>;
  end_MAX_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  end_MAX_GT?: InputMaybe<Scalars['Int']['input']>;
  end_MAX_GTE?: InputMaybe<Scalars['Int']['input']>;
  end_MAX_LT?: InputMaybe<Scalars['Int']['input']>;
  end_MAX_LTE?: InputMaybe<Scalars['Int']['input']>;
  end_MIN_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  end_MIN_GT?: InputMaybe<Scalars['Int']['input']>;
  end_MIN_GTE?: InputMaybe<Scalars['Int']['input']>;
  end_MIN_LT?: InputMaybe<Scalars['Int']['input']>;
  end_MIN_LTE?: InputMaybe<Scalars['Int']['input']>;
  end_SUM_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  end_SUM_GT?: InputMaybe<Scalars['Int']['input']>;
  end_SUM_GTE?: InputMaybe<Scalars['Int']['input']>;
  end_SUM_LT?: InputMaybe<Scalars['Int']['input']>;
  end_SUM_LTE?: InputMaybe<Scalars['Int']['input']>;
  start_AVERAGE_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  start_AVERAGE_GT?: InputMaybe<Scalars['Float']['input']>;
  start_AVERAGE_GTE?: InputMaybe<Scalars['Float']['input']>;
  start_AVERAGE_LT?: InputMaybe<Scalars['Float']['input']>;
  start_AVERAGE_LTE?: InputMaybe<Scalars['Float']['input']>;
  start_MAX_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  start_MAX_GT?: InputMaybe<Scalars['Int']['input']>;
  start_MAX_GTE?: InputMaybe<Scalars['Int']['input']>;
  start_MAX_LT?: InputMaybe<Scalars['Int']['input']>;
  start_MAX_LTE?: InputMaybe<Scalars['Int']['input']>;
  start_MIN_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  start_MIN_GT?: InputMaybe<Scalars['Int']['input']>;
  start_MIN_GTE?: InputMaybe<Scalars['Int']['input']>;
  start_MIN_LT?: InputMaybe<Scalars['Int']['input']>;
  start_MIN_LTE?: InputMaybe<Scalars['Int']['input']>;
  start_SUM_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  start_SUM_GT?: InputMaybe<Scalars['Int']['input']>;
  start_SUM_GTE?: InputMaybe<Scalars['Int']['input']>;
  start_SUM_LT?: InputMaybe<Scalars['Int']['input']>;
  start_SUM_LTE?: InputMaybe<Scalars['Int']['input']>;
};

export type HasFeatureSort = {
  end?: InputMaybe<SortDirection>;
  start?: InputMaybe<SortDirection>;
  strand?: InputMaybe<SortDirection>;
};

export type HasFeatureWhere = {
  AND?: InputMaybe<Array<HasFeatureWhere>>;
  NOT?: InputMaybe<HasFeatureWhere>;
  OR?: InputMaybe<Array<HasFeatureWhere>>;
  end?: InputMaybe<Scalars['Int']['input']>;
  end_GT?: InputMaybe<Scalars['Int']['input']>;
  end_GTE?: InputMaybe<Scalars['Int']['input']>;
  end_IN?: InputMaybe<Array<Scalars['Int']['input']>>;
  end_LT?: InputMaybe<Scalars['Int']['input']>;
  end_LTE?: InputMaybe<Scalars['Int']['input']>;
  start?: InputMaybe<Scalars['Int']['input']>;
  start_GT?: InputMaybe<Scalars['Int']['input']>;
  start_GTE?: InputMaybe<Scalars['Int']['input']>;
  start_IN?: InputMaybe<Array<Scalars['Int']['input']>>;
  start_LT?: InputMaybe<Scalars['Int']['input']>;
  start_LTE?: InputMaybe<Scalars['Int']['input']>;
  strand?: InputMaybe<Strand>;
  strand_IN?: InputMaybe<Array<InputMaybe<Strand>>>;
};

export type IdAggregateSelection = {
  __typename?: 'IDAggregateSelection';
  longest?: Maybe<Scalars['ID']['output']>;
  shortest?: Maybe<Scalars['ID']['output']>;
};

export type IntAggregateSelection = {
  __typename?: 'IntAggregateSelection';
  average?: Maybe<Scalars['Float']['output']>;
  max?: Maybe<Scalars['Int']['output']>;
  min?: Maybe<Scalars['Int']['output']>;
  sum?: Maybe<Scalars['Int']['output']>;
};

/** An interaction between a guide and a target that produces a target modification. */
export type Interaction = {
  __typename?: 'Interaction';
  /** The interation duplexes associated with the interaction */
  duplexes: Array<Duplex>;
  duplexesAggregate?: Maybe<InteractionDuplexDuplexesAggregationSelection>;
  duplexesConnection: InteractionDuplexesConnection;
  /** The guide that guides the modification */
  guide: Guide;
  guideAggregate?: Maybe<InteractionGuideGuideAggregationSelection>;
  guideConnection: InteractionGuideConnection;
  /** The Modification id */
  id: Scalars['ID']['output'];
  /** The modification */
  modification: Modification;
  modificationAggregate?: Maybe<InteractionModificationModificationAggregationSelection>;
  modificationConnection: InteractionModificationConnection;
  /** The target RNA sequence on which the modification append */
  target: Target;
  targetAggregate?: Maybe<InteractionTargetTargetAggregationSelection>;
  targetConnection: InteractionTargetConnection;
};


/** An interaction between a guide and a target that produces a target modification. */
export type InteractionDuplexesArgs = {
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  options?: InputMaybe<DuplexOptions>;
  where?: InputMaybe<DuplexWhere>;
};


/** An interaction between a guide and a target that produces a target modification. */
export type InteractionDuplexesAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  where?: InputMaybe<DuplexWhere>;
};


/** An interaction between a guide and a target that produces a target modification. */
export type InteractionDuplexesConnectionArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  sort?: InputMaybe<Array<InteractionDuplexesConnectionSort>>;
  where?: InputMaybe<InteractionDuplexesConnectionWhere>;
};


/** An interaction between a guide and a target that produces a target modification. */
export type InteractionGuideArgs = {
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  options?: InputMaybe<GuideOptions>;
  where?: InputMaybe<GuideWhere>;
};


/** An interaction between a guide and a target that produces a target modification. */
export type InteractionGuideAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  where?: InputMaybe<GuideWhere>;
};


/** An interaction between a guide and a target that produces a target modification. */
export type InteractionGuideConnectionArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  sort?: InputMaybe<Array<InteractionGuideConnectionSort>>;
  where?: InputMaybe<InteractionGuideConnectionWhere>;
};


/** An interaction between a guide and a target that produces a target modification. */
export type InteractionModificationArgs = {
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  options?: InputMaybe<ModificationOptions>;
  where?: InputMaybe<ModificationWhere>;
};


/** An interaction between a guide and a target that produces a target modification. */
export type InteractionModificationAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  where?: InputMaybe<ModificationWhere>;
};


/** An interaction between a guide and a target that produces a target modification. */
export type InteractionModificationConnectionArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  sort?: InputMaybe<Array<InteractionModificationConnectionSort>>;
  where?: InputMaybe<InteractionModificationConnectionWhere>;
};


/** An interaction between a guide and a target that produces a target modification. */
export type InteractionTargetArgs = {
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  options?: InputMaybe<TargetOptions>;
  where?: InputMaybe<TargetWhere>;
};


/** An interaction between a guide and a target that produces a target modification. */
export type InteractionTargetAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  where?: InputMaybe<TargetWhere>;
};


/** An interaction between a guide and a target that produces a target modification. */
export type InteractionTargetConnectionArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  sort?: InputMaybe<Array<InteractionTargetConnectionSort>>;
  where?: InputMaybe<InteractionTargetConnectionWhere>;
};

export type InteractionAggregateSelection = {
  __typename?: 'InteractionAggregateSelection';
  count: Scalars['Int']['output'];
  id: IdAggregateSelection;
};

export type InteractionDuplexDuplexesAggregationSelection = {
  __typename?: 'InteractionDuplexDuplexesAggregationSelection';
  count: Scalars['Int']['output'];
  node?: Maybe<InteractionDuplexDuplexesNodeAggregateSelection>;
};

export type InteractionDuplexDuplexesNodeAggregateSelection = {
  __typename?: 'InteractionDuplexDuplexesNodeAggregateSelection';
  id: IdAggregateSelection;
  index: IntAggregateSelection;
};

export type InteractionDuplexesAggregateInput = {
  AND?: InputMaybe<Array<InteractionDuplexesAggregateInput>>;
  NOT?: InputMaybe<InteractionDuplexesAggregateInput>;
  OR?: InputMaybe<Array<InteractionDuplexesAggregateInput>>;
  count?: InputMaybe<Scalars['Int']['input']>;
  count_GT?: InputMaybe<Scalars['Int']['input']>;
  count_GTE?: InputMaybe<Scalars['Int']['input']>;
  count_LT?: InputMaybe<Scalars['Int']['input']>;
  count_LTE?: InputMaybe<Scalars['Int']['input']>;
  node?: InputMaybe<InteractionDuplexesNodeAggregationWhereInput>;
};

export type InteractionDuplexesConnection = {
  __typename?: 'InteractionDuplexesConnection';
  edges: Array<InteractionDuplexesRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int']['output'];
};

export type InteractionDuplexesConnectionSort = {
  node?: InputMaybe<DuplexSort>;
};

export type InteractionDuplexesConnectionWhere = {
  AND?: InputMaybe<Array<InteractionDuplexesConnectionWhere>>;
  NOT?: InputMaybe<InteractionDuplexesConnectionWhere>;
  OR?: InputMaybe<Array<InteractionDuplexesConnectionWhere>>;
  node?: InputMaybe<DuplexWhere>;
};

export type InteractionDuplexesNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<InteractionDuplexesNodeAggregationWhereInput>>;
  NOT?: InputMaybe<InteractionDuplexesNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<InteractionDuplexesNodeAggregationWhereInput>>;
  index_AVERAGE_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  index_AVERAGE_GT?: InputMaybe<Scalars['Float']['input']>;
  index_AVERAGE_GTE?: InputMaybe<Scalars['Float']['input']>;
  index_AVERAGE_LT?: InputMaybe<Scalars['Float']['input']>;
  index_AVERAGE_LTE?: InputMaybe<Scalars['Float']['input']>;
  index_MAX_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  index_MAX_GT?: InputMaybe<Scalars['Int']['input']>;
  index_MAX_GTE?: InputMaybe<Scalars['Int']['input']>;
  index_MAX_LT?: InputMaybe<Scalars['Int']['input']>;
  index_MAX_LTE?: InputMaybe<Scalars['Int']['input']>;
  index_MIN_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  index_MIN_GT?: InputMaybe<Scalars['Int']['input']>;
  index_MIN_GTE?: InputMaybe<Scalars['Int']['input']>;
  index_MIN_LT?: InputMaybe<Scalars['Int']['input']>;
  index_MIN_LTE?: InputMaybe<Scalars['Int']['input']>;
  index_SUM_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  index_SUM_GT?: InputMaybe<Scalars['Int']['input']>;
  index_SUM_GTE?: InputMaybe<Scalars['Int']['input']>;
  index_SUM_LT?: InputMaybe<Scalars['Int']['input']>;
  index_SUM_LTE?: InputMaybe<Scalars['Int']['input']>;
};

export type InteractionDuplexesRelationship = {
  __typename?: 'InteractionDuplexesRelationship';
  cursor: Scalars['String']['output'];
  node: Duplex;
};

export type InteractionEdge = {
  __typename?: 'InteractionEdge';
  cursor: Scalars['String']['output'];
  node: Interaction;
};

export type InteractionGuideAggregateInput = {
  AND?: InputMaybe<Array<InteractionGuideAggregateInput>>;
  NOT?: InputMaybe<InteractionGuideAggregateInput>;
  OR?: InputMaybe<Array<InteractionGuideAggregateInput>>;
  count?: InputMaybe<Scalars['Int']['input']>;
  count_GT?: InputMaybe<Scalars['Int']['input']>;
  count_GTE?: InputMaybe<Scalars['Int']['input']>;
  count_LT?: InputMaybe<Scalars['Int']['input']>;
  count_LTE?: InputMaybe<Scalars['Int']['input']>;
  node?: InputMaybe<InteractionGuideNodeAggregationWhereInput>;
};

export type InteractionGuideConnection = {
  __typename?: 'InteractionGuideConnection';
  edges: Array<InteractionGuideRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int']['output'];
};

export type InteractionGuideConnectionSort = {
  node?: InputMaybe<GuideSort>;
};

export type InteractionGuideConnectionWhere = {
  AND?: InputMaybe<Array<InteractionGuideConnectionWhere>>;
  NOT?: InputMaybe<InteractionGuideConnectionWhere>;
  OR?: InputMaybe<Array<InteractionGuideConnectionWhere>>;
  node?: InputMaybe<GuideWhere>;
};

export type InteractionGuideGuideAggregationSelection = {
  __typename?: 'InteractionGuideGuideAggregationSelection';
  count: Scalars['Int']['output'];
  node?: Maybe<InteractionGuideGuideNodeAggregateSelection>;
};

export type InteractionGuideGuideNodeAggregateSelection = {
  __typename?: 'InteractionGuideGuideNodeAggregateSelection';
  chebi_id: StringAggregateSelection;
  description: StringAggregateSelection;
  id: IdAggregateSelection;
  length: IntAggregateSelection;
  name: StringAggregateSelection;
  seq: StringAggregateSelection;
  so_id: StringAggregateSelection;
  url: StringAggregateSelection;
};

export type InteractionGuideNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<InteractionGuideNodeAggregationWhereInput>>;
  NOT?: InputMaybe<InteractionGuideNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<InteractionGuideNodeAggregationWhereInput>>;
  chebi_id_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  chebi_id_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  chebi_id_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  chebi_id_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  chebi_id_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  chebi_id_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  description_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  description_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  length_AVERAGE_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  length_AVERAGE_GT?: InputMaybe<Scalars['Float']['input']>;
  length_AVERAGE_GTE?: InputMaybe<Scalars['Float']['input']>;
  length_AVERAGE_LT?: InputMaybe<Scalars['Float']['input']>;
  length_AVERAGE_LTE?: InputMaybe<Scalars['Float']['input']>;
  length_MAX_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  length_MAX_GT?: InputMaybe<Scalars['Int']['input']>;
  length_MAX_GTE?: InputMaybe<Scalars['Int']['input']>;
  length_MAX_LT?: InputMaybe<Scalars['Int']['input']>;
  length_MAX_LTE?: InputMaybe<Scalars['Int']['input']>;
  length_MIN_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  length_MIN_GT?: InputMaybe<Scalars['Int']['input']>;
  length_MIN_GTE?: InputMaybe<Scalars['Int']['input']>;
  length_MIN_LT?: InputMaybe<Scalars['Int']['input']>;
  length_MIN_LTE?: InputMaybe<Scalars['Int']['input']>;
  length_SUM_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  length_SUM_GT?: InputMaybe<Scalars['Int']['input']>;
  length_SUM_GTE?: InputMaybe<Scalars['Int']['input']>;
  length_SUM_LT?: InputMaybe<Scalars['Int']['input']>;
  length_SUM_LTE?: InputMaybe<Scalars['Int']['input']>;
  name_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  name_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  seq_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  seq_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  seq_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  seq_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  seq_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  seq_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  seq_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  seq_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  seq_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  seq_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  seq_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  seq_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  seq_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  seq_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  seq_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  so_id_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  url_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  url_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
};

export type InteractionGuideRelationship = {
  __typename?: 'InteractionGuideRelationship';
  cursor: Scalars['String']['output'];
  node: Guide;
};

export type InteractionModificationAggregateInput = {
  AND?: InputMaybe<Array<InteractionModificationAggregateInput>>;
  NOT?: InputMaybe<InteractionModificationAggregateInput>;
  OR?: InputMaybe<Array<InteractionModificationAggregateInput>>;
  count?: InputMaybe<Scalars['Int']['input']>;
  count_GT?: InputMaybe<Scalars['Int']['input']>;
  count_GTE?: InputMaybe<Scalars['Int']['input']>;
  count_LT?: InputMaybe<Scalars['Int']['input']>;
  count_LTE?: InputMaybe<Scalars['Int']['input']>;
  node?: InputMaybe<InteractionModificationNodeAggregationWhereInput>;
};

export type InteractionModificationConnection = {
  __typename?: 'InteractionModificationConnection';
  edges: Array<InteractionModificationRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int']['output'];
};

export type InteractionModificationConnectionSort = {
  node?: InputMaybe<ModificationSort>;
};

export type InteractionModificationConnectionWhere = {
  AND?: InputMaybe<Array<InteractionModificationConnectionWhere>>;
  NOT?: InputMaybe<InteractionModificationConnectionWhere>;
  OR?: InputMaybe<Array<InteractionModificationConnectionWhere>>;
  node?: InputMaybe<ModificationWhere>;
};

export type InteractionModificationModificationAggregationSelection = {
  __typename?: 'InteractionModificationModificationAggregationSelection';
  count: Scalars['Int']['output'];
  node?: Maybe<InteractionModificationModificationNodeAggregateSelection>;
};

export type InteractionModificationModificationNodeAggregateSelection = {
  __typename?: 'InteractionModificationModificationNodeAggregateSelection';
  id: IdAggregateSelection;
  name: StringAggregateSelection;
  nucleotide: StringAggregateSelection;
  position: IntAggregateSelection;
  symbol: StringAggregateSelection;
  symbol_label: StringAggregateSelection;
  type_label: StringAggregateSelection;
  type_short_label: StringAggregateSelection;
};

export type InteractionModificationNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<InteractionModificationNodeAggregationWhereInput>>;
  NOT?: InputMaybe<InteractionModificationNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<InteractionModificationNodeAggregationWhereInput>>;
  name_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  name_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  nucleotide_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  nucleotide_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  nucleotide_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  nucleotide_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  nucleotide_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  nucleotide_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  nucleotide_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  nucleotide_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  nucleotide_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  nucleotide_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  nucleotide_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  nucleotide_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  nucleotide_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  nucleotide_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  nucleotide_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  position_AVERAGE_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  position_AVERAGE_GT?: InputMaybe<Scalars['Float']['input']>;
  position_AVERAGE_GTE?: InputMaybe<Scalars['Float']['input']>;
  position_AVERAGE_LT?: InputMaybe<Scalars['Float']['input']>;
  position_AVERAGE_LTE?: InputMaybe<Scalars['Float']['input']>;
  position_MAX_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  position_MAX_GT?: InputMaybe<Scalars['Int']['input']>;
  position_MAX_GTE?: InputMaybe<Scalars['Int']['input']>;
  position_MAX_LT?: InputMaybe<Scalars['Int']['input']>;
  position_MAX_LTE?: InputMaybe<Scalars['Int']['input']>;
  position_MIN_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  position_MIN_GT?: InputMaybe<Scalars['Int']['input']>;
  position_MIN_GTE?: InputMaybe<Scalars['Int']['input']>;
  position_MIN_LT?: InputMaybe<Scalars['Int']['input']>;
  position_MIN_LTE?: InputMaybe<Scalars['Int']['input']>;
  position_SUM_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  position_SUM_GT?: InputMaybe<Scalars['Int']['input']>;
  position_SUM_GTE?: InputMaybe<Scalars['Int']['input']>;
  position_SUM_LT?: InputMaybe<Scalars['Int']['input']>;
  position_SUM_LTE?: InputMaybe<Scalars['Int']['input']>;
  symbol_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  symbol_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  symbol_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  symbol_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  symbol_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  symbol_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  symbol_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  symbol_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  symbol_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  symbol_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  symbol_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  symbol_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  symbol_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  symbol_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  symbol_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  symbol_label_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  symbol_label_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  symbol_label_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  symbol_label_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  symbol_label_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  symbol_label_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  symbol_label_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  symbol_label_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  symbol_label_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  symbol_label_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  symbol_label_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  symbol_label_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  symbol_label_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  symbol_label_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  symbol_label_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  type_label_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  type_label_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  type_label_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  type_label_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  type_label_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  type_label_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  type_label_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  type_label_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  type_label_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  type_label_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  type_label_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  type_label_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  type_label_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  type_label_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  type_label_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  type_short_label_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  type_short_label_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  type_short_label_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  type_short_label_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  type_short_label_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  type_short_label_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  type_short_label_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  type_short_label_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  type_short_label_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  type_short_label_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  type_short_label_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  type_short_label_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  type_short_label_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  type_short_label_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  type_short_label_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
};

export type InteractionModificationRelationship = {
  __typename?: 'InteractionModificationRelationship';
  cursor: Scalars['String']['output'];
  node: Modification;
};

export type InteractionOptions = {
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  /** Specify one or more InteractionSort objects to sort Interactions by. The sorts will be applied in the order in which they are arranged in the array. */
  sort?: InputMaybe<Array<InteractionSort>>;
};

/** Fields to sort Interactions by. The order in which sorts are applied is not guaranteed when specifying many fields in one InteractionSort object. */
export type InteractionSort = {
  id?: InputMaybe<SortDirection>;
};

export type InteractionTargetAggregateInput = {
  AND?: InputMaybe<Array<InteractionTargetAggregateInput>>;
  NOT?: InputMaybe<InteractionTargetAggregateInput>;
  OR?: InputMaybe<Array<InteractionTargetAggregateInput>>;
  count?: InputMaybe<Scalars['Int']['input']>;
  count_GT?: InputMaybe<Scalars['Int']['input']>;
  count_GTE?: InputMaybe<Scalars['Int']['input']>;
  count_LT?: InputMaybe<Scalars['Int']['input']>;
  count_LTE?: InputMaybe<Scalars['Int']['input']>;
  node?: InputMaybe<InteractionTargetNodeAggregationWhereInput>;
};

export type InteractionTargetConnection = {
  __typename?: 'InteractionTargetConnection';
  edges: Array<InteractionTargetRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int']['output'];
};

export type InteractionTargetConnectionSort = {
  node?: InputMaybe<TargetSort>;
};

export type InteractionTargetConnectionWhere = {
  AND?: InputMaybe<Array<InteractionTargetConnectionWhere>>;
  NOT?: InputMaybe<InteractionTargetConnectionWhere>;
  OR?: InputMaybe<Array<InteractionTargetConnectionWhere>>;
  node?: InputMaybe<TargetWhere>;
};

export type InteractionTargetNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<InteractionTargetNodeAggregationWhereInput>>;
  NOT?: InputMaybe<InteractionTargetNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<InteractionTargetNodeAggregationWhereInput>>;
  chebi_id_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  chebi_id_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  chebi_id_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  chebi_id_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  chebi_id_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  chebi_id_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  description_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  description_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  length_AVERAGE_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  length_AVERAGE_GT?: InputMaybe<Scalars['Float']['input']>;
  length_AVERAGE_GTE?: InputMaybe<Scalars['Float']['input']>;
  length_AVERAGE_LT?: InputMaybe<Scalars['Float']['input']>;
  length_AVERAGE_LTE?: InputMaybe<Scalars['Float']['input']>;
  length_MAX_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  length_MAX_GT?: InputMaybe<Scalars['Int']['input']>;
  length_MAX_GTE?: InputMaybe<Scalars['Int']['input']>;
  length_MAX_LT?: InputMaybe<Scalars['Int']['input']>;
  length_MAX_LTE?: InputMaybe<Scalars['Int']['input']>;
  length_MIN_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  length_MIN_GT?: InputMaybe<Scalars['Int']['input']>;
  length_MIN_GTE?: InputMaybe<Scalars['Int']['input']>;
  length_MIN_LT?: InputMaybe<Scalars['Int']['input']>;
  length_MIN_LTE?: InputMaybe<Scalars['Int']['input']>;
  length_SUM_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  length_SUM_GT?: InputMaybe<Scalars['Int']['input']>;
  length_SUM_GTE?: InputMaybe<Scalars['Int']['input']>;
  length_SUM_LT?: InputMaybe<Scalars['Int']['input']>;
  length_SUM_LTE?: InputMaybe<Scalars['Int']['input']>;
  name_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  name_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  secondary_struct_file_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  secondary_struct_file_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  secondary_struct_file_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  secondary_struct_file_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  secondary_struct_file_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  secondary_struct_file_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  secondary_struct_file_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  secondary_struct_file_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  secondary_struct_file_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  secondary_struct_file_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  secondary_struct_file_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  secondary_struct_file_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  secondary_struct_file_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  secondary_struct_file_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  secondary_struct_file_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  seq_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  seq_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  seq_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  seq_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  seq_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  seq_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  seq_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  seq_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  seq_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  seq_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  seq_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  seq_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  seq_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  seq_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  seq_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  so_id_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  unit_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  unit_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  unit_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  unit_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  unit_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  unit_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  unit_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  unit_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  unit_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  unit_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  unit_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  unit_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  unit_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  unit_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  unit_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  url_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  url_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
};

export type InteractionTargetRelationship = {
  __typename?: 'InteractionTargetRelationship';
  cursor: Scalars['String']['output'];
  node: Target;
};

export type InteractionTargetTargetAggregationSelection = {
  __typename?: 'InteractionTargetTargetAggregationSelection';
  count: Scalars['Int']['output'];
  node?: Maybe<InteractionTargetTargetNodeAggregateSelection>;
};

export type InteractionTargetTargetNodeAggregateSelection = {
  __typename?: 'InteractionTargetTargetNodeAggregateSelection';
  chebi_id: StringAggregateSelection;
  description: StringAggregateSelection;
  id: IdAggregateSelection;
  length: IntAggregateSelection;
  name: StringAggregateSelection;
  secondary_struct_file: StringAggregateSelection;
  seq: StringAggregateSelection;
  so_id: StringAggregateSelection;
  unit: StringAggregateSelection;
  url: StringAggregateSelection;
};

export type InteractionWhere = {
  AND?: InputMaybe<Array<InteractionWhere>>;
  NOT?: InputMaybe<InteractionWhere>;
  OR?: InputMaybe<Array<InteractionWhere>>;
  duplexesAggregate?: InputMaybe<InteractionDuplexesAggregateInput>;
  /** Return Interactions where all of the related InteractionDuplexesConnections match this filter */
  duplexesConnection_ALL?: InputMaybe<InteractionDuplexesConnectionWhere>;
  /** Return Interactions where none of the related InteractionDuplexesConnections match this filter */
  duplexesConnection_NONE?: InputMaybe<InteractionDuplexesConnectionWhere>;
  /** Return Interactions where one of the related InteractionDuplexesConnections match this filter */
  duplexesConnection_SINGLE?: InputMaybe<InteractionDuplexesConnectionWhere>;
  /** Return Interactions where some of the related InteractionDuplexesConnections match this filter */
  duplexesConnection_SOME?: InputMaybe<InteractionDuplexesConnectionWhere>;
  /** Return Interactions where all of the related Duplexes match this filter */
  duplexes_ALL?: InputMaybe<DuplexWhere>;
  /** Return Interactions where none of the related Duplexes match this filter */
  duplexes_NONE?: InputMaybe<DuplexWhere>;
  /** Return Interactions where one of the related Duplexes match this filter */
  duplexes_SINGLE?: InputMaybe<DuplexWhere>;
  /** Return Interactions where some of the related Duplexes match this filter */
  duplexes_SOME?: InputMaybe<DuplexWhere>;
  guide?: InputMaybe<GuideWhere>;
  guideAggregate?: InputMaybe<InteractionGuideAggregateInput>;
  guideConnection?: InputMaybe<InteractionGuideConnectionWhere>;
  guideConnection_NOT?: InputMaybe<InteractionGuideConnectionWhere>;
  guide_NOT?: InputMaybe<GuideWhere>;
  id?: InputMaybe<Scalars['ID']['input']>;
  id_CONTAINS?: InputMaybe<Scalars['ID']['input']>;
  id_ENDS_WITH?: InputMaybe<Scalars['ID']['input']>;
  id_IN?: InputMaybe<Array<Scalars['ID']['input']>>;
  id_STARTS_WITH?: InputMaybe<Scalars['ID']['input']>;
  modification?: InputMaybe<ModificationWhere>;
  modificationAggregate?: InputMaybe<InteractionModificationAggregateInput>;
  modificationConnection?: InputMaybe<InteractionModificationConnectionWhere>;
  modificationConnection_NOT?: InputMaybe<InteractionModificationConnectionWhere>;
  modification_NOT?: InputMaybe<ModificationWhere>;
  target?: InputMaybe<TargetWhere>;
  targetAggregate?: InputMaybe<InteractionTargetAggregateInput>;
  targetConnection?: InputMaybe<InteractionTargetConnectionWhere>;
  targetConnection_NOT?: InputMaybe<InteractionTargetConnectionWhere>;
  target_NOT?: InputMaybe<TargetWhere>;
};

export type InteractionsConnection = {
  __typename?: 'InteractionsConnection';
  edges: Array<InteractionEdge>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int']['output'];
};

/** A group of isoform SnoRNAs */
export type Isoform = GuideGroup & {
  __typename?: 'Isoform';
  /** The guides that are part of isoform group */
  guides: Array<Guide>;
  guidesAggregate?: Maybe<IsoformGuideGuidesAggregationSelection>;
  guidesConnection: GuideGroupGuidesConnection;
  /** The isoform id */
  id: Scalars['ID']['output'];
};


/** A group of isoform SnoRNAs */
export type IsoformGuidesArgs = {
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  options?: InputMaybe<GuideOptions>;
  where?: InputMaybe<GuideWhere>;
};


/** A group of isoform SnoRNAs */
export type IsoformGuidesAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  where?: InputMaybe<GuideWhere>;
};


/** A group of isoform SnoRNAs */
export type IsoformGuidesConnectionArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  sort?: InputMaybe<Array<GuideGroupGuidesConnectionSort>>;
  where?: InputMaybe<GuideGroupGuidesConnectionWhere>;
};

export type IsoformAggregateSelection = {
  __typename?: 'IsoformAggregateSelection';
  count: Scalars['Int']['output'];
  id: IdAggregateSelection;
};

export type IsoformEdge = {
  __typename?: 'IsoformEdge';
  cursor: Scalars['String']['output'];
  node: Isoform;
};

export type IsoformGuideGuidesAggregationSelection = {
  __typename?: 'IsoformGuideGuidesAggregationSelection';
  count: Scalars['Int']['output'];
  node?: Maybe<IsoformGuideGuidesNodeAggregateSelection>;
};

export type IsoformGuideGuidesNodeAggregateSelection = {
  __typename?: 'IsoformGuideGuidesNodeAggregateSelection';
  chebi_id: StringAggregateSelection;
  description: StringAggregateSelection;
  id: IdAggregateSelection;
  length: IntAggregateSelection;
  name: StringAggregateSelection;
  seq: StringAggregateSelection;
  so_id: StringAggregateSelection;
  url: StringAggregateSelection;
};

export type IsoformGuidesAggregateInput = {
  AND?: InputMaybe<Array<IsoformGuidesAggregateInput>>;
  NOT?: InputMaybe<IsoformGuidesAggregateInput>;
  OR?: InputMaybe<Array<IsoformGuidesAggregateInput>>;
  count?: InputMaybe<Scalars['Int']['input']>;
  count_GT?: InputMaybe<Scalars['Int']['input']>;
  count_GTE?: InputMaybe<Scalars['Int']['input']>;
  count_LT?: InputMaybe<Scalars['Int']['input']>;
  count_LTE?: InputMaybe<Scalars['Int']['input']>;
  node?: InputMaybe<IsoformGuidesNodeAggregationWhereInput>;
};

export type IsoformGuidesNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<IsoformGuidesNodeAggregationWhereInput>>;
  NOT?: InputMaybe<IsoformGuidesNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<IsoformGuidesNodeAggregationWhereInput>>;
  chebi_id_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  chebi_id_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  chebi_id_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  chebi_id_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  chebi_id_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  chebi_id_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  description_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  description_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  length_AVERAGE_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  length_AVERAGE_GT?: InputMaybe<Scalars['Float']['input']>;
  length_AVERAGE_GTE?: InputMaybe<Scalars['Float']['input']>;
  length_AVERAGE_LT?: InputMaybe<Scalars['Float']['input']>;
  length_AVERAGE_LTE?: InputMaybe<Scalars['Float']['input']>;
  length_MAX_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  length_MAX_GT?: InputMaybe<Scalars['Int']['input']>;
  length_MAX_GTE?: InputMaybe<Scalars['Int']['input']>;
  length_MAX_LT?: InputMaybe<Scalars['Int']['input']>;
  length_MAX_LTE?: InputMaybe<Scalars['Int']['input']>;
  length_MIN_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  length_MIN_GT?: InputMaybe<Scalars['Int']['input']>;
  length_MIN_GTE?: InputMaybe<Scalars['Int']['input']>;
  length_MIN_LT?: InputMaybe<Scalars['Int']['input']>;
  length_MIN_LTE?: InputMaybe<Scalars['Int']['input']>;
  length_SUM_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  length_SUM_GT?: InputMaybe<Scalars['Int']['input']>;
  length_SUM_GTE?: InputMaybe<Scalars['Int']['input']>;
  length_SUM_LT?: InputMaybe<Scalars['Int']['input']>;
  length_SUM_LTE?: InputMaybe<Scalars['Int']['input']>;
  name_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  name_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  seq_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  seq_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  seq_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  seq_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  seq_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  seq_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  seq_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  seq_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  seq_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  seq_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  seq_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  seq_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  seq_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  seq_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  seq_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  so_id_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  url_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  url_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
};

export type IsoformOptions = {
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  /** Specify one or more IsoformSort objects to sort Isoforms by. The sorts will be applied in the order in which they are arranged in the array. */
  sort?: InputMaybe<Array<IsoformSort>>;
};

/** Fields to sort Isoforms by. The order in which sorts are applied is not guaranteed when specifying many fields in one IsoformSort object. */
export type IsoformSort = {
  id?: InputMaybe<SortDirection>;
};

export type IsoformWhere = {
  AND?: InputMaybe<Array<IsoformWhere>>;
  NOT?: InputMaybe<IsoformWhere>;
  OR?: InputMaybe<Array<IsoformWhere>>;
  guidesAggregate?: InputMaybe<IsoformGuidesAggregateInput>;
  /** Return Isoforms where all of the related GuideGroupGuidesConnections match this filter */
  guidesConnection_ALL?: InputMaybe<GuideGroupGuidesConnectionWhere>;
  /** Return Isoforms where none of the related GuideGroupGuidesConnections match this filter */
  guidesConnection_NONE?: InputMaybe<GuideGroupGuidesConnectionWhere>;
  /** Return Isoforms where one of the related GuideGroupGuidesConnections match this filter */
  guidesConnection_SINGLE?: InputMaybe<GuideGroupGuidesConnectionWhere>;
  /** Return Isoforms where some of the related GuideGroupGuidesConnections match this filter */
  guidesConnection_SOME?: InputMaybe<GuideGroupGuidesConnectionWhere>;
  /** Return Isoforms where all of the related Guides match this filter */
  guides_ALL?: InputMaybe<GuideWhere>;
  /** Return Isoforms where none of the related Guides match this filter */
  guides_NONE?: InputMaybe<GuideWhere>;
  /** Return Isoforms where one of the related Guides match this filter */
  guides_SINGLE?: InputMaybe<GuideWhere>;
  /** Return Isoforms where some of the related Guides match this filter */
  guides_SOME?: InputMaybe<GuideWhere>;
  id?: InputMaybe<Scalars['ID']['input']>;
  id_CONTAINS?: InputMaybe<Scalars['ID']['input']>;
  id_ENDS_WITH?: InputMaybe<Scalars['ID']['input']>;
  id_IN?: InputMaybe<Array<Scalars['ID']['input']>>;
  id_STARTS_WITH?: InputMaybe<Scalars['ID']['input']>;
};

export type IsoformsConnection = {
  __typename?: 'IsoformsConnection';
  edges: Array<IsoformEdge>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int']['output'];
};

/** A modification can be either Pseudouridylation, 2'-O-Ribose-Methylation, Acetylation or something else */
export enum ModifType {
  /** Acetylation (Ace) */
  Ace = 'Ace',
  /** 2'-O-Ribose-Methylation (Nm) */
  Nm = 'Nm',
  /** Anything else */
  Other = 'Other',
  /** Pseudouridylation (Psi) */
  Psi = 'Psi'
}

/** The modification is guided by a guide on a target RNA sequence */
export type Modification = {
  __typename?: 'Modification';
  /** Table entries associated to the modification */
  entries: Array<TableEntry>;
  entriesConnection: ModificationEntriesConnection;
  /** The list of guides that can guide the modification */
  guides: Array<Guide>;
  guidesAggregate?: Maybe<ModificationGuideGuidesAggregationSelection>;
  guidesConnection: ModificationGuidesConnection;
  /** The Modification id */
  id: Scalars['ID']['output'];
  /** Interactions that cause the modification */
  interactions: Array<Interaction>;
  interactionsAggregate?: Maybe<ModificationInteractionInteractionsAggregationSelection>;
  interactionsConnection: ModificationInteractionsConnection;
  /** The modification common name */
  name: Scalars['String']['output'];
  /** The nucleotide */
  nucleotide: Scalars['String']['output'];
  /** The position on Target sequence */
  position: Scalars['Int']['output'];
  /** The modified nucleotide */
  symbol: Scalars['String']['output'];
  /** The result of the modification with markdown syntax */
  symbol_label: Scalars['String']['output'];
  /** The RNA sequence on which the modification append */
  target: Target;
  targetAggregate?: Maybe<ModificationTargetTargetAggregationSelection>;
  targetConnection: ModificationTargetConnection;
  /** Type of modification like Pseudouridylation or 2′-O-Ribose-Methylation */
  type?: Maybe<ModifType>;
  /** Label of modification type formatted with markdown syntax */
  type_label?: Maybe<Scalars['String']['output']>;
  /** Short label of modification type formatted with markdown syntax */
  type_short_label?: Maybe<Scalars['String']['output']>;
};


/** The modification is guided by a guide on a target RNA sequence */
export type ModificationEntriesArgs = {
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  options?: InputMaybe<TableEntryOptions>;
  where?: InputMaybe<TableEntryWhere>;
};


/** The modification is guided by a guide on a target RNA sequence */
export type ModificationEntriesConnectionArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  sort?: InputMaybe<Array<ModificationEntriesConnectionSort>>;
  where?: InputMaybe<ModificationEntriesConnectionWhere>;
};


/** The modification is guided by a guide on a target RNA sequence */
export type ModificationGuidesArgs = {
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  options?: InputMaybe<GuideOptions>;
  where?: InputMaybe<GuideWhere>;
};


/** The modification is guided by a guide on a target RNA sequence */
export type ModificationGuidesAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  where?: InputMaybe<GuideWhere>;
};


/** The modification is guided by a guide on a target RNA sequence */
export type ModificationGuidesConnectionArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  sort?: InputMaybe<Array<ModificationGuidesConnectionSort>>;
  where?: InputMaybe<ModificationGuidesConnectionWhere>;
};


/** The modification is guided by a guide on a target RNA sequence */
export type ModificationInteractionsArgs = {
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  options?: InputMaybe<InteractionOptions>;
  where?: InputMaybe<InteractionWhere>;
};


/** The modification is guided by a guide on a target RNA sequence */
export type ModificationInteractionsAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  where?: InputMaybe<InteractionWhere>;
};


/** The modification is guided by a guide on a target RNA sequence */
export type ModificationInteractionsConnectionArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  sort?: InputMaybe<Array<ModificationInteractionsConnectionSort>>;
  where?: InputMaybe<ModificationInteractionsConnectionWhere>;
};


/** The modification is guided by a guide on a target RNA sequence */
export type ModificationTargetArgs = {
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  options?: InputMaybe<TargetOptions>;
  where?: InputMaybe<TargetWhere>;
};


/** The modification is guided by a guide on a target RNA sequence */
export type ModificationTargetAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  where?: InputMaybe<TargetWhere>;
};


/** The modification is guided by a guide on a target RNA sequence */
export type ModificationTargetConnectionArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  sort?: InputMaybe<Array<ModificationTargetConnectionSort>>;
  where?: InputMaybe<ModificationTargetConnectionWhere>;
};

export type ModificationAggregateSelection = {
  __typename?: 'ModificationAggregateSelection';
  count: Scalars['Int']['output'];
  id: IdAggregateSelection;
  name: StringAggregateSelection;
  nucleotide: StringAggregateSelection;
  position: IntAggregateSelection;
  symbol: StringAggregateSelection;
  symbol_label: StringAggregateSelection;
  type_label: StringAggregateSelection;
  type_short_label: StringAggregateSelection;
};

export type ModificationEdge = {
  __typename?: 'ModificationEdge';
  cursor: Scalars['String']['output'];
  node: Modification;
};

export type ModificationEntriesAggregateInput = {
  AND?: InputMaybe<Array<ModificationEntriesAggregateInput>>;
  NOT?: InputMaybe<ModificationEntriesAggregateInput>;
  OR?: InputMaybe<Array<ModificationEntriesAggregateInput>>;
  count?: InputMaybe<Scalars['Int']['input']>;
  count_GT?: InputMaybe<Scalars['Int']['input']>;
  count_GTE?: InputMaybe<Scalars['Int']['input']>;
  count_LT?: InputMaybe<Scalars['Int']['input']>;
  count_LTE?: InputMaybe<Scalars['Int']['input']>;
  node?: InputMaybe<ModificationEntriesNodeAggregationWhereInput>;
};

export type ModificationEntriesConnection = {
  __typename?: 'ModificationEntriesConnection';
  edges: Array<ModificationEntriesRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int']['output'];
};

export type ModificationEntriesConnectionSort = {
  node?: InputMaybe<TableEntrySort>;
};

export type ModificationEntriesConnectionWhere = {
  AND?: InputMaybe<Array<ModificationEntriesConnectionWhere>>;
  NOT?: InputMaybe<ModificationEntriesConnectionWhere>;
  OR?: InputMaybe<Array<ModificationEntriesConnectionWhere>>;
  node?: InputMaybe<TableEntryWhere>;
};

export type ModificationEntriesNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<ModificationEntriesNodeAggregationWhereInput>>;
  NOT?: InputMaybe<ModificationEntriesNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<ModificationEntriesNodeAggregationWhereInput>>;
};

export type ModificationEntriesRelationship = {
  __typename?: 'ModificationEntriesRelationship';
  cursor: Scalars['String']['output'];
  node: TableEntry;
};

export type ModificationGuideGuidesAggregationSelection = {
  __typename?: 'ModificationGuideGuidesAggregationSelection';
  count: Scalars['Int']['output'];
  node?: Maybe<ModificationGuideGuidesNodeAggregateSelection>;
};

export type ModificationGuideGuidesNodeAggregateSelection = {
  __typename?: 'ModificationGuideGuidesNodeAggregateSelection';
  chebi_id: StringAggregateSelection;
  description: StringAggregateSelection;
  id: IdAggregateSelection;
  length: IntAggregateSelection;
  name: StringAggregateSelection;
  seq: StringAggregateSelection;
  so_id: StringAggregateSelection;
  url: StringAggregateSelection;
};

export type ModificationGuidesAggregateInput = {
  AND?: InputMaybe<Array<ModificationGuidesAggregateInput>>;
  NOT?: InputMaybe<ModificationGuidesAggregateInput>;
  OR?: InputMaybe<Array<ModificationGuidesAggregateInput>>;
  count?: InputMaybe<Scalars['Int']['input']>;
  count_GT?: InputMaybe<Scalars['Int']['input']>;
  count_GTE?: InputMaybe<Scalars['Int']['input']>;
  count_LT?: InputMaybe<Scalars['Int']['input']>;
  count_LTE?: InputMaybe<Scalars['Int']['input']>;
  node?: InputMaybe<ModificationGuidesNodeAggregationWhereInput>;
};

export type ModificationGuidesConnection = {
  __typename?: 'ModificationGuidesConnection';
  edges: Array<ModificationGuidesRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int']['output'];
};

export type ModificationGuidesConnectionSort = {
  node?: InputMaybe<GuideSort>;
};

export type ModificationGuidesConnectionWhere = {
  AND?: InputMaybe<Array<ModificationGuidesConnectionWhere>>;
  NOT?: InputMaybe<ModificationGuidesConnectionWhere>;
  OR?: InputMaybe<Array<ModificationGuidesConnectionWhere>>;
  node?: InputMaybe<GuideWhere>;
};

export type ModificationGuidesNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<ModificationGuidesNodeAggregationWhereInput>>;
  NOT?: InputMaybe<ModificationGuidesNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<ModificationGuidesNodeAggregationWhereInput>>;
  chebi_id_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  chebi_id_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  chebi_id_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  chebi_id_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  chebi_id_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  chebi_id_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  description_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  description_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  length_AVERAGE_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  length_AVERAGE_GT?: InputMaybe<Scalars['Float']['input']>;
  length_AVERAGE_GTE?: InputMaybe<Scalars['Float']['input']>;
  length_AVERAGE_LT?: InputMaybe<Scalars['Float']['input']>;
  length_AVERAGE_LTE?: InputMaybe<Scalars['Float']['input']>;
  length_MAX_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  length_MAX_GT?: InputMaybe<Scalars['Int']['input']>;
  length_MAX_GTE?: InputMaybe<Scalars['Int']['input']>;
  length_MAX_LT?: InputMaybe<Scalars['Int']['input']>;
  length_MAX_LTE?: InputMaybe<Scalars['Int']['input']>;
  length_MIN_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  length_MIN_GT?: InputMaybe<Scalars['Int']['input']>;
  length_MIN_GTE?: InputMaybe<Scalars['Int']['input']>;
  length_MIN_LT?: InputMaybe<Scalars['Int']['input']>;
  length_MIN_LTE?: InputMaybe<Scalars['Int']['input']>;
  length_SUM_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  length_SUM_GT?: InputMaybe<Scalars['Int']['input']>;
  length_SUM_GTE?: InputMaybe<Scalars['Int']['input']>;
  length_SUM_LT?: InputMaybe<Scalars['Int']['input']>;
  length_SUM_LTE?: InputMaybe<Scalars['Int']['input']>;
  name_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  name_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  seq_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  seq_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  seq_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  seq_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  seq_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  seq_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  seq_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  seq_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  seq_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  seq_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  seq_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  seq_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  seq_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  seq_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  seq_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  so_id_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  url_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  url_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
};

export type ModificationGuidesRelationship = {
  __typename?: 'ModificationGuidesRelationship';
  cursor: Scalars['String']['output'];
  node: Guide;
};

export type ModificationInteractionInteractionsAggregationSelection = {
  __typename?: 'ModificationInteractionInteractionsAggregationSelection';
  count: Scalars['Int']['output'];
  node?: Maybe<ModificationInteractionInteractionsNodeAggregateSelection>;
};

export type ModificationInteractionInteractionsNodeAggregateSelection = {
  __typename?: 'ModificationInteractionInteractionsNodeAggregateSelection';
  id: IdAggregateSelection;
};

export type ModificationInteractionsAggregateInput = {
  AND?: InputMaybe<Array<ModificationInteractionsAggregateInput>>;
  NOT?: InputMaybe<ModificationInteractionsAggregateInput>;
  OR?: InputMaybe<Array<ModificationInteractionsAggregateInput>>;
  count?: InputMaybe<Scalars['Int']['input']>;
  count_GT?: InputMaybe<Scalars['Int']['input']>;
  count_GTE?: InputMaybe<Scalars['Int']['input']>;
  count_LT?: InputMaybe<Scalars['Int']['input']>;
  count_LTE?: InputMaybe<Scalars['Int']['input']>;
  node?: InputMaybe<ModificationInteractionsNodeAggregationWhereInput>;
};

export type ModificationInteractionsConnection = {
  __typename?: 'ModificationInteractionsConnection';
  edges: Array<ModificationInteractionsRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int']['output'];
};

export type ModificationInteractionsConnectionSort = {
  node?: InputMaybe<InteractionSort>;
};

export type ModificationInteractionsConnectionWhere = {
  AND?: InputMaybe<Array<ModificationInteractionsConnectionWhere>>;
  NOT?: InputMaybe<ModificationInteractionsConnectionWhere>;
  OR?: InputMaybe<Array<ModificationInteractionsConnectionWhere>>;
  node?: InputMaybe<InteractionWhere>;
};

export type ModificationInteractionsNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<ModificationInteractionsNodeAggregationWhereInput>>;
  NOT?: InputMaybe<ModificationInteractionsNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<ModificationInteractionsNodeAggregationWhereInput>>;
};

export type ModificationInteractionsRelationship = {
  __typename?: 'ModificationInteractionsRelationship';
  cursor: Scalars['String']['output'];
  node: Interaction;
};

export type ModificationOptions = {
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  /** Specify one or more ModificationSort objects to sort Modifications by. The sorts will be applied in the order in which they are arranged in the array. */
  sort?: InputMaybe<Array<ModificationSort>>;
};

/** Fields to sort Modifications by. The order in which sorts are applied is not guaranteed when specifying many fields in one ModificationSort object. */
export type ModificationSort = {
  id?: InputMaybe<SortDirection>;
  name?: InputMaybe<SortDirection>;
  nucleotide?: InputMaybe<SortDirection>;
  position?: InputMaybe<SortDirection>;
  symbol?: InputMaybe<SortDirection>;
  symbol_label?: InputMaybe<SortDirection>;
  type?: InputMaybe<SortDirection>;
  type_label?: InputMaybe<SortDirection>;
  type_short_label?: InputMaybe<SortDirection>;
};

export type ModificationTargetAggregateInput = {
  AND?: InputMaybe<Array<ModificationTargetAggregateInput>>;
  NOT?: InputMaybe<ModificationTargetAggregateInput>;
  OR?: InputMaybe<Array<ModificationTargetAggregateInput>>;
  count?: InputMaybe<Scalars['Int']['input']>;
  count_GT?: InputMaybe<Scalars['Int']['input']>;
  count_GTE?: InputMaybe<Scalars['Int']['input']>;
  count_LT?: InputMaybe<Scalars['Int']['input']>;
  count_LTE?: InputMaybe<Scalars['Int']['input']>;
  node?: InputMaybe<ModificationTargetNodeAggregationWhereInput>;
};

export type ModificationTargetConnection = {
  __typename?: 'ModificationTargetConnection';
  edges: Array<ModificationTargetRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int']['output'];
};

export type ModificationTargetConnectionSort = {
  node?: InputMaybe<TargetSort>;
};

export type ModificationTargetConnectionWhere = {
  AND?: InputMaybe<Array<ModificationTargetConnectionWhere>>;
  NOT?: InputMaybe<ModificationTargetConnectionWhere>;
  OR?: InputMaybe<Array<ModificationTargetConnectionWhere>>;
  node?: InputMaybe<TargetWhere>;
};

export type ModificationTargetNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<ModificationTargetNodeAggregationWhereInput>>;
  NOT?: InputMaybe<ModificationTargetNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<ModificationTargetNodeAggregationWhereInput>>;
  chebi_id_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  chebi_id_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  chebi_id_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  chebi_id_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  chebi_id_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  chebi_id_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  description_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  description_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  length_AVERAGE_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  length_AVERAGE_GT?: InputMaybe<Scalars['Float']['input']>;
  length_AVERAGE_GTE?: InputMaybe<Scalars['Float']['input']>;
  length_AVERAGE_LT?: InputMaybe<Scalars['Float']['input']>;
  length_AVERAGE_LTE?: InputMaybe<Scalars['Float']['input']>;
  length_MAX_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  length_MAX_GT?: InputMaybe<Scalars['Int']['input']>;
  length_MAX_GTE?: InputMaybe<Scalars['Int']['input']>;
  length_MAX_LT?: InputMaybe<Scalars['Int']['input']>;
  length_MAX_LTE?: InputMaybe<Scalars['Int']['input']>;
  length_MIN_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  length_MIN_GT?: InputMaybe<Scalars['Int']['input']>;
  length_MIN_GTE?: InputMaybe<Scalars['Int']['input']>;
  length_MIN_LT?: InputMaybe<Scalars['Int']['input']>;
  length_MIN_LTE?: InputMaybe<Scalars['Int']['input']>;
  length_SUM_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  length_SUM_GT?: InputMaybe<Scalars['Int']['input']>;
  length_SUM_GTE?: InputMaybe<Scalars['Int']['input']>;
  length_SUM_LT?: InputMaybe<Scalars['Int']['input']>;
  length_SUM_LTE?: InputMaybe<Scalars['Int']['input']>;
  name_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  name_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  secondary_struct_file_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  secondary_struct_file_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  secondary_struct_file_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  secondary_struct_file_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  secondary_struct_file_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  secondary_struct_file_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  secondary_struct_file_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  secondary_struct_file_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  secondary_struct_file_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  secondary_struct_file_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  secondary_struct_file_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  secondary_struct_file_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  secondary_struct_file_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  secondary_struct_file_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  secondary_struct_file_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  seq_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  seq_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  seq_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  seq_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  seq_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  seq_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  seq_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  seq_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  seq_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  seq_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  seq_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  seq_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  seq_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  seq_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  seq_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  so_id_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  unit_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  unit_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  unit_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  unit_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  unit_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  unit_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  unit_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  unit_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  unit_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  unit_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  unit_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  unit_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  unit_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  unit_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  unit_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  url_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  url_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
};

export type ModificationTargetRelationship = {
  __typename?: 'ModificationTargetRelationship';
  cursor: Scalars['String']['output'];
  node: Target;
};

export type ModificationTargetTargetAggregationSelection = {
  __typename?: 'ModificationTargetTargetAggregationSelection';
  count: Scalars['Int']['output'];
  node?: Maybe<ModificationTargetTargetNodeAggregateSelection>;
};

export type ModificationTargetTargetNodeAggregateSelection = {
  __typename?: 'ModificationTargetTargetNodeAggregateSelection';
  chebi_id: StringAggregateSelection;
  description: StringAggregateSelection;
  id: IdAggregateSelection;
  length: IntAggregateSelection;
  name: StringAggregateSelection;
  secondary_struct_file: StringAggregateSelection;
  seq: StringAggregateSelection;
  so_id: StringAggregateSelection;
  unit: StringAggregateSelection;
  url: StringAggregateSelection;
};

export type ModificationWhere = {
  AND?: InputMaybe<Array<ModificationWhere>>;
  NOT?: InputMaybe<ModificationWhere>;
  OR?: InputMaybe<Array<ModificationWhere>>;
  entriesAggregate?: InputMaybe<ModificationEntriesAggregateInput>;
  /** Return Modifications where all of the related ModificationEntriesConnections match this filter */
  entriesConnection_ALL?: InputMaybe<ModificationEntriesConnectionWhere>;
  /** Return Modifications where none of the related ModificationEntriesConnections match this filter */
  entriesConnection_NONE?: InputMaybe<ModificationEntriesConnectionWhere>;
  /** Return Modifications where one of the related ModificationEntriesConnections match this filter */
  entriesConnection_SINGLE?: InputMaybe<ModificationEntriesConnectionWhere>;
  /** Return Modifications where some of the related ModificationEntriesConnections match this filter */
  entriesConnection_SOME?: InputMaybe<ModificationEntriesConnectionWhere>;
  /** Return Modifications where all of the related TableEntries match this filter */
  entries_ALL?: InputMaybe<TableEntryWhere>;
  /** Return Modifications where none of the related TableEntries match this filter */
  entries_NONE?: InputMaybe<TableEntryWhere>;
  /** Return Modifications where one of the related TableEntries match this filter */
  entries_SINGLE?: InputMaybe<TableEntryWhere>;
  /** Return Modifications where some of the related TableEntries match this filter */
  entries_SOME?: InputMaybe<TableEntryWhere>;
  guidesAggregate?: InputMaybe<ModificationGuidesAggregateInput>;
  /** Return Modifications where all of the related ModificationGuidesConnections match this filter */
  guidesConnection_ALL?: InputMaybe<ModificationGuidesConnectionWhere>;
  /** Return Modifications where none of the related ModificationGuidesConnections match this filter */
  guidesConnection_NONE?: InputMaybe<ModificationGuidesConnectionWhere>;
  /** Return Modifications where one of the related ModificationGuidesConnections match this filter */
  guidesConnection_SINGLE?: InputMaybe<ModificationGuidesConnectionWhere>;
  /** Return Modifications where some of the related ModificationGuidesConnections match this filter */
  guidesConnection_SOME?: InputMaybe<ModificationGuidesConnectionWhere>;
  /** Return Modifications where all of the related Guides match this filter */
  guides_ALL?: InputMaybe<GuideWhere>;
  /** Return Modifications where none of the related Guides match this filter */
  guides_NONE?: InputMaybe<GuideWhere>;
  /** Return Modifications where one of the related Guides match this filter */
  guides_SINGLE?: InputMaybe<GuideWhere>;
  /** Return Modifications where some of the related Guides match this filter */
  guides_SOME?: InputMaybe<GuideWhere>;
  id?: InputMaybe<Scalars['ID']['input']>;
  id_CONTAINS?: InputMaybe<Scalars['ID']['input']>;
  id_ENDS_WITH?: InputMaybe<Scalars['ID']['input']>;
  id_IN?: InputMaybe<Array<Scalars['ID']['input']>>;
  id_STARTS_WITH?: InputMaybe<Scalars['ID']['input']>;
  interactionsAggregate?: InputMaybe<ModificationInteractionsAggregateInput>;
  /** Return Modifications where all of the related ModificationInteractionsConnections match this filter */
  interactionsConnection_ALL?: InputMaybe<ModificationInteractionsConnectionWhere>;
  /** Return Modifications where none of the related ModificationInteractionsConnections match this filter */
  interactionsConnection_NONE?: InputMaybe<ModificationInteractionsConnectionWhere>;
  /** Return Modifications where one of the related ModificationInteractionsConnections match this filter */
  interactionsConnection_SINGLE?: InputMaybe<ModificationInteractionsConnectionWhere>;
  /** Return Modifications where some of the related ModificationInteractionsConnections match this filter */
  interactionsConnection_SOME?: InputMaybe<ModificationInteractionsConnectionWhere>;
  /** Return Modifications where all of the related Interactions match this filter */
  interactions_ALL?: InputMaybe<InteractionWhere>;
  /** Return Modifications where none of the related Interactions match this filter */
  interactions_NONE?: InputMaybe<InteractionWhere>;
  /** Return Modifications where one of the related Interactions match this filter */
  interactions_SINGLE?: InputMaybe<InteractionWhere>;
  /** Return Modifications where some of the related Interactions match this filter */
  interactions_SOME?: InputMaybe<InteractionWhere>;
  name?: InputMaybe<Scalars['String']['input']>;
  name_CONTAINS?: InputMaybe<Scalars['String']['input']>;
  name_ENDS_WITH?: InputMaybe<Scalars['String']['input']>;
  name_IN?: InputMaybe<Array<Scalars['String']['input']>>;
  name_STARTS_WITH?: InputMaybe<Scalars['String']['input']>;
  nucleotide?: InputMaybe<Scalars['String']['input']>;
  nucleotide_CONTAINS?: InputMaybe<Scalars['String']['input']>;
  nucleotide_ENDS_WITH?: InputMaybe<Scalars['String']['input']>;
  nucleotide_IN?: InputMaybe<Array<Scalars['String']['input']>>;
  nucleotide_STARTS_WITH?: InputMaybe<Scalars['String']['input']>;
  position?: InputMaybe<Scalars['Int']['input']>;
  position_GT?: InputMaybe<Scalars['Int']['input']>;
  position_GTE?: InputMaybe<Scalars['Int']['input']>;
  position_IN?: InputMaybe<Array<Scalars['Int']['input']>>;
  position_LT?: InputMaybe<Scalars['Int']['input']>;
  position_LTE?: InputMaybe<Scalars['Int']['input']>;
  symbol?: InputMaybe<Scalars['String']['input']>;
  symbol_CONTAINS?: InputMaybe<Scalars['String']['input']>;
  symbol_ENDS_WITH?: InputMaybe<Scalars['String']['input']>;
  symbol_IN?: InputMaybe<Array<Scalars['String']['input']>>;
  symbol_STARTS_WITH?: InputMaybe<Scalars['String']['input']>;
  symbol_label?: InputMaybe<Scalars['String']['input']>;
  symbol_label_CONTAINS?: InputMaybe<Scalars['String']['input']>;
  symbol_label_ENDS_WITH?: InputMaybe<Scalars['String']['input']>;
  symbol_label_IN?: InputMaybe<Array<Scalars['String']['input']>>;
  symbol_label_STARTS_WITH?: InputMaybe<Scalars['String']['input']>;
  target?: InputMaybe<TargetWhere>;
  targetAggregate?: InputMaybe<ModificationTargetAggregateInput>;
  targetConnection?: InputMaybe<ModificationTargetConnectionWhere>;
  targetConnection_NOT?: InputMaybe<ModificationTargetConnectionWhere>;
  target_NOT?: InputMaybe<TargetWhere>;
  type?: InputMaybe<ModifType>;
  type_IN?: InputMaybe<Array<InputMaybe<ModifType>>>;
  type_label?: InputMaybe<Scalars['String']['input']>;
  type_label_CONTAINS?: InputMaybe<Scalars['String']['input']>;
  type_label_ENDS_WITH?: InputMaybe<Scalars['String']['input']>;
  type_label_IN?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  type_label_STARTS_WITH?: InputMaybe<Scalars['String']['input']>;
  type_short_label?: InputMaybe<Scalars['String']['input']>;
  type_short_label_CONTAINS?: InputMaybe<Scalars['String']['input']>;
  type_short_label_ENDS_WITH?: InputMaybe<Scalars['String']['input']>;
  type_short_label_IN?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  type_short_label_STARTS_WITH?: InputMaybe<Scalars['String']['input']>;
};

export type ModificationsConnection = {
  __typename?: 'ModificationsConnection';
  edges: Array<ModificationEdge>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int']['output'];
};

/** An Organism (e.g. Arabodopsis Taliana) */
export type Organism = {
  __typename?: 'Organism';
  /** Alternative names, if any */
  altname?: Maybe<Array<Scalars['String']['output']>>;
  /** Genomes associated with the organism */
  genomes: Array<Genome>;
  genomesAggregate?: Maybe<OrganismGenomeGenomesAggregationSelection>;
  genomesConnection: OrganismGenomesConnection;
  /** The NCBI Taxonomy ID of the organism (e.g. 3702) */
  id: Scalars['Int']['output'];
  /** Common name formatted in markdown */
  label: Scalars['String']['output'];
  /** Common name (e.g. Arabodopsis thaliana) */
  name: Scalars['String']['output'];
  /** Short name formatted in markdown */
  shortlabel: Scalars['String']['output'];
  /** Short name (e.g. A. thaliana) */
  shortname: Scalars['String']['output'];
  /** rows associated with organism */
  tableEntries: Array<TableEntry>;
  tableEntriesAggregate?: Maybe<OrganismTableEntryTableEntriesAggregationSelection>;
  tableEntriesConnection: OrganismTableEntriesConnection;
};


/** An Organism (e.g. Arabodopsis Taliana) */
export type OrganismGenomesArgs = {
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  options?: InputMaybe<GenomeOptions>;
  where?: InputMaybe<GenomeWhere>;
};


/** An Organism (e.g. Arabodopsis Taliana) */
export type OrganismGenomesAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  where?: InputMaybe<GenomeWhere>;
};


/** An Organism (e.g. Arabodopsis Taliana) */
export type OrganismGenomesConnectionArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  sort?: InputMaybe<Array<OrganismGenomesConnectionSort>>;
  where?: InputMaybe<OrganismGenomesConnectionWhere>;
};


/** An Organism (e.g. Arabodopsis Taliana) */
export type OrganismTableEntriesArgs = {
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  options?: InputMaybe<TableEntryOptions>;
  where?: InputMaybe<TableEntryWhere>;
};


/** An Organism (e.g. Arabodopsis Taliana) */
export type OrganismTableEntriesAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  where?: InputMaybe<TableEntryWhere>;
};


/** An Organism (e.g. Arabodopsis Taliana) */
export type OrganismTableEntriesConnectionArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  sort?: InputMaybe<Array<OrganismTableEntriesConnectionSort>>;
  where?: InputMaybe<OrganismTableEntriesConnectionWhere>;
};

export type OrganismAggregateSelection = {
  __typename?: 'OrganismAggregateSelection';
  count: Scalars['Int']['output'];
  id: IntAggregateSelection;
  label: StringAggregateSelection;
  name: StringAggregateSelection;
  shortlabel: StringAggregateSelection;
  shortname: StringAggregateSelection;
};

export type OrganismEdge = {
  __typename?: 'OrganismEdge';
  cursor: Scalars['String']['output'];
  node: Organism;
};

export type OrganismGenomeGenomesAggregationSelection = {
  __typename?: 'OrganismGenomeGenomesAggregationSelection';
  count: Scalars['Int']['output'];
  node?: Maybe<OrganismGenomeGenomesNodeAggregateSelection>;
};

export type OrganismGenomeGenomesNodeAggregateSelection = {
  __typename?: 'OrganismGenomeGenomesNodeAggregateSelection';
  description: StringAggregateSelection;
  id: IdAggregateSelection;
  so_id: StringAggregateSelection;
  source: StringAggregateSelection;
  url: StringAggregateSelection;
  version: StringAggregateSelection;
};

export type OrganismGenomesAggregateInput = {
  AND?: InputMaybe<Array<OrganismGenomesAggregateInput>>;
  NOT?: InputMaybe<OrganismGenomesAggregateInput>;
  OR?: InputMaybe<Array<OrganismGenomesAggregateInput>>;
  count?: InputMaybe<Scalars['Int']['input']>;
  count_GT?: InputMaybe<Scalars['Int']['input']>;
  count_GTE?: InputMaybe<Scalars['Int']['input']>;
  count_LT?: InputMaybe<Scalars['Int']['input']>;
  count_LTE?: InputMaybe<Scalars['Int']['input']>;
  node?: InputMaybe<OrganismGenomesNodeAggregationWhereInput>;
};

export type OrganismGenomesConnection = {
  __typename?: 'OrganismGenomesConnection';
  edges: Array<OrganismGenomesRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int']['output'];
};

export type OrganismGenomesConnectionSort = {
  node?: InputMaybe<GenomeSort>;
};

export type OrganismGenomesConnectionWhere = {
  AND?: InputMaybe<Array<OrganismGenomesConnectionWhere>>;
  NOT?: InputMaybe<OrganismGenomesConnectionWhere>;
  OR?: InputMaybe<Array<OrganismGenomesConnectionWhere>>;
  node?: InputMaybe<GenomeWhere>;
};

export type OrganismGenomesNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<OrganismGenomesNodeAggregationWhereInput>>;
  NOT?: InputMaybe<OrganismGenomesNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<OrganismGenomesNodeAggregationWhereInput>>;
  description_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  description_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  so_id_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  source_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  source_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  source_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  source_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  source_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  source_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  source_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  source_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  source_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  source_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  source_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  source_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  source_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  source_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  source_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  url_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  url_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  version_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  version_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  version_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  version_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  version_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  version_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  version_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  version_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  version_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  version_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  version_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  version_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  version_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  version_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  version_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
};

export type OrganismGenomesRelationship = {
  __typename?: 'OrganismGenomesRelationship';
  cursor: Scalars['String']['output'];
  node: Genome;
};

export type OrganismOptions = {
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  /** Specify one or more OrganismSort objects to sort Organisms by. The sorts will be applied in the order in which they are arranged in the array. */
  sort?: InputMaybe<Array<OrganismSort>>;
};

/** Fields to sort Organisms by. The order in which sorts are applied is not guaranteed when specifying many fields in one OrganismSort object. */
export type OrganismSort = {
  id?: InputMaybe<SortDirection>;
  label?: InputMaybe<SortDirection>;
  name?: InputMaybe<SortDirection>;
  shortlabel?: InputMaybe<SortDirection>;
  shortname?: InputMaybe<SortDirection>;
};

export type OrganismTableEntriesAggregateInput = {
  AND?: InputMaybe<Array<OrganismTableEntriesAggregateInput>>;
  NOT?: InputMaybe<OrganismTableEntriesAggregateInput>;
  OR?: InputMaybe<Array<OrganismTableEntriesAggregateInput>>;
  count?: InputMaybe<Scalars['Int']['input']>;
  count_GT?: InputMaybe<Scalars['Int']['input']>;
  count_GTE?: InputMaybe<Scalars['Int']['input']>;
  count_LT?: InputMaybe<Scalars['Int']['input']>;
  count_LTE?: InputMaybe<Scalars['Int']['input']>;
  node?: InputMaybe<OrganismTableEntriesNodeAggregationWhereInput>;
};

export type OrganismTableEntriesConnection = {
  __typename?: 'OrganismTableEntriesConnection';
  edges: Array<OrganismTableEntriesRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int']['output'];
};

export type OrganismTableEntriesConnectionSort = {
  node?: InputMaybe<TableEntrySort>;
};

export type OrganismTableEntriesConnectionWhere = {
  AND?: InputMaybe<Array<OrganismTableEntriesConnectionWhere>>;
  NOT?: InputMaybe<OrganismTableEntriesConnectionWhere>;
  OR?: InputMaybe<Array<OrganismTableEntriesConnectionWhere>>;
  node?: InputMaybe<TableEntryWhere>;
};

export type OrganismTableEntriesNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<OrganismTableEntriesNodeAggregationWhereInput>>;
  NOT?: InputMaybe<OrganismTableEntriesNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<OrganismTableEntriesNodeAggregationWhereInput>>;
};

export type OrganismTableEntriesRelationship = {
  __typename?: 'OrganismTableEntriesRelationship';
  cursor: Scalars['String']['output'];
  node: TableEntry;
};

export type OrganismTableEntryTableEntriesAggregationSelection = {
  __typename?: 'OrganismTableEntryTableEntriesAggregationSelection';
  count: Scalars['Int']['output'];
  node?: Maybe<OrganismTableEntryTableEntriesNodeAggregateSelection>;
};

export type OrganismTableEntryTableEntriesNodeAggregateSelection = {
  __typename?: 'OrganismTableEntryTableEntriesNodeAggregateSelection';
  id: IdAggregateSelection;
};

export type OrganismWhere = {
  AND?: InputMaybe<Array<OrganismWhere>>;
  NOT?: InputMaybe<OrganismWhere>;
  OR?: InputMaybe<Array<OrganismWhere>>;
  altname?: InputMaybe<Array<Scalars['String']['input']>>;
  altname_INCLUDES?: InputMaybe<Scalars['String']['input']>;
  genomesAggregate?: InputMaybe<OrganismGenomesAggregateInput>;
  /** Return Organisms where all of the related OrganismGenomesConnections match this filter */
  genomesConnection_ALL?: InputMaybe<OrganismGenomesConnectionWhere>;
  /** Return Organisms where none of the related OrganismGenomesConnections match this filter */
  genomesConnection_NONE?: InputMaybe<OrganismGenomesConnectionWhere>;
  /** Return Organisms where one of the related OrganismGenomesConnections match this filter */
  genomesConnection_SINGLE?: InputMaybe<OrganismGenomesConnectionWhere>;
  /** Return Organisms where some of the related OrganismGenomesConnections match this filter */
  genomesConnection_SOME?: InputMaybe<OrganismGenomesConnectionWhere>;
  /** Return Organisms where all of the related Genomes match this filter */
  genomes_ALL?: InputMaybe<GenomeWhere>;
  /** Return Organisms where none of the related Genomes match this filter */
  genomes_NONE?: InputMaybe<GenomeWhere>;
  /** Return Organisms where one of the related Genomes match this filter */
  genomes_SINGLE?: InputMaybe<GenomeWhere>;
  /** Return Organisms where some of the related Genomes match this filter */
  genomes_SOME?: InputMaybe<GenomeWhere>;
  id?: InputMaybe<Scalars['Int']['input']>;
  id_GT?: InputMaybe<Scalars['Int']['input']>;
  id_GTE?: InputMaybe<Scalars['Int']['input']>;
  id_IN?: InputMaybe<Array<Scalars['Int']['input']>>;
  id_LT?: InputMaybe<Scalars['Int']['input']>;
  id_LTE?: InputMaybe<Scalars['Int']['input']>;
  label?: InputMaybe<Scalars['String']['input']>;
  label_CONTAINS?: InputMaybe<Scalars['String']['input']>;
  label_ENDS_WITH?: InputMaybe<Scalars['String']['input']>;
  label_IN?: InputMaybe<Array<Scalars['String']['input']>>;
  label_STARTS_WITH?: InputMaybe<Scalars['String']['input']>;
  name?: InputMaybe<Scalars['String']['input']>;
  name_CONTAINS?: InputMaybe<Scalars['String']['input']>;
  name_ENDS_WITH?: InputMaybe<Scalars['String']['input']>;
  name_IN?: InputMaybe<Array<Scalars['String']['input']>>;
  name_STARTS_WITH?: InputMaybe<Scalars['String']['input']>;
  shortlabel?: InputMaybe<Scalars['String']['input']>;
  shortlabel_CONTAINS?: InputMaybe<Scalars['String']['input']>;
  shortlabel_ENDS_WITH?: InputMaybe<Scalars['String']['input']>;
  shortlabel_IN?: InputMaybe<Array<Scalars['String']['input']>>;
  shortlabel_STARTS_WITH?: InputMaybe<Scalars['String']['input']>;
  shortname?: InputMaybe<Scalars['String']['input']>;
  shortname_CONTAINS?: InputMaybe<Scalars['String']['input']>;
  shortname_ENDS_WITH?: InputMaybe<Scalars['String']['input']>;
  shortname_IN?: InputMaybe<Array<Scalars['String']['input']>>;
  shortname_STARTS_WITH?: InputMaybe<Scalars['String']['input']>;
  tableEntriesAggregate?: InputMaybe<OrganismTableEntriesAggregateInput>;
  /** Return Organisms where all of the related OrganismTableEntriesConnections match this filter */
  tableEntriesConnection_ALL?: InputMaybe<OrganismTableEntriesConnectionWhere>;
  /** Return Organisms where none of the related OrganismTableEntriesConnections match this filter */
  tableEntriesConnection_NONE?: InputMaybe<OrganismTableEntriesConnectionWhere>;
  /** Return Organisms where one of the related OrganismTableEntriesConnections match this filter */
  tableEntriesConnection_SINGLE?: InputMaybe<OrganismTableEntriesConnectionWhere>;
  /** Return Organisms where some of the related OrganismTableEntriesConnections match this filter */
  tableEntriesConnection_SOME?: InputMaybe<OrganismTableEntriesConnectionWhere>;
  /** Return Organisms where all of the related TableEntries match this filter */
  tableEntries_ALL?: InputMaybe<TableEntryWhere>;
  /** Return Organisms where none of the related TableEntries match this filter */
  tableEntries_NONE?: InputMaybe<TableEntryWhere>;
  /** Return Organisms where one of the related TableEntries match this filter */
  tableEntries_SINGLE?: InputMaybe<TableEntryWhere>;
  /** Return Organisms where some of the related TableEntries match this filter */
  tableEntries_SOME?: InputMaybe<TableEntryWhere>;
};

export type OrganismsConnection = {
  __typename?: 'OrganismsConnection';
  edges: Array<OrganismEdge>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int']['output'];
};

/** Pagination information (Relay) */
export type PageInfo = {
  __typename?: 'PageInfo';
  endCursor?: Maybe<Scalars['String']['output']>;
  hasNextPage: Scalars['Boolean']['output'];
  hasPreviousPage: Scalars['Boolean']['output'];
  startCursor?: Maybe<Scalars['String']['output']>;
};

export type Query = {
  __typename?: 'Query';
  alignGroups: Array<AlignGroup>;
  alignGroupsAggregate: AlignGroupAggregateSelection;
  alignGroupsConnection: AlignGroupsConnection;
  alignedModifications: Array<AlignedModification>;
  alignedModificationsAggregate: AlignedModificationAggregateSelection;
  alignedModificationsConnection: AlignedModificationsConnection;
  alignments: Array<Alignment>;
  alignmentsAggregate: AlignmentAggregateSelection;
  alignmentsConnection: AlignmentsConnection;
  chromosomes: Array<Chromosome>;
  chromosomesAggregate: ChromosomeAggregateSelection;
  chromosomesConnection: ChromosomesConnection;
  clusters: Array<Cluster>;
  clustersAggregate: ClusterAggregateSelection;
  clustersConnection: ClustersConnection;
  documents: Array<Document>;
  documentsAggregate: DocumentAggregateSelection;
  documentsConnection: DocumentsConnection;
  duplexes: Array<Duplex>;
  duplexesAggregate: DuplexAggregateSelection;
  duplexesConnection: DuplexesConnection;
  genericSequences: Array<GenericSequence>;
  genericSequencesAggregate: GenericSequenceAggregateSelection;
  genericSequencesConnection: GenericSequencesConnection;
  genomes: Array<Genome>;
  genomesAggregate: GenomeAggregateSelection;
  genomesConnection: GenomesConnection;
  getTracks: Array<Track>;
  guideGroups: Array<GuideGroup>;
  guideGroupsAggregate: GuideGroupAggregateSelection;
  guideGroupsConnection: GuideGroupsConnection;
  guides: Array<Guide>;
  guidesAggregate: GuideAggregateSelection;
  guidesConnection: GuidesConnection;
  interactions: Array<Interaction>;
  interactionsAggregate: InteractionAggregateSelection;
  interactionsConnection: InteractionsConnection;
  isoforms: Array<Isoform>;
  isoformsAggregate: IsoformAggregateSelection;
  isoformsConnection: IsoformsConnection;
  modifications: Array<Modification>;
  modificationsAggregate: ModificationAggregateSelection;
  modificationsConnection: ModificationsConnection;
  organisms: Array<Organism>;
  organismsAggregate: OrganismAggregateSelection;
  organismsConnection: OrganismsConnection;
  sequenceGroups: Array<SequenceGroup>;
  sequenceGroupsAggregate: SequenceGroupAggregateSelection;
  sequenceGroupsConnection: SequenceGroupsConnection;
  sequenceTracks: Array<SequenceTrack>;
  sequenceTracksAggregate: SequenceTrackAggregateSelection;
  sequenceTracksConnection: SequenceTracksConnection;
  sequences: Array<Sequence>;
  sequencesAggregate: SequenceAggregateSelection;
  sequencesConnection: SequencesConnection;
  structureTracks: Array<StructureTrack>;
  structureTracksAggregate: StructureTrackAggregateSelection;
  structureTracksConnection: StructureTracksConnection;
  tableEntries: Array<TableEntry>;
  tableEntriesAggregate: TableEntryAggregateSelection;
  tableEntriesConnection: TableEntriesConnection;
  targets: Array<Target>;
  targetsAggregate: TargetAggregateSelection;
  targetsConnection: TargetsConnection;
  tracks: Array<Track>;
  tracksAggregate: TrackAggregateSelection;
  tracksConnection: TracksConnection;
};


export type QueryAlignGroupsArgs = {
  options?: InputMaybe<AlignGroupOptions>;
  where?: InputMaybe<AlignGroupWhere>;
};


export type QueryAlignGroupsAggregateArgs = {
  where?: InputMaybe<AlignGroupWhere>;
};


export type QueryAlignGroupsConnectionArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  sort?: InputMaybe<Array<InputMaybe<AlignGroupSort>>>;
  where?: InputMaybe<AlignGroupWhere>;
};


export type QueryAlignedModificationsArgs = {
  options?: InputMaybe<AlignedModificationOptions>;
  where?: InputMaybe<AlignedModificationWhere>;
};


export type QueryAlignedModificationsAggregateArgs = {
  where?: InputMaybe<AlignedModificationWhere>;
};


export type QueryAlignedModificationsConnectionArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  sort?: InputMaybe<Array<InputMaybe<AlignedModificationSort>>>;
  where?: InputMaybe<AlignedModificationWhere>;
};


export type QueryAlignmentsArgs = {
  options?: InputMaybe<AlignmentOptions>;
  where?: InputMaybe<AlignmentWhere>;
};


export type QueryAlignmentsAggregateArgs = {
  where?: InputMaybe<AlignmentWhere>;
};


export type QueryAlignmentsConnectionArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  sort?: InputMaybe<Array<InputMaybe<AlignmentSort>>>;
  where?: InputMaybe<AlignmentWhere>;
};


export type QueryChromosomesArgs = {
  options?: InputMaybe<ChromosomeOptions>;
  where?: InputMaybe<ChromosomeWhere>;
};


export type QueryChromosomesAggregateArgs = {
  where?: InputMaybe<ChromosomeWhere>;
};


export type QueryChromosomesConnectionArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  sort?: InputMaybe<Array<InputMaybe<ChromosomeSort>>>;
  where?: InputMaybe<ChromosomeWhere>;
};


export type QueryClustersArgs = {
  options?: InputMaybe<ClusterOptions>;
  where?: InputMaybe<ClusterWhere>;
};


export type QueryClustersAggregateArgs = {
  where?: InputMaybe<ClusterWhere>;
};


export type QueryClustersConnectionArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  sort?: InputMaybe<Array<InputMaybe<ClusterSort>>>;
  where?: InputMaybe<ClusterWhere>;
};


export type QueryDocumentsArgs = {
  options?: InputMaybe<DocumentOptions>;
  where?: InputMaybe<DocumentWhere>;
};


export type QueryDocumentsAggregateArgs = {
  where?: InputMaybe<DocumentWhere>;
};


export type QueryDocumentsConnectionArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  sort?: InputMaybe<Array<InputMaybe<DocumentSort>>>;
  where?: InputMaybe<DocumentWhere>;
};


export type QueryDuplexesArgs = {
  options?: InputMaybe<DuplexOptions>;
  where?: InputMaybe<DuplexWhere>;
};


export type QueryDuplexesAggregateArgs = {
  where?: InputMaybe<DuplexWhere>;
};


export type QueryDuplexesConnectionArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  sort?: InputMaybe<Array<InputMaybe<DuplexSort>>>;
  where?: InputMaybe<DuplexWhere>;
};


export type QueryGenericSequencesArgs = {
  options?: InputMaybe<GenericSequenceOptions>;
  where?: InputMaybe<GenericSequenceWhere>;
};


export type QueryGenericSequencesAggregateArgs = {
  where?: InputMaybe<GenericSequenceWhere>;
};


export type QueryGenericSequencesConnectionArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  sort?: InputMaybe<Array<InputMaybe<GenericSequenceSort>>>;
  where?: InputMaybe<GenericSequenceWhere>;
};


export type QueryGenomesArgs = {
  options?: InputMaybe<GenomeOptions>;
  where?: InputMaybe<GenomeWhere>;
};


export type QueryGenomesAggregateArgs = {
  where?: InputMaybe<GenomeWhere>;
};


export type QueryGenomesConnectionArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  sort?: InputMaybe<Array<InputMaybe<GenomeSort>>>;
  where?: InputMaybe<GenomeWhere>;
};


export type QueryGetTracksArgs = {
  tracks?: InputMaybe<Array<InputMaybe<TrackInput>>>;
};


export type QueryGuideGroupsArgs = {
  options?: InputMaybe<GuideGroupOptions>;
  where?: InputMaybe<GuideGroupWhere>;
};


export type QueryGuideGroupsAggregateArgs = {
  where?: InputMaybe<GuideGroupWhere>;
};


export type QueryGuideGroupsConnectionArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  sort?: InputMaybe<Array<InputMaybe<GuideGroupSort>>>;
  where?: InputMaybe<GuideGroupWhere>;
};


export type QueryGuidesArgs = {
  options?: InputMaybe<GuideOptions>;
  where?: InputMaybe<GuideWhere>;
};


export type QueryGuidesAggregateArgs = {
  where?: InputMaybe<GuideWhere>;
};


export type QueryGuidesConnectionArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  sort?: InputMaybe<Array<InputMaybe<GuideSort>>>;
  where?: InputMaybe<GuideWhere>;
};


export type QueryInteractionsArgs = {
  options?: InputMaybe<InteractionOptions>;
  where?: InputMaybe<InteractionWhere>;
};


export type QueryInteractionsAggregateArgs = {
  where?: InputMaybe<InteractionWhere>;
};


export type QueryInteractionsConnectionArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  sort?: InputMaybe<Array<InputMaybe<InteractionSort>>>;
  where?: InputMaybe<InteractionWhere>;
};


export type QueryIsoformsArgs = {
  options?: InputMaybe<IsoformOptions>;
  where?: InputMaybe<IsoformWhere>;
};


export type QueryIsoformsAggregateArgs = {
  where?: InputMaybe<IsoformWhere>;
};


export type QueryIsoformsConnectionArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  sort?: InputMaybe<Array<InputMaybe<IsoformSort>>>;
  where?: InputMaybe<IsoformWhere>;
};


export type QueryModificationsArgs = {
  options?: InputMaybe<ModificationOptions>;
  where?: InputMaybe<ModificationWhere>;
};


export type QueryModificationsAggregateArgs = {
  where?: InputMaybe<ModificationWhere>;
};


export type QueryModificationsConnectionArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  sort?: InputMaybe<Array<InputMaybe<ModificationSort>>>;
  where?: InputMaybe<ModificationWhere>;
};


export type QueryOrganismsArgs = {
  options?: InputMaybe<OrganismOptions>;
  where?: InputMaybe<OrganismWhere>;
};


export type QueryOrganismsAggregateArgs = {
  where?: InputMaybe<OrganismWhere>;
};


export type QueryOrganismsConnectionArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  sort?: InputMaybe<Array<InputMaybe<OrganismSort>>>;
  where?: InputMaybe<OrganismWhere>;
};


export type QuerySequenceGroupsArgs = {
  options?: InputMaybe<SequenceGroupOptions>;
  where?: InputMaybe<SequenceGroupWhere>;
};


export type QuerySequenceGroupsAggregateArgs = {
  where?: InputMaybe<SequenceGroupWhere>;
};


export type QuerySequenceGroupsConnectionArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  sort?: InputMaybe<Array<InputMaybe<SequenceGroupSort>>>;
  where?: InputMaybe<SequenceGroupWhere>;
};


export type QuerySequenceTracksArgs = {
  options?: InputMaybe<SequenceTrackOptions>;
  where?: InputMaybe<SequenceTrackWhere>;
};


export type QuerySequenceTracksAggregateArgs = {
  where?: InputMaybe<SequenceTrackWhere>;
};


export type QuerySequenceTracksConnectionArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  sort?: InputMaybe<Array<InputMaybe<SequenceTrackSort>>>;
  where?: InputMaybe<SequenceTrackWhere>;
};


export type QuerySequencesArgs = {
  options?: InputMaybe<SequenceOptions>;
  where?: InputMaybe<SequenceWhere>;
};


export type QuerySequencesAggregateArgs = {
  where?: InputMaybe<SequenceWhere>;
};


export type QuerySequencesConnectionArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  sort?: InputMaybe<Array<InputMaybe<SequenceSort>>>;
  where?: InputMaybe<SequenceWhere>;
};


export type QueryStructureTracksArgs = {
  options?: InputMaybe<StructureTrackOptions>;
  where?: InputMaybe<StructureTrackWhere>;
};


export type QueryStructureTracksAggregateArgs = {
  where?: InputMaybe<StructureTrackWhere>;
};


export type QueryStructureTracksConnectionArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  sort?: InputMaybe<Array<InputMaybe<StructureTrackSort>>>;
  where?: InputMaybe<StructureTrackWhere>;
};


export type QueryTableEntriesArgs = {
  options?: InputMaybe<TableEntryOptions>;
  where?: InputMaybe<TableEntryWhere>;
};


export type QueryTableEntriesAggregateArgs = {
  where?: InputMaybe<TableEntryWhere>;
};


export type QueryTableEntriesConnectionArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  sort?: InputMaybe<Array<InputMaybe<TableEntrySort>>>;
  where?: InputMaybe<TableEntryWhere>;
};


export type QueryTargetsArgs = {
  options?: InputMaybe<TargetOptions>;
  where?: InputMaybe<TargetWhere>;
};


export type QueryTargetsAggregateArgs = {
  where?: InputMaybe<TargetWhere>;
};


export type QueryTargetsConnectionArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  sort?: InputMaybe<Array<InputMaybe<TargetSort>>>;
  where?: InputMaybe<TargetWhere>;
};


export type QueryTracksArgs = {
  options?: InputMaybe<TrackOptions>;
  where?: InputMaybe<TrackWhere>;
};


export type QueryTracksAggregateArgs = {
  where?: InputMaybe<TrackWhere>;
};


export type QueryTracksConnectionArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  sort?: InputMaybe<Array<InputMaybe<TrackSort>>>;
  where?: InputMaybe<TrackWhere>;
};

export type Sequence = {
  /** ALternative sequence names */
  altnames?: Maybe<Array<Scalars['String']['output']>>;
  /** The annotation of the sequence if any */
  annotation?: Maybe<AnnotationType>;
  /** The Chemical Entities of Biological Interest Ontology (ChEBI) id. Default value is CHEBI_33696: nucleic acid */
  chebi_id?: Maybe<Scalars['String']['output']>;
  /** The class of the sequence (rRNA, snRNA, snoRNA, etc.) */
  class: SequenceClass;
  /** The sequence description (i.e. the comment part in fasta file) */
  description?: Maybe<Scalars['String']['output']>;
  /** The features associated with the sequence */
  features: Array<Sequence>;
  featuresConnection: SequenceFeaturesConnection;
  /** The reference genome the sequence comes from */
  genome?: Maybe<Genome>;
  genomeConnection: SequenceGenomeConnection;
  /** The type of GraphQL object */
  graphql_type: GraphQlType;
  /** The sequence ID (i.e. id part in fasta file) */
  id: Scalars['ID']['output'];
  /** The length of sequence in number of nucleotide */
  length: Scalars['Int']['output'];
  /** Common sequence name */
  name?: Maybe<Scalars['String']['output']>;
  /** The sequence itself (url can be used instead for huge sequence). Sequence is alway from 5' to 3' */
  seq?: Maybe<Scalars['String']['output']>;
  /** The Sequence Ontology (so) id */
  so_id?: Maybe<Scalars['String']['output']>;
  /** The type of the sequence (DNA or RNA) */
  type: SequenceType;
  /** The url to get the file containning the sequence */
  url?: Maybe<Scalars['String']['output']>;
};


export type SequenceFeaturesArgs = {
  options?: InputMaybe<SequenceOptions>;
  where?: InputMaybe<SequenceWhere>;
};


export type SequenceFeaturesConnectionArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  sort?: InputMaybe<Array<SequenceFeaturesConnectionSort>>;
  where?: InputMaybe<SequenceFeaturesConnectionWhere>;
};


export type SequenceGenomeArgs = {
  options?: InputMaybe<GenomeOptions>;
  where?: InputMaybe<GenomeWhere>;
};


export type SequenceGenomeConnectionArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  sort?: InputMaybe<Array<SequenceGenomeConnectionSort>>;
  where?: InputMaybe<SequenceGenomeConnectionWhere>;
};

export type SequenceAggregateSelection = {
  __typename?: 'SequenceAggregateSelection';
  chebi_id: StringAggregateSelection;
  count: Scalars['Int']['output'];
  description: StringAggregateSelection;
  id: IdAggregateSelection;
  length: IntAggregateSelection;
  name: StringAggregateSelection;
  seq: StringAggregateSelection;
  so_id: StringAggregateSelection;
  url: StringAggregateSelection;
};

export enum SequenceClass {
  /** Annotation */
  Annotation = 'Annotation',
  /** Chromosome */
  Chromosome = 'Chromosome',
  /** Duplex Fragment */
  DuplexFragment = 'DuplexFragment',
  /** Anything else */
  Other = 'Other',
  /** Ribosomic RNA */
  RRna = 'rRNA',
  /** Small nuclear RNA */
  SnRna = 'snRNA',
  /** Small nucleolar */
  SnoRna = 'snoRNA'
}

export type SequenceEdge = {
  __typename?: 'SequenceEdge';
  cursor: Scalars['String']['output'];
  node: Sequence;
};

export type SequenceFeaturesAggregateInput = {
  AND?: InputMaybe<Array<SequenceFeaturesAggregateInput>>;
  NOT?: InputMaybe<SequenceFeaturesAggregateInput>;
  OR?: InputMaybe<Array<SequenceFeaturesAggregateInput>>;
  count?: InputMaybe<Scalars['Int']['input']>;
  count_GT?: InputMaybe<Scalars['Int']['input']>;
  count_GTE?: InputMaybe<Scalars['Int']['input']>;
  count_LT?: InputMaybe<Scalars['Int']['input']>;
  count_LTE?: InputMaybe<Scalars['Int']['input']>;
  edge?: InputMaybe<SequenceFeaturesEdgeAggregationWhereInput>;
  node?: InputMaybe<SequenceFeaturesNodeAggregationWhereInput>;
};

export type SequenceFeaturesConnection = {
  __typename?: 'SequenceFeaturesConnection';
  edges: Array<SequenceFeaturesRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int']['output'];
};

export type SequenceFeaturesConnectionSort = {
  edge?: InputMaybe<SequenceFeaturesEdgeSort>;
  node?: InputMaybe<SequenceSort>;
};

export type SequenceFeaturesConnectionWhere = {
  AND?: InputMaybe<Array<SequenceFeaturesConnectionWhere>>;
  NOT?: InputMaybe<SequenceFeaturesConnectionWhere>;
  OR?: InputMaybe<Array<SequenceFeaturesConnectionWhere>>;
  edge?: InputMaybe<SequenceFeaturesEdgeWhere>;
  node?: InputMaybe<SequenceWhere>;
};

export type SequenceFeaturesEdgeAggregationWhereInput = {
  /**
   * Relationship properties when source node is of type:
   * * Chromosome
   * * Guide
   * * Target
   * * GenericSequence
   */
  HasFeature?: InputMaybe<HasFeatureAggregationWhereInput>;
};

export type SequenceFeaturesEdgeSort = {
  /**
   * Relationship properties when source node is of type:
   * * Chromosome
   * * Guide
   * * Target
   * * GenericSequence
   */
  HasFeature?: InputMaybe<HasFeatureSort>;
};

export type SequenceFeaturesEdgeWhere = {
  /**
   * Relationship properties when source node is of type:
   * * Chromosome
   * * Guide
   * * Target
   * * GenericSequence
   */
  HasFeature?: InputMaybe<HasFeatureWhere>;
};

export type SequenceFeaturesNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<SequenceFeaturesNodeAggregationWhereInput>>;
  NOT?: InputMaybe<SequenceFeaturesNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<SequenceFeaturesNodeAggregationWhereInput>>;
  chebi_id_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  chebi_id_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  chebi_id_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  chebi_id_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  chebi_id_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  chebi_id_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  description_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  description_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  length_AVERAGE_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  length_AVERAGE_GT?: InputMaybe<Scalars['Float']['input']>;
  length_AVERAGE_GTE?: InputMaybe<Scalars['Float']['input']>;
  length_AVERAGE_LT?: InputMaybe<Scalars['Float']['input']>;
  length_AVERAGE_LTE?: InputMaybe<Scalars['Float']['input']>;
  length_MAX_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  length_MAX_GT?: InputMaybe<Scalars['Int']['input']>;
  length_MAX_GTE?: InputMaybe<Scalars['Int']['input']>;
  length_MAX_LT?: InputMaybe<Scalars['Int']['input']>;
  length_MAX_LTE?: InputMaybe<Scalars['Int']['input']>;
  length_MIN_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  length_MIN_GT?: InputMaybe<Scalars['Int']['input']>;
  length_MIN_GTE?: InputMaybe<Scalars['Int']['input']>;
  length_MIN_LT?: InputMaybe<Scalars['Int']['input']>;
  length_MIN_LTE?: InputMaybe<Scalars['Int']['input']>;
  length_SUM_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  length_SUM_GT?: InputMaybe<Scalars['Int']['input']>;
  length_SUM_GTE?: InputMaybe<Scalars['Int']['input']>;
  length_SUM_LT?: InputMaybe<Scalars['Int']['input']>;
  length_SUM_LTE?: InputMaybe<Scalars['Int']['input']>;
  name_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  name_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  seq_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  seq_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  seq_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  seq_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  seq_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  seq_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  seq_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  seq_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  seq_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  seq_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  seq_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  seq_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  seq_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  seq_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  seq_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  so_id_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  url_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  url_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
};

export type SequenceFeaturesRelationship = {
  __typename?: 'SequenceFeaturesRelationship';
  cursor: Scalars['String']['output'];
  node: Sequence;
  properties: SequenceFeaturesRelationshipProperties;
};

export type SequenceFeaturesRelationshipProperties = HasFeature;

export type SequenceGenomeAggregateInput = {
  AND?: InputMaybe<Array<SequenceGenomeAggregateInput>>;
  NOT?: InputMaybe<SequenceGenomeAggregateInput>;
  OR?: InputMaybe<Array<SequenceGenomeAggregateInput>>;
  count?: InputMaybe<Scalars['Int']['input']>;
  count_GT?: InputMaybe<Scalars['Int']['input']>;
  count_GTE?: InputMaybe<Scalars['Int']['input']>;
  count_LT?: InputMaybe<Scalars['Int']['input']>;
  count_LTE?: InputMaybe<Scalars['Int']['input']>;
  node?: InputMaybe<SequenceGenomeNodeAggregationWhereInput>;
};

export type SequenceGenomeConnection = {
  __typename?: 'SequenceGenomeConnection';
  edges: Array<SequenceGenomeRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int']['output'];
};

export type SequenceGenomeConnectionSort = {
  node?: InputMaybe<GenomeSort>;
};

export type SequenceGenomeConnectionWhere = {
  AND?: InputMaybe<Array<SequenceGenomeConnectionWhere>>;
  NOT?: InputMaybe<SequenceGenomeConnectionWhere>;
  OR?: InputMaybe<Array<SequenceGenomeConnectionWhere>>;
  node?: InputMaybe<GenomeWhere>;
};

export type SequenceGenomeNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<SequenceGenomeNodeAggregationWhereInput>>;
  NOT?: InputMaybe<SequenceGenomeNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<SequenceGenomeNodeAggregationWhereInput>>;
  description_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  description_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  so_id_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  source_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  source_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  source_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  source_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  source_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  source_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  source_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  source_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  source_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  source_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  source_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  source_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  source_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  source_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  source_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  url_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  url_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  version_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  version_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  version_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  version_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  version_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  version_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  version_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  version_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  version_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  version_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  version_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  version_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  version_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  version_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  version_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
};

export type SequenceGenomeRelationship = {
  __typename?: 'SequenceGenomeRelationship';
  cursor: Scalars['String']['output'];
  node: Genome;
};

export type SequenceGroup = {
  /** The description of the group (Optional) */
  description?: Maybe<Scalars['String']['output']>;
  /** The group id */
  id: Scalars['ID']['output'];
  /** The name of the group (Optional) */
  name?: Maybe<Scalars['String']['output']>;
  /** The sequences that are part of group */
  sequences: Array<Sequence>;
};

export type SequenceGroupAggregateSelection = {
  __typename?: 'SequenceGroupAggregateSelection';
  count: Scalars['Int']['output'];
  description: StringAggregateSelection;
  id: IdAggregateSelection;
  name: StringAggregateSelection;
};

export type SequenceGroupEdge = {
  __typename?: 'SequenceGroupEdge';
  cursor: Scalars['String']['output'];
  node: SequenceGroup;
};

export enum SequenceGroupImplementation {
  AlignGroup = 'AlignGroup'
}

export type SequenceGroupOptions = {
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  /** Specify one or more SequenceGroupSort objects to sort SequenceGroups by. The sorts will be applied in the order in which they are arranged in the array. */
  sort?: InputMaybe<Array<InputMaybe<SequenceGroupSort>>>;
};

/** Fields to sort SequenceGroups by. The order in which sorts are applied is not guaranteed when specifying many fields in one SequenceGroupSort object. */
export type SequenceGroupSort = {
  description?: InputMaybe<SortDirection>;
  id?: InputMaybe<SortDirection>;
  name?: InputMaybe<SortDirection>;
};

export type SequenceGroupWhere = {
  AND?: InputMaybe<Array<SequenceGroupWhere>>;
  NOT?: InputMaybe<SequenceGroupWhere>;
  OR?: InputMaybe<Array<SequenceGroupWhere>>;
  description?: InputMaybe<Scalars['String']['input']>;
  description_CONTAINS?: InputMaybe<Scalars['String']['input']>;
  description_ENDS_WITH?: InputMaybe<Scalars['String']['input']>;
  description_IN?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  description_STARTS_WITH?: InputMaybe<Scalars['String']['input']>;
  id?: InputMaybe<Scalars['ID']['input']>;
  id_CONTAINS?: InputMaybe<Scalars['ID']['input']>;
  id_ENDS_WITH?: InputMaybe<Scalars['ID']['input']>;
  id_IN?: InputMaybe<Array<Scalars['ID']['input']>>;
  id_STARTS_WITH?: InputMaybe<Scalars['ID']['input']>;
  name?: InputMaybe<Scalars['String']['input']>;
  name_CONTAINS?: InputMaybe<Scalars['String']['input']>;
  name_ENDS_WITH?: InputMaybe<Scalars['String']['input']>;
  name_IN?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  name_STARTS_WITH?: InputMaybe<Scalars['String']['input']>;
  typename_IN?: InputMaybe<Array<SequenceGroupImplementation>>;
};

export type SequenceGroupsConnection = {
  __typename?: 'SequenceGroupsConnection';
  edges: Array<SequenceGroupEdge>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int']['output'];
};

export enum SequenceImplementation {
  Chromosome = 'Chromosome',
  GenericSequence = 'GenericSequence',
  Guide = 'Guide',
  Target = 'Target'
}

export type SequenceOptions = {
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  /** Specify one or more SequenceSort objects to sort Sequences by. The sorts will be applied in the order in which they are arranged in the array. */
  sort?: InputMaybe<Array<InputMaybe<SequenceSort>>>;
};

/** Fields to sort Sequences by. The order in which sorts are applied is not guaranteed when specifying many fields in one SequenceSort object. */
export type SequenceSort = {
  annotation?: InputMaybe<SortDirection>;
  chebi_id?: InputMaybe<SortDirection>;
  class?: InputMaybe<SortDirection>;
  description?: InputMaybe<SortDirection>;
  graphql_type?: InputMaybe<SortDirection>;
  id?: InputMaybe<SortDirection>;
  length?: InputMaybe<SortDirection>;
  name?: InputMaybe<SortDirection>;
  seq?: InputMaybe<SortDirection>;
  so_id?: InputMaybe<SortDirection>;
  type?: InputMaybe<SortDirection>;
  url?: InputMaybe<SortDirection>;
};

/** Tracks encoding the RNA sequence adjusted to alignment by including some aditional '-' and correcting the modification position */
export type SequenceTrack = Track & {
  __typename?: 'SequenceTrack';
  /** The modifications which position are adjusted to current Alignement */
  aligned_modifications: Array<AlignedModification>;
  /** The sequence */
  content: Scalars['String']['output'];
  /** The track ID */
  id: Scalars['ID']['output'];
  /** Other tracks associated/coupled/grouped with the current track */
  links: Array<Track>;
  /** The track name */
  name?: Maybe<Scalars['String']['output']>;
  /** The sequence the track is associated to */
  ref: Sequence;
};

export type SequenceTrackAggregateSelection = {
  __typename?: 'SequenceTrackAggregateSelection';
  content: StringAggregateSelection;
  count: Scalars['Int']['output'];
  id: IdAggregateSelection;
  name: StringAggregateSelection;
};

export type SequenceTrackEdge = {
  __typename?: 'SequenceTrackEdge';
  cursor: Scalars['String']['output'];
  node: SequenceTrack;
};

export type SequenceTrackOptions = {
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  /** Specify one or more SequenceTrackSort objects to sort SequenceTracks by. The sorts will be applied in the order in which they are arranged in the array. */
  sort?: InputMaybe<Array<SequenceTrackSort>>;
};

/** Fields to sort SequenceTracks by. The order in which sorts are applied is not guaranteed when specifying many fields in one SequenceTrackSort object. */
export type SequenceTrackSort = {
  content?: InputMaybe<SortDirection>;
  id?: InputMaybe<SortDirection>;
  name?: InputMaybe<SortDirection>;
};

export type SequenceTrackWhere = {
  AND?: InputMaybe<Array<SequenceTrackWhere>>;
  NOT?: InputMaybe<SequenceTrackWhere>;
  OR?: InputMaybe<Array<SequenceTrackWhere>>;
  content?: InputMaybe<Scalars['String']['input']>;
  content_CONTAINS?: InputMaybe<Scalars['String']['input']>;
  content_ENDS_WITH?: InputMaybe<Scalars['String']['input']>;
  content_IN?: InputMaybe<Array<Scalars['String']['input']>>;
  content_STARTS_WITH?: InputMaybe<Scalars['String']['input']>;
  id?: InputMaybe<Scalars['ID']['input']>;
  id_CONTAINS?: InputMaybe<Scalars['ID']['input']>;
  id_ENDS_WITH?: InputMaybe<Scalars['ID']['input']>;
  id_IN?: InputMaybe<Array<Scalars['ID']['input']>>;
  id_STARTS_WITH?: InputMaybe<Scalars['ID']['input']>;
  name?: InputMaybe<Scalars['String']['input']>;
  name_CONTAINS?: InputMaybe<Scalars['String']['input']>;
  name_ENDS_WITH?: InputMaybe<Scalars['String']['input']>;
  name_IN?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  name_STARTS_WITH?: InputMaybe<Scalars['String']['input']>;
};

export type SequenceTracksConnection = {
  __typename?: 'SequenceTracksConnection';
  edges: Array<SequenceTrackEdge>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int']['output'];
};

/** A sequence can be either DNA or RNA */
export enum SequenceType {
  /** DNA */
  Dna = 'DNA',
  /** Anything else */
  Other = 'Other',
  /** Peptide */
  Pep = 'PEP',
  /** RNA */
  Rna = 'RNA'
}

export type SequenceWhere = {
  AND?: InputMaybe<Array<SequenceWhere>>;
  NOT?: InputMaybe<SequenceWhere>;
  OR?: InputMaybe<Array<SequenceWhere>>;
  altnames?: InputMaybe<Array<Scalars['String']['input']>>;
  altnames_INCLUDES?: InputMaybe<Scalars['String']['input']>;
  annotation?: InputMaybe<AnnotationType>;
  annotation_IN?: InputMaybe<Array<InputMaybe<AnnotationType>>>;
  chebi_id?: InputMaybe<Scalars['String']['input']>;
  chebi_id_CONTAINS?: InputMaybe<Scalars['String']['input']>;
  chebi_id_ENDS_WITH?: InputMaybe<Scalars['String']['input']>;
  chebi_id_IN?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  chebi_id_STARTS_WITH?: InputMaybe<Scalars['String']['input']>;
  class?: InputMaybe<SequenceClass>;
  class_IN?: InputMaybe<Array<SequenceClass>>;
  description?: InputMaybe<Scalars['String']['input']>;
  description_CONTAINS?: InputMaybe<Scalars['String']['input']>;
  description_ENDS_WITH?: InputMaybe<Scalars['String']['input']>;
  description_IN?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  description_STARTS_WITH?: InputMaybe<Scalars['String']['input']>;
  featuresAggregate?: InputMaybe<SequenceFeaturesAggregateInput>;
  /** Return Sequences where all of the related SequenceFeaturesConnections match this filter */
  featuresConnection_ALL?: InputMaybe<SequenceFeaturesConnectionWhere>;
  /** Return Sequences where none of the related SequenceFeaturesConnections match this filter */
  featuresConnection_NONE?: InputMaybe<SequenceFeaturesConnectionWhere>;
  /** Return Sequences where one of the related SequenceFeaturesConnections match this filter */
  featuresConnection_SINGLE?: InputMaybe<SequenceFeaturesConnectionWhere>;
  /** Return Sequences where some of the related SequenceFeaturesConnections match this filter */
  featuresConnection_SOME?: InputMaybe<SequenceFeaturesConnectionWhere>;
  /** Return Sequences where all of the related Sequences match this filter */
  features_ALL?: InputMaybe<SequenceWhere>;
  /** Return Sequences where none of the related Sequences match this filter */
  features_NONE?: InputMaybe<SequenceWhere>;
  /** Return Sequences where one of the related Sequences match this filter */
  features_SINGLE?: InputMaybe<SequenceWhere>;
  /** Return Sequences where some of the related Sequences match this filter */
  features_SOME?: InputMaybe<SequenceWhere>;
  genome?: InputMaybe<GenomeWhere>;
  genomeAggregate?: InputMaybe<SequenceGenomeAggregateInput>;
  genomeConnection?: InputMaybe<SequenceGenomeConnectionWhere>;
  genomeConnection_NOT?: InputMaybe<SequenceGenomeConnectionWhere>;
  genome_NOT?: InputMaybe<GenomeWhere>;
  graphql_type?: InputMaybe<GraphQlType>;
  graphql_type_IN?: InputMaybe<Array<GraphQlType>>;
  id?: InputMaybe<Scalars['ID']['input']>;
  id_CONTAINS?: InputMaybe<Scalars['ID']['input']>;
  id_ENDS_WITH?: InputMaybe<Scalars['ID']['input']>;
  id_IN?: InputMaybe<Array<Scalars['ID']['input']>>;
  id_STARTS_WITH?: InputMaybe<Scalars['ID']['input']>;
  length?: InputMaybe<Scalars['Int']['input']>;
  length_GT?: InputMaybe<Scalars['Int']['input']>;
  length_GTE?: InputMaybe<Scalars['Int']['input']>;
  length_IN?: InputMaybe<Array<Scalars['Int']['input']>>;
  length_LT?: InputMaybe<Scalars['Int']['input']>;
  length_LTE?: InputMaybe<Scalars['Int']['input']>;
  name?: InputMaybe<Scalars['String']['input']>;
  name_CONTAINS?: InputMaybe<Scalars['String']['input']>;
  name_ENDS_WITH?: InputMaybe<Scalars['String']['input']>;
  name_IN?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  name_STARTS_WITH?: InputMaybe<Scalars['String']['input']>;
  seq?: InputMaybe<Scalars['String']['input']>;
  seq_CONTAINS?: InputMaybe<Scalars['String']['input']>;
  seq_ENDS_WITH?: InputMaybe<Scalars['String']['input']>;
  seq_IN?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  seq_STARTS_WITH?: InputMaybe<Scalars['String']['input']>;
  so_id?: InputMaybe<Scalars['String']['input']>;
  so_id_CONTAINS?: InputMaybe<Scalars['String']['input']>;
  so_id_ENDS_WITH?: InputMaybe<Scalars['String']['input']>;
  so_id_IN?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  so_id_STARTS_WITH?: InputMaybe<Scalars['String']['input']>;
  type?: InputMaybe<SequenceType>;
  type_IN?: InputMaybe<Array<SequenceType>>;
  typename_IN?: InputMaybe<Array<SequenceImplementation>>;
  url?: InputMaybe<Scalars['String']['input']>;
  url_CONTAINS?: InputMaybe<Scalars['String']['input']>;
  url_ENDS_WITH?: InputMaybe<Scalars['String']['input']>;
  url_IN?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  url_STARTS_WITH?: InputMaybe<Scalars['String']['input']>;
};

export type SequencesConnection = {
  __typename?: 'SequencesConnection';
  edges: Array<SequenceEdge>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int']['output'];
};

/** An enum for sorting in either ascending or descending order. */
export enum SortDirection {
  /** Sort by field values in ascending order. */
  Asc = 'ASC',
  /** Sort by field values in descending order. */
  Desc = 'DESC'
}

/** Strand type as in GFF file */
export enum Strand {
  /** - */
  Negative = 'NEGATIVE',
  /** . */
  NotStranded = 'NOT_STRANDED',
  /** + */
  Positive = 'POSITIVE',
  /** ? */
  Unknown = 'UNKNOWN'
}

export type StringAggregateSelection = {
  __typename?: 'StringAggregateSelection';
  longest?: Maybe<Scalars['String']['output']>;
  shortest?: Maybe<Scalars['String']['output']>;
};

/** Tracks encoding the structure of the RNA sequence with parenthesis and dots chars */
export type StructureTrack = Track & {
  __typename?: 'StructureTrack';
  /** The sequence */
  content: Scalars['String']['output'];
  /** The track ID */
  id: Scalars['ID']['output'];
  /** Other tracks associated with the current track */
  links: Array<Track>;
  /** The track name */
  name?: Maybe<Scalars['String']['output']>;
  /** The sequence the track is associated to */
  ref: Sequence;
};

export type StructureTrackAggregateSelection = {
  __typename?: 'StructureTrackAggregateSelection';
  content: StringAggregateSelection;
  count: Scalars['Int']['output'];
  id: IdAggregateSelection;
  name: StringAggregateSelection;
};

export type StructureTrackEdge = {
  __typename?: 'StructureTrackEdge';
  cursor: Scalars['String']['output'];
  node: StructureTrack;
};

export type StructureTrackOptions = {
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  /** Specify one or more StructureTrackSort objects to sort StructureTracks by. The sorts will be applied in the order in which they are arranged in the array. */
  sort?: InputMaybe<Array<StructureTrackSort>>;
};

/** Fields to sort StructureTracks by. The order in which sorts are applied is not guaranteed when specifying many fields in one StructureTrackSort object. */
export type StructureTrackSort = {
  content?: InputMaybe<SortDirection>;
  id?: InputMaybe<SortDirection>;
  name?: InputMaybe<SortDirection>;
};

export type StructureTrackWhere = {
  AND?: InputMaybe<Array<StructureTrackWhere>>;
  NOT?: InputMaybe<StructureTrackWhere>;
  OR?: InputMaybe<Array<StructureTrackWhere>>;
  content?: InputMaybe<Scalars['String']['input']>;
  content_CONTAINS?: InputMaybe<Scalars['String']['input']>;
  content_ENDS_WITH?: InputMaybe<Scalars['String']['input']>;
  content_IN?: InputMaybe<Array<Scalars['String']['input']>>;
  content_STARTS_WITH?: InputMaybe<Scalars['String']['input']>;
  id?: InputMaybe<Scalars['ID']['input']>;
  id_CONTAINS?: InputMaybe<Scalars['ID']['input']>;
  id_ENDS_WITH?: InputMaybe<Scalars['ID']['input']>;
  id_IN?: InputMaybe<Array<Scalars['ID']['input']>>;
  id_STARTS_WITH?: InputMaybe<Scalars['ID']['input']>;
  name?: InputMaybe<Scalars['String']['input']>;
  name_CONTAINS?: InputMaybe<Scalars['String']['input']>;
  name_ENDS_WITH?: InputMaybe<Scalars['String']['input']>;
  name_IN?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  name_STARTS_WITH?: InputMaybe<Scalars['String']['input']>;
};

export type StructureTracksConnection = {
  __typename?: 'StructureTracksConnection';
  edges: Array<StructureTrackEdge>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int']['output'];
};

export type TableEntriesConnection = {
  __typename?: 'TableEntriesConnection';
  edges: Array<TableEntryEdge>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int']['output'];
};

/** A table entry (or row) that associate, a guide with a modification (and its respective target). */
export type TableEntry = {
  __typename?: 'TableEntry';
  /** The guide that guides the modification */
  guide?: Maybe<Guide>;
  guideAggregate?: Maybe<TableEntryGuideGuideAggregationSelection>;
  guideConnection: TableEntryGuideConnection;
  /** The Modification id */
  id: Scalars['ID']['output'];
  /** The modification */
  modification?: Maybe<Modification>;
  modificationAggregate?: Maybe<TableEntryModificationModificationAggregationSelection>;
  modificationConnection: TableEntryModificationConnection;
  /** The organism in which the guide and modifcation exists */
  organism: Organism;
  organismAggregate?: Maybe<TableEntryOrganismOrganismAggregationSelection>;
  organismConnection: TableEntryOrganismConnection;
  /** The target modified by the guide */
  target?: Maybe<Target>;
  targetAggregate?: Maybe<TableEntryTargetTargetAggregationSelection>;
  targetConnection: TableEntryTargetConnection;
};


/** A table entry (or row) that associate, a guide with a modification (and its respective target). */
export type TableEntryGuideArgs = {
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  options?: InputMaybe<GuideOptions>;
  where?: InputMaybe<GuideWhere>;
};


/** A table entry (or row) that associate, a guide with a modification (and its respective target). */
export type TableEntryGuideAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  where?: InputMaybe<GuideWhere>;
};


/** A table entry (or row) that associate, a guide with a modification (and its respective target). */
export type TableEntryGuideConnectionArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  sort?: InputMaybe<Array<TableEntryGuideConnectionSort>>;
  where?: InputMaybe<TableEntryGuideConnectionWhere>;
};


/** A table entry (or row) that associate, a guide with a modification (and its respective target). */
export type TableEntryModificationArgs = {
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  options?: InputMaybe<ModificationOptions>;
  where?: InputMaybe<ModificationWhere>;
};


/** A table entry (or row) that associate, a guide with a modification (and its respective target). */
export type TableEntryModificationAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  where?: InputMaybe<ModificationWhere>;
};


/** A table entry (or row) that associate, a guide with a modification (and its respective target). */
export type TableEntryModificationConnectionArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  sort?: InputMaybe<Array<TableEntryModificationConnectionSort>>;
  where?: InputMaybe<TableEntryModificationConnectionWhere>;
};


/** A table entry (or row) that associate, a guide with a modification (and its respective target). */
export type TableEntryOrganismArgs = {
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  options?: InputMaybe<OrganismOptions>;
  where?: InputMaybe<OrganismWhere>;
};


/** A table entry (or row) that associate, a guide with a modification (and its respective target). */
export type TableEntryOrganismAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  where?: InputMaybe<OrganismWhere>;
};


/** A table entry (or row) that associate, a guide with a modification (and its respective target). */
export type TableEntryOrganismConnectionArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  sort?: InputMaybe<Array<TableEntryOrganismConnectionSort>>;
  where?: InputMaybe<TableEntryOrganismConnectionWhere>;
};


/** A table entry (or row) that associate, a guide with a modification (and its respective target). */
export type TableEntryTargetArgs = {
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  options?: InputMaybe<TargetOptions>;
  where?: InputMaybe<TargetWhere>;
};


/** A table entry (or row) that associate, a guide with a modification (and its respective target). */
export type TableEntryTargetAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  where?: InputMaybe<TargetWhere>;
};


/** A table entry (or row) that associate, a guide with a modification (and its respective target). */
export type TableEntryTargetConnectionArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  sort?: InputMaybe<Array<TableEntryTargetConnectionSort>>;
  where?: InputMaybe<TableEntryTargetConnectionWhere>;
};

export type TableEntryAggregateSelection = {
  __typename?: 'TableEntryAggregateSelection';
  count: Scalars['Int']['output'];
  id: IdAggregateSelection;
};

export type TableEntryEdge = {
  __typename?: 'TableEntryEdge';
  cursor: Scalars['String']['output'];
  node: TableEntry;
};

export type TableEntryGuideAggregateInput = {
  AND?: InputMaybe<Array<TableEntryGuideAggregateInput>>;
  NOT?: InputMaybe<TableEntryGuideAggregateInput>;
  OR?: InputMaybe<Array<TableEntryGuideAggregateInput>>;
  count?: InputMaybe<Scalars['Int']['input']>;
  count_GT?: InputMaybe<Scalars['Int']['input']>;
  count_GTE?: InputMaybe<Scalars['Int']['input']>;
  count_LT?: InputMaybe<Scalars['Int']['input']>;
  count_LTE?: InputMaybe<Scalars['Int']['input']>;
  node?: InputMaybe<TableEntryGuideNodeAggregationWhereInput>;
};

export type TableEntryGuideConnection = {
  __typename?: 'TableEntryGuideConnection';
  edges: Array<TableEntryGuideRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int']['output'];
};

export type TableEntryGuideConnectionSort = {
  node?: InputMaybe<GuideSort>;
};

export type TableEntryGuideConnectionWhere = {
  AND?: InputMaybe<Array<TableEntryGuideConnectionWhere>>;
  NOT?: InputMaybe<TableEntryGuideConnectionWhere>;
  OR?: InputMaybe<Array<TableEntryGuideConnectionWhere>>;
  node?: InputMaybe<GuideWhere>;
};

export type TableEntryGuideGuideAggregationSelection = {
  __typename?: 'TableEntryGuideGuideAggregationSelection';
  count: Scalars['Int']['output'];
  node?: Maybe<TableEntryGuideGuideNodeAggregateSelection>;
};

export type TableEntryGuideGuideNodeAggregateSelection = {
  __typename?: 'TableEntryGuideGuideNodeAggregateSelection';
  chebi_id: StringAggregateSelection;
  description: StringAggregateSelection;
  id: IdAggregateSelection;
  length: IntAggregateSelection;
  name: StringAggregateSelection;
  seq: StringAggregateSelection;
  so_id: StringAggregateSelection;
  url: StringAggregateSelection;
};

export type TableEntryGuideNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<TableEntryGuideNodeAggregationWhereInput>>;
  NOT?: InputMaybe<TableEntryGuideNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<TableEntryGuideNodeAggregationWhereInput>>;
  chebi_id_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  chebi_id_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  chebi_id_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  chebi_id_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  chebi_id_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  chebi_id_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  description_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  description_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  length_AVERAGE_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  length_AVERAGE_GT?: InputMaybe<Scalars['Float']['input']>;
  length_AVERAGE_GTE?: InputMaybe<Scalars['Float']['input']>;
  length_AVERAGE_LT?: InputMaybe<Scalars['Float']['input']>;
  length_AVERAGE_LTE?: InputMaybe<Scalars['Float']['input']>;
  length_MAX_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  length_MAX_GT?: InputMaybe<Scalars['Int']['input']>;
  length_MAX_GTE?: InputMaybe<Scalars['Int']['input']>;
  length_MAX_LT?: InputMaybe<Scalars['Int']['input']>;
  length_MAX_LTE?: InputMaybe<Scalars['Int']['input']>;
  length_MIN_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  length_MIN_GT?: InputMaybe<Scalars['Int']['input']>;
  length_MIN_GTE?: InputMaybe<Scalars['Int']['input']>;
  length_MIN_LT?: InputMaybe<Scalars['Int']['input']>;
  length_MIN_LTE?: InputMaybe<Scalars['Int']['input']>;
  length_SUM_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  length_SUM_GT?: InputMaybe<Scalars['Int']['input']>;
  length_SUM_GTE?: InputMaybe<Scalars['Int']['input']>;
  length_SUM_LT?: InputMaybe<Scalars['Int']['input']>;
  length_SUM_LTE?: InputMaybe<Scalars['Int']['input']>;
  name_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  name_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  seq_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  seq_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  seq_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  seq_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  seq_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  seq_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  seq_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  seq_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  seq_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  seq_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  seq_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  seq_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  seq_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  seq_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  seq_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  so_id_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  url_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  url_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
};

export type TableEntryGuideRelationship = {
  __typename?: 'TableEntryGuideRelationship';
  cursor: Scalars['String']['output'];
  node: Guide;
};

export type TableEntryModificationAggregateInput = {
  AND?: InputMaybe<Array<TableEntryModificationAggregateInput>>;
  NOT?: InputMaybe<TableEntryModificationAggregateInput>;
  OR?: InputMaybe<Array<TableEntryModificationAggregateInput>>;
  count?: InputMaybe<Scalars['Int']['input']>;
  count_GT?: InputMaybe<Scalars['Int']['input']>;
  count_GTE?: InputMaybe<Scalars['Int']['input']>;
  count_LT?: InputMaybe<Scalars['Int']['input']>;
  count_LTE?: InputMaybe<Scalars['Int']['input']>;
  node?: InputMaybe<TableEntryModificationNodeAggregationWhereInput>;
};

export type TableEntryModificationConnection = {
  __typename?: 'TableEntryModificationConnection';
  edges: Array<TableEntryModificationRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int']['output'];
};

export type TableEntryModificationConnectionSort = {
  node?: InputMaybe<ModificationSort>;
};

export type TableEntryModificationConnectionWhere = {
  AND?: InputMaybe<Array<TableEntryModificationConnectionWhere>>;
  NOT?: InputMaybe<TableEntryModificationConnectionWhere>;
  OR?: InputMaybe<Array<TableEntryModificationConnectionWhere>>;
  node?: InputMaybe<ModificationWhere>;
};

export type TableEntryModificationModificationAggregationSelection = {
  __typename?: 'TableEntryModificationModificationAggregationSelection';
  count: Scalars['Int']['output'];
  node?: Maybe<TableEntryModificationModificationNodeAggregateSelection>;
};

export type TableEntryModificationModificationNodeAggregateSelection = {
  __typename?: 'TableEntryModificationModificationNodeAggregateSelection';
  id: IdAggregateSelection;
  name: StringAggregateSelection;
  nucleotide: StringAggregateSelection;
  position: IntAggregateSelection;
  symbol: StringAggregateSelection;
  symbol_label: StringAggregateSelection;
  type_label: StringAggregateSelection;
  type_short_label: StringAggregateSelection;
};

export type TableEntryModificationNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<TableEntryModificationNodeAggregationWhereInput>>;
  NOT?: InputMaybe<TableEntryModificationNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<TableEntryModificationNodeAggregationWhereInput>>;
  name_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  name_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  nucleotide_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  nucleotide_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  nucleotide_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  nucleotide_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  nucleotide_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  nucleotide_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  nucleotide_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  nucleotide_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  nucleotide_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  nucleotide_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  nucleotide_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  nucleotide_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  nucleotide_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  nucleotide_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  nucleotide_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  position_AVERAGE_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  position_AVERAGE_GT?: InputMaybe<Scalars['Float']['input']>;
  position_AVERAGE_GTE?: InputMaybe<Scalars['Float']['input']>;
  position_AVERAGE_LT?: InputMaybe<Scalars['Float']['input']>;
  position_AVERAGE_LTE?: InputMaybe<Scalars['Float']['input']>;
  position_MAX_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  position_MAX_GT?: InputMaybe<Scalars['Int']['input']>;
  position_MAX_GTE?: InputMaybe<Scalars['Int']['input']>;
  position_MAX_LT?: InputMaybe<Scalars['Int']['input']>;
  position_MAX_LTE?: InputMaybe<Scalars['Int']['input']>;
  position_MIN_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  position_MIN_GT?: InputMaybe<Scalars['Int']['input']>;
  position_MIN_GTE?: InputMaybe<Scalars['Int']['input']>;
  position_MIN_LT?: InputMaybe<Scalars['Int']['input']>;
  position_MIN_LTE?: InputMaybe<Scalars['Int']['input']>;
  position_SUM_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  position_SUM_GT?: InputMaybe<Scalars['Int']['input']>;
  position_SUM_GTE?: InputMaybe<Scalars['Int']['input']>;
  position_SUM_LT?: InputMaybe<Scalars['Int']['input']>;
  position_SUM_LTE?: InputMaybe<Scalars['Int']['input']>;
  symbol_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  symbol_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  symbol_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  symbol_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  symbol_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  symbol_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  symbol_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  symbol_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  symbol_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  symbol_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  symbol_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  symbol_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  symbol_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  symbol_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  symbol_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  symbol_label_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  symbol_label_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  symbol_label_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  symbol_label_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  symbol_label_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  symbol_label_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  symbol_label_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  symbol_label_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  symbol_label_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  symbol_label_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  symbol_label_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  symbol_label_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  symbol_label_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  symbol_label_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  symbol_label_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  type_label_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  type_label_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  type_label_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  type_label_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  type_label_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  type_label_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  type_label_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  type_label_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  type_label_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  type_label_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  type_label_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  type_label_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  type_label_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  type_label_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  type_label_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  type_short_label_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  type_short_label_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  type_short_label_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  type_short_label_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  type_short_label_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  type_short_label_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  type_short_label_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  type_short_label_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  type_short_label_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  type_short_label_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  type_short_label_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  type_short_label_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  type_short_label_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  type_short_label_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  type_short_label_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
};

export type TableEntryModificationRelationship = {
  __typename?: 'TableEntryModificationRelationship';
  cursor: Scalars['String']['output'];
  node: Modification;
};

export type TableEntryOptions = {
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  /** Specify one or more TableEntrySort objects to sort TableEntries by. The sorts will be applied in the order in which they are arranged in the array. */
  sort?: InputMaybe<Array<TableEntrySort>>;
};

export type TableEntryOrganismAggregateInput = {
  AND?: InputMaybe<Array<TableEntryOrganismAggregateInput>>;
  NOT?: InputMaybe<TableEntryOrganismAggregateInput>;
  OR?: InputMaybe<Array<TableEntryOrganismAggregateInput>>;
  count?: InputMaybe<Scalars['Int']['input']>;
  count_GT?: InputMaybe<Scalars['Int']['input']>;
  count_GTE?: InputMaybe<Scalars['Int']['input']>;
  count_LT?: InputMaybe<Scalars['Int']['input']>;
  count_LTE?: InputMaybe<Scalars['Int']['input']>;
  node?: InputMaybe<TableEntryOrganismNodeAggregationWhereInput>;
};

export type TableEntryOrganismConnection = {
  __typename?: 'TableEntryOrganismConnection';
  edges: Array<TableEntryOrganismRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int']['output'];
};

export type TableEntryOrganismConnectionSort = {
  node?: InputMaybe<OrganismSort>;
};

export type TableEntryOrganismConnectionWhere = {
  AND?: InputMaybe<Array<TableEntryOrganismConnectionWhere>>;
  NOT?: InputMaybe<TableEntryOrganismConnectionWhere>;
  OR?: InputMaybe<Array<TableEntryOrganismConnectionWhere>>;
  node?: InputMaybe<OrganismWhere>;
};

export type TableEntryOrganismNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<TableEntryOrganismNodeAggregationWhereInput>>;
  NOT?: InputMaybe<TableEntryOrganismNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<TableEntryOrganismNodeAggregationWhereInput>>;
  id_AVERAGE_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  id_AVERAGE_GT?: InputMaybe<Scalars['Float']['input']>;
  id_AVERAGE_GTE?: InputMaybe<Scalars['Float']['input']>;
  id_AVERAGE_LT?: InputMaybe<Scalars['Float']['input']>;
  id_AVERAGE_LTE?: InputMaybe<Scalars['Float']['input']>;
  id_MAX_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  id_MAX_GT?: InputMaybe<Scalars['Int']['input']>;
  id_MAX_GTE?: InputMaybe<Scalars['Int']['input']>;
  id_MAX_LT?: InputMaybe<Scalars['Int']['input']>;
  id_MAX_LTE?: InputMaybe<Scalars['Int']['input']>;
  id_MIN_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  id_MIN_GT?: InputMaybe<Scalars['Int']['input']>;
  id_MIN_GTE?: InputMaybe<Scalars['Int']['input']>;
  id_MIN_LT?: InputMaybe<Scalars['Int']['input']>;
  id_MIN_LTE?: InputMaybe<Scalars['Int']['input']>;
  id_SUM_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  id_SUM_GT?: InputMaybe<Scalars['Int']['input']>;
  id_SUM_GTE?: InputMaybe<Scalars['Int']['input']>;
  id_SUM_LT?: InputMaybe<Scalars['Int']['input']>;
  id_SUM_LTE?: InputMaybe<Scalars['Int']['input']>;
  label_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  label_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  label_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  label_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  label_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  label_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  label_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  label_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  label_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  label_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  label_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  label_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  label_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  label_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  label_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  name_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  name_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  shortlabel_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  shortlabel_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  shortlabel_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  shortlabel_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  shortlabel_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  shortlabel_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  shortlabel_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  shortlabel_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  shortlabel_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  shortlabel_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  shortlabel_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  shortlabel_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  shortlabel_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  shortlabel_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  shortlabel_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  shortname_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  shortname_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  shortname_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  shortname_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  shortname_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  shortname_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  shortname_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  shortname_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  shortname_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  shortname_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  shortname_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  shortname_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  shortname_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  shortname_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  shortname_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
};

export type TableEntryOrganismOrganismAggregationSelection = {
  __typename?: 'TableEntryOrganismOrganismAggregationSelection';
  count: Scalars['Int']['output'];
  node?: Maybe<TableEntryOrganismOrganismNodeAggregateSelection>;
};

export type TableEntryOrganismOrganismNodeAggregateSelection = {
  __typename?: 'TableEntryOrganismOrganismNodeAggregateSelection';
  id: IntAggregateSelection;
  label: StringAggregateSelection;
  name: StringAggregateSelection;
  shortlabel: StringAggregateSelection;
  shortname: StringAggregateSelection;
};

export type TableEntryOrganismRelationship = {
  __typename?: 'TableEntryOrganismRelationship';
  cursor: Scalars['String']['output'];
  node: Organism;
};

/** Fields to sort TableEntries by. The order in which sorts are applied is not guaranteed when specifying many fields in one TableEntrySort object. */
export type TableEntrySort = {
  id?: InputMaybe<SortDirection>;
};

export type TableEntryTargetAggregateInput = {
  AND?: InputMaybe<Array<TableEntryTargetAggregateInput>>;
  NOT?: InputMaybe<TableEntryTargetAggregateInput>;
  OR?: InputMaybe<Array<TableEntryTargetAggregateInput>>;
  count?: InputMaybe<Scalars['Int']['input']>;
  count_GT?: InputMaybe<Scalars['Int']['input']>;
  count_GTE?: InputMaybe<Scalars['Int']['input']>;
  count_LT?: InputMaybe<Scalars['Int']['input']>;
  count_LTE?: InputMaybe<Scalars['Int']['input']>;
  node?: InputMaybe<TableEntryTargetNodeAggregationWhereInput>;
};

export type TableEntryTargetConnection = {
  __typename?: 'TableEntryTargetConnection';
  edges: Array<TableEntryTargetRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int']['output'];
};

export type TableEntryTargetConnectionSort = {
  node?: InputMaybe<TargetSort>;
};

export type TableEntryTargetConnectionWhere = {
  AND?: InputMaybe<Array<TableEntryTargetConnectionWhere>>;
  NOT?: InputMaybe<TableEntryTargetConnectionWhere>;
  OR?: InputMaybe<Array<TableEntryTargetConnectionWhere>>;
  node?: InputMaybe<TargetWhere>;
};

export type TableEntryTargetNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<TableEntryTargetNodeAggregationWhereInput>>;
  NOT?: InputMaybe<TableEntryTargetNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<TableEntryTargetNodeAggregationWhereInput>>;
  chebi_id_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  chebi_id_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  chebi_id_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  chebi_id_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  chebi_id_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  chebi_id_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  description_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  description_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  length_AVERAGE_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  length_AVERAGE_GT?: InputMaybe<Scalars['Float']['input']>;
  length_AVERAGE_GTE?: InputMaybe<Scalars['Float']['input']>;
  length_AVERAGE_LT?: InputMaybe<Scalars['Float']['input']>;
  length_AVERAGE_LTE?: InputMaybe<Scalars['Float']['input']>;
  length_MAX_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  length_MAX_GT?: InputMaybe<Scalars['Int']['input']>;
  length_MAX_GTE?: InputMaybe<Scalars['Int']['input']>;
  length_MAX_LT?: InputMaybe<Scalars['Int']['input']>;
  length_MAX_LTE?: InputMaybe<Scalars['Int']['input']>;
  length_MIN_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  length_MIN_GT?: InputMaybe<Scalars['Int']['input']>;
  length_MIN_GTE?: InputMaybe<Scalars['Int']['input']>;
  length_MIN_LT?: InputMaybe<Scalars['Int']['input']>;
  length_MIN_LTE?: InputMaybe<Scalars['Int']['input']>;
  length_SUM_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  length_SUM_GT?: InputMaybe<Scalars['Int']['input']>;
  length_SUM_GTE?: InputMaybe<Scalars['Int']['input']>;
  length_SUM_LT?: InputMaybe<Scalars['Int']['input']>;
  length_SUM_LTE?: InputMaybe<Scalars['Int']['input']>;
  name_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  name_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  secondary_struct_file_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  secondary_struct_file_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  secondary_struct_file_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  secondary_struct_file_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  secondary_struct_file_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  secondary_struct_file_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  secondary_struct_file_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  secondary_struct_file_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  secondary_struct_file_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  secondary_struct_file_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  secondary_struct_file_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  secondary_struct_file_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  secondary_struct_file_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  secondary_struct_file_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  secondary_struct_file_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  seq_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  seq_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  seq_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  seq_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  seq_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  seq_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  seq_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  seq_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  seq_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  seq_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  seq_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  seq_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  seq_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  seq_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  seq_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  so_id_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  unit_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  unit_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  unit_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  unit_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  unit_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  unit_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  unit_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  unit_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  unit_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  unit_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  unit_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  unit_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  unit_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  unit_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  unit_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  url_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  url_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
};

export type TableEntryTargetRelationship = {
  __typename?: 'TableEntryTargetRelationship';
  cursor: Scalars['String']['output'];
  node: Target;
};

export type TableEntryTargetTargetAggregationSelection = {
  __typename?: 'TableEntryTargetTargetAggregationSelection';
  count: Scalars['Int']['output'];
  node?: Maybe<TableEntryTargetTargetNodeAggregateSelection>;
};

export type TableEntryTargetTargetNodeAggregateSelection = {
  __typename?: 'TableEntryTargetTargetNodeAggregateSelection';
  chebi_id: StringAggregateSelection;
  description: StringAggregateSelection;
  id: IdAggregateSelection;
  length: IntAggregateSelection;
  name: StringAggregateSelection;
  secondary_struct_file: StringAggregateSelection;
  seq: StringAggregateSelection;
  so_id: StringAggregateSelection;
  unit: StringAggregateSelection;
  url: StringAggregateSelection;
};

export type TableEntryWhere = {
  AND?: InputMaybe<Array<TableEntryWhere>>;
  NOT?: InputMaybe<TableEntryWhere>;
  OR?: InputMaybe<Array<TableEntryWhere>>;
  guide?: InputMaybe<GuideWhere>;
  guideAggregate?: InputMaybe<TableEntryGuideAggregateInput>;
  guideConnection?: InputMaybe<TableEntryGuideConnectionWhere>;
  guideConnection_NOT?: InputMaybe<TableEntryGuideConnectionWhere>;
  guide_NOT?: InputMaybe<GuideWhere>;
  id?: InputMaybe<Scalars['ID']['input']>;
  id_CONTAINS?: InputMaybe<Scalars['ID']['input']>;
  id_ENDS_WITH?: InputMaybe<Scalars['ID']['input']>;
  id_IN?: InputMaybe<Array<Scalars['ID']['input']>>;
  id_STARTS_WITH?: InputMaybe<Scalars['ID']['input']>;
  modification?: InputMaybe<ModificationWhere>;
  modificationAggregate?: InputMaybe<TableEntryModificationAggregateInput>;
  modificationConnection?: InputMaybe<TableEntryModificationConnectionWhere>;
  modificationConnection_NOT?: InputMaybe<TableEntryModificationConnectionWhere>;
  modification_NOT?: InputMaybe<ModificationWhere>;
  organism?: InputMaybe<OrganismWhere>;
  organismAggregate?: InputMaybe<TableEntryOrganismAggregateInput>;
  organismConnection?: InputMaybe<TableEntryOrganismConnectionWhere>;
  organismConnection_NOT?: InputMaybe<TableEntryOrganismConnectionWhere>;
  organism_NOT?: InputMaybe<OrganismWhere>;
  target?: InputMaybe<TargetWhere>;
  targetAggregate?: InputMaybe<TableEntryTargetAggregateInput>;
  targetConnection?: InputMaybe<TableEntryTargetConnectionWhere>;
  targetConnection_NOT?: InputMaybe<TableEntryTargetConnectionWhere>;
  target_NOT?: InputMaybe<TargetWhere>;
};

/** A genome is a collection of sequences */
export type Target = Sequence & {
  __typename?: 'Target';
  /** ALternative guide names */
  altnames?: Maybe<Array<Scalars['String']['output']>>;
  /** The annotation of the sequence if any */
  annotation?: Maybe<AnnotationType>;
  /** The Chemical Entities of Biological Interest Ontology (ChEBI) id */
  chebi_id?: Maybe<Scalars['String']['output']>;
  /** The class of the sequence */
  class: SequenceClass;
  /** Guide description */
  description?: Maybe<Scalars['String']['output']>;
  /** Table entries associated to the guide */
  entries: Array<TableEntry>;
  entriesConnection: TargetEntriesConnection;
  /** The features associated with the target */
  features: Array<Sequence>;
  featuresAggregate?: Maybe<TargetSequenceFeaturesAggregationSelection>;
  featuresConnection: SequenceFeaturesConnection;
  /** The reference genome the sequence comes from */
  genome?: Maybe<Genome>;
  genomeAggregate?: Maybe<TargetGenomeGenomeAggregationSelection>;
  genomeConnection: SequenceGenomeConnection;
  /** The type of GraphQL object */
  graphql_type: GraphQlType;
  /** The target ID */
  id: Scalars['ID']['output'];
  /** The interactions the target acts in */
  interactions: Array<Interaction>;
  interactionsConnection: TargetInteractionsConnection;
  /** The length of sequence in number of nucleotide */
  length: Scalars['Int']['output'];
  /** Modifications that append on target */
  modifications: Array<Modification>;
  modificationsAggregate?: Maybe<TargetModificationModificationsAggregationSelection>;
  modificationsConnection: TargetModificationsConnection;
  /** Common guide name */
  name?: Maybe<Scalars['String']['output']>;
  /** The target is a feature from parent */
  parent?: Maybe<Sequence>;
  parentAggregate?: Maybe<TargetSequenceParentAggregationSelection>;
  parentConnection: TargetParentConnection;
  /** The filename of the secondary structure */
  secondary_struct_file?: Maybe<Scalars['String']['output']>;
  /** The sequence itself (url can be used instead for huge sequence) */
  seq?: Maybe<Scalars['String']['output']>;
  /** The Sequence Ontology (so) id */
  so_id?: Maybe<Scalars['String']['output']>;
  /** The type of the target */
  type: SequenceType;
  /** Unit Group (like LSU/SSU) */
  unit?: Maybe<Scalars['String']['output']>;
  /** The url to get the file containning the sequence */
  url?: Maybe<Scalars['String']['output']>;
};


/** A genome is a collection of sequences */
export type TargetEntriesArgs = {
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  options?: InputMaybe<TableEntryOptions>;
  where?: InputMaybe<TableEntryWhere>;
};


/** A genome is a collection of sequences */
export type TargetEntriesConnectionArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  sort?: InputMaybe<Array<TargetEntriesConnectionSort>>;
  where?: InputMaybe<TargetEntriesConnectionWhere>;
};


/** A genome is a collection of sequences */
export type TargetFeaturesArgs = {
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  options?: InputMaybe<SequenceOptions>;
  where?: InputMaybe<SequenceWhere>;
};


/** A genome is a collection of sequences */
export type TargetFeaturesAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  where?: InputMaybe<SequenceWhere>;
};


/** A genome is a collection of sequences */
export type TargetFeaturesConnectionArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  sort?: InputMaybe<Array<SequenceFeaturesConnectionSort>>;
  where?: InputMaybe<SequenceFeaturesConnectionWhere>;
};


/** A genome is a collection of sequences */
export type TargetGenomeArgs = {
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  options?: InputMaybe<GenomeOptions>;
  where?: InputMaybe<GenomeWhere>;
};


/** A genome is a collection of sequences */
export type TargetGenomeAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  where?: InputMaybe<GenomeWhere>;
};


/** A genome is a collection of sequences */
export type TargetGenomeConnectionArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  sort?: InputMaybe<Array<SequenceGenomeConnectionSort>>;
  where?: InputMaybe<SequenceGenomeConnectionWhere>;
};


/** A genome is a collection of sequences */
export type TargetInteractionsArgs = {
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  options?: InputMaybe<InteractionOptions>;
  where?: InputMaybe<InteractionWhere>;
};


/** A genome is a collection of sequences */
export type TargetInteractionsConnectionArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  sort?: InputMaybe<Array<TargetInteractionsConnectionSort>>;
  where?: InputMaybe<TargetInteractionsConnectionWhere>;
};


/** A genome is a collection of sequences */
export type TargetModificationsArgs = {
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  options?: InputMaybe<ModificationOptions>;
  where?: InputMaybe<ModificationWhere>;
};


/** A genome is a collection of sequences */
export type TargetModificationsAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  where?: InputMaybe<ModificationWhere>;
};


/** A genome is a collection of sequences */
export type TargetModificationsConnectionArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  sort?: InputMaybe<Array<TargetModificationsConnectionSort>>;
  where?: InputMaybe<TargetModificationsConnectionWhere>;
};


/** A genome is a collection of sequences */
export type TargetParentArgs = {
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  options?: InputMaybe<SequenceOptions>;
  where?: InputMaybe<SequenceWhere>;
};


/** A genome is a collection of sequences */
export type TargetParentAggregateArgs = {
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  where?: InputMaybe<SequenceWhere>;
};


/** A genome is a collection of sequences */
export type TargetParentConnectionArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  directed?: InputMaybe<Scalars['Boolean']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  sort?: InputMaybe<Array<TargetParentConnectionSort>>;
  where?: InputMaybe<TargetParentConnectionWhere>;
};

export type TargetAggregateSelection = {
  __typename?: 'TargetAggregateSelection';
  chebi_id: StringAggregateSelection;
  count: Scalars['Int']['output'];
  description: StringAggregateSelection;
  id: IdAggregateSelection;
  length: IntAggregateSelection;
  name: StringAggregateSelection;
  secondary_struct_file: StringAggregateSelection;
  seq: StringAggregateSelection;
  so_id: StringAggregateSelection;
  unit: StringAggregateSelection;
  url: StringAggregateSelection;
};

export type TargetEdge = {
  __typename?: 'TargetEdge';
  cursor: Scalars['String']['output'];
  node: Target;
};

export type TargetEntriesAggregateInput = {
  AND?: InputMaybe<Array<TargetEntriesAggregateInput>>;
  NOT?: InputMaybe<TargetEntriesAggregateInput>;
  OR?: InputMaybe<Array<TargetEntriesAggregateInput>>;
  count?: InputMaybe<Scalars['Int']['input']>;
  count_GT?: InputMaybe<Scalars['Int']['input']>;
  count_GTE?: InputMaybe<Scalars['Int']['input']>;
  count_LT?: InputMaybe<Scalars['Int']['input']>;
  count_LTE?: InputMaybe<Scalars['Int']['input']>;
  node?: InputMaybe<TargetEntriesNodeAggregationWhereInput>;
};

export type TargetEntriesConnection = {
  __typename?: 'TargetEntriesConnection';
  edges: Array<TargetEntriesRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int']['output'];
};

export type TargetEntriesConnectionSort = {
  node?: InputMaybe<TableEntrySort>;
};

export type TargetEntriesConnectionWhere = {
  AND?: InputMaybe<Array<TargetEntriesConnectionWhere>>;
  NOT?: InputMaybe<TargetEntriesConnectionWhere>;
  OR?: InputMaybe<Array<TargetEntriesConnectionWhere>>;
  node?: InputMaybe<TableEntryWhere>;
};

export type TargetEntriesNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<TargetEntriesNodeAggregationWhereInput>>;
  NOT?: InputMaybe<TargetEntriesNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<TargetEntriesNodeAggregationWhereInput>>;
};

export type TargetEntriesRelationship = {
  __typename?: 'TargetEntriesRelationship';
  cursor: Scalars['String']['output'];
  node: TableEntry;
};

export type TargetFeaturesAggregateInput = {
  AND?: InputMaybe<Array<TargetFeaturesAggregateInput>>;
  NOT?: InputMaybe<TargetFeaturesAggregateInput>;
  OR?: InputMaybe<Array<TargetFeaturesAggregateInput>>;
  count?: InputMaybe<Scalars['Int']['input']>;
  count_GT?: InputMaybe<Scalars['Int']['input']>;
  count_GTE?: InputMaybe<Scalars['Int']['input']>;
  count_LT?: InputMaybe<Scalars['Int']['input']>;
  count_LTE?: InputMaybe<Scalars['Int']['input']>;
  edge?: InputMaybe<HasFeatureAggregationWhereInput>;
  node?: InputMaybe<TargetFeaturesNodeAggregationWhereInput>;
};

export type TargetFeaturesNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<TargetFeaturesNodeAggregationWhereInput>>;
  NOT?: InputMaybe<TargetFeaturesNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<TargetFeaturesNodeAggregationWhereInput>>;
  chebi_id_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  chebi_id_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  chebi_id_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  chebi_id_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  chebi_id_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  chebi_id_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  description_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  description_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  length_AVERAGE_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  length_AVERAGE_GT?: InputMaybe<Scalars['Float']['input']>;
  length_AVERAGE_GTE?: InputMaybe<Scalars['Float']['input']>;
  length_AVERAGE_LT?: InputMaybe<Scalars['Float']['input']>;
  length_AVERAGE_LTE?: InputMaybe<Scalars['Float']['input']>;
  length_MAX_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  length_MAX_GT?: InputMaybe<Scalars['Int']['input']>;
  length_MAX_GTE?: InputMaybe<Scalars['Int']['input']>;
  length_MAX_LT?: InputMaybe<Scalars['Int']['input']>;
  length_MAX_LTE?: InputMaybe<Scalars['Int']['input']>;
  length_MIN_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  length_MIN_GT?: InputMaybe<Scalars['Int']['input']>;
  length_MIN_GTE?: InputMaybe<Scalars['Int']['input']>;
  length_MIN_LT?: InputMaybe<Scalars['Int']['input']>;
  length_MIN_LTE?: InputMaybe<Scalars['Int']['input']>;
  length_SUM_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  length_SUM_GT?: InputMaybe<Scalars['Int']['input']>;
  length_SUM_GTE?: InputMaybe<Scalars['Int']['input']>;
  length_SUM_LT?: InputMaybe<Scalars['Int']['input']>;
  length_SUM_LTE?: InputMaybe<Scalars['Int']['input']>;
  name_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  name_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  seq_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  seq_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  seq_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  seq_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  seq_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  seq_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  seq_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  seq_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  seq_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  seq_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  seq_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  seq_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  seq_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  seq_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  seq_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  so_id_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  url_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  url_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
};

export type TargetGenomeAggregateInput = {
  AND?: InputMaybe<Array<TargetGenomeAggregateInput>>;
  NOT?: InputMaybe<TargetGenomeAggregateInput>;
  OR?: InputMaybe<Array<TargetGenomeAggregateInput>>;
  count?: InputMaybe<Scalars['Int']['input']>;
  count_GT?: InputMaybe<Scalars['Int']['input']>;
  count_GTE?: InputMaybe<Scalars['Int']['input']>;
  count_LT?: InputMaybe<Scalars['Int']['input']>;
  count_LTE?: InputMaybe<Scalars['Int']['input']>;
  node?: InputMaybe<TargetGenomeNodeAggregationWhereInput>;
};

export type TargetGenomeGenomeAggregationSelection = {
  __typename?: 'TargetGenomeGenomeAggregationSelection';
  count: Scalars['Int']['output'];
  node?: Maybe<TargetGenomeGenomeNodeAggregateSelection>;
};

export type TargetGenomeGenomeNodeAggregateSelection = {
  __typename?: 'TargetGenomeGenomeNodeAggregateSelection';
  description: StringAggregateSelection;
  id: IdAggregateSelection;
  so_id: StringAggregateSelection;
  source: StringAggregateSelection;
  url: StringAggregateSelection;
  version: StringAggregateSelection;
};

export type TargetGenomeNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<TargetGenomeNodeAggregationWhereInput>>;
  NOT?: InputMaybe<TargetGenomeNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<TargetGenomeNodeAggregationWhereInput>>;
  description_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  description_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  so_id_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  source_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  source_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  source_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  source_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  source_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  source_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  source_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  source_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  source_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  source_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  source_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  source_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  source_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  source_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  source_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  url_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  url_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  version_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  version_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  version_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  version_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  version_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  version_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  version_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  version_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  version_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  version_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  version_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  version_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  version_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  version_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  version_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
};

export type TargetInteractionsAggregateInput = {
  AND?: InputMaybe<Array<TargetInteractionsAggregateInput>>;
  NOT?: InputMaybe<TargetInteractionsAggregateInput>;
  OR?: InputMaybe<Array<TargetInteractionsAggregateInput>>;
  count?: InputMaybe<Scalars['Int']['input']>;
  count_GT?: InputMaybe<Scalars['Int']['input']>;
  count_GTE?: InputMaybe<Scalars['Int']['input']>;
  count_LT?: InputMaybe<Scalars['Int']['input']>;
  count_LTE?: InputMaybe<Scalars['Int']['input']>;
  node?: InputMaybe<TargetInteractionsNodeAggregationWhereInput>;
};

export type TargetInteractionsConnection = {
  __typename?: 'TargetInteractionsConnection';
  edges: Array<TargetInteractionsRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int']['output'];
};

export type TargetInteractionsConnectionSort = {
  node?: InputMaybe<InteractionSort>;
};

export type TargetInteractionsConnectionWhere = {
  AND?: InputMaybe<Array<TargetInteractionsConnectionWhere>>;
  NOT?: InputMaybe<TargetInteractionsConnectionWhere>;
  OR?: InputMaybe<Array<TargetInteractionsConnectionWhere>>;
  node?: InputMaybe<InteractionWhere>;
};

export type TargetInteractionsNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<TargetInteractionsNodeAggregationWhereInput>>;
  NOT?: InputMaybe<TargetInteractionsNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<TargetInteractionsNodeAggregationWhereInput>>;
};

export type TargetInteractionsRelationship = {
  __typename?: 'TargetInteractionsRelationship';
  cursor: Scalars['String']['output'];
  node: Interaction;
};

export type TargetModificationModificationsAggregationSelection = {
  __typename?: 'TargetModificationModificationsAggregationSelection';
  count: Scalars['Int']['output'];
  node?: Maybe<TargetModificationModificationsNodeAggregateSelection>;
};

export type TargetModificationModificationsNodeAggregateSelection = {
  __typename?: 'TargetModificationModificationsNodeAggregateSelection';
  id: IdAggregateSelection;
  name: StringAggregateSelection;
  nucleotide: StringAggregateSelection;
  position: IntAggregateSelection;
  symbol: StringAggregateSelection;
  symbol_label: StringAggregateSelection;
  type_label: StringAggregateSelection;
  type_short_label: StringAggregateSelection;
};

export type TargetModificationsAggregateInput = {
  AND?: InputMaybe<Array<TargetModificationsAggregateInput>>;
  NOT?: InputMaybe<TargetModificationsAggregateInput>;
  OR?: InputMaybe<Array<TargetModificationsAggregateInput>>;
  count?: InputMaybe<Scalars['Int']['input']>;
  count_GT?: InputMaybe<Scalars['Int']['input']>;
  count_GTE?: InputMaybe<Scalars['Int']['input']>;
  count_LT?: InputMaybe<Scalars['Int']['input']>;
  count_LTE?: InputMaybe<Scalars['Int']['input']>;
  node?: InputMaybe<TargetModificationsNodeAggregationWhereInput>;
};

export type TargetModificationsConnection = {
  __typename?: 'TargetModificationsConnection';
  edges: Array<TargetModificationsRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int']['output'];
};

export type TargetModificationsConnectionSort = {
  node?: InputMaybe<ModificationSort>;
};

export type TargetModificationsConnectionWhere = {
  AND?: InputMaybe<Array<TargetModificationsConnectionWhere>>;
  NOT?: InputMaybe<TargetModificationsConnectionWhere>;
  OR?: InputMaybe<Array<TargetModificationsConnectionWhere>>;
  node?: InputMaybe<ModificationWhere>;
};

export type TargetModificationsNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<TargetModificationsNodeAggregationWhereInput>>;
  NOT?: InputMaybe<TargetModificationsNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<TargetModificationsNodeAggregationWhereInput>>;
  name_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  name_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  nucleotide_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  nucleotide_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  nucleotide_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  nucleotide_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  nucleotide_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  nucleotide_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  nucleotide_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  nucleotide_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  nucleotide_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  nucleotide_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  nucleotide_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  nucleotide_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  nucleotide_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  nucleotide_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  nucleotide_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  position_AVERAGE_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  position_AVERAGE_GT?: InputMaybe<Scalars['Float']['input']>;
  position_AVERAGE_GTE?: InputMaybe<Scalars['Float']['input']>;
  position_AVERAGE_LT?: InputMaybe<Scalars['Float']['input']>;
  position_AVERAGE_LTE?: InputMaybe<Scalars['Float']['input']>;
  position_MAX_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  position_MAX_GT?: InputMaybe<Scalars['Int']['input']>;
  position_MAX_GTE?: InputMaybe<Scalars['Int']['input']>;
  position_MAX_LT?: InputMaybe<Scalars['Int']['input']>;
  position_MAX_LTE?: InputMaybe<Scalars['Int']['input']>;
  position_MIN_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  position_MIN_GT?: InputMaybe<Scalars['Int']['input']>;
  position_MIN_GTE?: InputMaybe<Scalars['Int']['input']>;
  position_MIN_LT?: InputMaybe<Scalars['Int']['input']>;
  position_MIN_LTE?: InputMaybe<Scalars['Int']['input']>;
  position_SUM_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  position_SUM_GT?: InputMaybe<Scalars['Int']['input']>;
  position_SUM_GTE?: InputMaybe<Scalars['Int']['input']>;
  position_SUM_LT?: InputMaybe<Scalars['Int']['input']>;
  position_SUM_LTE?: InputMaybe<Scalars['Int']['input']>;
  symbol_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  symbol_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  symbol_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  symbol_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  symbol_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  symbol_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  symbol_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  symbol_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  symbol_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  symbol_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  symbol_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  symbol_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  symbol_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  symbol_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  symbol_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  symbol_label_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  symbol_label_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  symbol_label_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  symbol_label_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  symbol_label_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  symbol_label_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  symbol_label_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  symbol_label_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  symbol_label_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  symbol_label_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  symbol_label_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  symbol_label_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  symbol_label_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  symbol_label_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  symbol_label_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  type_label_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  type_label_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  type_label_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  type_label_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  type_label_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  type_label_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  type_label_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  type_label_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  type_label_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  type_label_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  type_label_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  type_label_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  type_label_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  type_label_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  type_label_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  type_short_label_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  type_short_label_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  type_short_label_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  type_short_label_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  type_short_label_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  type_short_label_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  type_short_label_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  type_short_label_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  type_short_label_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  type_short_label_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  type_short_label_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  type_short_label_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  type_short_label_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  type_short_label_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  type_short_label_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
};

export type TargetModificationsRelationship = {
  __typename?: 'TargetModificationsRelationship';
  cursor: Scalars['String']['output'];
  node: Modification;
};

export type TargetOptions = {
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  /** Specify one or more TargetSort objects to sort Targets by. The sorts will be applied in the order in which they are arranged in the array. */
  sort?: InputMaybe<Array<TargetSort>>;
};

export type TargetParentAggregateInput = {
  AND?: InputMaybe<Array<TargetParentAggregateInput>>;
  NOT?: InputMaybe<TargetParentAggregateInput>;
  OR?: InputMaybe<Array<TargetParentAggregateInput>>;
  count?: InputMaybe<Scalars['Int']['input']>;
  count_GT?: InputMaybe<Scalars['Int']['input']>;
  count_GTE?: InputMaybe<Scalars['Int']['input']>;
  count_LT?: InputMaybe<Scalars['Int']['input']>;
  count_LTE?: InputMaybe<Scalars['Int']['input']>;
  edge?: InputMaybe<HasFeatureAggregationWhereInput>;
  node?: InputMaybe<TargetParentNodeAggregationWhereInput>;
};

export type TargetParentConnection = {
  __typename?: 'TargetParentConnection';
  edges: Array<TargetParentRelationship>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int']['output'];
};

export type TargetParentConnectionSort = {
  edge?: InputMaybe<HasFeatureSort>;
  node?: InputMaybe<SequenceSort>;
};

export type TargetParentConnectionWhere = {
  AND?: InputMaybe<Array<TargetParentConnectionWhere>>;
  NOT?: InputMaybe<TargetParentConnectionWhere>;
  OR?: InputMaybe<Array<TargetParentConnectionWhere>>;
  edge?: InputMaybe<HasFeatureWhere>;
  node?: InputMaybe<SequenceWhere>;
};

export type TargetParentNodeAggregationWhereInput = {
  AND?: InputMaybe<Array<TargetParentNodeAggregationWhereInput>>;
  NOT?: InputMaybe<TargetParentNodeAggregationWhereInput>;
  OR?: InputMaybe<Array<TargetParentNodeAggregationWhereInput>>;
  chebi_id_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  chebi_id_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  chebi_id_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  chebi_id_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  chebi_id_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  chebi_id_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  chebi_id_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  description_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  description_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  description_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  description_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  description_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  length_AVERAGE_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  length_AVERAGE_GT?: InputMaybe<Scalars['Float']['input']>;
  length_AVERAGE_GTE?: InputMaybe<Scalars['Float']['input']>;
  length_AVERAGE_LT?: InputMaybe<Scalars['Float']['input']>;
  length_AVERAGE_LTE?: InputMaybe<Scalars['Float']['input']>;
  length_MAX_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  length_MAX_GT?: InputMaybe<Scalars['Int']['input']>;
  length_MAX_GTE?: InputMaybe<Scalars['Int']['input']>;
  length_MAX_LT?: InputMaybe<Scalars['Int']['input']>;
  length_MAX_LTE?: InputMaybe<Scalars['Int']['input']>;
  length_MIN_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  length_MIN_GT?: InputMaybe<Scalars['Int']['input']>;
  length_MIN_GTE?: InputMaybe<Scalars['Int']['input']>;
  length_MIN_LT?: InputMaybe<Scalars['Int']['input']>;
  length_MIN_LTE?: InputMaybe<Scalars['Int']['input']>;
  length_SUM_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  length_SUM_GT?: InputMaybe<Scalars['Int']['input']>;
  length_SUM_GTE?: InputMaybe<Scalars['Int']['input']>;
  length_SUM_LT?: InputMaybe<Scalars['Int']['input']>;
  length_SUM_LTE?: InputMaybe<Scalars['Int']['input']>;
  name_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  name_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  name_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  name_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  name_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  seq_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  seq_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  seq_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  seq_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  seq_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  seq_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  seq_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  seq_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  seq_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  seq_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  seq_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  seq_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  seq_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  seq_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  seq_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  so_id_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  so_id_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  so_id_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  so_id_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  url_AVERAGE_LENGTH_EQUAL?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_GT?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_GTE?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_LT?: InputMaybe<Scalars['Float']['input']>;
  url_AVERAGE_LENGTH_LTE?: InputMaybe<Scalars['Float']['input']>;
  url_LONGEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  url_LONGEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_EQUAL?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_GT?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_GTE?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_LT?: InputMaybe<Scalars['Int']['input']>;
  url_SHORTEST_LENGTH_LTE?: InputMaybe<Scalars['Int']['input']>;
};

export type TargetParentRelationship = {
  __typename?: 'TargetParentRelationship';
  cursor: Scalars['String']['output'];
  node: Sequence;
  properties: HasFeature;
};

export type TargetSequenceFeaturesAggregationSelection = {
  __typename?: 'TargetSequenceFeaturesAggregationSelection';
  count: Scalars['Int']['output'];
  edge?: Maybe<TargetSequenceFeaturesEdgeAggregateSelection>;
  node?: Maybe<TargetSequenceFeaturesNodeAggregateSelection>;
};

export type TargetSequenceFeaturesEdgeAggregateSelection = {
  __typename?: 'TargetSequenceFeaturesEdgeAggregateSelection';
  end: IntAggregateSelection;
  start: IntAggregateSelection;
};

export type TargetSequenceFeaturesNodeAggregateSelection = {
  __typename?: 'TargetSequenceFeaturesNodeAggregateSelection';
  chebi_id: StringAggregateSelection;
  description: StringAggregateSelection;
  id: IdAggregateSelection;
  length: IntAggregateSelection;
  name: StringAggregateSelection;
  seq: StringAggregateSelection;
  so_id: StringAggregateSelection;
  url: StringAggregateSelection;
};

export type TargetSequenceParentAggregationSelection = {
  __typename?: 'TargetSequenceParentAggregationSelection';
  count: Scalars['Int']['output'];
  edge?: Maybe<TargetSequenceParentEdgeAggregateSelection>;
  node?: Maybe<TargetSequenceParentNodeAggregateSelection>;
};

export type TargetSequenceParentEdgeAggregateSelection = {
  __typename?: 'TargetSequenceParentEdgeAggregateSelection';
  end: IntAggregateSelection;
  start: IntAggregateSelection;
};

export type TargetSequenceParentNodeAggregateSelection = {
  __typename?: 'TargetSequenceParentNodeAggregateSelection';
  chebi_id: StringAggregateSelection;
  description: StringAggregateSelection;
  id: IdAggregateSelection;
  length: IntAggregateSelection;
  name: StringAggregateSelection;
  seq: StringAggregateSelection;
  so_id: StringAggregateSelection;
  url: StringAggregateSelection;
};

/** Fields to sort Targets by. The order in which sorts are applied is not guaranteed when specifying many fields in one TargetSort object. */
export type TargetSort = {
  annotation?: InputMaybe<SortDirection>;
  chebi_id?: InputMaybe<SortDirection>;
  class?: InputMaybe<SortDirection>;
  description?: InputMaybe<SortDirection>;
  graphql_type?: InputMaybe<SortDirection>;
  id?: InputMaybe<SortDirection>;
  length?: InputMaybe<SortDirection>;
  name?: InputMaybe<SortDirection>;
  secondary_struct_file?: InputMaybe<SortDirection>;
  seq?: InputMaybe<SortDirection>;
  so_id?: InputMaybe<SortDirection>;
  type?: InputMaybe<SortDirection>;
  unit?: InputMaybe<SortDirection>;
  url?: InputMaybe<SortDirection>;
};

export type TargetWhere = {
  AND?: InputMaybe<Array<TargetWhere>>;
  NOT?: InputMaybe<TargetWhere>;
  OR?: InputMaybe<Array<TargetWhere>>;
  altnames?: InputMaybe<Array<Scalars['String']['input']>>;
  altnames_INCLUDES?: InputMaybe<Scalars['String']['input']>;
  annotation?: InputMaybe<AnnotationType>;
  annotation_IN?: InputMaybe<Array<InputMaybe<AnnotationType>>>;
  chebi_id?: InputMaybe<Scalars['String']['input']>;
  chebi_id_CONTAINS?: InputMaybe<Scalars['String']['input']>;
  chebi_id_ENDS_WITH?: InputMaybe<Scalars['String']['input']>;
  chebi_id_IN?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  chebi_id_STARTS_WITH?: InputMaybe<Scalars['String']['input']>;
  class?: InputMaybe<SequenceClass>;
  class_IN?: InputMaybe<Array<SequenceClass>>;
  description?: InputMaybe<Scalars['String']['input']>;
  description_CONTAINS?: InputMaybe<Scalars['String']['input']>;
  description_ENDS_WITH?: InputMaybe<Scalars['String']['input']>;
  description_IN?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  description_STARTS_WITH?: InputMaybe<Scalars['String']['input']>;
  entriesAggregate?: InputMaybe<TargetEntriesAggregateInput>;
  /** Return Targets where all of the related TargetEntriesConnections match this filter */
  entriesConnection_ALL?: InputMaybe<TargetEntriesConnectionWhere>;
  /** Return Targets where none of the related TargetEntriesConnections match this filter */
  entriesConnection_NONE?: InputMaybe<TargetEntriesConnectionWhere>;
  /** Return Targets where one of the related TargetEntriesConnections match this filter */
  entriesConnection_SINGLE?: InputMaybe<TargetEntriesConnectionWhere>;
  /** Return Targets where some of the related TargetEntriesConnections match this filter */
  entriesConnection_SOME?: InputMaybe<TargetEntriesConnectionWhere>;
  /** Return Targets where all of the related TableEntries match this filter */
  entries_ALL?: InputMaybe<TableEntryWhere>;
  /** Return Targets where none of the related TableEntries match this filter */
  entries_NONE?: InputMaybe<TableEntryWhere>;
  /** Return Targets where one of the related TableEntries match this filter */
  entries_SINGLE?: InputMaybe<TableEntryWhere>;
  /** Return Targets where some of the related TableEntries match this filter */
  entries_SOME?: InputMaybe<TableEntryWhere>;
  featuresAggregate?: InputMaybe<TargetFeaturesAggregateInput>;
  /** Return Targets where all of the related SequenceFeaturesConnections match this filter */
  featuresConnection_ALL?: InputMaybe<SequenceFeaturesConnectionWhere>;
  /** Return Targets where none of the related SequenceFeaturesConnections match this filter */
  featuresConnection_NONE?: InputMaybe<SequenceFeaturesConnectionWhere>;
  /** Return Targets where one of the related SequenceFeaturesConnections match this filter */
  featuresConnection_SINGLE?: InputMaybe<SequenceFeaturesConnectionWhere>;
  /** Return Targets where some of the related SequenceFeaturesConnections match this filter */
  featuresConnection_SOME?: InputMaybe<SequenceFeaturesConnectionWhere>;
  /** Return Targets where all of the related Sequences match this filter */
  features_ALL?: InputMaybe<SequenceWhere>;
  /** Return Targets where none of the related Sequences match this filter */
  features_NONE?: InputMaybe<SequenceWhere>;
  /** Return Targets where one of the related Sequences match this filter */
  features_SINGLE?: InputMaybe<SequenceWhere>;
  /** Return Targets where some of the related Sequences match this filter */
  features_SOME?: InputMaybe<SequenceWhere>;
  genome?: InputMaybe<GenomeWhere>;
  genomeAggregate?: InputMaybe<TargetGenomeAggregateInput>;
  genomeConnection?: InputMaybe<SequenceGenomeConnectionWhere>;
  genomeConnection_NOT?: InputMaybe<SequenceGenomeConnectionWhere>;
  genome_NOT?: InputMaybe<GenomeWhere>;
  graphql_type?: InputMaybe<GraphQlType>;
  graphql_type_IN?: InputMaybe<Array<GraphQlType>>;
  id?: InputMaybe<Scalars['ID']['input']>;
  id_CONTAINS?: InputMaybe<Scalars['ID']['input']>;
  id_ENDS_WITH?: InputMaybe<Scalars['ID']['input']>;
  id_IN?: InputMaybe<Array<Scalars['ID']['input']>>;
  id_STARTS_WITH?: InputMaybe<Scalars['ID']['input']>;
  interactionsAggregate?: InputMaybe<TargetInteractionsAggregateInput>;
  /** Return Targets where all of the related TargetInteractionsConnections match this filter */
  interactionsConnection_ALL?: InputMaybe<TargetInteractionsConnectionWhere>;
  /** Return Targets where none of the related TargetInteractionsConnections match this filter */
  interactionsConnection_NONE?: InputMaybe<TargetInteractionsConnectionWhere>;
  /** Return Targets where one of the related TargetInteractionsConnections match this filter */
  interactionsConnection_SINGLE?: InputMaybe<TargetInteractionsConnectionWhere>;
  /** Return Targets where some of the related TargetInteractionsConnections match this filter */
  interactionsConnection_SOME?: InputMaybe<TargetInteractionsConnectionWhere>;
  /** Return Targets where all of the related Interactions match this filter */
  interactions_ALL?: InputMaybe<InteractionWhere>;
  /** Return Targets where none of the related Interactions match this filter */
  interactions_NONE?: InputMaybe<InteractionWhere>;
  /** Return Targets where one of the related Interactions match this filter */
  interactions_SINGLE?: InputMaybe<InteractionWhere>;
  /** Return Targets where some of the related Interactions match this filter */
  interactions_SOME?: InputMaybe<InteractionWhere>;
  length?: InputMaybe<Scalars['Int']['input']>;
  length_GT?: InputMaybe<Scalars['Int']['input']>;
  length_GTE?: InputMaybe<Scalars['Int']['input']>;
  length_IN?: InputMaybe<Array<Scalars['Int']['input']>>;
  length_LT?: InputMaybe<Scalars['Int']['input']>;
  length_LTE?: InputMaybe<Scalars['Int']['input']>;
  modificationsAggregate?: InputMaybe<TargetModificationsAggregateInput>;
  /** Return Targets where all of the related TargetModificationsConnections match this filter */
  modificationsConnection_ALL?: InputMaybe<TargetModificationsConnectionWhere>;
  /** Return Targets where none of the related TargetModificationsConnections match this filter */
  modificationsConnection_NONE?: InputMaybe<TargetModificationsConnectionWhere>;
  /** Return Targets where one of the related TargetModificationsConnections match this filter */
  modificationsConnection_SINGLE?: InputMaybe<TargetModificationsConnectionWhere>;
  /** Return Targets where some of the related TargetModificationsConnections match this filter */
  modificationsConnection_SOME?: InputMaybe<TargetModificationsConnectionWhere>;
  /** Return Targets where all of the related Modifications match this filter */
  modifications_ALL?: InputMaybe<ModificationWhere>;
  /** Return Targets where none of the related Modifications match this filter */
  modifications_NONE?: InputMaybe<ModificationWhere>;
  /** Return Targets where one of the related Modifications match this filter */
  modifications_SINGLE?: InputMaybe<ModificationWhere>;
  /** Return Targets where some of the related Modifications match this filter */
  modifications_SOME?: InputMaybe<ModificationWhere>;
  name?: InputMaybe<Scalars['String']['input']>;
  name_CONTAINS?: InputMaybe<Scalars['String']['input']>;
  name_ENDS_WITH?: InputMaybe<Scalars['String']['input']>;
  name_IN?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  name_STARTS_WITH?: InputMaybe<Scalars['String']['input']>;
  parent?: InputMaybe<SequenceWhere>;
  parentAggregate?: InputMaybe<TargetParentAggregateInput>;
  parentConnection?: InputMaybe<TargetParentConnectionWhere>;
  parentConnection_NOT?: InputMaybe<TargetParentConnectionWhere>;
  parent_NOT?: InputMaybe<SequenceWhere>;
  secondary_struct_file?: InputMaybe<Scalars['String']['input']>;
  secondary_struct_file_CONTAINS?: InputMaybe<Scalars['String']['input']>;
  secondary_struct_file_ENDS_WITH?: InputMaybe<Scalars['String']['input']>;
  secondary_struct_file_IN?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  secondary_struct_file_STARTS_WITH?: InputMaybe<Scalars['String']['input']>;
  seq?: InputMaybe<Scalars['String']['input']>;
  seq_CONTAINS?: InputMaybe<Scalars['String']['input']>;
  seq_ENDS_WITH?: InputMaybe<Scalars['String']['input']>;
  seq_IN?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  seq_STARTS_WITH?: InputMaybe<Scalars['String']['input']>;
  so_id?: InputMaybe<Scalars['String']['input']>;
  so_id_CONTAINS?: InputMaybe<Scalars['String']['input']>;
  so_id_ENDS_WITH?: InputMaybe<Scalars['String']['input']>;
  so_id_IN?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  so_id_STARTS_WITH?: InputMaybe<Scalars['String']['input']>;
  type?: InputMaybe<SequenceType>;
  type_IN?: InputMaybe<Array<SequenceType>>;
  unit?: InputMaybe<Scalars['String']['input']>;
  unit_CONTAINS?: InputMaybe<Scalars['String']['input']>;
  unit_ENDS_WITH?: InputMaybe<Scalars['String']['input']>;
  unit_IN?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  unit_STARTS_WITH?: InputMaybe<Scalars['String']['input']>;
  url?: InputMaybe<Scalars['String']['input']>;
  url_CONTAINS?: InputMaybe<Scalars['String']['input']>;
  url_ENDS_WITH?: InputMaybe<Scalars['String']['input']>;
  url_IN?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  url_STARTS_WITH?: InputMaybe<Scalars['String']['input']>;
};

export type TargetsConnection = {
  __typename?: 'TargetsConnection';
  edges: Array<TargetEdge>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int']['output'];
};

export type Track = {
  /** The sequence */
  content: Scalars['String']['output'];
  /** The track ID */
  id: Scalars['ID']['output'];
  /** Other tracks associated/coupled/grouped with the current track */
  links: Array<Track>;
  /** The track name */
  name?: Maybe<Scalars['String']['output']>;
  /** The sequence the track is associated to */
  ref: Sequence;
};

export type TrackAggregateSelection = {
  __typename?: 'TrackAggregateSelection';
  content: StringAggregateSelection;
  count: Scalars['Int']['output'];
  id: IdAggregateSelection;
  name: StringAggregateSelection;
};

export type TrackEdge = {
  __typename?: 'TrackEdge';
  cursor: Scalars['String']['output'];
  node: Track;
};

export enum TrackImplementation {
  SequenceTrack = 'SequenceTrack',
  StructureTrack = 'StructureTrack'
}

export type TrackInput = {
  target_id: Scalars['ID']['input'];
};

export type TrackOptions = {
  limit?: InputMaybe<Scalars['Int']['input']>;
  offset?: InputMaybe<Scalars['Int']['input']>;
  /** Specify one or more TrackSort objects to sort Tracks by. The sorts will be applied in the order in which they are arranged in the array. */
  sort?: InputMaybe<Array<InputMaybe<TrackSort>>>;
};

/** Fields to sort Tracks by. The order in which sorts are applied is not guaranteed when specifying many fields in one TrackSort object. */
export type TrackSort = {
  content?: InputMaybe<SortDirection>;
  id?: InputMaybe<SortDirection>;
  name?: InputMaybe<SortDirection>;
};

export type TrackWhere = {
  AND?: InputMaybe<Array<TrackWhere>>;
  NOT?: InputMaybe<TrackWhere>;
  OR?: InputMaybe<Array<TrackWhere>>;
  content?: InputMaybe<Scalars['String']['input']>;
  content_CONTAINS?: InputMaybe<Scalars['String']['input']>;
  content_ENDS_WITH?: InputMaybe<Scalars['String']['input']>;
  content_IN?: InputMaybe<Array<Scalars['String']['input']>>;
  content_STARTS_WITH?: InputMaybe<Scalars['String']['input']>;
  id?: InputMaybe<Scalars['ID']['input']>;
  id_CONTAINS?: InputMaybe<Scalars['ID']['input']>;
  id_ENDS_WITH?: InputMaybe<Scalars['ID']['input']>;
  id_IN?: InputMaybe<Array<Scalars['ID']['input']>>;
  id_STARTS_WITH?: InputMaybe<Scalars['ID']['input']>;
  name?: InputMaybe<Scalars['String']['input']>;
  name_CONTAINS?: InputMaybe<Scalars['String']['input']>;
  name_ENDS_WITH?: InputMaybe<Scalars['String']['input']>;
  name_IN?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  name_STARTS_WITH?: InputMaybe<Scalars['String']['input']>;
  typename_IN?: InputMaybe<Array<TrackImplementation>>;
};

export type TracksConnection = {
  __typename?: 'TracksConnection';
  edges: Array<TrackEdge>;
  pageInfo: PageInfo;
  totalCount: Scalars['Int']['output'];
};

export type HomeQueryQueryVariables = Exact<{ [key: string]: never; }>;


export type HomeQueryQuery = { __typename?: 'Query', modificationTypes: Array<{ __typename?: 'Modification', type?: ModifType | null, type_short_label?: string | null }>, guideSubclasses: Array<{ __typename?: 'Guide', subclass: GuideClass, subclass_label: string }>, targetUnits: Array<{ __typename?: 'Target', unit?: string | null }>, organisms: Array<{ __typename?: 'Organism', id: number, label: string }>, organismsAggregate: { __typename?: 'OrganismAggregateSelection', count: number } };

export type ModificationAndTargetSelectionQueryQueryVariables = Exact<{
  modificationTypes?: InputMaybe<Array<InputMaybe<ModifType>> | InputMaybe<ModifType>>;
  targetNames?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>> | InputMaybe<Scalars['String']['input']>>;
  organismIds?: InputMaybe<Array<Scalars['Int']['input']> | Scalars['Int']['input']>;
}>;


export type ModificationAndTargetSelectionQueryQuery = { __typename?: 'Query', targetsBase: Array<{ __typename?: 'Target', name?: string | null, unit?: string | null }>, targets: Array<{ __typename?: 'Target', name?: string | null, id: string }>, modificationsBase: Array<{ __typename?: 'Modification', type_label?: string | null, type?: ModifType | null }>, modifications: Array<{ __typename?: 'Modification', type?: ModifType | null }>, organismsBase: Array<{ __typename?: 'Organism', label: string, id: number }>, organisms: Array<{ __typename?: 'Organism', id: number }> };

export type GuideSelectionQueryQueryVariables = Exact<{
  guideSubclasses?: InputMaybe<Array<GuideClass> | GuideClass>;
  targetNames?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>> | InputMaybe<Scalars['String']['input']>>;
  modificationTypes?: InputMaybe<Array<InputMaybe<ModifType>> | InputMaybe<ModifType>>;
  organismIds?: InputMaybe<Array<Scalars['Int']['input']> | Scalars['Int']['input']>;
}>;


export type GuideSelectionQueryQuery = { __typename?: 'Query', guidesBase: Array<{ __typename?: 'Guide', subclass: GuideClass, subclass_label: string }>, guides: Array<{ __typename?: 'Guide', subclass: GuideClass }>, targetsBase: Array<{ __typename?: 'Target', name?: string | null, unit?: string | null }>, targets: Array<{ __typename?: 'Target', name?: string | null }>, modificationsBase: Array<{ __typename?: 'Modification', type_label?: string | null, type?: ModifType | null }>, modifications: Array<{ __typename?: 'Modification', type?: ModifType | null }>, organismsBase: Array<{ __typename?: 'Organism', label: string, id: number }>, organisms: Array<{ __typename?: 'Organism', id: number }> };

export type TableEntriesQueryQueryVariables = Exact<{ [key: string]: never; }>;


export type TableEntriesQueryQuery = { __typename?: 'Query', tableEntries: Array<{ __typename?: 'TableEntry', id: string, modification?: { __typename?: 'Modification', id: string, name: string, position: number, symbol: string, symbol_label: string, type?: ModifType | null, type_short_label?: string | null } | null, guide?: { __typename?: 'Guide', id: string, name?: string | null, class: SequenceClass, subclass: GuideClass, subclass_label: string, length: number, seq?: string | null, host_genes: Array<string>, chromosomeConnection: { __typename?: 'GuideParentConnection', edges: Array<{ __typename?: 'GuideParentRelationship', properties: { __typename?: 'HasFeature', start: number, end: number, strand?: Strand | null }, node: { __typename?: 'Chromosome', id: string, name?: string | null, length: number } | { __typename?: 'GenericSequence', id: string, name?: string | null, length: number } | { __typename?: 'Guide', id: string, name?: string | null, length: number } | { __typename?: 'Target', id: string, name?: string | null, length: number } }> } } | null, target?: { __typename?: 'Target', id: string, name?: string | null, class: SequenceClass, unit?: string | null, length: number, chromosomeConnection: { __typename?: 'TargetParentConnection', edges: Array<{ __typename?: 'TargetParentRelationship', properties: { __typename?: 'HasFeature', start: number, end: number, strand?: Strand | null }, node: { __typename?: 'Chromosome', id: string, name?: string | null } | { __typename?: 'GenericSequence', id: string, name?: string | null } | { __typename?: 'Guide', id: string, name?: string | null } | { __typename?: 'Target', id: string, name?: string | null } }> } } | null, organism: { __typename?: 'Organism', id: number, label: string, name: string } }>, modificationsBase: Array<{ __typename?: 'Modification', symbol: string, symbol_label: string, type?: ModifType | null, type_short_label?: string | null }>, targetsBase: Array<{ __typename?: 'Target', name?: string | null, unit?: string | null, class: SequenceClass }>, guidesBase: Array<{ __typename?: 'Guide', subclass: GuideClass, subclass_label: string, class: SequenceClass }>, organismsBase: Array<{ __typename?: 'Organism', id: number, name: string, label: string }> };

export type OrganismByIdQueryQueryVariables = Exact<{
  id?: InputMaybe<Scalars['Int']['input']>;
}>;


export type OrganismByIdQueryQuery = { __typename?: 'Query', organisms: Array<{ __typename?: 'Organism', id: number, label: string, shortname: string, genomes: Array<{ __typename?: 'Genome', sequences: Array<{ __typename?: 'Chromosome', id: string, name?: string | null, altnames?: Array<string> | null, description?: string | null, length: number, graphql_type: GraphQlType, guides: { __typename?: 'SequenceFeaturesConnection', totalCount: number } } | { __typename?: 'GenericSequence', id: string, name?: string | null, altnames?: Array<string> | null, description?: string | null, length: number, graphql_type: GraphQlType, guides: { __typename?: 'SequenceFeaturesConnection', totalCount: number } } | { __typename?: 'Guide', id: string, name?: string | null, altnames?: Array<string> | null, description?: string | null, length: number, graphql_type: GraphQlType, guides: { __typename?: 'SequenceFeaturesConnection', totalCount: number } } | { __typename?: 'Target', id: string, name?: string | null, altnames?: Array<string> | null, description?: string | null, length: number, graphql_type: GraphQlType, guides: { __typename?: 'SequenceFeaturesConnection', totalCount: number } }> }> }>, modifications: Array<{ __typename?: 'Modification', id: string, name: string, symbol_label: string, type_short_label?: string | null, type?: ModifType | null, guidesAggregate?: { __typename?: 'ModificationGuideGuidesAggregationSelection', count: number } | null }>, allModificationsCount: { __typename?: 'ModificationAggregateSelection', count: number }, nonOrphanModificationsCount: { __typename?: 'ModificationAggregateSelection', count: number }, targets: Array<{ __typename?: 'Target', id: string, name?: string | null, class: SequenceClass, unit?: string | null, modificationsAggregate?: { __typename?: 'TargetModificationModificationsAggregationSelection', count: number } | null }>, allTargetsCount: { __typename?: 'TargetAggregateSelection', count: number }, guides: Array<{ __typename?: 'Guide', id: string, name?: string | null, class: SequenceClass, subclass: GuideClass, subclass_label: string, chromosomeConnection: { __typename?: 'GuideParentConnection', edges: Array<{ __typename?: 'GuideParentRelationship', properties: { __typename?: 'HasFeature', start: number, end: number }, node: { __typename?: 'Chromosome', id: string } | { __typename?: 'GenericSequence', id: string } | { __typename?: 'Guide', id: string } | { __typename?: 'Target', id: string } }> }, modificationsAggregate?: { __typename?: 'GuideModificationModificationsAggregationSelection', count: number } | null }>, allGuidesCount: { __typename?: 'GuideAggregateSelection', count: number }, nonOrphanGuidesCount: { __typename?: 'GuideAggregateSelection', count: number } };

export type ModificationByIdQueryQueryVariables = Exact<{
  id?: InputMaybe<Scalars['ID']['input']>;
}>;


export type ModificationByIdQueryQuery = { __typename?: 'Query', modifications: Array<{ __typename?: 'Modification', id: string, name: string, type?: ModifType | null, type_short_label?: string | null, symbol: string, symbol_label: string, position: number, target: { __typename?: 'Target', id: string, name?: string | null, class: SequenceClass, unit?: string | null, genome?: { __typename?: 'Genome', organism?: { __typename?: 'Organism', id: number, label: string } | null } | null }, interactions: Array<{ __typename?: 'Interaction', duplexes: Array<{ __typename?: 'Duplex', index: number, primaryStrandsConnection: { __typename?: 'DuplexStrandsConnection', edges: Array<{ __typename?: 'DuplexStrandsRelationship', properties: { __typename?: 'BindIn', start: number, end: number, primary?: boolean | null }, node: { __typename?: 'GenericSequence', seq?: string | null, parentConnection: { __typename?: 'GenericSequenceParentConnection', edges: Array<{ __typename?: 'GenericSequenceParentRelationship', properties: { __typename?: 'HasFeature', start: number, end: number } }> } } }> }, secondaryStrandsConnection: { __typename?: 'DuplexStrandsConnection', edges: Array<{ __typename?: 'DuplexStrandsRelationship', properties: { __typename?: 'BindIn', start: number, end: number, primary?: boolean | null }, node: { __typename?: 'GenericSequence', seq?: string | null, parentConnection: { __typename?: 'GenericSequenceParentConnection', edges: Array<{ __typename?: 'GenericSequenceParentRelationship', properties: { __typename?: 'HasFeature', start: number, end: number } }> } } }> } }>, guide: { __typename?: 'Guide', id: string, name?: string | null, subclass_label: string, class: SequenceClass }, target: { __typename?: 'Target', id: string, name?: string | null, unit?: string | null, class: SequenceClass } }> }>, guides: Array<{ __typename?: 'Guide', id: string, name?: string | null, class: SequenceClass, chromosome?: { __typename?: 'Chromosome', name?: string | null } | { __typename?: 'GenericSequence', name?: string | null } | { __typename?: 'Guide', name?: string | null } | { __typename?: 'Target', name?: string | null } | null }> };

export type GuideByIdQueryQueryVariables = Exact<{
  id?: InputMaybe<Scalars['ID']['input']>;
}>;


export type GuideByIdQueryQuery = { __typename?: 'Query', guides: Array<{ __typename?: 'Guide', id: string, name?: string | null, altnames?: Array<string> | null, description?: string | null, length: number, class: SequenceClass, subclass_label: string, host_genes: Array<string>, seq?: string | null, chebi_id?: string | null, so_id?: string | null, chromosomeConnection: { __typename?: 'GuideParentConnection', edges: Array<{ __typename?: 'GuideParentRelationship', properties: { __typename?: 'HasFeature', start: number, end: number, strand?: Strand | null }, node: { __typename?: 'Chromosome', id: string, name?: string | null, length: number, graphql_type: GraphQlType } | { __typename?: 'GenericSequence', id: string, name?: string | null, length: number, graphql_type: GraphQlType } | { __typename?: 'Guide', id: string, name?: string | null, length: number, graphql_type: GraphQlType } | { __typename?: 'Target', id: string, name?: string | null, length: number, graphql_type: GraphQlType } }> }, genome?: { __typename?: 'Genome', organism?: { __typename?: 'Organism', id: number, label: string } | null } | null, boxConnections: { __typename?: 'SequenceFeaturesConnection', edges: Array<{ __typename?: 'SequenceFeaturesRelationship', properties: { __typename?: 'HasFeature', start: number, end: number }, node: { __typename?: 'Chromosome', id: string, annotation?: AnnotationType | null } | { __typename?: 'GenericSequence', id: string, annotation?: AnnotationType | null } | { __typename?: 'Guide', id: string, annotation?: AnnotationType | null } | { __typename?: 'Target', id: string, annotation?: AnnotationType | null } }> }, modifications: Array<{ __typename?: 'Modification', id: string, name: string, type?: ModifType | null }>, modificationsAggregate?: { __typename?: 'GuideModificationModificationsAggregationSelection', count: number } | null, interactions: Array<{ __typename?: 'Interaction', duplexes: Array<{ __typename?: 'Duplex', index: number, primaryStrandsConnection: { __typename?: 'DuplexStrandsConnection', edges: Array<{ __typename?: 'DuplexStrandsRelationship', properties: { __typename?: 'BindIn', start: number, end: number, primary?: boolean | null }, node: { __typename?: 'GenericSequence', seq?: string | null, parentConnection: { __typename?: 'GenericSequenceParentConnection', edges: Array<{ __typename?: 'GenericSequenceParentRelationship', properties: { __typename?: 'HasFeature', start: number, end: number } }> } } }> }, secondaryStrandsConnection: { __typename?: 'DuplexStrandsConnection', edges: Array<{ __typename?: 'DuplexStrandsRelationship', properties: { __typename?: 'BindIn', start: number, end: number, primary?: boolean | null }, node: { __typename?: 'GenericSequence', seq?: string | null, parentConnection: { __typename?: 'GenericSequenceParentConnection', edges: Array<{ __typename?: 'GenericSequenceParentRelationship', properties: { __typename?: 'HasFeature', start: number, end: number } }> } } }> } }>, modification: { __typename?: 'Modification', id: string, name: string, position: number, symbol: string, symbol_label: string, type?: ModifType | null, type_short_label?: string | null }, target: { __typename?: 'Target', id: string, name?: string | null, class: SequenceClass, unit?: string | null } }>, cluster?: { __typename?: 'Cluster', id: string } | null, clusterAggregate?: { __typename?: 'GuideClusterClusterAggregationSelection', count: number } | null, isoform?: { __typename?: 'Isoform', guides: Array<{ __typename?: 'Guide', id: string, name?: string | null }>, guidesAggregate?: { __typename?: 'IsoformGuideGuidesAggregationSelection', count: number } | null } | null, isoformAggregate?: { __typename?: 'GuideIsoformIsoformAggregationSelection', count: number } | null }>, targets: Array<{ __typename?: 'Target', id: string, name?: string | null, class: SequenceClass, unit?: string | null }> };

export type TargetByIdQueryQueryVariables = Exact<{
  id?: InputMaybe<Scalars['ID']['input']>;
}>;


export type TargetByIdQueryQuery = { __typename?: 'Query', targets: Array<{ __typename?: 'Target', id: string, name?: string | null, altnames?: Array<string> | null, description?: string | null, length: number, class: SequenceClass, unit?: string | null, seq?: string | null, chebi_id?: string | null, so_id?: string | null, url?: string | null, secondary_struct_file?: string | null, chromosomeConnection: { __typename?: 'TargetParentConnection', edges: Array<{ __typename?: 'TargetParentRelationship', properties: { __typename?: 'HasFeature', start: number, end: number, strand?: Strand | null }, node: { __typename?: 'Chromosome', name?: string | null, graphql_type: GraphQlType } | { __typename?: 'GenericSequence', name?: string | null, graphql_type: GraphQlType } | { __typename?: 'Guide', name?: string | null, graphql_type: GraphQlType } | { __typename?: 'Target', name?: string | null, graphql_type: GraphQlType } }> }, genome?: { __typename?: 'Genome', organism?: { __typename?: 'Organism', id: number, label: string } | null } | null, modifications: Array<{ __typename?: 'Modification', id: string, name: string, position: number, type?: ModifType | null, symbol: string }>, modificationsAggregate?: { __typename?: 'TargetModificationModificationsAggregationSelection', count: number } | null, interactions: Array<{ __typename?: 'Interaction', duplexes: Array<{ __typename?: 'Duplex', index: number, primaryStrandsConnection: { __typename?: 'DuplexStrandsConnection', edges: Array<{ __typename?: 'DuplexStrandsRelationship', properties: { __typename?: 'BindIn', start: number, end: number, primary?: boolean | null }, node: { __typename?: 'GenericSequence', seq?: string | null, parentConnection: { __typename?: 'GenericSequenceParentConnection', edges: Array<{ __typename?: 'GenericSequenceParentRelationship', properties: { __typename?: 'HasFeature', start: number, end: number } }> } } }> }, secondaryStrandsConnection: { __typename?: 'DuplexStrandsConnection', edges: Array<{ __typename?: 'DuplexStrandsRelationship', properties: { __typename?: 'BindIn', start: number, end: number, primary?: boolean | null }, node: { __typename?: 'GenericSequence', seq?: string | null, parentConnection: { __typename?: 'GenericSequenceParentConnection', edges: Array<{ __typename?: 'GenericSequenceParentRelationship', properties: { __typename?: 'HasFeature', start: number, end: number } }> } } }> } }>, modification: { __typename?: 'Modification', id: string, name: string, position: number, symbol: string, symbol_label: string, type?: ModifType | null, type_short_label?: string | null }, guide: { __typename?: 'Guide', id: string, name?: string | null, subclass_label: string, class: SequenceClass } }> }>, guides: Array<{ __typename?: 'Guide', id: string, name?: string | null, class: SequenceClass, chromosome?: { __typename?: 'Chromosome', name?: string | null } | { __typename?: 'GenericSequence', name?: string | null } | { __typename?: 'Guide', name?: string | null } | { __typename?: 'Target', name?: string | null } | null }> };

export type ClusterByIdQueryQueryVariables = Exact<{
  id?: InputMaybe<Scalars['ID']['input']>;
}>;


export type ClusterByIdQueryQuery = { __typename?: 'Query', clusters: Array<{ __typename?: 'Cluster', id: string, guides: Array<{ __typename?: 'Guide', id: string, name?: string | null, class: SequenceClass, subclass_label: string, modificationsAggregate?: { __typename?: 'GuideModificationModificationsAggregationSelection', count: number } | null, chromosomeConnection: { __typename?: 'GuideParentConnection', edges: Array<{ __typename?: 'GuideParentRelationship', properties: { __typename?: 'HasFeature', start: number, end: number } }> } }>, guidesAggregate?: { __typename?: 'ClusterGuideGuidesAggregationSelection', count: number } | null, referenceGuide: Array<{ __typename?: 'Guide', chromosome?: { __typename?: 'Chromosome', id: string, name?: string | null } | { __typename?: 'GenericSequence', id: string, name?: string | null } | { __typename?: 'Guide', id: string, name?: string | null } | { __typename?: 'Target', id: string, name?: string | null } | null, genome?: { __typename?: 'Genome', organism?: { __typename?: 'Organism', label: string, id: number } | null } | null }> }> };

export type TargetAlignmentQueryQueryVariables = Exact<{
  targetName?: InputMaybe<Scalars['String']['input']>;
  organismIds?: InputMaybe<Array<Scalars['Int']['input']> | Scalars['Int']['input']>;
}>;


export type TargetAlignmentQueryQuery = { __typename?: 'Query', targetBase: Array<{ __typename?: 'Target', name?: string | null, unit?: string | null, genome?: { __typename?: 'Genome', organism?: { __typename?: 'Organism', id: number, label: string } | null } | null }>, organismsBase: Array<{ __typename?: 'Organism', label: string, id: number }>, organisms: Array<{ __typename?: 'Organism', id: number }>, selectableTargets: Array<{ __typename?: 'Target', id: string, genome?: { __typename?: 'Genome', organism?: { __typename?: 'Organism', shortlabel: string } | null } | null }> };

export type GuideAlignmentQueryQueryVariables = Exact<{
  guideSubclasses?: InputMaybe<Array<GuideClass> | GuideClass>;
  guideName?: InputMaybe<Scalars['String']['input']>;
  organismIds?: InputMaybe<Array<Scalars['Int']['input']> | Scalars['Int']['input']>;
}>;


export type GuideAlignmentQueryQuery = { __typename?: 'Query', guideBase: Array<{ __typename?: 'Guide', name?: string | null, subclass: GuideClass, subclass_label: string, genome?: { __typename?: 'Genome', organism?: { __typename?: 'Organism', id: number } | null } | null }>, guideNamesFilteredBySubclass: Array<{ __typename?: 'Guide', name?: string | null, genome?: { __typename?: 'Genome', organism?: { __typename?: 'Organism', id: number } | null } | null }>, organismsBase: Array<{ __typename?: 'Organism', label: string, id: number }>, organisms: Array<{ __typename?: 'Organism', id: number }>, selectableGuides: Array<{ __typename?: 'Guide', id: string, genome?: { __typename?: 'Genome', organism?: { __typename?: 'Organism', shortlabel: string } | null } | null }> };

export type DatabaseStatisticsQueryQueryVariables = Exact<{ [key: string]: never; }>;


export type DatabaseStatisticsQueryQuery = { __typename?: 'Query', organismsAggregate: { __typename?: 'OrganismAggregateSelection', count: number }, allModifications: Array<{ __typename?: 'Modification', type?: ModifType | null }>, allModificationsCount: { __typename?: 'ModificationAggregateSelection', count: number }, nonOrphanModificationsCount: { __typename?: 'ModificationAggregateSelection', count: number }, allGuides: Array<{ __typename?: 'Guide', subclass: GuideClass }>, allGuidesCount: { __typename?: 'GuideAggregateSelection', count: number }, nonOrphanGuidesCount: { __typename?: 'GuideAggregateSelection', count: number } };

export type LegalDocumentListQueryQueryVariables = Exact<{ [key: string]: never; }>;


export type LegalDocumentListQueryQuery = { __typename?: 'Query', documents: Array<{ __typename?: 'Document', id: string, menu_label: string }> };

export type DocumentByIdQueryQueryVariables = Exact<{
  id?: InputMaybe<Scalars['ID']['input']>;
}>;


export type DocumentByIdQueryQuery = { __typename?: 'Query', documents: Array<{ __typename?: 'Document', id: string, menu_label: string, content: string }> };


export const HomeQueryDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"homeQuery"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","alias":{"kind":"Name","value":"modificationTypes"},"name":{"kind":"Name","value":"modifications"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"type"}},{"kind":"Field","name":{"kind":"Name","value":"type_short_label"}}]}},{"kind":"Field","alias":{"kind":"Name","value":"guideSubclasses"},"name":{"kind":"Name","value":"guides"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"subclass"}},{"kind":"Field","name":{"kind":"Name","value":"subclass_label"}}]}},{"kind":"Field","alias":{"kind":"Name","value":"targetUnits"},"name":{"kind":"Name","value":"targets"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"unit"}}]}},{"kind":"Field","name":{"kind":"Name","value":"organisms"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"label"}}]}},{"kind":"Field","name":{"kind":"Name","value":"organismsAggregate"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"count"}}]}}]}}]} as unknown as DocumentNode<HomeQueryQuery, HomeQueryQueryVariables>;
export const ModificationAndTargetSelectionQueryDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"modificationAndTargetSelectionQuery"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"modificationTypes"}},"type":{"kind":"ListType","type":{"kind":"NamedType","name":{"kind":"Name","value":"ModifType"}}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"targetNames"}},"type":{"kind":"ListType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"organismIds"}},"type":{"kind":"ListType","type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"Int"}}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","alias":{"kind":"Name","value":"targetsBase"},"name":{"kind":"Name","value":"targets"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"unit"}}]}},{"kind":"Field","name":{"kind":"Name","value":"targets"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"where"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"modifications_SOME"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"type_IN"},"value":{"kind":"Variable","name":{"kind":"Name","value":"modificationTypes"}}}]}},{"kind":"ObjectField","name":{"kind":"Name","value":"genome"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"organism"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"id_IN"},"value":{"kind":"Variable","name":{"kind":"Name","value":"organismIds"}}}]}}]}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"id"}}]}},{"kind":"Field","alias":{"kind":"Name","value":"modificationsBase"},"name":{"kind":"Name","value":"modifications"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"type_label"}},{"kind":"Field","name":{"kind":"Name","value":"type"}}]}},{"kind":"Field","name":{"kind":"Name","value":"modifications"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"where"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"target"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"name_IN"},"value":{"kind":"Variable","name":{"kind":"Name","value":"targetNames"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"genome"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"organism"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"id_IN"},"value":{"kind":"Variable","name":{"kind":"Name","value":"organismIds"}}}]}}]}}]}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"type"}}]}},{"kind":"Field","alias":{"kind":"Name","value":"organismsBase"},"name":{"kind":"Name","value":"organisms"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"label"}},{"kind":"Field","name":{"kind":"Name","value":"id"}}]}},{"kind":"Field","name":{"kind":"Name","value":"organisms"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"where"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"tableEntries_SOME"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"modification"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"type_IN"},"value":{"kind":"Variable","name":{"kind":"Name","value":"modificationTypes"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"target"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"name_IN"},"value":{"kind":"Variable","name":{"kind":"Name","value":"targetNames"}}}]}}]}}]}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}}]}}]}}]} as unknown as DocumentNode<ModificationAndTargetSelectionQueryQuery, ModificationAndTargetSelectionQueryQueryVariables>;
export const GuideSelectionQueryDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"guideSelectionQuery"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"guideSubclasses"}},"type":{"kind":"ListType","type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"GuideClass"}}}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"targetNames"}},"type":{"kind":"ListType","type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"modificationTypes"}},"type":{"kind":"ListType","type":{"kind":"NamedType","name":{"kind":"Name","value":"ModifType"}}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"organismIds"}},"type":{"kind":"ListType","type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"Int"}}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","alias":{"kind":"Name","value":"guidesBase"},"name":{"kind":"Name","value":"guides"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"subclass"}},{"kind":"Field","name":{"kind":"Name","value":"subclass_label"}}]}},{"kind":"Field","name":{"kind":"Name","value":"guides"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"where"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"modifications_SOME"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"target"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"name_IN"},"value":{"kind":"Variable","name":{"kind":"Name","value":"targetNames"}}}]}},{"kind":"ObjectField","name":{"kind":"Name","value":"type_IN"},"value":{"kind":"Variable","name":{"kind":"Name","value":"modificationTypes"}}}]}},{"kind":"ObjectField","name":{"kind":"Name","value":"genome"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"organism"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"id_IN"},"value":{"kind":"Variable","name":{"kind":"Name","value":"organismIds"}}}]}}]}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"subclass"}}]}},{"kind":"Field","alias":{"kind":"Name","value":"targetsBase"},"name":{"kind":"Name","value":"targets"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"unit"}}]}},{"kind":"Field","name":{"kind":"Name","value":"targets"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"where"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"modifications_SOME"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"guides_SOME"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"subclass_IN"},"value":{"kind":"Variable","name":{"kind":"Name","value":"guideSubclasses"}}}]}},{"kind":"ObjectField","name":{"kind":"Name","value":"type_IN"},"value":{"kind":"Variable","name":{"kind":"Name","value":"modificationTypes"}}}]}},{"kind":"ObjectField","name":{"kind":"Name","value":"genome"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"organism"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"id_IN"},"value":{"kind":"Variable","name":{"kind":"Name","value":"organismIds"}}}]}}]}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"name"}}]}},{"kind":"Field","alias":{"kind":"Name","value":"modificationsBase"},"name":{"kind":"Name","value":"modifications"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"type_label"}},{"kind":"Field","name":{"kind":"Name","value":"type"}}]}},{"kind":"Field","name":{"kind":"Name","value":"modifications"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"where"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"target"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"name_IN"},"value":{"kind":"Variable","name":{"kind":"Name","value":"targetNames"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"genome"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"organism"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"id_IN"},"value":{"kind":"Variable","name":{"kind":"Name","value":"organismIds"}}}]}}]}}]}},{"kind":"ObjectField","name":{"kind":"Name","value":"guides_SOME"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"subclass_IN"},"value":{"kind":"Variable","name":{"kind":"Name","value":"guideSubclasses"}}}]}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"type"}}]}},{"kind":"Field","alias":{"kind":"Name","value":"organismsBase"},"name":{"kind":"Name","value":"organisms"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"label"}},{"kind":"Field","name":{"kind":"Name","value":"id"}}]}},{"kind":"Field","name":{"kind":"Name","value":"organisms"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"where"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"tableEntries_SOME"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"modification"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"type_IN"},"value":{"kind":"Variable","name":{"kind":"Name","value":"modificationTypes"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"target"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"name_IN"},"value":{"kind":"Variable","name":{"kind":"Name","value":"targetNames"}}}]}}]}},{"kind":"ObjectField","name":{"kind":"Name","value":"guide"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"subclass_IN"},"value":{"kind":"Variable","name":{"kind":"Name","value":"guideSubclasses"}}}]}}]}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}}]}}]}}]} as unknown as DocumentNode<GuideSelectionQueryQuery, GuideSelectionQueryQueryVariables>;
export const TableEntriesQueryDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"tableEntriesQuery"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"tableEntries"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"modification"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"position"}},{"kind":"Field","name":{"kind":"Name","value":"symbol"}},{"kind":"Field","name":{"kind":"Name","value":"symbol_label"}},{"kind":"Field","name":{"kind":"Name","value":"type"}},{"kind":"Field","name":{"kind":"Name","value":"type_short_label"}}]}},{"kind":"Field","name":{"kind":"Name","value":"guide"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"class"}},{"kind":"Field","name":{"kind":"Name","value":"subclass"}},{"kind":"Field","name":{"kind":"Name","value":"subclass_label"}},{"kind":"Field","name":{"kind":"Name","value":"length"}},{"kind":"Field","name":{"kind":"Name","value":"seq"}},{"kind":"Field","alias":{"kind":"Name","value":"chromosomeConnection"},"name":{"kind":"Name","value":"parentConnection"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"where"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"node"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"graphql_type"},"value":{"kind":"EnumValue","value":"Chromosome"}}]}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"edges"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"properties"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"start"}},{"kind":"Field","name":{"kind":"Name","value":"end"}},{"kind":"Field","name":{"kind":"Name","value":"strand"}}]}},{"kind":"Field","name":{"kind":"Name","value":"node"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"length"}}]}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"host_genes"}}]}},{"kind":"Field","name":{"kind":"Name","value":"target"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"class"}},{"kind":"Field","name":{"kind":"Name","value":"unit"}},{"kind":"Field","name":{"kind":"Name","value":"length"}},{"kind":"Field","alias":{"kind":"Name","value":"chromosomeConnection"},"name":{"kind":"Name","value":"parentConnection"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"where"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"node"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"graphql_type"},"value":{"kind":"EnumValue","value":"Chromosome"}}]}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"edges"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"properties"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"start"}},{"kind":"Field","name":{"kind":"Name","value":"end"}},{"kind":"Field","name":{"kind":"Name","value":"strand"}}]}},{"kind":"Field","name":{"kind":"Name","value":"node"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}}]}}]}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"organism"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"label"}},{"kind":"Field","name":{"kind":"Name","value":"name"}}]}}]}},{"kind":"Field","alias":{"kind":"Name","value":"modificationsBase"},"name":{"kind":"Name","value":"modifications"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"symbol"}},{"kind":"Field","name":{"kind":"Name","value":"symbol_label"}},{"kind":"Field","name":{"kind":"Name","value":"type"}},{"kind":"Field","name":{"kind":"Name","value":"type_short_label"}}]}},{"kind":"Field","alias":{"kind":"Name","value":"targetsBase"},"name":{"kind":"Name","value":"targets"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"unit"}},{"kind":"Field","name":{"kind":"Name","value":"class"}}]}},{"kind":"Field","alias":{"kind":"Name","value":"guidesBase"},"name":{"kind":"Name","value":"guides"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"subclass"}},{"kind":"Field","name":{"kind":"Name","value":"subclass_label"}},{"kind":"Field","name":{"kind":"Name","value":"class"}}]}},{"kind":"Field","alias":{"kind":"Name","value":"organismsBase"},"name":{"kind":"Name","value":"organisms"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"label"}}]}}]}}]} as unknown as DocumentNode<TableEntriesQueryQuery, TableEntriesQueryQueryVariables>;
export const OrganismByIdQueryDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"organismByIdQuery"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"id"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"Int"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"organisms"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"where"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"id"}}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"label"}},{"kind":"Field","name":{"kind":"Name","value":"shortname"}},{"kind":"Field","name":{"kind":"Name","value":"genomes"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"sequences"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"where"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"graphql_type"},"value":{"kind":"EnumValue","value":"Chromosome"}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"altnames"}},{"kind":"Field","name":{"kind":"Name","value":"description"}},{"kind":"Field","name":{"kind":"Name","value":"length"}},{"kind":"Field","name":{"kind":"Name","value":"graphql_type"}},{"kind":"Field","alias":{"kind":"Name","value":"guides"},"name":{"kind":"Name","value":"featuresConnection"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"where"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"node"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"graphql_type"},"value":{"kind":"EnumValue","value":"Guide"}}]}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"totalCount"}}]}}]}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"modifications"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"where"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"target"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"genome"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"organism"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"id"}}}]}}]}}]}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"symbol_label"}},{"kind":"Field","name":{"kind":"Name","value":"type_short_label"}},{"kind":"Field","name":{"kind":"Name","value":"type"}},{"kind":"Field","name":{"kind":"Name","value":"guidesAggregate"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"count"}}]}}]}},{"kind":"Field","alias":{"kind":"Name","value":"allModificationsCount"},"name":{"kind":"Name","value":"modificationsAggregate"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"where"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"target"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"genome"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"organism"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"id"}}}]}}]}}]}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"count"}}]}},{"kind":"Field","alias":{"kind":"Name","value":"nonOrphanModificationsCount"},"name":{"kind":"Name","value":"modificationsAggregate"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"where"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"guidesAggregate"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"count_GT"},"value":{"kind":"IntValue","value":"0"}}]}},{"kind":"ObjectField","name":{"kind":"Name","value":"target"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"genome"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"organism"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"id"}}}]}}]}}]}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"count"}}]}},{"kind":"Field","name":{"kind":"Name","value":"targets"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"where"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"genome"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"organism"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"id"}}}]}}]}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"class"}},{"kind":"Field","name":{"kind":"Name","value":"unit"}},{"kind":"Field","name":{"kind":"Name","value":"modificationsAggregate"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"count"}}]}}]}},{"kind":"Field","alias":{"kind":"Name","value":"allTargetsCount"},"name":{"kind":"Name","value":"targetsAggregate"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"where"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"genome"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"organism"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"id"}}}]}}]}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"count"}}]}},{"kind":"Field","name":{"kind":"Name","value":"guides"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"where"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"genome"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"organism"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"id"}}}]}}]}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"class"}},{"kind":"Field","name":{"kind":"Name","value":"subclass"}},{"kind":"Field","name":{"kind":"Name","value":"subclass_label"}},{"kind":"Field","alias":{"kind":"Name","value":"chromosomeConnection"},"name":{"kind":"Name","value":"parentConnection"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"where"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"node"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"graphql_type"},"value":{"kind":"EnumValue","value":"Chromosome"}}]}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"edges"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"properties"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"start"}},{"kind":"Field","name":{"kind":"Name","value":"end"}}]}},{"kind":"Field","name":{"kind":"Name","value":"node"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}}]}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"modificationsAggregate"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"count"}}]}}]}},{"kind":"Field","alias":{"kind":"Name","value":"allGuidesCount"},"name":{"kind":"Name","value":"guidesAggregate"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"where"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"genome"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"organism"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"id"}}}]}}]}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"count"}}]}},{"kind":"Field","alias":{"kind":"Name","value":"nonOrphanGuidesCount"},"name":{"kind":"Name","value":"guidesAggregate"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"where"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"modificationsAggregate"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"count_GT"},"value":{"kind":"IntValue","value":"0"}}]}},{"kind":"ObjectField","name":{"kind":"Name","value":"genome"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"organism"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"id"}}}]}}]}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"count"}}]}}]}}]} as unknown as DocumentNode<OrganismByIdQueryQuery, OrganismByIdQueryQueryVariables>;
export const ModificationByIdQueryDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"modificationByIdQuery"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"id"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"ID"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"modifications"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"where"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"id"}}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"type"}},{"kind":"Field","name":{"kind":"Name","value":"type_short_label"}},{"kind":"Field","name":{"kind":"Name","value":"symbol"}},{"kind":"Field","name":{"kind":"Name","value":"symbol_label"}},{"kind":"Field","name":{"kind":"Name","value":"position"}},{"kind":"Field","name":{"kind":"Name","value":"target"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"class"}},{"kind":"Field","name":{"kind":"Name","value":"unit"}},{"kind":"Field","name":{"kind":"Name","value":"genome"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"organism"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"label"}}]}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"interactions"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"duplexes"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","alias":{"kind":"Name","value":"primaryStrandsConnection"},"name":{"kind":"Name","value":"strandsConnection"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"where"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"edge"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"primary"},"value":{"kind":"BooleanValue","value":true}}]}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"edges"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"properties"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"start"}},{"kind":"Field","name":{"kind":"Name","value":"end"}},{"kind":"Field","name":{"kind":"Name","value":"primary"}}]}},{"kind":"Field","name":{"kind":"Name","value":"node"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"seq"}},{"kind":"Field","name":{"kind":"Name","value":"parentConnection"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"edges"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"properties"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"start"}},{"kind":"Field","name":{"kind":"Name","value":"end"}}]}}]}}]}}]}}]}}]}},{"kind":"Field","alias":{"kind":"Name","value":"secondaryStrandsConnection"},"name":{"kind":"Name","value":"strandsConnection"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"where"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"edge"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"primary"},"value":{"kind":"BooleanValue","value":false}}]}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"edges"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"properties"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"start"}},{"kind":"Field","name":{"kind":"Name","value":"end"}},{"kind":"Field","name":{"kind":"Name","value":"primary"}}]}},{"kind":"Field","name":{"kind":"Name","value":"node"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"seq"}},{"kind":"Field","name":{"kind":"Name","value":"parentConnection"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"edges"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"properties"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"start"}},{"kind":"Field","name":{"kind":"Name","value":"end"}}]}}]}}]}}]}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"index"}}]}},{"kind":"Field","name":{"kind":"Name","value":"guide"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"subclass_label"}},{"kind":"Field","name":{"kind":"Name","value":"class"}}]}},{"kind":"Field","name":{"kind":"Name","value":"target"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"unit"}},{"kind":"Field","name":{"kind":"Name","value":"class"}}]}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"guides"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"where"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"modifications_SOME"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"id"}}}]}}]}},{"kind":"Argument","name":{"kind":"Name","value":"options"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"sort"},"value":{"kind":"ListValue","values":[{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"id"},"value":{"kind":"EnumValue","value":"ASC"}}]}]}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"class"}},{"kind":"Field","alias":{"kind":"Name","value":"chromosome"},"name":{"kind":"Name","value":"parent"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"where"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"graphql_type"},"value":{"kind":"EnumValue","value":"Chromosome"}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"name"}}]}}]}}]}}]} as unknown as DocumentNode<ModificationByIdQueryQuery, ModificationByIdQueryQueryVariables>;
export const GuideByIdQueryDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"guideByIdQuery"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"id"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"ID"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"guides"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"where"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"id"}}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"altnames"}},{"kind":"Field","name":{"kind":"Name","value":"description"}},{"kind":"Field","name":{"kind":"Name","value":"length"}},{"kind":"Field","name":{"kind":"Name","value":"class"}},{"kind":"Field","name":{"kind":"Name","value":"subclass_label"}},{"kind":"Field","alias":{"kind":"Name","value":"chromosomeConnection"},"name":{"kind":"Name","value":"parentConnection"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"where"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"node"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"graphql_type"},"value":{"kind":"EnumValue","value":"Chromosome"}}]}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"edges"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"properties"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"start"}},{"kind":"Field","name":{"kind":"Name","value":"end"}},{"kind":"Field","name":{"kind":"Name","value":"strand"}}]}},{"kind":"Field","name":{"kind":"Name","value":"node"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"length"}},{"kind":"Field","name":{"kind":"Name","value":"graphql_type"}}]}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"host_genes"}},{"kind":"Field","name":{"kind":"Name","value":"seq"}},{"kind":"Field","name":{"kind":"Name","value":"genome"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"organism"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"label"}}]}}]}},{"kind":"Field","alias":{"kind":"Name","value":"boxConnections"},"name":{"kind":"Name","value":"featuresConnection"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"where"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"NOT"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"node"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"class"},"value":{"kind":"EnumValue","value":"DuplexFragment"}}]}}]}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"edges"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"properties"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"InlineFragment","typeCondition":{"kind":"NamedType","name":{"kind":"Name","value":"HasFeature"}},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"start"}},{"kind":"Field","name":{"kind":"Name","value":"end"}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"node"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"annotation"}}]}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"modifications"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"options"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"sort"},"value":{"kind":"ListValue","values":[{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"position"},"value":{"kind":"EnumValue","value":"ASC"}}]}]}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"type"}}]}},{"kind":"Field","name":{"kind":"Name","value":"modificationsAggregate"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"count"}}]}},{"kind":"Field","name":{"kind":"Name","value":"interactions"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"duplexes"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","alias":{"kind":"Name","value":"primaryStrandsConnection"},"name":{"kind":"Name","value":"strandsConnection"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"where"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"edge"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"primary"},"value":{"kind":"BooleanValue","value":true}}]}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"edges"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"properties"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"start"}},{"kind":"Field","name":{"kind":"Name","value":"end"}},{"kind":"Field","name":{"kind":"Name","value":"primary"}}]}},{"kind":"Field","name":{"kind":"Name","value":"node"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"seq"}},{"kind":"Field","name":{"kind":"Name","value":"parentConnection"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"edges"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"properties"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"start"}},{"kind":"Field","name":{"kind":"Name","value":"end"}}]}}]}}]}}]}}]}}]}},{"kind":"Field","alias":{"kind":"Name","value":"secondaryStrandsConnection"},"name":{"kind":"Name","value":"strandsConnection"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"where"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"edge"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"primary"},"value":{"kind":"BooleanValue","value":false}}]}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"edges"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"properties"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"start"}},{"kind":"Field","name":{"kind":"Name","value":"end"}},{"kind":"Field","name":{"kind":"Name","value":"primary"}}]}},{"kind":"Field","name":{"kind":"Name","value":"node"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"seq"}},{"kind":"Field","name":{"kind":"Name","value":"parentConnection"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"edges"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"properties"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"start"}},{"kind":"Field","name":{"kind":"Name","value":"end"}}]}}]}}]}}]}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"index"}}]}},{"kind":"Field","name":{"kind":"Name","value":"modification"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"position"}},{"kind":"Field","name":{"kind":"Name","value":"symbol"}},{"kind":"Field","name":{"kind":"Name","value":"symbol_label"}},{"kind":"Field","name":{"kind":"Name","value":"type"}},{"kind":"Field","name":{"kind":"Name","value":"type_short_label"}}]}},{"kind":"Field","name":{"kind":"Name","value":"target"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"class"}},{"kind":"Field","name":{"kind":"Name","value":"unit"}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"cluster"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}}]}},{"kind":"Field","name":{"kind":"Name","value":"clusterAggregate"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"count"}}]}},{"kind":"Field","name":{"kind":"Name","value":"isoform"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"guides"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}}]}},{"kind":"Field","name":{"kind":"Name","value":"guidesAggregate"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"count"}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"isoformAggregate"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"count"}}]}},{"kind":"Field","name":{"kind":"Name","value":"chebi_id"}},{"kind":"Field","name":{"kind":"Name","value":"so_id"}}]}},{"kind":"Field","name":{"kind":"Name","value":"targets"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"where"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"modifications_SOME"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"guides_SOME"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"id"}}}]}}]}}]}},{"kind":"Argument","name":{"kind":"Name","value":"options"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"sort"},"value":{"kind":"ListValue","values":[{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"name"},"value":{"kind":"EnumValue","value":"ASC"}}]}]}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"class"}},{"kind":"Field","name":{"kind":"Name","value":"unit"}}]}}]}}]} as unknown as DocumentNode<GuideByIdQueryQuery, GuideByIdQueryQueryVariables>;
export const TargetByIdQueryDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"targetByIdQuery"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"id"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"ID"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"targets"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"where"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"id"}}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"altnames"}},{"kind":"Field","name":{"kind":"Name","value":"description"}},{"kind":"Field","name":{"kind":"Name","value":"length"}},{"kind":"Field","name":{"kind":"Name","value":"class"}},{"kind":"Field","name":{"kind":"Name","value":"unit"}},{"kind":"Field","alias":{"kind":"Name","value":"chromosomeConnection"},"name":{"kind":"Name","value":"parentConnection"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"where"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"node"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"graphql_type"},"value":{"kind":"EnumValue","value":"Chromosome"}}]}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"edges"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"properties"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"start"}},{"kind":"Field","name":{"kind":"Name","value":"end"}},{"kind":"Field","name":{"kind":"Name","value":"strand"}}]}},{"kind":"Field","name":{"kind":"Name","value":"node"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"graphql_type"}}]}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"seq"}},{"kind":"Field","name":{"kind":"Name","value":"genome"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"organism"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"label"}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"modifications"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"options"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"sort"},"value":{"kind":"ListValue","values":[{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"position"},"value":{"kind":"EnumValue","value":"ASC"}}]}]}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"position"}},{"kind":"Field","name":{"kind":"Name","value":"type"}},{"kind":"Field","name":{"kind":"Name","value":"symbol"}}]}},{"kind":"Field","name":{"kind":"Name","value":"modificationsAggregate"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"count"}}]}},{"kind":"Field","name":{"kind":"Name","value":"interactions"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"duplexes"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","alias":{"kind":"Name","value":"primaryStrandsConnection"},"name":{"kind":"Name","value":"strandsConnection"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"where"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"edge"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"primary"},"value":{"kind":"BooleanValue","value":true}}]}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"edges"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"properties"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"start"}},{"kind":"Field","name":{"kind":"Name","value":"end"}},{"kind":"Field","name":{"kind":"Name","value":"primary"}}]}},{"kind":"Field","name":{"kind":"Name","value":"node"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"seq"}},{"kind":"Field","name":{"kind":"Name","value":"parentConnection"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"edges"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"properties"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"start"}},{"kind":"Field","name":{"kind":"Name","value":"end"}}]}}]}}]}}]}}]}}]}},{"kind":"Field","alias":{"kind":"Name","value":"secondaryStrandsConnection"},"name":{"kind":"Name","value":"strandsConnection"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"where"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"edge"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"primary"},"value":{"kind":"BooleanValue","value":false}}]}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"edges"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"properties"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"start"}},{"kind":"Field","name":{"kind":"Name","value":"end"}},{"kind":"Field","name":{"kind":"Name","value":"primary"}}]}},{"kind":"Field","name":{"kind":"Name","value":"node"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"seq"}},{"kind":"Field","name":{"kind":"Name","value":"parentConnection"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"edges"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"properties"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"start"}},{"kind":"Field","name":{"kind":"Name","value":"end"}}]}}]}}]}}]}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"index"}}]}},{"kind":"Field","name":{"kind":"Name","value":"modification"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"position"}},{"kind":"Field","name":{"kind":"Name","value":"symbol"}},{"kind":"Field","name":{"kind":"Name","value":"symbol_label"}},{"kind":"Field","name":{"kind":"Name","value":"type"}},{"kind":"Field","name":{"kind":"Name","value":"type_short_label"}}]}},{"kind":"Field","name":{"kind":"Name","value":"guide"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"subclass_label"}},{"kind":"Field","name":{"kind":"Name","value":"class"}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"chebi_id"}},{"kind":"Field","name":{"kind":"Name","value":"so_id"}},{"kind":"Field","name":{"kind":"Name","value":"url"}},{"kind":"Field","name":{"kind":"Name","value":"secondary_struct_file"}}]}},{"kind":"Field","name":{"kind":"Name","value":"guides"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"where"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"modifications_SOME"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"target"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"id"}}}]}}]}}]}},{"kind":"Argument","name":{"kind":"Name","value":"options"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"sort"},"value":{"kind":"ListValue","values":[{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"id"},"value":{"kind":"EnumValue","value":"ASC"}}]}]}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"class"}},{"kind":"Field","alias":{"kind":"Name","value":"chromosome"},"name":{"kind":"Name","value":"parent"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"where"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"graphql_type"},"value":{"kind":"EnumValue","value":"Chromosome"}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"name"}}]}}]}}]}}]} as unknown as DocumentNode<TargetByIdQueryQuery, TargetByIdQueryQueryVariables>;
export const ClusterByIdQueryDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"clusterByIdQuery"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"id"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"ID"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"clusters"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"where"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"id"}}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"guides"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"options"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"sort"},"value":{"kind":"ListValue","values":[{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"id"},"value":{"kind":"EnumValue","value":"ASC"}}]}]}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"class"}},{"kind":"Field","name":{"kind":"Name","value":"subclass_label"}},{"kind":"Field","name":{"kind":"Name","value":"modificationsAggregate"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"count"}}]}},{"kind":"Field","alias":{"kind":"Name","value":"chromosomeConnection"},"name":{"kind":"Name","value":"parentConnection"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"where"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"node"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"graphql_type"},"value":{"kind":"EnumValue","value":"Chromosome"}}]}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"edges"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"properties"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"start"}},{"kind":"Field","name":{"kind":"Name","value":"end"}}]}}]}}]}}]}},{"kind":"Field","name":{"kind":"Name","value":"guidesAggregate"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"count"}}]}},{"kind":"Field","alias":{"kind":"Name","value":"referenceGuide"},"name":{"kind":"Name","value":"guides"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"options"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"limit"},"value":{"kind":"IntValue","value":"1"}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","alias":{"kind":"Name","value":"chromosome"},"name":{"kind":"Name","value":"parent"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"where"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"graphql_type"},"value":{"kind":"EnumValue","value":"Chromosome"}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"name"}}]}},{"kind":"Field","name":{"kind":"Name","value":"genome"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"organism"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"label"}},{"kind":"Field","name":{"kind":"Name","value":"id"}}]}}]}}]}}]}}]}}]} as unknown as DocumentNode<ClusterByIdQueryQuery, ClusterByIdQueryQueryVariables>;
export const TargetAlignmentQueryDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"targetAlignmentQuery"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"targetName"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"organismIds"}},"type":{"kind":"ListType","type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"Int"}}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","alias":{"kind":"Name","value":"targetBase"},"name":{"kind":"Name","value":"targets"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"unit"}},{"kind":"Field","name":{"kind":"Name","value":"genome"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"organism"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"label"}}]}}]}}]}},{"kind":"Field","alias":{"kind":"Name","value":"organismsBase"},"name":{"kind":"Name","value":"organisms"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"label"}},{"kind":"Field","name":{"kind":"Name","value":"id"}}]}},{"kind":"Field","name":{"kind":"Name","value":"organisms"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"where"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"tableEntries_SOME"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"modification"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"target"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"name"},"value":{"kind":"Variable","name":{"kind":"Name","value":"targetName"}}}]}}]}}]}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}}]}},{"kind":"Field","alias":{"kind":"Name","value":"selectableTargets"},"name":{"kind":"Name","value":"targets"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"where"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"name"},"value":{"kind":"Variable","name":{"kind":"Name","value":"targetName"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"genome"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"organism"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"id_IN"},"value":{"kind":"Variable","name":{"kind":"Name","value":"organismIds"}}}]}}]}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"genome"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"organism"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"shortlabel"}}]}}]}}]}}]}}]} as unknown as DocumentNode<TargetAlignmentQueryQuery, TargetAlignmentQueryQueryVariables>;
export const GuideAlignmentQueryDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"guideAlignmentQuery"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"guideSubclasses"}},"type":{"kind":"ListType","type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"GuideClass"}}}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"guideName"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"String"}}},{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"organismIds"}},"type":{"kind":"ListType","type":{"kind":"NonNullType","type":{"kind":"NamedType","name":{"kind":"Name","value":"Int"}}}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","alias":{"kind":"Name","value":"guideBase"},"name":{"kind":"Name","value":"guides"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"subclass"}},{"kind":"Field","name":{"kind":"Name","value":"subclass_label"}},{"kind":"Field","name":{"kind":"Name","value":"genome"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"organism"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}}]}}]}}]}},{"kind":"Field","alias":{"kind":"Name","value":"guideNamesFilteredBySubclass"},"name":{"kind":"Name","value":"guides"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"where"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"subclass_IN"},"value":{"kind":"Variable","name":{"kind":"Name","value":"guideSubclasses"}}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"name"}},{"kind":"Field","name":{"kind":"Name","value":"genome"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"organism"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}}]}}]}}]}},{"kind":"Field","alias":{"kind":"Name","value":"organismsBase"},"name":{"kind":"Name","value":"organisms"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"label"}},{"kind":"Field","name":{"kind":"Name","value":"id"}}]}},{"kind":"Field","name":{"kind":"Name","value":"organisms"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"where"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"tableEntries_SOME"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"guide"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"name"},"value":{"kind":"Variable","name":{"kind":"Name","value":"guideName"}}}]}}]}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}}]}},{"kind":"Field","alias":{"kind":"Name","value":"selectableGuides"},"name":{"kind":"Name","value":"guides"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"where"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"name"},"value":{"kind":"Variable","name":{"kind":"Name","value":"guideName"}}},{"kind":"ObjectField","name":{"kind":"Name","value":"genome"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"organism"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"id_IN"},"value":{"kind":"Variable","name":{"kind":"Name","value":"organismIds"}}}]}}]}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"genome"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"organism"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"shortlabel"}}]}}]}}]}}]}}]} as unknown as DocumentNode<GuideAlignmentQueryQuery, GuideAlignmentQueryQueryVariables>;
export const DatabaseStatisticsQueryDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"databaseStatisticsQuery"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"organismsAggregate"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"count"}}]}},{"kind":"Field","alias":{"kind":"Name","value":"allModifications"},"name":{"kind":"Name","value":"modifications"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"type"}}]}},{"kind":"Field","alias":{"kind":"Name","value":"allModificationsCount"},"name":{"kind":"Name","value":"modificationsAggregate"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"count"}}]}},{"kind":"Field","alias":{"kind":"Name","value":"nonOrphanModificationsCount"},"name":{"kind":"Name","value":"modificationsAggregate"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"where"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"guidesAggregate"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"count_GT"},"value":{"kind":"IntValue","value":"0"}}]}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"count"}}]}},{"kind":"Field","alias":{"kind":"Name","value":"allGuides"},"name":{"kind":"Name","value":"guides"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"subclass"}}]}},{"kind":"Field","alias":{"kind":"Name","value":"allGuidesCount"},"name":{"kind":"Name","value":"guidesAggregate"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"count"}}]}},{"kind":"Field","alias":{"kind":"Name","value":"nonOrphanGuidesCount"},"name":{"kind":"Name","value":"guidesAggregate"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"where"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"modificationsAggregate"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"count_GT"},"value":{"kind":"IntValue","value":"0"}}]}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"count"}}]}}]}}]} as unknown as DocumentNode<DatabaseStatisticsQueryQuery, DatabaseStatisticsQueryQueryVariables>;
export const LegalDocumentListQueryDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"legalDocumentListQuery"},"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"documents"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"where"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"types_INCLUDES"},"value":{"kind":"StringValue","value":"legal","block":false}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"menu_label"}}]}}]}}]} as unknown as DocumentNode<LegalDocumentListQueryQuery, LegalDocumentListQueryQueryVariables>;
export const DocumentByIdQueryDocument = {"kind":"Document","definitions":[{"kind":"OperationDefinition","operation":"query","name":{"kind":"Name","value":"documentByIdQuery"},"variableDefinitions":[{"kind":"VariableDefinition","variable":{"kind":"Variable","name":{"kind":"Name","value":"id"}},"type":{"kind":"NamedType","name":{"kind":"Name","value":"ID"}}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"documents"},"arguments":[{"kind":"Argument","name":{"kind":"Name","value":"where"},"value":{"kind":"ObjectValue","fields":[{"kind":"ObjectField","name":{"kind":"Name","value":"id"},"value":{"kind":"Variable","name":{"kind":"Name","value":"id"}}}]}}],"selectionSet":{"kind":"SelectionSet","selections":[{"kind":"Field","name":{"kind":"Name","value":"id"}},{"kind":"Field","name":{"kind":"Name","value":"menu_label"}},{"kind":"Field","name":{"kind":"Name","value":"content"}}]}}]}}]} as unknown as DocumentNode<DocumentByIdQueryQuery, DocumentByIdQueryQueryVariables>;