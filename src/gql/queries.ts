/**
 * Utils imports
 */
import { graphql } from './codegen'

/**
 * Get the list of the organisms present in the database
 */
export const homeQuery = graphql(/* GraphQL */ `
  query homeQuery {
    modificationTypes: modifications {
      type
      type_short_label
    }

    guideSubclasses: guides {
      subclass
      subclass_label
    }

    targetUnits: targets {
      unit
    }

    organisms {
      id
      label
    }

    organismsAggregate {
      count
    }
  }
`)

/**
 * Get available data in the base matching given filters for modification and
 * target selection.
 */
export const modificationAndTargetSelectionQuery = graphql(/* GraphQL */ `
  query modificationAndTargetSelectionQuery(
    $modificationTypes: [ModifType]
    $targetNames: [String]
    $organismIds: [Int!]
  ) {
    targetsBase: targets {
      name
      unit
    }

    targets(
      where: {
        modifications_SOME: { type_IN: $modificationTypes }
        genome: { organism: { id_IN: $organismIds } }
      }
    ) {
      name
      id
    }

    modificationsBase: modifications {
      type_label
      type
    }

    modifications(
      where: {
        target: {
          name_IN: $targetNames
          genome: { organism: { id_IN: $organismIds } }
        }
      }
    ) {
      type
    }

    organismsBase: organisms {
      label
      id
    }

    organisms(
      where: {
        tableEntries_SOME: {
          modification: {
            type_IN: $modificationTypes
            target: { name_IN: $targetNames }
          }
        }
      }
    ) {
      id
    }
  }
`)

/**
 * Get available data in the base matching given filters for guide selection.
 */
export const guideSelectionQuery = graphql(/* GraphQL */ `
  query guideSelectionQuery(
    $guideSubclasses: [GuideClass!]
    $targetNames: [String]
    $modificationTypes: [ModifType]
    $organismIds: [Int!]
  ) {
    guidesBase: guides {
      subclass
      subclass_label
    }

    guides(
      where: {
        modifications_SOME: {
          target: { name_IN: $targetNames }
          type_IN: $modificationTypes
        }
        genome: { organism: { id_IN: $organismIds } }
      }
    ) {
      subclass
    }

    targetsBase: targets {
      name
      unit
    }

    targets(
      where: {
        modifications_SOME: {
          guides_SOME: { subclass_IN: $guideSubclasses }
          type_IN: $modificationTypes
        }
        genome: { organism: { id_IN: $organismIds } }
      }
    ) {
      name
    }

    modificationsBase: modifications {
      type_label
      type
    }

    modifications(
      where: {
        target: {
          name_IN: $targetNames
          genome: { organism: { id_IN: $organismIds } }
        }
        guides_SOME: { subclass_IN: $guideSubclasses }
      }
    ) {
      type
    }

    organismsBase: organisms {
      label
      id
    }

    organisms(
      where: {
        tableEntries_SOME: {
          modification: {
            type_IN: $modificationTypes
            target: { name_IN: $targetNames }
          }
          guide: { subclass_IN: $guideSubclasses }
        }
      }
    ) {
      id
    }
  }
`)

/**
 * Get the entries for data table display
 */
export const tableEntriesQuery = graphql(/* GraphQL */ `
  query tableEntriesQuery {
    tableEntries {
      id
      modification {
        id
        name
        position
        symbol
        symbol_label
        type
        type_short_label
      }
      guide {
        id
        name
        class
        subclass
        subclass_label
        length
        seq
        chromosomeConnection: parentConnection(
          where: { node: { graphql_type: Chromosome } }
        ) {
          edges {
            properties {
              start
              end
              strand
            }
            node {
              id
              name
              length
            }
          }
        }
        host_genes
      }
      target {
        id
        name
        class
        unit
        length
        chromosomeConnection: parentConnection(
          where: { node: { graphql_type: Chromosome } }
        ) {
          edges {
            properties {
              start
              end
              strand
            }
            node {
              id
              name
            }
          }
        }
      }
      organism {
        id
        label
        name
      }
    }

    modificationsBase: modifications {
      symbol
      symbol_label
      type
      type_short_label
    }

    targetsBase: targets {
      name
      unit
      class
    }

    guidesBase: guides {
      subclass
      subclass_label
      class
    }

    organismsBase: organisms {
      id
      name
      label
    }
  }
`)

/**
 * Get data about an organism by its ID
 */
export const organismByIdQuery = graphql(/* GraphQL */ `
  query organismByIdQuery($id: Int) {
    organisms(where: { id: $id }) {
      id
      label
      shortname
      genomes {
        sequences(where: { graphql_type: Chromosome }) {
          id
          name
          altnames
          description
          length
          graphql_type
          guides: featuresConnection(where: { node: { graphql_type: Guide } }) {
            totalCount
          }
        }
      }
    }

    modifications(where: { target: { genome: { organism: { id: $id } } } }) {
      id
      name
      symbol_label
      type_short_label
      type
      guidesAggregate {
        count
      }
    }

    allModificationsCount: modificationsAggregate(
      where: { target: { genome: { organism: { id: $id } } } }
    ) {
      count
    }

    nonOrphanModificationsCount: modificationsAggregate(
      where: {
        guidesAggregate: { count_GT: 0 }
        target: { genome: { organism: { id: $id } } }
      }
    ) {
      count
    }

    targets(where: { genome: { organism: { id: $id } } }) {
      id
      name
      class
      unit
      modificationsAggregate {
        count
      }
    }

    allTargetsCount: targetsAggregate(
      where: { genome: { organism: { id: $id } } }
    ) {
      count
    }

    guides(where: { genome: { organism: { id: $id } } }) {
      id
      name
      class
      subclass
      subclass_label
      chromosomeConnection: parentConnection(
        where: { node: { graphql_type: Chromosome } }
      ) {
        edges {
          properties {
            start
            end
          }
          node {
            id
          }
        }
      }
      modificationsAggregate {
        count
      }
    }

    allGuidesCount: guidesAggregate(
      where: { genome: { organism: { id: $id } } }
    ) {
      count
    }

    nonOrphanGuidesCount: guidesAggregate(
      where: {
        modificationsAggregate: { count_GT: 0 }
        genome: { organism: { id: $id } }
      }
    ) {
      count
    }
  }
`)

/**
 * Get data about a modification by its ID
 */
export const modificationByIdQuery = graphql(/* GraphQL */ `
  query modificationByIdQuery($id: ID) {
    modifications(where: { id: $id }) {
      id
      name
      type
      type_short_label
      symbol
      symbol_label
      position
      target {
        id
        name
        class
        unit
        genome {
          organism {
            id
            label
          }
        }
      }
      # chebi_id
      # so_id

      interactions {
        duplexes {
          primaryStrandsConnection: strandsConnection(
            where: { edge: { primary: true } }
          ) {
            edges {
              properties {
                start
                end
                primary
              }
              node {
                seq
                parentConnection {
                  edges {
                    properties {
                      start
                      end
                    }
                  }
                }
              }
            }
          }
          secondaryStrandsConnection: strandsConnection(
            where: { edge: { primary: false } }
          ) {
            edges {
              properties {
                start
                end
                primary
              }
              node {
                seq
                parentConnection {
                  edges {
                    properties {
                      start
                      end
                    }
                  }
                }
              }
            }
          }
          index
        }
        guide {
          id
          name
          subclass_label
          class
        }
        target {
          id
          name
          unit
          class
        }
      }
    }

    guides(
      where: { modifications_SOME: { id: $id } }
      options: { sort: [{ id: ASC }] }
    ) {
      id
      name
      class
      chromosome: parent(where: { graphql_type: Chromosome }) {
        name
      }
    }
  }
`)

/**
 * Get data about a guide by its ID
 */
export const guideByIdQuery = graphql(/* GraphQL */ `
  query guideByIdQuery($id: ID) {
    guides(where: { id: $id }) {
      id
      name
      altnames
      description
      length
      class
      subclass_label
      chromosomeConnection: parentConnection(
        where: { node: { graphql_type: Chromosome } }
      ) {
        edges {
          properties {
            start
            end
            strand
          }
          node {
            id
            name
            length
            graphql_type
          }
        }
      }
      host_genes
      seq
      genome {
        organism {
          id
          label
        }
      }
      boxConnections: featuresConnection(
        where: { NOT: { node: { class: DuplexFragment } } }
      ) {
        edges {
          properties {
            ... on HasFeature {
              start
              end
            }
          }
          node {
            id
            annotation
          }
        }
      }
      modifications(options: { sort: [{ position: ASC }] }) {
        id
        name
        type
      }
      modificationsAggregate {
        count
      }
      interactions {
        duplexes {
          primaryStrandsConnection: strandsConnection(
            where: { edge: { primary: true } }
          ) {
            edges {
              properties {
                start
                end
                primary
              }
              node {
                seq
                parentConnection {
                  edges {
                    properties {
                      start
                      end
                    }
                  }
                }
              }
            }
          }
          secondaryStrandsConnection: strandsConnection(
            where: { edge: { primary: false } }
          ) {
            edges {
              properties {
                start
                end
                primary
              }
              node {
                seq
                parentConnection {
                  edges {
                    properties {
                      start
                      end
                    }
                  }
                }
              }
            }
          }
          index
        }
        modification {
          id
          name
          position
          symbol
          symbol_label
          type
          type_short_label
        }
        target {
          id
          name
          class
          unit
        }
      }
      cluster {
        id
      }
      clusterAggregate {
        count
      }
      isoform {
        guides {
          id
          name
        }
        guidesAggregate {
          count
        }
      }
      isoformAggregate {
        count
      }
      chebi_id
      so_id
    }

    targets(
      where: { modifications_SOME: { guides_SOME: { id: $id } } }
      options: { sort: [{ name: ASC }] }
    ) {
      id
      name
      class
      unit
    }
  }
`)

/**
 * Get data about a target by its ID
 */
export const targetByIdQuery = graphql(/* GraphQL */ `
  query targetByIdQuery($id: ID) {
    targets(where: { id: $id }) {
      id
      name
      altnames
      description
      length
      class
      unit
      chromosomeConnection: parentConnection(
        where: { node: { graphql_type: Chromosome } }
      ) {
        edges {
          properties {
            start
            end
            strand
          }
          node {
            name
            graphql_type
          }
        }
      }
      seq
      genome {
        organism {
          id
          label
        }
      }
      modifications(options: { sort: [{ position: ASC }] }) {
        id
        name
        position
        type
        symbol
      }
      modificationsAggregate {
        count
      }
      interactions {
        duplexes {
          primaryStrandsConnection: strandsConnection(
            where: { edge: { primary: true } }
          ) {
            edges {
              properties {
                start
                end
                primary
              }
              node {
                seq
                parentConnection {
                  edges {
                    properties {
                      start
                      end
                    }
                  }
                }
              }
            }
          }
          secondaryStrandsConnection: strandsConnection(
            where: { edge: { primary: false } }
          ) {
            edges {
              properties {
                start
                end
                primary
              }
              node {
                seq
                parentConnection {
                  edges {
                    properties {
                      start
                      end
                    }
                  }
                }
              }
            }
          }
          index
        }
        modification {
          id
          name
          position
          symbol
          symbol_label
          type
          type_short_label
        }
        guide {
          id
          name
          subclass_label
          class
        }
      }
      chebi_id
      so_id
      url
      secondary_struct_file
    }

    guides(
      where: { modifications_SOME: { target: { id: $id } } }
      options: { sort: [{ id: ASC }] }
    ) {
      id
      name
      class
      chromosome: parent(where: { graphql_type: Chromosome }) {
        name
      }
    }
  }
`)

/**
 * Get data about a cluster by its ID
 */
export const clusterByIdQuery = graphql(/* GraphQL */ `
  query clusterByIdQuery($id: ID) {
    clusters(where: { id: $id }) {
      id
      guides(options: { sort: [{ id: ASC }] }) {
        id
        name
        class
        subclass_label
        modificationsAggregate {
          count
        }
        chromosomeConnection: parentConnection(
          where: { node: { graphql_type: Chromosome } }
        ) {
          edges {
            properties {
              start
              end
            }
          }
        }
      }
      guidesAggregate {
        count
      }
      referenceGuide: guides(options: { limit: 1 }) {
        chromosome: parent(where: { graphql_type: Chromosome }) {
          id
          name
        }
        genome {
          organism {
            label
            id
          }
        }
      }
    }
  }
`)

/**
 * Get necessary data to select sequences for alignment.
 */
export const targetAlignmentQuery = graphql(/* GraphQL */ `
  query targetAlignmentQuery($targetName: String, $organismIds: [Int!]) {
    targetBase: targets {
      name
      unit
      genome {
        organism {
          id
          label
        }
      }
    }

    organismsBase: organisms {
      label
      id
    }

    organisms(
      where: {
        tableEntries_SOME: { modification: { target: { name: $targetName } } }
      }
    ) {
      id
    }

    selectableTargets: targets(
      where: {
        name: $targetName
        genome: { organism: { id_IN: $organismIds } }
      }
    ) {
      id
      genome {
        organism {
          shortlabel
        }
      }
    }
  }
`)

/**
 * Get necessary data to select sequences for alignment.
 */
export const guideAlignmentQuery = graphql(/* GraphQL */ `
  query guideAlignmentQuery(
    $guideSubclasses: [GuideClass!]
    $guideName: String
    $organismIds: [Int!]
  ) {
    guideBase: guides {
      name
      subclass
      subclass_label
      genome {
        organism {
          id
        }
      }
    }

    guideNamesFilteredBySubclass: guides(
      where: { subclass_IN: $guideSubclasses }
    ) {
      name
      genome {
        organism {
          id
        }
      }
    }

    organismsBase: organisms {
      label
      id
    }

    organisms(where: { tableEntries_SOME: { guide: { name: $guideName } } }) {
      id
    }

    selectableGuides: guides(
      where: { name: $guideName, genome: { organism: { id_IN: $organismIds } } }
    ) {
      id
      genome {
        organism {
          shortlabel
        }
      }
    }
  }
`)

/**
 * Get statistics about the database.
 */
export const databaseStatisticsQuery = graphql(/* GraphQL */ `
  query databaseStatisticsQuery {
    organismsAggregate {
      count
    }

    allModifications: modifications {
      type
    }

    allModificationsCount: modificationsAggregate {
      count
    }

    nonOrphanModificationsCount: modificationsAggregate(
      where: { guidesAggregate: { count_GT: 0 } }
    ) {
      count
    }

    allGuides: guides {
      subclass
    }

    allGuidesCount: guidesAggregate {
      count
    }

    nonOrphanGuidesCount: guidesAggregate(
      where: { modificationsAggregate: { count_GT: 0 } }
    ) {
      count
    }
  }
`)

/**
 * Get the list of legal documents of the website
 */
export const legalDocumentListQuery = graphql(/* GraphQL */ `
  query legalDocumentListQuery {
    documents(where: { types_INCLUDES: "legal" }) {
      id
      menu_label
    }
  }
`)

/**
 * Get a document by its ID
 */
export const documentByIdQuery = graphql(/* GraphQL */ `
  query documentByIdQuery($id: ID) {
    documents(where: { id: $id }) {
      id
      menu_label
      content
    }
  }
`)
