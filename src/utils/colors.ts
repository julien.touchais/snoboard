/**
 * Other 3rd-party imports
 */
import chroma from 'chroma-js'
/**
 * Types imports
 */
import { AnnotationType, ModifType } from '@/gql/codegen/graphql'
import type {
  TailwindDefaultColorNameModel,
  TailwindDefaultColorShadeModel
} from '@/typings/styleTypes'
import { isIn, type RNANucleotideModel } from '@/typings/typeUtils'
/**
 * Assets imports
 */
import tailwindDefaultColors from 'tailwindcss/colors'

/**
 * Constant associating nucleotide letter with corresponding Tailwind color.
 */
const NUCLEOTIDE_TAILWIND_COLORS = {
  A: 'lime',
  U: 'red',
  C: 'orange',
  G: 'yellow'
} satisfies { [k: string]: TailwindDefaultColorNameModel }

/**
 * Constant associating modification type with corresponding Tailwind color.
 */
const MODIFICATION_TAILWIND_COLORS = {
  [ModifType.Ace]: 'teal',
  [ModifType.Nm]: 'sky',
  [ModifType.Psi]: 'violet',
  [ModifType.Other]: 'slate'
} satisfies { [k: string]: TailwindDefaultColorNameModel }

/**
 * Constant associating box type with corresponding Tailwind color.
 */
const BOX_TAILWIND_COLORS = {
  [AnnotationType.CBox]: 'cyan',
  [AnnotationType.DBox]: 'sky',
  [AnnotationType.AcaBox]: 'purple',
  [AnnotationType.HBox]: 'indigo',
  [AnnotationType.Other]: 'slate'
} satisfies { [k: string]: TailwindDefaultColorNameModel }

/**
 * Gets the color code of a Tailwind color name with a given shade.
 * @param colorName The Tailwind color of which to get the color code.
 * @param shade The shade in which to get the color.
 * @returns If a shade is asked for, the HEX color code of the given Tailwind
 * color in the given shade, otherwise, the Tailwind color name without
 * modification (i.e. `colorName`).
 */
export function getColorWithOptionalShade(
  colorName: TailwindDefaultColorNameModel
): TailwindDefaultColorNameModel
export function getColorWithOptionalShade(
  colorName: TailwindDefaultColorNameModel,
  shade: TailwindDefaultColorShadeModel
): string
export function getColorWithOptionalShade(
  colorName: TailwindDefaultColorNameModel,
  shade?: TailwindDefaultColorShadeModel
) {
  return shade ? tailwindDefaultColors[colorName][shade] : colorName
}

/**
 * Gets the color code or Tailwind color name corresponding to a nucleotide.
 * @param nucleotide The nucleotide letter of which to get the color.
 * @param shade The shade in which to get the color.
 * @returns If a shade is asked for, the HEX color code, otherwise,
 * the Tailwind color name.
 */
export function getNucleotideColor(
  nucleotide: RNANucleotideModel
): TailwindDefaultColorNameModel
export function getNucleotideColor(
  nucleotide: RNANucleotideModel,
  shade: TailwindDefaultColorShadeModel
): string
export function getNucleotideColor(
  nucleotide: RNANucleotideModel,
  shade?: TailwindDefaultColorShadeModel
) {
  return shade
    ? getColorWithOptionalShade(NUCLEOTIDE_TAILWIND_COLORS[nucleotide], shade)
    : getColorWithOptionalShade(NUCLEOTIDE_TAILWIND_COLORS[nucleotide])
}

/**
 * Gets the color code or Tailwind color name corresponding to a type of
 * modification.
 * @param modificationType The modification type of which to get the color.
 * @param shade The shade in which to get the color.
 * @returns If a shade is asked for, the HEX color code, otherwise,
 * the Tailwind color name.
 */
export function getModificationColor(
  modificationType: ModifType
): TailwindDefaultColorNameModel
export function getModificationColor(
  modificationType: ModifType,
  shade: TailwindDefaultColorShadeModel
): string
export function getModificationColor(
  modificationType: ModifType,
  shade?: TailwindDefaultColorShadeModel
) {
  return shade
    ? getColorWithOptionalShade(
        MODIFICATION_TAILWIND_COLORS[modificationType],
        shade
      )
    : getColorWithOptionalShade(MODIFICATION_TAILWIND_COLORS[modificationType])
}

/**
 * Gets the color code or Tailwind color name corresponding to a type of box.
 * @param boxType The box type of which to get the color.
 * @param shade The shade in which to get the color.
 * @returns If a shade is asked for, the HEX color code, otherwise,
 * the Tailwind color name.
 */
export function getBoxColor(
  boxType: AnnotationType
): TailwindDefaultColorNameModel
export function getBoxColor(
  boxType: AnnotationType,
  shade: TailwindDefaultColorShadeModel
): string
export function getBoxColor(
  boxType: AnnotationType,
  shade?: TailwindDefaultColorShadeModel
) {
  return shade
    ? getColorWithOptionalShade(
        BOX_TAILWIND_COLORS[
          isIn(boxType, BOX_TAILWIND_COLORS) ? boxType : AnnotationType.Other
        ],
        shade
      )
    : getColorWithOptionalShade(
        BOX_TAILWIND_COLORS[
          isIn(boxType, BOX_TAILWIND_COLORS) ? boxType : AnnotationType.Other
        ]
      )
}

/**
 * Generates a color from two other, located in the middle in terms of hue
 * (contrast & luminosity are provided directly)
 * @param startColor The first color from which to compute middle color.
 * @param endColor The second color from which to compute middle color.
 * @param middleContrast The contrast to apply to middle color.
 * @param middleLuminosity The luminosity to apply to middle color.
 * @returns The generates middle color.
 */
const composeMiddleColor = (
  startColor: string,
  endColor: string,
  middleContrast: number,
  middleLuminosity: number
) => {
  const startHue = chroma(startColor).get('hcl.h')
  const endHue = chroma(endColor).get('hcl.h')
  const arc1 = Math.abs(startHue - endHue)
  const arc2 = 360 - arc1
  const shortestArc = Math.min(arc1, arc2)
  const middleHue =
    arc1 < arc2
      ? Math.min(startHue, endHue)
      : Math.max(startHue, endHue) + shortestArc / 2
  const midColor = chroma(middleHue, middleContrast, middleLuminosity, 'hcl')
  return midColor
}

/**
 * Generates a divergent scale from the two extremities of the scale, which is a
 * scale passing by a middle point different from the middle of the two colors.
 * @param startColor The starting color of the scale.
 * @param endColor The ending color of the scale.
 * @param colors The number of color to generate in the scale.
 * @returns The scale in the form of an array of hex code strings.
 */
export const composeDivergentScale = (
  startColor: string,
  endColor: string,
  colors: number
) => {
  if (!(chroma.valid(startColor) && chroma.valid(endColor) && colors)) {
    console.warn(
      `Unable to generate scale from: startColor=${startColor}, endColor=${endColor}, colors=${colors} ;`,
      "Using defaults ('#fafa6e', '#9affe9','#2A4858',6)"
    )
    return chroma.scale(['#fafa6e', '#9affe9', '#2A4858']).mode('lch').colors(6)
  }
  return chroma
    .scale([
      startColor,
      composeMiddleColor(startColor, endColor, 35, 95),
      endColor
    ])
    .mode('lch')
    .colors(colors)
}
