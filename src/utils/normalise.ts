/**
 * Other 3rd-party imports
 */
import { findIndex as _findIndex, mapValues as _mapValues } from 'lodash-es'
/**
 * Types imports
 */
import type {
  C4GEdgeModel,
  C4GGraphModel,
  C4GNodeModel
} from '@/typings/Codev4GraphFormat'

/**
 * Return the distance between two nodes
 * @param node1 The first node
 * @param node2 The second node
 * @returns The distance if both nodes are positioned, undefined otherwise
 */
export const nodesDistance = (
  node1: C4GNodeModel,
  node2: C4GNodeModel
): number | undefined =>
  node1.metadata?.position?.x &&
  node1.metadata?.position?.y &&
  node2.metadata?.position?.x &&
  node2.metadata?.position?.y &&
  Math.sqrt(
    Math.pow(
      Math.abs(node1.metadata.position.x - node2.metadata.position.x),
      2
    ) +
      Math.pow(
        Math.abs(node1.metadata.position.y - node2.metadata.position.y),
        2
      )
  )

/**
 * Normalise a graph by applying a resize ratio, computed from desired length of
 * a particular edge (determined based on the provided mechanism) ; and returns a
 * graph with the normalisation applied
 * @param graph The graph to normalise
 * @param normalisedEdgeLength
 *    The length of the edges in the normalised structure
 * @param referenceEdgeGetter The function used to get the edge which will serve
 *    as a reference for normalisation (i.e. its length will be used to compute
 *    the normalisation ratio to apply on the graph)
 */
export const composeNormalisedGraph = (
  graph: C4GGraphModel,
  normalisedEdgeLength: number,
  referenceEdgeGetter: (graph: C4GGraphModel) => C4GEdgeModel | undefined = (
    graph
  ) => graph.graph?.edges?.[0]
): C4GGraphModel => {
  // Return the graph without modification if conditions for normalisation
  // are not met
  if (
    !graph.graph.nodes || // No node
    Object.values(graph.graph.nodes).length < 2 || // Single node
    Object.values(graph.graph.nodes).find(
      // Not all nodes are positioned
      (node) =>
        node.metadata?.position?.x === undefined ||
        node.metadata?.position?.y === undefined
    )
  ) {
    console.warn('Cannot normalise')
    return graph
  }
  // Get the reference edge using the provided getter
  const referenceEdge = referenceEdgeGetter(graph)

  // Extract the source & target nodes from the reference edge
  const referenceSourceNode =
    referenceEdge && graph.graph.nodes[referenceEdge.source]
  const referenceTargetNode =
    referenceEdge && graph.graph.nodes[referenceEdge.target]

  // Get the two first nodes for fallback
  // We checked that those 2 nodes exist in the graph (nodes.length >= 2) at the
  // beginning, but TS can't handle this so we force remove `undefined` option
  // with a non-null type assertion (`!`)
  const firstNode = Object.values(graph.graph.nodes)[0]!
  const secondNode = Object.values(graph.graph.nodes)[1]!

  // Get the length of the reference edge
  const referenceEdgeLength =
    referenceSourceNode && referenceTargetNode
      ? nodesDistance(referenceSourceNode, referenceTargetNode)
      : nodesDistance(firstNode, secondNode)

  // Compute the normalisation ratio
  const normalisationRatio =
    normalisedEdgeLength / (referenceEdgeLength || normalisedEdgeLength)

  // Apply normalisation to the nodes
  const normalisedNodes = _mapValues(graph.graph.nodes, (node) => {
    // Normalise position if present
    const normalisedNode = node.metadata?.position
      ? {
          ...node,
          metadata: {
            ...node.metadata,
            position: {
              x: node.metadata.position.x * normalisationRatio,
              y: node.metadata.position.y * normalisationRatio
            }
          }
        }
      : node

    // Check if additional metadata is present, if not return & continue
    // normalisation on next node
    if (!normalisedNode.metadata?.data) {
      return normalisedNode
    }

    // Get indexes of additional metadata to normalise (-1 if not present)
    const endPositionLabelDataIndex = _findIndex(normalisedNode.metadata.data, [
      'label',
      'endPositionLabel'
    ])
    const nucleotidePositionLabelDataIndex = _findIndex(
      normalisedNode.metadata.data,
      ['label', 'nucleotidePositionLabel']
    )
    const nucleotidePositionLineDataIndex = _findIndex(
      normalisedNode.metadata.data,
      ['label', 'nucleotidePositionLine']
    )

    // Get values of additional metadata to normalise (`undefined` if not present)
    const endPositionLabelDataValue =
      endPositionLabelDataIndex !== -1
        ? normalisedNode.metadata.data[endPositionLabelDataIndex]?.value
        : undefined
    const nucleotidePositionLabelDataValue =
      nucleotidePositionLabelDataIndex !== -1
        ? normalisedNode.metadata.data[nucleotidePositionLabelDataIndex]?.value
        : undefined
    const nucleotidePositionLineDataValue =
      nucleotidePositionLineDataIndex !== -1
        ? normalisedNode.metadata.data[nucleotidePositionLineDataIndex]?.value
        : undefined

    // Normalise additional metadata when present
    if (endPositionLabelDataValue) {
      endPositionLabelDataValue.x *= normalisationRatio
      endPositionLabelDataValue.y *= normalisationRatio
    }
    if (nucleotidePositionLabelDataValue) {
      nucleotidePositionLabelDataValue.x *= normalisationRatio
      nucleotidePositionLabelDataValue.y *= normalisationRatio
    }
    if (nucleotidePositionLineDataValue) {
      nucleotidePositionLineDataValue.x1 *= normalisationRatio
      nucleotidePositionLineDataValue.x2 *= normalisationRatio
      nucleotidePositionLineDataValue.y1 *= normalisationRatio
      nucleotidePositionLineDataValue.y2 *= normalisationRatio
    }

    return normalisedNode
  })

  return {
    graph: {
      ...graph.graph,
      nodes: normalisedNodes
    }
  }
}
