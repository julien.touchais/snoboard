/**
 * In an array of sequences (e.g. for an alignment), computes the conservation
 * rate of a reference seq., i.e. the proportion of seq. in the array which are
 * identical to it.
 * @param sequences The sequences among which to compute the conservation.
 * @param referenceSequence The sequence for which to compute the conservation.
 * @returns The proportion of the sequences in the array which are identical to
 * the reference one.
 */
export const composeConservationRate = (
  sequences: string[],
  referenceSequence: string
): number =>
  sequences.filter((sequence) => sequence && sequence === referenceSequence)
    .length / (sequences.length || 1)
