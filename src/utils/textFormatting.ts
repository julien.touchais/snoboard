/**
 * Other 3rd-party imports
 */
import { capitalize as _capitalize } from 'lodash-es'
/**
 * Types imports
 */
import { GuideClass, ModifType, AnnotationType } from '@/gql/codegen/graphql'

/**
 * Format an organism name to the good typography.
 * @param organismName Name of the organism.
 * @param shortGenus Wether to use a genus reduced to a single letter or use the
 * full genus.
 */
export const formatOrganismName = (
  organismName: string,
  shortGenus: boolean = false
): string => {
  const splitOrganismName = organismName.split(' ')

  if (splitOrganismName[0]) {
    if (splitOrganismName[1]) {
      return `${
        shortGenus
          ? `${splitOrganismName[0][0]?.toUpperCase()}.`
          : _capitalize(splitOrganismName[0].toLowerCase())
      } ${splitOrganismName[1].toLowerCase()}`
    }

    return splitOrganismName[0].toLowerCase()
  }

  console.error(
    "[formatOrganismName]: wrong organism name format, please use '<genus> <species>' or '<species>' format."
  )
  return organismName
}

/**
 * Format a guide subclass code into a readable string.
 * @param guideSubclass Subclass code of the guide.
 */
export const formatGuideSubclass = (guideSubclass?: GuideClass) => {
  switch (guideSubclass) {
    case GuideClass.Cd:
      return 'C/D box'

    case GuideClass.Haca:
      return 'H/ACA box'

    case GuideClass.Other:
      return 'Other'

    case GuideClass.ScaRna:
      return 'scaRNA'

    default:
      return 'Unknown Type'
  }
}

/**
 * Format an annotation type (i.e. generally guide box type) code into a
 * readable string.
 * @param annotationType Type code of the annotation.
 */
export const formatAnnotationType = (annotationType?: AnnotationType) => {
  switch (annotationType) {
    case AnnotationType.CBox:
      return 'C Box'

    case AnnotationType.DBox:
      return 'D Box'

    case AnnotationType.HBox:
      return 'H Box'

    case AnnotationType.AcaBox:
      return 'ACA Box'

    case AnnotationType.Other:
      return 'Other'

    default:
      return 'Unknown Type'
  }
}

/**
 * Format a modification type code into a readable string.
 * @param modificationType Type code of the modification.
 * @description This utility doesn't include markup, so text style could be
 * incorrect compared to the formal representation of the modification types
 * (e.g. 2'-O-meth should normally have the `O` italicised and it won't be
 * included in the result of the formatting.
 */
export const formatModificationType = (modificationType?: ModifType) => {
  switch (modificationType) {
    case ModifType.Nm:
      return "2'-O-me"

    case ModifType.Psi:
      return 'Pseudouridylation'

    case ModifType.Other:
      return 'Other'

    default:
      return 'Unknown Type'
  }
}

/**
 * Format a number y separating the thousands by a character.
 * Digits after the decimal point are kept as is.
 *
 * @param number Number to format.
 * @param [separator=","] The character to use as separator.
 * @example
 * ```js
 * separateThousands(31556925.25472448, ' ')
 * // ↪ 31 556 925.25472448
 * ```
 */
export const separateThousands = (
  number: number | bigint,
  separator: string = ','
): string => {
  if (typeof number === 'number' && number > Number.MAX_SAFE_INTEGER)
    throw new Error('Too large Number to be formatted, use BigInt instead.')
  if (number.toString().includes('.')) {
    // If position has digits after the decimal point, format only
    // the integer part and concat the decimal part to it.
    return (
      separateThousands(parseInt(`${number}`)) +
      number.toString().match(/(?<decimal>\.\d+)/)?.groups?.decimal
    )
  }

  if (number < 1000) {
    return number.toString()
  }

  const splitNumber = number
    .toString()
    .match(/(?<remainder>\d+)(?<chunk>\d{3})/)

  return (
    separateThousands(parseInt(splitNumber?.groups?.remainder || '')) +
    separator +
    splitNumber?.groups?.chunk
  )
}

/**
 * Pluralise an input text by adding a `s` at the end, if the provided count
 * exceeds 1.
 * @param amount The number to use to know if we have to pluralise the input.
 * @param singular The text to pluralise if necessary.
 * @returns The input text, pluralised if needed.
 */
export const composePlural = (amount: number, singular: string) =>
  singular + (amount > 1 ? 's' : '')
