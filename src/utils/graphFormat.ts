/**
 * Types imports
 */
import type { C4GGraphModel } from '@/typings/Codev4GraphFormat'
import type { JGFGraphModel } from '@/typings/JSONGraphFormat'
import { mapValues as _mapValues } from 'lodash-es'

/**
 * "Fix" a graph by transforming it to proper C4G format
 * @param JGFGraph The graph to "fix"
 */
export const fixGraphFormat = (JGFGraph: JGFGraphModel): C4GGraphModel => {
  if (!('edges' in JGFGraph.graph)) {
    return { graph: {} }
  }

  return {
    graph: {
      ...JGFGraph.graph,
      metadata: {
        data: Object.entries(JGFGraph.graph.metadata || {}).map(
          ([metadataId, metadataValue]) => ({
            label: metadataId,
            type: typeof metadataValue,
            value: metadataValue
          })
        )
      },
      nodes: _mapValues(JGFGraph.graph.nodes, (node) => ({
        label: node.label,
        metadata: node.metadata && {
          position: {
            x:
              'x' in node.metadata && typeof node.metadata.x === 'number'
                ? node.metadata.x
                : 0,
            y:
              'y' in node.metadata && typeof node.metadata.y === 'number'
                ? node.metadata.y
                : 0
          },
          data: [
            {
              label: 'nucleotidePosition',
              type: 'number',
              value:
                'position' in node.metadata &&
                typeof node.metadata.position === 'number'
                  ? node.metadata.position
                  : {}
            }
          ]
        }
      })),
      edges: JGFGraph.graph.edges?.map((edge) => ({
        ...edge,
        metadata: edge.metadata && {
          data: [
            {
              label: 'type',
              type: 'string',
              value:
                'type' in edge.metadata &&
                typeof edge.metadata.type === 'string'
                  ? edge.metadata.type
                  : 'Unknown type'
            }
          ]
        }
      }))
    }
  }
}
