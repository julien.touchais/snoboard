/**
 * Components imports
 */
import IconFa6SolidTable from '~icons/fa6-solid/table'
import IconFa6SolidDatabase from '~icons/fa6-solid/database'
import IconFa6SolidCircleQuestion from '~icons/fa6-solid/circle-question'
/**
 * Types imports
 */
import type { Component } from 'vue'
import type { MenuItem } from 'primevue/menuitem'
/**
 * Assets imports
 */
import * as helpGallery from '@/assets/images/help'
import { logoUrl } from '@/assets/images'

/**
 * A menu item, with an additional icon as a component.
 */
export interface CustomMenuItem extends MenuItem {
  /** The menu item icon as a component. */
  iconComponent: Component
}

/**
 * An item of the help tour.
 */
interface HelpTourItem {
  /** Help tour item ID. */
  id: string
  /** Help tour item title. */
  title: string
  /** Help tour item content. */
  content: string
}

/**
 * The different modes available for advanced selection.
 */
export const SELECTION_MODES = ['modification', 'guide', 'target'] as const

/**
 * The different modes available for alignment.
 */
export const ALIGNMENT_MODES = ['guide', 'target'] as const

/**
 * The items to display in the main navigation menu.
 */
export const MENU_ITEMS: CustomMenuItem[] = [
  {
    key: 'table',
    label: 'Data Table',
    iconComponent: IconFa6SolidTable,
    routerDest: { name: 'table' }
  },
  {
    key: 'api',
    label: 'API',
    iconComponent: IconFa6SolidDatabase,
    routerDest: { name: 'api' }
  },
  {
    key: 'help',
    label: 'Help',
    iconComponent: IconFa6SolidCircleQuestion,
    routerDest: { name: 'help' }
  }
]

export const HELP_TOUR_ITEMS: HelpTourItem[] = [
  {
    id: 'intro',
    title: 'How does snoBoard works ?',
    content: `This guide will take you through a quick tour of the way
**snoBoard** is organised, and how to use it.

> Use the <!--left--> <kbd class="font-mono">&xlarr;</kbd> and <!--right-->
> <kbd class="font-mono">&xrarr;</kbd> <!--keys or--> buttons to navigate in this guide.`
  },
  {
    id: 'table-intro',
    title: 'Data table',
    content: `Navigation in Snoboard's data is carried out by means of a **data
table**, used in various ways and which can be easily browsed using a few
functions.

![A data table](${helpGallery.helpTable})`
  },
  {
    id: 'table-row',
    title: 'Rows & Columns',
    content: `There are **three types of data** on snoBoard: **modifications**,
**guides** and **targets**. Each row in the data table is a
modification/guide/target triplet, which generally **represents an interaction**
between a guide and a target that results in a modification.

> Some information may be missing from a row, e.g. if a guide has no known guided
> modification.

The **selector above the data table** can be used to **show or hide columns**.

In the rows, some cells are **links to detail pages** about the data in question.

![A row in the data table](${helpGallery.helpTableWithRow})`
  },
  {
    id: 'table-sort-single',
    title: 'Sorting data',
    content: `A column can be **sorted** by **clicking on its title**. **Click
repeatedly** to **change the sorting direction**, and again to deactivate it.

> While a sort is active, an icon in the column title indicates the sort
> direction.

![A sorted column in the data table](${helpGallery.helpTableSortSingle})`
  },
  {
    id: 'table-sort-multiple',
    title: 'Multiple sort',
    content: `You can also **sort multiple columns** at once: first sort one
column as described before, then **press & hold <kbd>ctrl</kbd>/<kbd>⌘</kbd>
key** and **click on the title of the other column(s)**.

> Numbers will appear next to the sort direction icon to indicate the order in
> which the columns are sorted.

![Two sorted columns in the data table](${helpGallery.helpTableSortMultiple})

To **change the sort direction** for a column, **press & hold
<kbd>ctrl</kbd>/<kbd>⌘</kbd> key** and **click on the column title**.

> Beware that clicking until the filter is deactivated may unintentionally
> change the sort order.`
  },
  {
    id: 'table-filter',
    title: 'Filtering data',
    content: `**Filters** can be applied to the table columns by **clicking on the
<svg class="inline-block" width="14" height="14" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg">
  <path fill="currentColor" d="M8.64708 14H5.35296C5.18981 13.9979 5.03395 13.9321 4.91858 13.8167C4.8032 13.7014 4.73745 13.5455 4.73531 13.3824V7L0.329431 0.98C0.259794 0.889466 0.217389 0.780968 0.20718 0.667208C0.19697 0.553448 0.219379 0.439133 0.271783 0.337647C0.324282 0.236453 0.403423 0.151519 0.500663 0.0920138C0.597903 0.0325088 0.709548 0.000692754 0.823548 0H13.1765C13.2905 0.000692754 13.4021 0.0325088 13.4994 0.0920138C13.5966 0.151519 13.6758 0.236453 13.7283 0.337647C13.7807 0.439133 13.8031 0.553448 13.7929 0.667208C13.7826 0.780968 13.7402 0.889466 13.6706 0.98L9.26472 7V13.3824C9.26259 13.5455 9.19683 13.7014 9.08146 13.8167C8.96609 13.9321 8.81022 13.9979 8.64708 14ZM5.97061 12.7647H8.02943V6.79412C8.02878 6.66289 8.07229 6.53527 8.15296 6.43177L11.9412 1.23529H2.05884L5.86355 6.43177C5.94422 6.53527 5.98773 6.66289 5.98708 6.79412L5.97061 12.7647Z" />
</svg> icon** in the column title.

> Depending on the filtered column, either a **multiple-choice selector** or a
> **text input** will be available to select/set the filter to be applied.

![A column with a filter in the data table](${helpGallery.helpTableFilter})

You can also apply a **global filter** to the table, using the **field above the
data table**. The **search bar** on the home page and at the top of the other
pages can be used to search the data table.

<img src="${helpGallery.helpSearch}" alt="The search bar" style="width: 50%;" />`
  },
  {
    id: 'menu-data',
    title: 'Home page navigation',
    content: `Now that we have seen the principles of the data table, lets get
back to the **home page**.

Under the search bar, a **menu** is available for a **quick access to snoBoard's
main pages and data**.

![The "Guides" menu entry on the home page](${helpGallery.helpMenuGuides})

The **first three items** correspond to the **three types of data** present on
snoBoard (modifications, guides & targets). For each one, **three actions**
are possible:
- **List all** the object of the chosen type (e.g. all guides) ;
- **List** the object **in a particular category** of the chosen type (e.g.
  targets only of the SSU) ;
- Navigate to a **selection form**, where it is possible to select
  **characteristics to filter** the listed objects.

> The object list uses the data table, configured with the appropriate columns
> & filters, to display only relevant information.`
  },
  {
    id: 'menu-conservation',
    title: 'Home page navigation (conservation)',
    content: `The next item contains links to the **conservation analysis
sections**: one for **guides** and one for **targets**.

![The "Conservation" menu entry on the home page](${helpGallery.helpMenuConservation})`
  },
  {
    id: 'menu-conservation',
    title: 'Home page navigation (statistics)',
    content: `Finally, the last item contains links to various **statistics**
about the snoBoard data:
- Some **database-wide statistics**, such as the number of guides or the
distribution of different types of modifications in the base ;
- Some **organism-specific informations**, with additional data linked to the
organism in question.

![The "Statistics" menu entry on the home page](${helpGallery.helpMenuStatistics})`
  },
  {
    id: 'details',
    title: 'Detail pages',
    content: `Once you have found an object (modification, guide or target) in
the data table, if you want to know more about it, some columns contain
**links** to **detail pages**, where you can find all **the information about
that object**.

This includes **general information**, **linked data** (e.g. isoforms of a
guide), **sequences** for guides & targets, and **graphics** (e.g. secondary
structure of a target).

![The details page](${helpGallery.helpDetailsUrl})`
  },
  {
    id: 'organism',
    title: 'Organism page',
    content: `Similar pages exist for **organisms** (the pages accessible from
the home page): there you can find **various statistics** about this organism
(e.g. number of guides, % of modifications associated with a guide), a list to
**the different objects of this organism**, as well as **a simple karyotype**
showing the **position of the objects on the chromosomes**.

![The organism statistics page](${helpGallery.helpOrganism})`
  },
  {
    id: 'conservation-alignment',
    title: 'Conservation (alignment)',
    content: `As said before, snoBoard offers the possibility to perform
**conservation analysis** against the sequences in the base, i.e. targets &
guides.

From the home screen, when you navigate to the **_Conservation_** section (
either guides or targets), a selector allows to **chose which sequences to
compare**.

![The alignment selector](${helpGallery.helpAlignmentSelection})

Once some sequences are selected, their **alignment is displayed** underneath.
If aligning **target sequences**, the **modifications** of the sequences which
are in the base are **highlighted** on the alignment.

![A target alignment](${helpGallery.helpAlignment})`
  },
  {
    id: 'conservation-bases',
    title: 'Conservation (bases)',
    content: `Between the sequence selector and the alignement, some controls
are available to **display conservation analysis on the alignment**.

The first analysis is the **bases conservation analysis**, which **colours the
nucleotides** based on their **conservation rate in the alignement**: if a base
is highly conserved, it will appear green, otherwise orange or red. The
**thresholds can be configured** to suit your needs.

![A target alignment with bases
conservation enabled](${helpGallery.helpAlignmentBases})`
  },
  {
    id: 'conservation-modifications',
    title: 'Conservation (modifications)',
    content: `The second analysis is the **modifications conservation analysis**
, which **adds a track** to the alignment containing **the modifications
conserved** across the sequences.

![A target alignment with modifications conservation
enabled](${helpGallery.helpAlignmentModifications})

This analysis **needs to be configured** with several parameters:

- the **sequences** to analyse ;
- the **type of the modifications** to analyse ;
- the **type of analysis** to perform.

Two analysis modes _(= types)_ are available:

- **\`Common\`** will show **modifications which are conserved on all the
  selected sequences** ;
- **\`Specific\`** will also show **modifications which are conserved on all the
  selected sequences**, but **only on them**.

They can be represented graphically this way:

<table style="max-width: fit-content; margin: auto;">
<thead>
<tr>
<th><strong><code>Common</code></strong> mode</th>
<th><strong><code>Specific</code></strong> mode</th>
</tr>
</thead>
<tbody>
<tr>
<td><img alt="A Venn diagram representing the behavior of the &quot;common&quot; conservation analysis" src="/src/assets/images/help/alignment-venn-common.svg"></td>
<td><img alt="A Venn diagram representing the behavior of the &quot;specific&quot; conservation analysis" src="/src/assets/images/help/alignment-venn-specific.svg"></td>
</tr>
</tbody>
</table>

**Each circle** represents the **modifications of a sequence** in the
alignment (here three sequences are aligned,one per organism), and the
**intersections** of the circles are **modifications which are conserved** across
the sequences.

The sequences of _A. thaliana_ and _S. cerevisiae_ are selected for the analysis,
so the **kept modifications** are the ones which **are conserved between those
sequences**, but in **\`Specific\`** mode the modifications which **are also
conserved in the non-selected sequence** of _H. sapiens_ **are excluded** from the
results of the analysis.
`
  },
  {
    id: 'conclusion',
    title: '',
    content: `![snoBoard's logo](${logoUrl})

_And that's it, you've reached the end of this guide, we hope that you now have
a basic understanding of how the site work.  
If it is not the case, or you find anything missing or which can be improved,
don't hesitate to [contact us](/contact)!_

##### In the meantime, have a good ride on **snoBoard**!`
  }
]
