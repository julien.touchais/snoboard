/**
 * Creates an array of numbers (positive and/or negative) progressing from start
 * up to, but not including, end. If provided, a custom step can be used.
 * @param start The start of the range.
 * @param end The end of the range.
 * @param step The value to increment or decrement by.
 * @returns
 */
export function range(start: number, end: number, step?: number): number[]
/**
 * Creates an array of numbers (positive and/or negative) progressing from 0
 * up to, but not including, end.
 * @param end The end of the range.
 * @returns
 */
export function range(end: number): number[]
export function range(
  startOrEnd: number,
  optionalEnd?: number,
  optionalStep?: number
): number[] {
  const start = optionalEnd ? startOrEnd : 0
  const end = optionalEnd || startOrEnd
  const rangeLength = end - start
  const rangeDirection = rangeLength / Math.abs(rangeLength)
  const step = optionalStep || rangeDirection
  if (rangeDirection * step < 0) {
    return []
  }
  return Array.from(
    { length: Math.floor((end - start) / step) },
    (_, i) => start + i * step
  )
}
