/**
 * Resolves a promise after a given time.
 * @param delay The delay in ms to wait before resolving promise
 * @returns A promise which will resolve after the given time
 */
export const promisedWait = (delay: number) =>
  new Promise<void>((resolve) => {
    setTimeout(() => resolve(), delay)
  })
