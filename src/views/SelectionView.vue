<script setup lang="ts">
/**
 * Vue imports
 */
import { computed, onMounted, ref } from 'vue'
/**
 * Components imports
 */
import MainLayout from '@/layouts/MainLayout.vue'
import SelectionForm from '@/components/SelectionForm.vue'
import Button from 'primevue/button'
import IconFa6SolidSliders from '~icons/fa6-solid/sliders'
import IconFa6SolidTable from '~icons/fa6-solid/table'
import IconFa6SolidBullseye from '~icons/fa6-solid/bullseye'
import IconFa6SolidCircleNodes from '~icons/fa6-solid/circle-nodes'
import IconSnoboardSequence from '~icons/snoboard/sequence'
/**
 * Composables imports
 */
import { useRouter } from 'vue-router'
import { useTitle } from '@vueuse/core'
/**
 * Other 3rd-party imports
 */
import { capitalize as _capitalize } from 'lodash-es'
/**
 * Types imports
 */
import type { SelectionModel } from '@/components/SelectionForm.vue'
/**
 * Utils imports
 */
import { SELECTION_MODES } from '@/utils/constant'

/**
 * Type of data to select.
 */
export type SelectionModesType = (typeof SELECTION_MODES)[number]

/**
 * A selection option.
 */
export interface SelectionOptionModel {
  /** Label of the option. */
  label: string
  /** Value of the option. */
  value: string
  /** Optional disabling state of the option. */
  isDisabled?: boolean
}

/**
 * The columns to display in the table when navigating to it, depending on the
 * active tab (i.e. form mode).
 */
const TABLE_COLUMNS_BY_MODE = {
  modification: [
    'modificationId',
    'modificationName',
    'modificationPosition',
    'modificationSymbol',
    'modificationType',
    'targetName',
    'organismName',
    'organismId'
  ],
  guide: [
    'guideId',
    'guideName',
    'guideClass',
    'guideSubclass',
    'guideChromosomeName',
    'guideLength',
    'organismName',
    'organismId'
  ],
  target: [
    'targetId',
    'targetName',
    'targetLength',
    'targetClass',
    'targetUnit',
    'targetChromosomeName',
    'organismName',
    'organismId'
  ]
}

/**
 * Component props.
 */
const props = defineProps<{
  /** The mode in which the selection form is at landing (usually passed
   * from URL). */
  initialMode: SelectionModesType
}>()

/**
 * Vue Router instance reactive object.
 */
const router = useRouter()

/**
 * The currently selected mode.
 */
const activeMode = ref(props.initialMode)

/**
 * Title of the page, reactively updated when data is fetched.
 */
const pageTitle = computed(
  () => `${_capitalize(activeMode.value)} • Selection | snoBoard`
)
// Bind actual page title to computed one
onMounted(() => useTitle(pageTitle))

/**
 * Callback to change the URL when switching tab.
 * @param e The event emitted when changing tab.
 */
const updateUrlQuery = (newMode: SelectionModesType) => {
  if (newMode) {
    router.replace({
      name: 'selection',
      query: { mode: newMode }
    })
  }
}

/**
 * The current selection.
 */
const selection = ref<SelectionModel>({
  modification: undefined,
  guide: undefined,
  target: undefined
})

/**
 * The filter to apply to the table when navigating to it, derived from current
 * selection.
 */
const tableFilters = computed(() => ({
  guideSubclass:
    activeMode.value === 'guide'
      ? selection.value.guide?.guideSubclassLabels
      : undefined,
  targetName: selection.value[activeMode.value]?.targetNames,
  modificationType: selection.value[activeMode.value]?.modificationTypes,
  organismId: selection.value[activeMode.value]?.organismIds
}))

/**
 * The ID of the target which matches the selection if it is unique (i.e. a
 * single target name & a single species is selected), `undefined` otherwise.
 * One target ID is used for each mode able to provide one.
 */
const onlyTargetIdByMode = ref<{ modification?: string; target?: string }>({
  modification: undefined,
  target: undefined
})

/**
 * The `onlyTargetId` (see `onlyTargetIdByMode`) corresponding to the active
 * mode.
 */
const onlyTargetIdActiveMode = computed(() =>
  activeMode.value === 'modification'
    ? onlyTargetIdByMode.value.modification
    : activeMode.value === 'target'
      ? onlyTargetIdByMode.value.target
      : undefined
)
</script>

<template>
  <MainLayout padded>
    <h1 class="mb-8 text-center text-3xl font-semibold text-slate-700">
      <icon-fa6-solid-sliders class="mb-1 mr-2 inline" />
      Advanced selection
    </h1>

    <h3 class="my-8 text-center italic text-slate-500">
      Filter the {{ activeMode }}s of the snoBoard's database to list them in
      the table.
    </h3>

    <SelectionForm
      v-model:current-mode-model="activeMode"
      v-model:selection-model="selection"
      v-model:only-target-id-by-mode-model="onlyTargetIdByMode"
      @update:current-mode-model="updateUrlQuery"
    />

    <div class="mt-8 flex justify-center gap-4">
      <RouterLink
        :to="{
          name: 'table',
          query: {
            columns: TABLE_COLUMNS_BY_MODE[activeMode],
            ...tableFilters
          }
        }"
      >
        <Button>
          <icon-fa6-solid-table />
          <span class="ml-2">View in table</span>
        </Button>
      </RouterLink>

      <div
        v-if="activeMode === 'target'"
        v-tooltip.top="
          !onlyTargetIdByMode.target && {
            value: 'Select a unique target/species pair',
            pt: {
              text: {
                style: {
                  textAlign: 'center',
                  fontStyle: 'italic'
                }
              }
            }
          }
        "
      >
        <RouterLink
          :to="{
            name: 'targetDetails',
            query: {
              id: onlyTargetIdByMode.target
            },
            hash: '#sequence-panel'
          }"
          :class="{ 'pointer-events-none': !onlyTargetIdByMode.target }"
        >
          <Button :disabled="!onlyTargetIdByMode.target">
            <icon-snoboard-sequence />
            <span class="ml-2">View sequence</span>
          </Button>
        </RouterLink>
      </div>

      <div
        v-if="activeMode === 'target' || activeMode === 'modification'"
        v-tooltip.top="
          !onlyTargetIdActiveMode && {
            value: 'Select a unique target/species pair',
            pt: {
              text: {
                style: {
                  textAlign: 'center',
                  fontStyle: 'italic'
                }
              }
            }
          }
        "
      >
        <RouterLink
          :to="{
            name: 'targetDetails',
            query: {
              id: onlyTargetIdActiveMode,
              graphicsTab: '2d-struct'
            },
            hash: '#graphics-panel'
          }"
          :class="{ 'pointer-events-none': !onlyTargetIdActiveMode }"
        >
          <Button :disabled="!onlyTargetIdActiveMode">
            <icon-fa6-solid-bullseye v-if="activeMode === 'modification'" />
            <icon-fa6-solid-circle-nodes v-else />
            <span class="ml-2">
              View
              {{ activeMode === 'modification' ? 'on target' : '' }}
              structure
            </span>
          </Button>
        </RouterLink>
      </div>
    </div>
  </MainLayout>
</template>
