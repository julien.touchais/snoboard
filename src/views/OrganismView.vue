<script setup lang="ts">
/**
 * Vue imports
 */
import { ref, computed, toRef, onMounted } from 'vue'
/**
 * Components imports
 */
import MainLayout from '@/layouts/MainLayout.vue'
import KaryotypeBoard, {
  type ChromosomeModel
} from '@/components/KaryotypeBoard.vue'
import BaseLinkListCard from '@/components/BaseLinkListCard.vue'
import BaseRenderedMarkdown from '@/components/BaseRenderedMarkdown.vue'
import Panel from 'primevue/panel'
import Card from 'primevue/card'
import Button from 'primevue/button'
import Divider from 'primevue/divider'
import Chart from 'primevue/chart'
import ToggleButton from 'primevue/togglebutton'
import IconTablerSquareRoundedChevronRightFilled from '~icons/tabler/square-rounded-chevron-right-filled'
import IconTablerSquareRoundedChevronRight from '~icons/tabler/square-rounded-chevron-right'
import IconFa6SolidDna from '~icons/fa6-solid/dna'
import IconFa6SolidFileLines from '~icons/fa6-solid/file-lines'
import IconFa6SolidTable from '~icons/fa6-solid/table'
import IconCarbonAccessibilityColor from '~icons/carbon/accessibility-color'
import IconCarbonAccessibilityColorFilled from '~icons/carbon/accessibility-color-filled'
/**
 * Composables imports
 */
import { useQuery } from '@urql/vue'
import { useTitle } from '@vueuse/core'
/**
 * Other 3rd-party imports
 */
import {
  sortBy as _sortBy,
  capitalize as _capitalize,
  countBy as _countBy
} from 'lodash-es'
import pattern from 'patternomaly'
/**
 * Types imports
 */
import type { LinkListItemModel } from '@/components/BaseLinkListCard.vue'
import type { GenomeObjectModel } from '@/components/KaryotypeBoard.vue'
import type { TailwindDefaultShadableColorNameModel } from '@/typings/styleTypes'
import {
  GraphQlType,
  GuideClass,
  SequenceClass,
  type ModifType
} from '@/gql/codegen/graphql'
import type { ChartOptions } from 'chart.js'
/**
 * Utils imports
 */
import {
  composePlural,
  formatGuideSubclass,
  formatModificationType
} from '@/utils/textFormatting'
import { organismByIdQuery } from '@/gql/queries'
import { isDefined } from '@/typings/typeUtils'
import {
  composeDivergentScale,
  getColorWithOptionalShade,
  getModificationColor
} from '@/utils/colors'

/**
 * An information field.
 */
type FieldNameModel =
  | 'modifications'
  | 'guides'
  | 'targets'
  | 'stats-modifications'
  | 'stats-guides'
  | 'none'

/**
 * The options for the doughnut chart (chart.js options).
 */
const DOUGHNUT_CHART_OPTIONS: ChartOptions = {
  plugins: {
    legend: {
      // display: false,
      position: 'right',
      labels: {
        color: getColorWithOptionalShade('slate', '700'),
        font: {
          size: 18,
          weight: 700,
          style: 'italic'
        },
        padding: 16,
        filter: (legendItem, chartData) =>
          legendItem.index != null &&
          !!chartData.datasets[0]?.data[legendItem.index],
        pointStyle: 'rectRounded',
        usePointStyle: true
      }
    }
  },
  layout: {
    padding: 25
  },
  datasets: {
    doughnut: {
      borderRadius: 10,
      offset: 50,
      hoverOffset: 75
    }
  }
}

/**
 * Component props.
 */
const props = defineProps<{
  // The ID of the organism to display
  organismId: number
}>()

/**
 * Reactive urql GraphQL query object, updated with query state & response.
 */
const gqlQuery = useQuery({
  query: organismByIdQuery,
  variables: toRef(() => ({
    id: props.organismId
  }))
})

/**
 * The organism to display, reactively updated when fetched.
 */
const organism = computed(() => gqlQuery.data.value?.organisms[0])

/**
 * The modifications of this organism targets, reactively updated when fetched.
 */
const modifications = computed(() => ({
  /** The list of the modifications of this organism targets. */
  list: gqlQuery.data.value?.modifications,
  /** The count of the modifications of this organism targets. */
  totalCount: gqlQuery.data.value?.allModificationsCount.count,
  /** The count of the modifications of this organism targets which have at
   * least one identified guide.
   */
  nonOrphanCount: gqlQuery.data.value?.nonOrphanModificationsCount.count
}))

/**
 * The targets of this organism, reactively updated when fetched.
 */
const targets = computed(() => ({
  /** The list of the guides of this organism. */
  list: gqlQuery.data.value?.targets,
  /** The count of the targets of this organism. */
  totalCount: gqlQuery.data.value?.allTargetsCount.count
}))

/**
 * The guides of this organism, reactively updated when fetched.
 */
const guides = computed(() => ({
  /** The list of the guides of this organism. */
  list: gqlQuery.data.value?.guides,
  /** The count of the guides of this organism. */
  totalCount: gqlQuery.data.value?.allGuidesCount.count,
  /** The count of the guides of this organism guiding at least one
   * modification.
   */
  nonOrphanCount: gqlQuery.data.value?.nonOrphanGuidesCount.count
}))

/**
 * Title of the page, reactively updated when data is fetched.
 */
const pageTitle = computed(() =>
  organism.value
    ? `${organism.value.shortname} • Organism | snoBoard`
    : 'Organism | snoBoard'
)
// Bind actual page title to computed one.
onMounted(() => useTitle(pageTitle))

/**
 * The field selected for displaying the list of its entries (targets, guides
 * or modifications).
 */
const selectedField = ref<FieldNameModel>('none')

/**
 * DOM Element corresponding to the list of links to the entries
 * of the selected field.
 */
const linkListCardComponent = ref()
// Note on the type argument used here:
//   since `BaseLinkListCard` is a generic component, the usual
//   `InstanceType<typeof MyComponent>` does not work, nor does the
//   `ComponentInstance` utility type introduced in Vue 3.4 (see
//   https://github.com/vuejs/core/issues/10077).
//   Thus, this workaround has been found until a working version of
//   `ComponentInstance` is actually implemented (see
//   https://github.com/vuejs/core/issues/10077#issuecomment-1922735476 for
//   the workaround's source)
// Parameters<NonNullable<Parameters<typeof BaseLinkListCard>['2']>>['0'] | null
// UPDATE: not working yet, wait until deps update (vue-tsc, typescript)

/**
 * Select a field & scroll to top of the link list when doing so.
 * @param fieldName Name of the field to select.
 */
const selectField = (fieldName: FieldNameModel) => {
  selectedField.value = fieldName
  // See above for why using a non-null type assertion (`!`)
  linkListCardComponent.value?.scrollToListTop!()
}

/**
 * The Tailwind color associated with a given field.
 */
const colorByField: { [k: string]: TailwindDefaultShadableColorNameModel } = {
  modifications: 'sky',
  guides: 'lime',
  targets: 'amber'
}

// /**
//  * For each field, the number of 'linked data' associated with each ID of that
//  * field.
//  */
// const rowEntriesCountByField = computed(() => ({
//   /** Number of guides guiding each modification */
//   modifications: _countBy(organism.value?.table_entries, 'modification.id'),
//   /** Number of guides guiding each modification */
//   guides: _countBy(organism.value?.table_entries, 'guide.id'),
//   /** Number of guides guiding each modification */
//   targets: _countBy(linkedModifications.value, 'target.id'),
//   none: {}
// }))

/**
 * For each field, a `LinkListItemModel` of the unique entries of that field.
 */
const itemsByField = computed<{
  [k: string]:
    | LinkListItemModel<ModifType | SequenceClass | string>[]
    | undefined
}>(() => ({
  modifications: modifications.value.list?.map((modification) => ({
    id: modification.id,
    title: modification.name,
    subtitle: modification.id,
    link: {
      name: `modificationDetails`,
      query: { id: modification.id }
    },
    details: [
      modification.type_short_label || '',
      modification.symbol_label,
      `${modification.guidesAggregate?.count} linked guide${
        (modification.guidesAggregate?.count || 0) > 1 ? 's' : ''
      }`
    ]
  })),
  guides: guides.value.list?.map((guide) => ({
    id: guide.id,
    title: guide.name || '',
    subtitle: guide.id,
    link: {
      name: `guideDetails`,
      query: { id: guide.id }
    },
    details: [
      guide.class,
      guide.subclass_label,
      `${guide.modificationsAggregate?.count} guided modification${
        (guide.modificationsAggregate?.count || 0) > 1 ? 's' : ''
      }`
    ]
  })),
  targets: targets.value.list?.map((target) => ({
    id: target.id,
    title: target.name || '',
    subtitle: target.id,
    link: {
      name: `${selectedField.value.slice(0, -1)}Details`,
      query: { id: target.id }
    },
    details: [
      target.class || SequenceClass.Other,
      target.unit || undefined,
      `${target.modificationsAggregate?.count} registered modification${
        (target.modificationsAggregate?.count || 0) > 1 ? 's' : ''
      }`
    ]
  })),
  none: []
}))

/**
 * A list of the main info of each chromosome of the organism to display on
 * the karyotype (i.e. len > 1e6).
 */
const karyotypeChromosomes = computed<ChromosomeModel[]>(
  () =>
    organism.value?.genomes[0]?.sequences
      .filter((sequence) => sequence.graphql_type === GraphQlType.Chromosome)
      .map<ChromosomeModel>((chromosome) => ({
        name: chromosome.name || '',
        id: chromosome.id,
        length: chromosome.length,
        centromereCenter: Math.round(chromosome.length / 2),
        textLeft: 'C/D box',
        textRight: 'H/ACA box',
        metadata: {
          guideCount: chromosome.guides.totalCount,
          altnames: chromosome.altnames,
          description: chromosome.description
        }
      }))
      .filter((chr) => chr.length > 1e6) || []
)

/**
 * A list of 'objects' to display on the karyotype, (i.e. the organism guides).
 */
const karyotypeObjects = computed<GenomeObjectModel[] | undefined>(() =>
  guides.value.list
    ?.filter((guide) => guide.subclass === GuideClass.Cd)
    .map<GenomeObjectModel | undefined>((guide) =>
      guide.chromosomeConnection.edges[0]
        ? {
            name: guide.name || '',
            id: guide.id,
            start: guide.chromosomeConnection.edges[0].properties.start,
            end: guide.chromosomeConnection.edges[0].properties.end,
            chromosomeId: guide.chromosomeConnection.edges[0].node.id,
            side: 'left',
            color: '#efcb58',
            highlightColor: '#e87'
          }
        : undefined
    )
    .filter(isDefined)
    .concat(
      guides.value.list
        ?.filter((guide) => guide.subclass === GuideClass.Haca)
        .map<GenomeObjectModel | undefined>((guide) =>
          guide.chromosomeConnection.edges[0]
            ? {
                name: guide.name || '',
                id: guide.id,
                start: guide.chromosomeConnection.edges[0].properties.start,
                end: guide.chromosomeConnection.edges[0].properties.end,
                chromosomeId: guide.chromosomeConnection.edges[0].node.id,
                side: 'right',
                color: '#c8baff',
                highlightColor: '#8bf'
              }
            : undefined
        )
        .filter(isDefined)
    )
)

/**
 * For each chromosome, a `LinkListItemModel` of the guides present on that
 * chromosome.
 */
const guidesByKaryotypeChromosome = computed<{
  [k: string]: LinkListItemModel[]
}>(() =>
  karyotypeChromosomes.value.reduce<{ [k: string]: LinkListItemModel[] }>(
    (guidesByKaryotypeChromosome, currKaryotypeChromosome) => ({
      ...guidesByKaryotypeChromosome,
      [currKaryotypeChromosome.id]: _sortBy(
        guides.value.list?.filter(
          (guide) =>
            guide.chromosomeConnection.edges[0]?.node.id ===
              currKaryotypeChromosome.id &&
            (guide.subclass === GuideClass.Cd ||
              guide.subclass === GuideClass.Haca)
        ),
        (guide) => guide.chromosomeConnection.edges[0]?.properties.start
      ).map((guide) => ({
        id: guide.id,
        title: guide.name || '',
        subtitle: guide.id,
        link: {
          name: 'guideDetails',
          query: { id: guide.id }
        },
        details: [
          guide.class,
          `Start: ${guide.chromosomeConnection.edges[0]?.properties.start}`,
          `End: ${guide.chromosomeConnection.edges[0]?.properties.end}`
        ]
      }))
    }),
    {}
  )
)

/**
 * Coverages values (% of guides _(resp. modifications)_ which has at least one
 * modification _(resp.guide)_ linked to it), in percents.
 */
const coverages = computed(() => ({
  guide: (
    ((guides.value.nonOrphanCount || 0) / (guides.value.totalCount || 1)) *
    100
  ).toPrecision(3),
  modification: (
    ((modifications.value.nonOrphanCount || 0) /
      (modifications.value.totalCount || 1)) *
    100
  ).toPrecision(3)
}))

/**
 * Orphan counts (number of guides _(resp. modifications)_ which has no
 * modification _(resp.guide)_ linked to it).
 */
const orphanCounts = computed(() => ({
  'stats-guides':
    (guides.value.totalCount || 0) - (guides.value.nonOrphanCount || 0),
  'stats-modifications':
    (modifications.value.totalCount || 0) -
    (modifications.value.nonOrphanCount || 0)
}))

/**
 * Wether to display the charts in colour-blind mode or not.
 */
const isColourBlindMode = ref(false)

/**
 * For each guide subclass represented at least once in the database, how many
 * guides of this subclass is there.
 */
const guideCountBySubclass = computed<{ [k in GuideClass]?: number }>(() =>
  _countBy(gqlQuery.data.value?.guides, 'subclass')
)

/**
 * For each modification type represented at least once in the database, how many
 * modification of this type is there.
 */
const modificationCountByType = computed(() =>
  _countBy(gqlQuery.data.value?.modifications, 'type')
)

/**
 * Config for the various charts of the page.
 */
const chartConfigs = computed(() => ({
  'stats-guides': {
    labels: Object.keys(guideCountBySubclass.value).map((guideSubclassCode) =>
      formatGuideSubclass(guideSubclassCode as GuideClass)
    ),
    datasets: [
      {
        label: 'Count',
        data: Object.values(guideCountBySubclass.value),
        backgroundColor: isColourBlindMode.value
          ? pattern.generate(
              composeDivergentScale(
                '#65a30d',
                '#fbbf24',
                Object.values(guideCountBySubclass.value).length
              )
            )
          : composeDivergentScale(
              '#65a30d',
              '#fbbf24',
              Object.values(guideCountBySubclass.value).length
            ).map((hexCode) => `${hexCode}80`),
        hoverBackgroundColor: !isColourBlindMode.value
          ? composeDivergentScale(
              '#65a30d',
              '#fbbf24',
              Object.values(guideCountBySubclass.value).length
            ).map((hexCode) => `${hexCode}b0`)
          : undefined,
        borderColor: composeDivergentScale(
          '#65a30d',
          '#fbbf24',
          Object.values(guideCountBySubclass.value).length
        )
      }
    ]
  },
  'stats-modifications': {
    labels: Object.keys(modificationCountByType.value).map((modifTypeCode) =>
      formatModificationType(modifTypeCode as ModifType)
    ),
    datasets: [
      {
        label: 'Count',
        data: Object.values(modificationCountByType.value),
        backgroundColor: isColourBlindMode.value
          ? pattern.generate(
              Object.keys(modificationCountByType.value).map((modifTypeCode) =>
                getModificationColor(modifTypeCode as ModifType, '400')
              )
            )
          : Object.keys(modificationCountByType.value)
              .map((modifTypeCode) =>
                getModificationColor(modifTypeCode as ModifType, '400')
              )
              .map((hexCode) => `${hexCode}80`),
        hoverBackgroundColor: !isColourBlindMode.value
          ? Object.keys(modificationCountByType.value)
              .map((modifTypeCode) =>
                getModificationColor(modifTypeCode as ModifType, '400')
              )
              .map((hexCode) => `${hexCode}b0`)
          : undefined,
        borderColor: Object.keys(modificationCountByType.value).map(
          (modifTypeCode) =>
            getModificationColor(modifTypeCode as ModifType, '400')
        )
      }
    ]
  }
}))
</script>

<template>
  <MainLayout padded>
    <div class="relative hidden md:block">
      <RouterLink
        v-if="organism"
        :to="{ name: 'table', query: { organismId: organism.id } }"
      >
        <Button
          severity="secondary"
          outlined
          class="!absolute flex gap-2 !shadow-none top-0 right-8"
        >
          <icon-fa6-solid-table class="text-xl" />
          View table for {{ organism.shortname }}
        </Button>
      </RouterLink>
    </div>

    <h1
      v-if="organism"
      class="mb-4 text-center text-3xl font-semibold text-slate-700"
    >
      <BaseRenderedMarkdown :stringified-markdown="organism.label" />
    </h1>
    <h2 class="mb-8 text-center text-2xl text-slate-400">
      Organism •
      <span
        v-tooltip.bottom="'NCBI Taxonomy ID'"
        class="relative cursor-help font-mono after:absolute after:bottom-0 after:left-0 after:right-0 after:border-b-[3px] after:border-dotted after:border-current"
        >{{ organism?.id }}</span
      >
    </h2>

    <div class="mx-8 mb-16 grid grid-cols-3 gap-8">
      <div class="left flex flex-col gap-4">
        <Button
          unstyled
          :class="[
            { 'border-sky-600 shadow-lg': selectedField === 'modifications' },
            'flex cursor-pointer justify-between gap-[2ch] rounded-md border bg-slate-50 p-5 text-slate-700 hover:border-sky-600 hover:shadow-lg'
          ]"
          @click="selectField('modifications')"
        >
          <div
            class="flex max-w-[30ch] grow items-center justify-between text-xl"
          >
            <h4 class="italic">Registered modifications:</h4>
            <p class="font-semibold">
              {{ modifications.totalCount || '...' }}
            </p>
          </div>
          <icon-tabler-square-rounded-chevron-right-filled
            v-if="selectedField === 'modifications'"
            class="m-auto text-4xl text-sky-600"
          />
          <icon-tabler-square-rounded-chevron-right
            v-else
            class="m-auto text-4xl"
          />
        </Button>

        <Button
          unstyled
          :class="[
            { 'border-lime-600 shadow-lg': selectedField === 'guides' },
            'flex cursor-pointer justify-between gap-[2ch] rounded-md border bg-slate-50 p-5 text-slate-700 hover:border-lime-600 hover:shadow-lg'
          ]"
          @click="selectField('guides')"
        >
          <div
            class="flex max-w-[30ch] grow items-center justify-between text-xl"
          >
            <h4 class="italic">Registered guides:</h4>
            <p class="font-semibold">
              {{ guides.totalCount || '...' }}
            </p>
          </div>
          <icon-tabler-square-rounded-chevron-right-filled
            v-if="selectedField === 'guides'"
            class="m-auto text-4xl text-lime-600"
          />
          <icon-tabler-square-rounded-chevron-right
            v-else
            class="m-auto text-4xl"
          />
        </Button>

        <Button
          unstyled
          :class="[
            { 'border-amber-600 shadow-lg': selectedField === 'targets' },
            'flex cursor-pointer justify-between gap-[2ch] rounded-md border bg-slate-50 p-5 text-slate-700 hover:border-amber-600 hover:shadow-lg'
          ]"
          @click="selectField('targets')"
        >
          <div
            class="flex max-w-[30ch] grow items-center justify-between text-xl"
          >
            <h4 class="italic">Registered targets:</h4>
            <p class="font-semibold">
              {{ targets.totalCount || '...' }}
            </p>
          </div>
          <icon-tabler-square-rounded-chevron-right-filled
            v-if="selectedField === 'targets'"
            class="m-auto text-4xl text-amber-600"
          />
          <icon-tabler-square-rounded-chevron-right
            v-else
            class="m-auto text-4xl"
          />
        </Button>

        <Button
          unstyled
          :class="[
            {
              'border-slate-600 shadow-lg':
                selectedField === 'stats-modifications'
            },
            'flex cursor-pointer justify-between gap-[2ch] rounded-md border bg-slate-50 p-5 text-slate-700 hover:border-slate-600 hover:shadow-lg'
          ]"
          @click="selectField('stats-modifications')"
        >
          <div class="max-w-[30ch] grow text-left text-xl">
            <div class="flex items-center justify-between">
              <h4 class="italic">Modification coverage:</h4>
              <p class="font-semibold">{{ coverages.modification }} %</p>
            </div>
            <small class="mt-2 block text-sm italic text-slate-400">
              % of modifications which are associated with at least one guide
            </small>
          </div>
          <icon-tabler-square-rounded-chevron-right-filled
            v-if="selectedField === 'stats-modifications'"
            class="m-auto text-4xl text-slate-600"
          />
          <icon-tabler-square-rounded-chevron-right
            v-else
            class="m-auto text-4xl"
          />
        </Button>

        <Button
          unstyled
          :class="[
            {
              'border-slate-600 shadow-lg': selectedField === 'stats-guides'
            },
            'flex cursor-pointer justify-between gap-[2ch] rounded-md border bg-slate-50 p-5 text-slate-700 hover:border-slate-600 hover:shadow-lg'
          ]"
          @click="selectField('stats-guides')"
        >
          <div class="max-w-[30ch] grow text-left text-xl">
            <div class="flex items-center justify-between">
              <h4 class="italic">Guide coverage:</h4>
              <p class="font-semibold">{{ coverages.guide }} %</p>
            </div>
            <small class="text-sm italic text-slate-400">
              % of guides which are implied in at least one modification
            </small>
          </div>
          <icon-tabler-square-rounded-chevron-right-filled
            v-if="selectedField === 'stats-guides'"
            class="m-auto text-4xl text-slate-600"
          />
          <icon-tabler-square-rounded-chevron-right
            v-else
            class="m-auto text-4xl"
          />
        </Button>
      </div>

      <div class="right col-span-2">
        <BaseLinkListCard
          v-if="
            selectedField !== 'stats-guides' &&
            selectedField !== 'stats-modifications'
          "
          ref="linkListCardComponent"
          :color="colorByField[selectedField]"
          outlined
          :link-items="itemsByField[selectedField] || []"
          :disabled="selectedField === 'none'"
        >
          <template #disabled>
            <strong>&larr;</strong>
            Chose a data type on the left to see its elements, or a coverage to
            see some data statistics
          </template>
          <template #detail0="{ detail }">
            <BaseRenderedMarkdown
              :stringified-markdown="detail || ''"
              inline-content
            />
          </template>
          <template #detail1="{ detail }">
            <BaseRenderedMarkdown
              :stringified-markdown="detail || ''"
              inline-content
            />
          </template>
          <template #detail2="{ detail }">
            <BaseRenderedMarkdown
              :stringified-markdown="detail || ''"
              inline-content
            />
          </template>
        </BaseLinkListCard>

        <Card v-else class="border border-slate-600">
          <template #title>
            <h3 class="text-center text-xl font-semibold">
              {{ _capitalize(selectedField.match(/stats-(\w+)/)?.[1]) }}
              statistics
            </h3>
          </template>
          <template #content>
            <div class="grid grid-cols-3 gap-x-4">
              <span class="my-auto text-left italic text-slate-400">
                {{
                  composePlural(
                    orphanCounts[selectedField],
                    `Orphan ${selectedField.match(/stats-(\w+?)s?$/)?.[1]}`
                  )
                }}:
              </span>
              <em
                class="col-span-2 my-auto text-left text-lg font-bold not-italic text-slate-700"
              >
                {{ orphanCounts[selectedField] }}
              </em>

              <Divider class="col-span-3" />

              <span class="my-auto text-left italic text-slate-400">
                Repartition of the
                {{ selectedField.match(/stats-(\w+)/)?.[1] }} by type:
              </span>
              <div class="relative col-span-2">
                <ToggleButton
                  v-model="isColourBlindMode"
                  on-label="Colour-blind mode"
                  off-label="Colour-blind mode"
                  class="!absolute right-0 top-0 z-10 gap-2"
                >
                  <template #icon="{ value }">
                    <icon-carbon-accessibility-color-filled
                      v-if="value"
                      class="text-2xl"
                    />
                    <icon-carbon-accessibility-color v-else class="text-2xl" />
                  </template>
                </ToggleButton>
                <Chart
                  type="doughnut"
                  :data="chartConfigs[selectedField]"
                  :options="DOUGHNUT_CHART_OPTIONS"
                  class="mx-auto aspect-square w-full max-w-2xl"
                />
              </div>
            </div>
          </template>
        </Card>
      </div>
    </div>

    <Panel
      toggleable
      class="mx-auto max-w-7xl break-words 2xl:max-w-[100rem]"
      :pt="{
        header: { class: '!bg-slate-50' },
        toggler: { class: 'hover:!bg-slate-100' }
      }"
    >
      <template #header="scope">
        <icon-fa6-solid-dna :class="scope.class" />
        <span :class="scope.class">Chromosomes</span>
      </template>

      <KaryotypeBoard
        v-if="organism"
        :chromosomes="karyotypeChromosomes"
        :objects="karyotypeObjects"
      >
        <template #karyotypeChromosomeAppend="{ chromosome }">
          <p class="text-center text-xl">
            Guides:
            <span class="font-bold italic">
              {{ chromosome.metadata?.guideCount }}
            </span>
          </p>
        </template>

        <template
          #expandedChromosomeAppend="{
            chromosome,
            highlightObject,
            unhighlightObject
          }"
        >
          <Card
            v-if="
              chromosome.metadata?.description ||
              chromosome.metadata?.altnames?.length
            "
            class="mx-auto mb-8 max-w-4xl border text-center !text-lg text-slate-700 !shadow-none 2xl:max-w-5xl"
            :pt="{
              content: {
                style: {
                  padding: 0,
                  textAlign: 'justify'
                }
              }
            }"
          >
            <template v-if="chromosome.metadata?.description" #title>
              <span>
                <icon-fa6-solid-file-lines class="mb-1 mr-2 inline-block" />
                <span>Description</span>
              </span>
            </template>

            <template #content>
              <BaseRenderedMarkdown
                v-if="chromosome.metadata?.description"
                :stringified-markdown="chromosome.metadata.description"
                class="flex justify-center"
              />

              <div
                :class="[
                  'text-center italic text-slate-400',
                  { 'mt-4': chromosome.metadata?.description }
                ]"
              >
                Length:
                <em class="text-lg font-bold not-italic text-slate-700">{{
                  chromosome.length
                }}</em>
              </div>

              <div
                v-if="chromosome.metadata?.altnames?.length"
                class="mt-4 italic text-slate-400"
              >
                Alternative names:
                {{ chromosome.metadata.altnames.join(', ') }}
              </div>
            </template>
          </Card>

          <BaseLinkListCard
            list-title="Guides list"
            :link-items="guidesByKaryotypeChromosome[chromosome.id] || []"
            color="sky"
            outlined
            :hover-event="true"
            @enter-item="highlightObject"
            @leave-item="unhighlightObject"
          />
        </template>
      </KaryotypeBoard>
    </Panel>
  </MainLayout>
</template>
