<script setup lang="ts">
/**
 * Vue imports
 */
import { computed, onMounted, ref, toRef } from 'vue'
/**
 * Components imports
 */
import MainLayout from '@/layouts/MainLayout.vue'
import InteractionBoard from '@/components/InteractionBoard.vue'
import BaseRenderedMarkdown from '@/components/BaseRenderedMarkdown.vue'
import TabPanel from 'primevue/tabpanel'
import TabView from 'primevue/tabview'
import Divider from 'primevue/divider'
import Chip from 'primevue/chip'
import Panel from 'primevue/panel'
import Tag from 'primevue/tag'
import Button from 'primevue/button'
import IconFa6SolidCircleInfo from '~icons/fa6-solid/circle-info'
import IconFa6SolidDrawPolygon from '~icons/fa6-solid/draw-polygon'
import IconFa6SolidTable from '~icons/fa6-solid/table'
/**
 * Composables imports
 */
import { useQuery } from '@urql/vue'
import { useTitle } from '@vueuse/core'
/**
 * Other 3rd-party imports
 */
import { sortBy as _sortBy } from 'lodash-es'
/**
 * Types imports
 */
import type { InteractionCardModel } from '@/components/InteractionCard.vue'
/**
 * Utils imports
 */
import { getModificationColor } from '@/utils/colors'
import { modificationByIdQuery } from '@/gql/queries'
import { isDefined } from '@/typings/typeUtils'
import { SequenceClass } from '@/gql/codegen/graphql'

/**
 * Enum to represent the tabs in the 'Graphics' panel.
 */
enum GraphicsTabEnum {
  interaction = 0
}

/**
 * Component props
 */
const props = defineProps<{
  /** The ID of the guide to display */
  modificationId: string
  /** The tab to display initially in the 'Graphics' panel */
  initialGraphicsPanelTab: keyof typeof GraphicsTabEnum
}>()

/**
 * Reactive urql GraphQL query object, updated with query state & response
 */
const gqlQuery = useQuery({
  query: modificationByIdQuery,
  variables: toRef(() => ({
    id: props.modificationId
  }))
})

/**
 * The modification to display, reactively updated when fetched
 */
const modification = computed(() => gqlQuery.data.value?.modifications[0])

/**
 * The list of the guides guiding the modification,
 * reactively updated when fetched
 */
const linkedGuides = computed(() =>
  _sortBy(gqlQuery.data.value?.guides, 'chromosome.name')
)

/**
 * Title of the page, reactively updated when data is fetched
 */
const pageTitle = computed(() =>
  modification.value
    ? `${modification.value.name} • Modification | snoBoard`
    : 'Modification | snoBoard'
)
// Bind actual page title to computed one
onMounted(() => useTitle(pageTitle))

/**
 * The list of the interaction, formatted for display. If some mandatory fields
 * for display (mainly sequences) are missing, the corresponding interaction
 * will not be present in the list (and thus not available for display).
 */

const interactionList = computed<InteractionCardModel[] | undefined>(() => {
  if (!modification.value) return undefined
  return modification.value.interactions
    .map<InteractionCardModel | undefined>((interaction) => {
      if (!modification.value) return undefined
      const duplexes = interaction.duplexes.map<
        InteractionCardModel['duplexes'][0] | undefined
      >((duplex) => {
        const primaryStrandConnection = duplex.primaryStrandsConnection.edges[0]
        const secondaryStrandConnection =
          duplex.secondaryStrandsConnection.edges[0]
        if (
          !primaryStrandConnection?.node.seq ||
          !secondaryStrandConnection?.node.seq
        )
          return undefined
        return {
          primaryFragment: {
            seq: primaryStrandConnection.node.seq,
            start: primaryStrandConnection.properties.start,
            end: primaryStrandConnection.properties.end,
            onParentPosition: {
              start:
                primaryStrandConnection.node.parentConnection.edges[0]
                  ?.properties.start,
              end: primaryStrandConnection.node.parentConnection.edges[0]
                ?.properties.end
            }
          },
          secondaryFragment: {
            seq: secondaryStrandConnection.node.seq,
            start: secondaryStrandConnection.properties.start,
            end: secondaryStrandConnection.properties.end,
            onParentPosition: {
              start:
                secondaryStrandConnection.node.parentConnection.edges[0]
                  ?.properties.start,
              end: secondaryStrandConnection.node.parentConnection.edges[0]
                ?.properties.end
            }
          },
          index: duplex.index
        }
      })
      if (!duplexes.every(isDefined)) return undefined
      return {
        guide: {
          id: interaction.guide.id,
          name: interaction.guide.name || undefined,
          class: interaction.guide.class,
          subclass_label: interaction.guide.subclass_label
        },
        target: {
          id: interaction.target.id,
          name: interaction.target.name || undefined,
          class: interaction.target.class || SequenceClass.Other,
          unit: interaction.target.unit || undefined
        },
        modification: {
          id: modification.value.id,
          name: modification.value.name,
          type: modification.value.type || undefined,
          position: modification.value.position,
          symbol: modification.value.symbol,
          symbol_label: modification.value.symbol_label
        },
        duplexes
      }
    })
    .filter(isDefined)
})

/**
 * Tailwind color corresponding to the modification
 */
const modificationTailwindColor = computed(() =>
  modification.value?.type
    ? getModificationColor(modification.value?.type)
    : 'slate'
)

/**
 * Number of guides linked to the modification, equals to `'...'` if not fetched yet
 */
const linkedGuidesCount = computed(() =>
  linkedGuides.value === undefined ? '...' : linkedGuides.value.length
)

/**
 * Name of "Guide(s)" field in "Informations" panel (potentially plural).
 */
const linkedGuidesFieldName = computed(
  () =>
    `Guide${
      typeof linkedGuidesCount.value === 'number' && linkedGuidesCount.value > 1
        ? 's'
        : ''
    }`
)

// /**
//  * The various reference IDs, cleaned from their prefix
//  */
// const referenceIdsCleaned = computed(() => ({
//   SO: modification.value?.so_id?.match(/SO_(\d+)/)?.[1],
//   ChEBI: modification.value?.chebi_id?.match(/CHEBI_(\d+)/)?.[1]
// }))
//
// /**
//  * The various reference links
//  */
// const referenceLinks = computed(() => ({
//   SO: `http://www.sequenceontology.org/browser/current_release/term/SO:${referenceIdsCleaned.value.SO}`,
//   ChEBI: `https://www.ebi.ac.uk/chebi/searchId.do?chebiId=${referenceIdsCleaned.value.ChEBI}`
// }))

/**
 * The selected tab in the 'Graphics' panel.
 */
const selectedGraphicsTab = ref(GraphicsTabEnum[props.initialGraphicsPanelTab])
</script>

<template>
  <MainLayout padded>
    <div class="relative hidden md:block">
      <RouterLink
        v-if="modification"
        :to="{ name: 'table', query: { modificationId: modification.id } }"
      >
        <Button
          severity="secondary"
          outlined
          class="!absolute flex gap-2 !shadow-none top-0 right-8"
        >
          <icon-fa6-solid-table class="text-xl" />
          View table for {{ modification.name }}
        </Button>
      </RouterLink>
    </div>

    <h1 class="mb-4 text-center text-3xl font-semibold text-slate-700">
      {{ modification?.name }}
    </h1>
    <h2 class="mb-8 text-center text-2xl text-slate-400">
      Modification • <span class="font-mono">{{ modification?.id }}</span>
    </h2>

    <Panel
      toggleable
      class="mx-auto mb-16 max-w-6xl text-center 2xl:max-w-7xl"
      :pt="{
        header: { class: '!bg-slate-50' },
        toggler: { class: 'hover:!bg-slate-100' }
      }"
    >
      <template #header="scope">
        <icon-fa6-solid-circle-info :class="scope.class" />
        <span :class="scope.class">Informations</span>
      </template>
      <div class="grid grid-cols-3 gap-x-4">
        <div
          class="relative my-auto italic text-slate-400 after:absolute after:-right-[1px] after:top-0 after:-mr-2 after:h-full after:rounded after:border-l-[1px] after:border-slate-200"
        >
          Modification type:
          <em class="text-lg font-bold not-italic text-slate-700">
            <BaseRenderedMarkdown
              v-if="modification?.type_short_label"
              :stringified-markdown="modification.type_short_label"
              inline-content
            />
            -
            <Tag
              :class="
                modification?.symbol_label && [
                  `!text-${modificationTailwindColor}-600`,
                  `!bg-${modificationTailwindColor}-100`,
                  `border-${modificationTailwindColor}-600`,
                  'border-2 !text-sm'
                ]
              "
            >
              <BaseRenderedMarkdown
                :stringified-markdown="modification?.symbol_label || '...'"
              />
            </Tag>
          </em>
        </div>

        <div
          class="relative my-auto italic text-slate-400 after:absolute after:-right-[1px] after:top-0 after:-mr-2 after:h-full after:rounded after:border-l-[1px] after:border-slate-200"
        >
          Target:
          <RouterLink
            :to="{
              name: 'targetDetails',
              query: { id: modification?.target.id }
            }"
          >
            <Chip
              class="ml-2 border-2 !border-amber-600 !bg-amber-100 !font-semibold not-italic !text-amber-600"
            >
              {{ modification?.target.name || modification?.target.id }} -
              {{
                (modification?.target.unit
                  ? `${modification?.target.unit} `
                  : '') + modification?.target.class
              }}
            </Chip>
          </RouterLink>
        </div>

        <div class="my-auto italic text-slate-400">
          Organism:
          <RouterLink
            v-if="modification?.target.genome?.organism?.label"
            :to="{
              name: 'organism',
              query: { id: modification?.target.genome?.organism?.id }
            }"
            class="text-lg font-bold not-italic text-slate-700 underline transition-all duration-300 ease-in-out hover:text-indigo-600"
          >
            <BaseRenderedMarkdown
              :stringified-markdown="modification.target.genome.organism.label"
            />
          </RouterLink>
        </div>

        <Divider class="col-span-3" />

        <span class="my-auto text-left italic text-slate-400">
          Position (on target):
        </span>
        <em
          class="col-span-2 my-auto text-left text-lg font-bold not-italic text-slate-700"
        >
          {{ modification?.position }}
        </em>

        <template v-if="false">
          <Divider class="col-span-3" />

          <span class="my-auto text-left italic text-slate-400">
            Reference genome:
          </span>
          <em
            class="col-span-2 my-auto text-left text-lg font-bold italic text-slate-700"
          >
            None
          </em>
        </template>

        <Divider class="col-span-3" />

        <span class="my-auto text-left italic text-slate-400">
          {{ linkedGuidesFieldName }} ({{ linkedGuidesCount }}):
        </span>
        <div
          class="col-span-2 my-auto flex flex-wrap justify-between gap-x-2 gap-y-1"
        >
          <em
            v-if="linkedGuidesCount === 0"
            class="font-bold italic text-slate-700"
            >None</em
          >
          <RouterLink
            v-for="linkedGuide in linkedGuides"
            :key="linkedGuide.id"
            :to="{
              name: 'guideDetails',
              query: { id: linkedGuide.id }
            }"
          >
            <Chip
              class="border-2 !border-lime-600 !bg-lime-100 !font-semibold !text-lime-600"
            >
              {{ linkedGuide.name }} -
              {{ linkedGuide.class }}
            </Chip>
          </RouterLink>
          <!-- The following div is here to ensure that Chips stay left-aligned
              in the flex div -->
          <div class="flex-1" />
        </div>

        <!-- <Divider class="col-span-3" />

        <div class="col-span-3 text-left italic text-slate-400">
          References:
          <a :href="referenceLinks.SO">
            <Chip
              class="ml-2 border-2 !border-red-600 !bg-red-100 !pl-0 !font-semibold !text-red-600"
            >
              <Chip
                class="mr-2 !bg-white !font-semibold not-italic !text-slate-600"
                >SO</Chip
              >
              {{ referenceIdsCleaned.SO }}
            </Chip>
          </a>
          <a :href="referenceLinks.ChEBI">
            <Chip
              class="ml-2 border-2 !border-red-600 !bg-red-100 !pl-0 !font-semibold !text-red-600"
            >
              <Chip
                class="mr-2 !bg-white !font-semibold not-italic !text-slate-600"
                >ChEBI</Chip
              >
              {{ referenceIdsCleaned.ChEBI }}
            </Chip>
          </a>
        </div> -->
      </div>
    </Panel>

    <Panel
      id="graphics-panel"
      toggleable
      class="mx-auto mb-16 max-w-7xl break-words 2xl:max-w-[100rem]"
      :pt="{
        header: { class: '!bg-slate-50' },
        toggler: { class: 'hover:!bg-slate-100' }
      }"
    >
      <template #header="scope">
        <icon-fa6-solid-draw-polygon :class="scope.class" />
        <span :class="scope.class">Graphics</span>
      </template>

      <TabView v-model:active-index="selectedGraphicsTab">
        <TabPanel header="Local interaction">
          <!-- v-if is to ensure we can access and map `modification.interactions` -->
          <InteractionBoard
            v-if="interactionList"
            :interaction-list="interactionList"
            selection-field-path1="guide.id"
            selection-field-legend1="Guides"
          >
            <template #item-label-1="{ currentValue }">
              <strong>{{ currentValue.guide.name }}</strong>
              <em class="italic text-slate-400">
                -
                {{ currentValue.guide.class }}
              </em>
            </template>

            <template #selection-incomplete-message>
              Please select a guide on the left to visualise the interaction.
            </template>
          </InteractionBoard>
        </TabPanel>
      </TabView>
    </Panel>
  </MainLayout>
</template>
