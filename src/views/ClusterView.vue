<script setup lang="ts">
/**
 * Vue imports
 */
import { computed, onMounted, toRef } from 'vue'
/**
 * Components imports
 */
import MainLayout from '@/layouts/MainLayout.vue'
import BaseLinkListCard from '@/components/BaseLinkListCard.vue'
import BaseRenderedMarkdown from '@/components/BaseRenderedMarkdown.vue'
import Divider from 'primevue/divider'
import Panel from 'primevue/panel'
import IconFa6SolidCircleInfo from '~icons/fa6-solid/circle-info'
import IconFa6SolidArrowUpRightFromSquare from '~icons/fa6-solid/arrow-up-right-from-square'
import IconFa6SolidList from '~icons/fa6-solid/list'
import IconEmojioneThinkingFace from '~icons/emojione/thinking-face'
/**
 * Composables imports
 */
import { useQuery } from '@urql/vue'
import { useTitle } from '@vueuse/core'
/**
 * Other 3rd-party imports
 */
import { omit as _omit, minBy as _minBy, maxBy as _maxBy } from 'lodash-es'
/**
 * Types imports
 */

import type { LinkListItemModel } from '@/components/BaseLinkListCard.vue'
/**
 * Utils imports
 */
import { separateThousands } from '@/utils/textFormatting'
import { clusterByIdQuery } from '@/gql/queries'

/**
 * Component props
 */
const props = defineProps<{
  /** The ID of the cluster to display */
  clusterId: string
}>()

/**
 * Reactive urql GraphQL query object, updated with query state & response
 */
const gqlQuery = useQuery({
  query: clusterByIdQuery,
  variables: toRef(() => ({
    id: props.clusterId
  }))
})

/**
 * The cluster to display, reactively updated when fetched
 */
const cluster = computed(() =>
  gqlQuery.data.value?.clusters[0]
    ? {
        ..._omit(gqlQuery.data.value.clusters[0], ['referenceGuide']),
        ...gqlQuery.data.value.clusters[0].referenceGuide[0]
      }
    : undefined
)

/**
 * Title of the page, reactively updated when data is fetched
 */
const pageTitle = computed(() =>
  cluster.value
    ? `${cluster.value.id} • Cluster | snoBoard`
    : 'Cluster | snoBoard'
)
// Bind actual page title to computed one
onMounted(() => useTitle(pageTitle))

/**
 * First guide in the cluster
 */
const firstGuide = computed(() =>
  _minBy(
    cluster.value?.guides,
    'chromosomeConnection.edges[0].properties.start'
  )
)

/**
 * Formatted positions (start & end) of the first guide in the cluster
 * on its parent
 */
const firstGuideFormattedPositions = computed(() =>
  firstGuide.value?.chromosomeConnection.edges[0]
    ? {
        start: separateThousands(
          firstGuide.value.chromosomeConnection.edges[0].properties.start
        ),
        end: separateThousands(
          firstGuide.value.chromosomeConnection.edges[0].properties.end
        )
      }
    : undefined
)

/**
 * Last guide in the cluster
 */
const lastGuide = computed(() =>
  _maxBy(cluster.value?.guides, 'chromosomeConnection.edges[0].properties.end')
)

/**
 * Formatted positions (start & end) of the last guide in the cluster
 * on its parent
 */
const lastGuideFormattedPositions = computed(() =>
  lastGuide.value?.chromosomeConnection.edges[0]
    ? {
        start: separateThousands(
          lastGuide.value.chromosomeConnection.edges[0].properties.start
        ),
        end: separateThousands(
          lastGuide.value.chromosomeConnection.edges[0].properties.end
        )
      }
    : undefined
)

/**
 * Guides of the cluster, formatted for use in a link list
 */
const linkListGuides = computed<LinkListItemModel[]>(
  () =>
    cluster.value?.guides?.map((guide) => ({
      id: guide.id,
      link: {
        name: 'guideDetails',
        query: { id: guide.id }
      },
      title: guide.name || guide.id,
      subtitle: guide.id,
      details: [
        guide.class,
        guide.subclass_label,
        `${guide.modificationsAggregate?.count} guided modification${
          (guide.modificationsAggregate?.count || 0) > 1 ? 's' : ''
        }`
      ]
    })) || []
)
</script>

<template>
  <MainLayout padded>
    <h1 class="mb-4 text-center text-3xl font-semibold text-slate-700">
      Cluster
    </h1>
    <h2 class="mb-8 text-center font-mono text-2xl text-slate-400">
      {{ cluster?.id || '...' }}
    </h2>

    <Panel
      toggleable
      class="mx-auto mb-16 max-w-6xl text-center 2xl:max-w-7xl"
      :pt="{
        header: { class: '!bg-slate-50' },
        toggler: { class: 'hover:!bg-slate-100' }
      }"
    >
      <template #header="scope">
        <icon-fa6-solid-circle-info :class="scope.class" />
        <span :class="scope.class">Informations</span>
      </template>
      <div class="gap grid grid-cols-3">
        <div
          class="relative italic text-slate-400 after:absolute after:-right-[1px] after:h-full after:rounded after:border-l-[1px] after:border-slate-200"
        >
          Chromosome:
          <em class="text-lg font-bold italic text-slate-700">
            {{ cluster?.chromosome?.name || '...' }}
          </em>
        </div>
        <div
          class="relative italic text-slate-400 after:absolute after:-right-[1px] after:h-full after:rounded after:border-l-[1px] after:border-slate-200"
        >
          Guide count in cluster:
          <em class="text-lg font-bold italic text-slate-700">
            {{ cluster?.guidesAggregate?.count }}
          </em>
        </div>
        <div class="italic text-slate-400">
          Organism:
          <RouterLink
            v-if="cluster?.genome?.organism?.label"
            :to="{
              name: 'organism',
              query: { id: cluster.genome.organism.id }
            }"
            class="text-lg font-bold italic text-slate-700 underline transition-all duration-300 ease-in-out hover:text-indigo-600"
          >
            <BaseRenderedMarkdown
              :stringified-markdown="cluster.genome.organism.label"
              inline-content
            />
            <sup>
              <icon-fa6-solid-arrow-up-right-from-square class="inline" />
            </sup>
          </RouterLink>
        </div>

        <Divider class="col-span-3" />

        <div class="col-span-3 grid grid-cols-2">
          <div
            class="relative italic text-slate-400 after:absolute after:-right-[1px] after:h-full after:rounded after:border-l-[1px] after:border-slate-200"
          >
            <span v-tooltip.top="firstGuide?.name || '...'">First guide</span>
            location:
            <span
              v-if="firstGuideFormattedPositions"
              class="not-italic text-slate-700"
            >
              <em
                v-tooltip.top="
                  `Start position: ${firstGuideFormattedPositions.start}`
                "
                class="text-lg font-bold not-italic"
              >
                {{ firstGuideFormattedPositions.start }}
              </em>
              –
              <em
                v-tooltip.top="
                  `End position: ${firstGuideFormattedPositions.end}`
                "
                class="text-lg font-bold not-italic"
              >
                {{ firstGuideFormattedPositions.end }}
              </em>
            </span>
            <em v-else>?</em>
          </div>
          <div class="italic text-slate-400">
            <span v-tooltip.top="lastGuide?.name || '...'">Last guide</span>
            location:
            <span
              v-if="lastGuideFormattedPositions"
              class="not-italic text-slate-700"
            >
              <em
                v-tooltip.top="
                  `Start position: ${lastGuideFormattedPositions.start}`
                "
                class="text-lg font-bold not-italic"
              >
                {{ lastGuideFormattedPositions.start }}
              </em>
              –
              <em
                v-tooltip.top="
                  `End position: ${lastGuideFormattedPositions.end}`
                "
                class="text-lg font-bold not-italic"
              >
                {{ lastGuideFormattedPositions.end }}
              </em>
            </span>
            <em v-else>?</em>
          </div>
        </div>

        <Divider class="col-span-3" />

        <div class="col-span-3 text-left italic text-slate-400">
          Reference genome:
          <em class="text-lg font-bold not-italic text-slate-700">??</em>
        </div>
      </div>
    </Panel>

    <Panel toggleable class="mx-auto mb-16 max-w-6xl">
      <template #header="scope">
        <icon-fa6-solid-list :class="scope.class" />
        <span :class="scope.class"
          >Guides in cluster ({{
            cluster?.guidesAggregate?.count || '...'
          }})</span
        >
      </template>
      <BaseLinkListCard
        :disabled="!linkListGuides.length"
        :link-items="linkListGuides"
        class="!shadow-none"
      >
        <template #disabled>
          <div class="flex justify-center gap-2">
            No guide in this cluster ? Hmmm...
            <icon-emojione-thinking-face class="text-2xl" />
          </div>
        </template>
        <template #detail0="{ detail }">
          <BaseRenderedMarkdown
            :stringified-markdown="detail || ''"
            inline-content
          />
        </template>
        <template #detail1="{ detail }">
          <BaseRenderedMarkdown
            :stringified-markdown="detail || ''"
            inline-content
          />
        </template>
        <template #detail2="{ detail }">
          <BaseRenderedMarkdown
            :stringified-markdown="detail || ''"
            inline-content
          />
        </template>
      </BaseLinkListCard>
    </Panel>
  </MainLayout>
</template>

<style></style>
