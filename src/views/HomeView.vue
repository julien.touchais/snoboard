<script setup lang="ts">
/**
 * Vue imports
 */
import { computed, ref } from 'vue'
/**
 * Components imports
 */
import FooterLayout from '@/layouts/FooterLayout.vue'
import SearchBar from '@/components/SearchBar.vue'
import BaseRenderedMarkdown from '@/components/BaseRenderedMarkdown.vue'
import HelpDialog from '@/components/HelpDialog.vue'
import Image from 'primevue/image'
import Menubar from 'primevue/menubar'
import IconFa6SolidMagnifyingGlass from '~icons/fa6-solid/magnifying-glass'
import IconFa6SolidCircleXmark from '~icons/fa6-solid/circle-xmark'
import IconFa6SolidArrowRight from '~icons/fa6-solid/arrow-right'
import IconFa6SolidChevronDown from '~icons/fa6-solid/chevron-down'
import IconFa6SolidChevronRight from '~icons/fa6-solid/chevron-right'
import IconFa6SolidSliders from '~icons/fa6-solid/sliders'
import IconFa6SolidTable from '~icons/fa6-solid/table'
import IconFa6SolidDatabase from '~icons/fa6-solid/database'
import IconFa6SolidPaw from '~icons/fa6-solid/paw'
import IconFa6SolidBullseye from '~icons/fa6-solid/bullseye'
import IconTablerCircleArrowDown from '~icons/tabler/circle-arrow-down'
import IconTablerCircleArrowDownFilled from '~icons/tabler/circle-arrow-down-filled'
import IconSnoboardGuide from '~icons/snoboard/guide'
import IconSnoboardModification from '~icons/snoboard/modification'
/**
 * Composables imports
 */
import { useQuery } from '@urql/vue'
import { useRouter } from 'vue-router'
/**
 * Other 3rd-party imports
 */
import { uniqWith as _uniqWith, isEqual as _isEqual } from 'lodash-es'
/**
 * Types imports.
 */
import type { MenuItem } from 'primevue/menuitem'
import { GuideClass, ModifType } from '@/gql/codegen/graphql'
/**
 * Utils imports
 */
import { homeQuery } from '@/gql/queries'
import { MENU_ITEMS } from '@/utils/constant'
import { isDefined, isNonNullish } from '@/typings/typeUtils'
/**
 * Assets imports
 */
import { logoUrl } from '@/assets/images'
import tailwindDefaultColors from 'tailwindcss/colors'

/**
 * Useful columns to show to list guides in the table.
 */
const LIST_GUIDES_TABLE_COLUMNS = [
  'guideId',
  'guideName',
  'guideClass',
  'guideSubclass',
  'guideChromosomeName',
  'guideLength',
  'organismName',
  'organismId'
]

/**
 * Useful columns to show to list modifications in the table.
 */
const LIST_MODIFICATIONS_TABLE_COLUMNS = [
  'modificationId',
  'modificationName',
  'modificationPosition',
  'modificationSymbol',
  'modificationType',
  'targetName',
  'organismName',
  'organismId'
]

/**
 * Useful columns to show to list modifications in the table.
 */
const LIST_TARGETS_TABLE_COLUMNS = [
  'targetId',
  'targetName',
  'targetLength',
  'targetClass',
  'targetUnit',
  'targetChromosomeName',
  'organismName',
  'organismId'
]

/**
 * Useful columns to show to list organisms in the table.
 */
const LIST_ORGANISMS_TABLE_COLUMNS = ['organismName', 'organismId']

/**
 * Vue Router instance reactive object.
 */
const router = useRouter()

/**
 * Reactive urql GraphQL query object, updated with query state & response.
 */
const gqlQuery = useQuery({
  query: homeQuery
})

/**
 * The list of all the modification types present in the database,
 * reactively updated when fetched.
 */
const modificationTypesList = computed(() =>
  _uniqWith(gqlQuery.data.value?.modificationTypes, _isEqual)
)

/**
 * The list of all the guide subclasses present in the database,
 * reactively updated when fetched.
 */
const guideSubclassesList = computed(() =>
  _uniqWith(gqlQuery.data.value?.guideSubclasses, _isEqual)
)

/**
 * The list of all the target units present in the database,
 * reactively updated when fetched.
 */
const targetUnitsList = computed(() =>
  _uniqWith(gqlQuery.data.value?.targetUnits, _isEqual)
    .map((targetUnit) => targetUnit.unit)
    .filter(isNonNullish)
)

/**
 * The list of all the organisms present in the database,
 * reactively updated when fetched.
 */
const organismList = computed(() => gqlQuery.data.value?.organisms)

/**
 * The number of all the organisms present in the database, reactively updated
 * when fetched.
 */
const organismCount = computed(
  () => gqlQuery.data.value?.organismsAggregate.count
)

/**
 * Menu items.
 */
const menuItems = computed<MenuItem[]>(() => [
  {
    key: 'modifications',
    label: 'Modifications',
    items: [
      {
        label: 'All modifications',
        route: {
          name: 'table',
          query: {
            columns: LIST_MODIFICATIONS_TABLE_COLUMNS
          }
        },
        iconComponent: IconFa6SolidTable
      },
      ...modificationTypesList.value
        .filter((modificationType) => modificationType.type !== ModifType.Other)
        .map((modificationType) => ({
          key: `modifications${modificationType.type}`,
          label: modificationType.type_short_label || '',
          indent: 1,
          route: {
            name: 'table',
            query: {
              columns: LIST_MODIFICATIONS_TABLE_COLUMNS,
              modificationType: modificationType.type
            }
          }
        })),
      {
        key: 'modificationSelection',
        label: 'Advanced modification selection',
        route: {
          name: 'selection',
          query: {
            mode: 'modification'
          }
        },
        iconComponent: IconFa6SolidSliders
      }
    ]
  },
  {
    label: 'Guides',
    items: [
      {
        key: 'guides',
        label: 'All guides',
        route: {
          name: 'table',
          query: {
            columns: LIST_GUIDES_TABLE_COLUMNS
          }
        },
        iconComponent: IconFa6SolidTable
      },
      ...guideSubclassesList.value
        .filter((guideSubclass) => guideSubclass.subclass !== GuideClass.Other)
        .map((guideSubclass) => ({
          key: `guides${guideSubclass}`,
          label: guideSubclass.subclass_label,
          indent: 1,
          route: {
            name: 'table',
            query: {
              columns: LIST_GUIDES_TABLE_COLUMNS,
              guideSubclass: guideSubclass.subclass
            }
          }
        })),
      {
        key: 'guideSelection',
        label: 'Advanced guide selection',
        route: {
          name: 'selection',
          query: {
            mode: 'guide'
          }
        },
        iconComponent: IconFa6SolidSliders
      }
    ]
  },
  {
    key: 'targets',
    label: 'Targets',
    items: [
      {
        label: 'All targets',
        route: {
          name: 'table',
          query: {
            columns: LIST_TARGETS_TABLE_COLUMNS
          }
        },
        iconComponent: IconFa6SolidTable
      },
      ...targetUnitsList.value.map((targetUnit) => ({
        key: `targets${targetUnit}`,
        label: targetUnit,
        indent: 1,
        route: {
          name: 'table',
          query: {
            columns: LIST_TARGETS_TABLE_COLUMNS,
            targetUnit
          }
        }
      })),
      {
        key: 'targetSelection',
        label: 'Advanced target selection',
        route: {
          name: 'selection',
          query: {
            mode: 'target'
          }
        },
        iconComponent: IconFa6SolidSliders
      }
    ]
  },
  {
    key: 'organisms',
    label: 'Organisms',
    items: [
      ...(organismList.value?.map((organism) => ({
        key: `organisms${organism.id}`,
        label: organism.label,
        route: {
          name: 'table',
          query: {
            organismId: organism.id
          }
        }
      })) || [
        {
          label: '*No organism*',
          key: `noOrganism`
        }
      ])
    ]
  },
  {
    label: 'spacer',
    separator: true
  },
  {
    key: 'conservation',
    label: 'Conservation',
    items: [
      // {
      //   key: 'conservationModifications',
      //   label: 'Modifications',
      //   route: {
      //     name: 'alignment'
      //     // query: {
      //     //   type: 'modification'
      //     // }
      //   },
      //   iconComponent: IconSnoboardModification,
      //   disabled: true
      // },
      {
        key: 'conservationGuides',
        label: 'Guides',
        route: {
          name: 'alignment',
          query: {
            mode: 'guide'
          }
        },
        iconComponent: IconSnoboardGuide
      },
      {
        key: 'conservationTargets',
        label: 'Targets',
        route: {
          name: 'alignment',
          query: {
            mode: 'target'
          }
        },
        iconComponent: IconFa6SolidBullseye
      }
    ]
  },
  {
    key: 'statistics',
    label: 'Statistics',
    items: [
      {
        key: 'statisticsDB',
        label: 'Database-wide',
        route: {
          name: 'statistics'
        },
        iconComponent: IconFa6SolidDatabase
      },
      {
        key: 'statisticsOrganism',
        label: 'By organism',
        items: [
          ...(organismList.value?.slice(0, 3).map((organism) => ({
            key: `statisticsOrganism${organism.id}`,
            label: organism.label,
            route: {
              name: 'organism',
              query: {
                id: organism.id
              }
            },
            metadata: {
              type: 'organism'
            }
          })) || []),
          organismCount.value && organismCount.value > 3
            ? {
                label: `+${organismCount.value - 3} more...`,
                route: {
                  name: 'table',
                  query: { columns: LIST_ORGANISMS_TABLE_COLUMNS }
                },
                iconComponent: IconFa6SolidTable
              }
            : undefined
        ].filter(isDefined),
        iconComponent: IconFa6SolidPaw
      }
    ]
  }
])

/**
 * Template ref to the "About snoBoard" DOM Element.
 */
const aboutSectionElement = ref<HTMLElement>()

/**
 * The term currently being typed in the search bar.
 */
const searchTerm = ref('')

/**
 * Wether the help dialog should be displayed or not.
 */
const showHelpDialog = ref(false)
</script>

<template>
  <HelpDialog v-if="showHelpDialog" v-model:visible-model="showHelpDialog" />

  <FooterLayout>
    <main
      class="mx-auto flex min-h-screen max-w-6xl flex-col items-center justify-center gap-8 px-4 pb-36 text-center sm:gap-16 2xl:max-w-7xl"
    >
      <Menubar
        class="absolute right-8 top-8 flex flex-col items-end gap-2"
        :model="MENU_ITEMS"
      >
        <template #item="{ item, props }">
          <div
            v-if="item.key === 'help'"
            :="props.action"
            @click="showHelpDialog = true"
          >
            <component :is="item.iconComponent || ''" :="props.icon" />
            <span :="props.label">{{ item.label }}</span>
          </div>
          <RouterLink
            v-else-if="item.routerDest"
            :to="item.routerDest"
            :="props.action"
          >
            <component :is="item.iconComponent || ''" :="props.icon" />
            <span :="props.label">{{ item.label }}</span>
          </RouterLink>
        </template>
      </Menubar>

      <Image
        class="mx-auto inline-block w-3/4 max-w-sm"
        alt="snoBoard logo"
        :src="logoUrl"
      />

      <SearchBar
        v-model:search-term-model="searchTerm"
        class="w-full max-w-xl"
        @search="
          () => {
            router.push({
              name: 'table',
              query: {
                global: searchTerm
              }
            })
          }
        "
      >
        <template #icon="{ className }">
          <icon-fa6-solid-magnifying-glass :class="className" />
        </template>

        <template #clear-icon="{ clearSearch, searchEmpty, className }">
          <icon-fa6-solid-circle-xmark
            v-show="!searchEmpty"
            :class="[className, 'cursor-pointer transition-all']"
            @click="clearSearch()"
          />
        </template>

        <template #label>
          <span>Search</span>
        </template>

        <template #hint>
          <span class="mt-2 block text-sm">
            Examples:
            <RouterLink
              :to="{
                name: 'table',
                query: {
                  global: '18S'
                }
              }"
              class="italic text-lime-600 hover:underline"
            >
              18S</RouterLink
            >,
            <RouterLink
              :to="{
                name: 'table',
                query: {
                  global: 'm5U'
                }
              }"
              class="italic text-lime-600 hover:underline"
            >
              m5U</RouterLink
            >,
            <RouterLink
              :to="{
                name: 'table',
                query: {
                  global: 'SNR24'
                }
              }"
              class="italic text-lime-600 hover:underline"
            >
              SNR24</RouterLink
            >,
            <RouterLink
              :to="{
                name: 'table',
                query: {
                  global: 'Arabidopsis thaliana'
                }
              }"
              class="italic text-lime-600 hover:underline"
            >
              Arabidopsis thaliana</RouterLink
            >,…
          </span>
        </template>

        <template #button>
          <icon-fa6-solid-arrow-right />
        </template>
      </SearchBar>

      <div class="flex justify-center">
        <Menubar
          :model="menuItems"
          :pt="{
            submenu: {
              style: {
                minWidth: 'max-content'
              }
            },
            separator: {
              style: {
                margin: '0 1rem',
                border: `solid ${tailwindDefaultColors['slate'][300]} 1px`,
                height: '3rem',
                borderRadius: '999999px'
              }
            }
          }"
        >
          <template #item="{ item, props, hasSubmenu, root }">
            <span :class="[{ 'font-normal': item.indent }]">
              <RouterLink
                v-if="item.route && !hasSubmenu"
                :to="item.route"
                custom
                #="{ href, navigate }"
              >
                <a :href="href" :="props.action" @click="navigate">
                  <component
                    :is="item.iconComponent || ''"
                    v-if="item.iconComponent"
                    :="props.icon"
                  />
                  <span
                    v-if="item.indent"
                    class="mr-2"
                    :style="{ paddingLeft: `${item.indent}rem` }"
                  >
                    &rarr;
                  </span>
                  <BaseRenderedMarkdown
                    v-if="typeof item.label === 'string'"
                    :stringified-markdown="item.label"
                  />
                </a>
              </RouterLink>
              <a
                v-else
                :href="item.url"
                :target="item.target"
                :="props.action"
                class="flex"
              >
                <component
                  :is="item.iconComponent || ''"
                  v-if="item.iconComponent"
                  :="props.icon"
                />
                <span
                  v-if="item.indent"
                  class="mr-2"
                  :style="{ paddingLeft: `${item.indent}rem` }"
                >
                  &rarr;
                </span>
                <BaseRenderedMarkdown
                  v-if="typeof item.label === 'string'"
                  :stringified-markdown="item.label"
                />
                <icon-fa6-solid-chevron-down
                  v-if="hasSubmenu && root"
                  :="props.submenuicon"
                />
                <icon-fa6-solid-chevron-right
                  v-else-if="hasSubmenu"
                  :="props.submenuicon"
                />
              </a>
            </span>
          </template>
        </Menubar>
      </div>
    </main>

    <div class="relative">
      <button
        class="group absolute bottom-16 left-1/2 -translate-x-1/2 cursor-pointer rounded-full border p-2 pl-12 pr-4 text-2xl text-slate-400 shadow-lg transition-all duration-500 hover:border-slate-600 hover:text-slate-600 hover:shadow-2xl focus:border-slate-600 focus:text-slate-600 focus:shadow-2xl 2xl:bottom-24"
        @click="aboutSectionElement?.scrollIntoView({ behavior: 'smooth' })"
      >
        <icon-tabler-circle-arrow-down
          class="absolute left-2 top-1/2 -translate-y-1/2 text-3xl transition-all duration-500 group-hover:opacity-0 group-focus:opacity-0"
        />
        <icon-tabler-circle-arrow-down-filled
          class="absolute left-2 top-1/2 -translate-y-1/2 text-3xl opacity-0 transition-all duration-500 group-hover:opacity-100 group-focus:opacity-100"
        />
        About snoBoard
      </button>
    </div>

    <section
      ref="aboutSectionElement"
      class="prose prose-lg prose-slate mx-auto mb-8 pt-8 prose-headings:text-center prose-p:text-justify"
    >
      <h2>What is snoBoard ?</h2>
      <p>
        Lorem ipsum odor amet, consectetuer adipiscing elit. Lacus suscipit
        nullam rhoncus dui ex; efficitur leo? Interdum velit potenti habitant
        dapibus arcu suspendisse. Diam ante nisl leo, quisque potenti elementum
        est in. Libero quis cras eu dignissim litora. Vehicula mauris cubilia
        cubilia; sit vestibulum elit fusce laoreet. Efficitur inceptos nostra
        imperdiet neque semper dis. Orci vulputate pharetra dapibus odio nulla
        imperdiet.
      </p>
      <p>
        Dis dui malesuada turpis hac malesuada. Felis nullam pharetra vehicula
        vestibulum mollis. Molestie suscipit vulputate vehicula amet phasellus?
        Auctor integer taciti praesent tortor risus auctor dui. Lacinia tempus
        erat; egestas sapien metus eleifend lectus porttitor. Praesent urna
        class purus senectus quis tortor elit augue. Pellentesque rutrum mattis
        neque dapibus fringilla?
      </p>
      <p>
        Cubilia eros penatibus sodales volutpat massa primis dis hac. Nibh
        maecenas lobortis class proin vitae amet sed parturient. Tristique dis
        urna, per proin ante ac at. Faucibus risus taciti lacus sociosqu montes
        aliquet ornare. Libero fames maecenas tincidunt ad ad dis quis. Euismod
        congue morbi cras facilisi risus etiam justo.
      </p>
      <p>
        Metus tempus odio maecenas nisl amet volutpat porttitor dapibus. Natoque
        blandit a elementum tortor; aliquet eros. Elit sapien urna euismod,
        aptent nunc elementum. Curae pulvinar viverra montes tristique commodo
        cras, lectus euismod proin. Diam mi quis aliquam convallis quis dui.
        Fusce lacus nec rutrum rhoncus nisl congue ridiculus euismod lacus.
        Proin pharetra eleifend mus, ante sapien amet eros fusce. Ad rutrum
        felis semper vivamus hendrerit dis ridiculus sapien ut?
      </p>
      <p>
        Himenaeos turpis senectus aliquet luctus quisque facilisi nullam
        facilisi congue. Porttitor vitae elementum lobortis; consequat ac
        elementum purus! Cursus nunc ad sapien suscipit dignissim ipsum placerat
        fermentum. Bibendum non mauris tortor, arcu venenatis etiam malesuada.
        Urna taciti ultrices litora nostra efficitur velit tellus. Tellus felis
        imperdiet ad fusce hendrerit fermentum. Magna praesent aliquet
        adipiscing; mi diam per. Habitant at dapibus lobortis suscipit malesuada
        litora curae eget quis.
      </p>
    </section>
  </FooterLayout>
</template>
