<script setup lang="ts">
/**
 * Vue imports
 */
import { computed, onMounted, ref, toRef } from 'vue'
/**
 * Components imports
 */
import MainLayout from '@/layouts/MainLayout.vue'
import SequenceBoard from '@/components/SequenceBoard.vue'
import InteractionBoard from '@/components/InteractionBoard.vue'
import BaseDescribedChip from '@/components/BaseDescribedChip.vue'
import BaseRenderedMarkdown from '@/components/BaseRenderedMarkdown.vue'
import TabPanel from 'primevue/tabpanel'
import TabView from 'primevue/tabview'
import Divider from 'primevue/divider'
import Chip from 'primevue/chip'
import Card from 'primevue/card'
import Panel from 'primevue/panel'
import Button from 'primevue/button'
import IconFa6SolidFileLines from '~icons/fa6-solid/file-lines'
import IconFa6SolidCircleInfo from '~icons/fa6-solid/circle-info'
import IconFa6SolidDna from '~icons/fa6-solid/dna'
import IconFa6SolidDrawPolygon from '~icons/fa6-solid/draw-polygon'
import IconFa6SolidCircleMinus from '~icons/fa6-solid/circle-minus'
import IconFa6SolidCirclePlus from '~icons/fa6-solid/circle-plus'
import IconFa6SolidCircleQuestion from '~icons/fa6-solid/circle-question'
import IconFa6SolidCircleXmark from '~icons/fa6-solid/circle-xmark'
import IconFa6SolidTable from '~icons/fa6-solid/table'
/**
 * Composables imports
 */
import { useQuery } from '@urql/vue'
import { useTitle } from '@vueuse/core'
/**
 * Other 3rd-party imports
 */
import { uniq as _uniq } from 'lodash-es'
/**
 * Types import
 */
import type { TailwindDefaultColorNameModel } from '@/typings/styleTypes'
import type { InteractionCardModel } from '@/components/InteractionCard.vue'
import type { objectModel } from '@/components/SequenceBoard.vue'
import {
  GraphQlType,
  ModifType,
  Strand,
  AnnotationType,
  SequenceClass
} from '@/gql/codegen/graphql'
import type { LegendItemModel } from '@/components/BaseLegendButtonOverlay.vue'
/**
 * Utils imports
 */
import {
  composePlural,
  formatAnnotationType,
  separateThousands
} from '@/utils/textFormatting'
import { guideByIdQuery } from '@/gql/queries'
import { isDefined, isInEnum } from '@/typings/typeUtils'
import { getBoxColor, getModificationColor } from '@/utils/colors'

/**
 * Enum to represent the tabs in the 'Graphics' panel.
 */
enum GraphicsTabEnum {
  interaction = 0
}

/**
 * Utility constant to get the icon component corresponding to each strand value.
 */
const STRAND_CODE_TO_ICON_COMPONENT = {
  [Strand.Negative]: IconFa6SolidCircleMinus,
  [Strand.Positive]: IconFa6SolidCirclePlus,
  [Strand.Unknown]: IconFa6SolidCircleQuestion,
  [Strand.NotStranded]: IconFa6SolidCircleXmark
}

/**
 * Component props.
 */
const props = defineProps<{
  /** The ID of the guide to display. */
  guideId: string
  /** The tab to display initially in the 'Graphics' panel */
  initialGraphicsPanelTab: keyof typeof GraphicsTabEnum
}>()

/**
 * Reactive urql GraphQL query object, updated with query state & response.
 */
const gqlQuery = useQuery({
  query: guideByIdQuery,
  variables: toRef(() => ({
    id: props.guideId
  }))
})

/**
 * The guide to display, reactively updated when fetched.
 */
const guide = computed(
  () =>
    gqlQuery.data.value?.guides[0] && {
      ...gqlQuery.data.value?.guides[0],
      modifications: gqlQuery.data.value?.guides[0].modifications.map(
        (modification) => ({
          ...modification,
          tailwindColor: modification.type
            ? getModificationColor(modification.type)
            : 'slate'
        })
      )
    }
)

/**
 * The targets with which the guide is interacting.
 */
const linkedTargets = computed(() => gqlQuery.data.value?.targets)

/**
 * Title of the page, reactively updated when data is fetched.
 */
const pageTitle = computed(() =>
  guide.value ? `${guide.value.name} • Guide | snoBoard` : 'Guide | snoBoard'
)
// Bind actual page title to computed one
onMounted(() => useTitle(pageTitle))

/**
 * The list of the interaction, formatted for display. If some mandatory fields
 * for display (mainly sequences) are missing, the corresponding interaction
 * will not be present in the list (and thus not available for display).
 */
const interactionList = computed<InteractionCardModel[] | undefined>(() => {
  if (!guide.value) return undefined
  return guide.value.interactions
    .map<InteractionCardModel | undefined>((interaction) => {
      if (!guide.value) return undefined
      const duplexes = interaction.duplexes.map<
        InteractionCardModel['duplexes'][0] | undefined
      >((duplex) => {
        const primaryStrandConnection = duplex.primaryStrandsConnection.edges[0]
        const secondaryStrandConnection =
          duplex.secondaryStrandsConnection.edges[0]
        if (
          !primaryStrandConnection?.node.seq ||
          !secondaryStrandConnection?.node.seq
        )
          return undefined
        return {
          primaryFragment: {
            seq: primaryStrandConnection.node.seq,
            start: primaryStrandConnection.properties.start,
            end: primaryStrandConnection.properties.end,
            onParentPosition: {
              start:
                primaryStrandConnection.node.parentConnection.edges[0]
                  ?.properties.start,
              end: primaryStrandConnection.node.parentConnection.edges[0]
                ?.properties.end
            }
          },
          secondaryFragment: {
            seq: secondaryStrandConnection.node.seq,
            start: secondaryStrandConnection.properties.start,
            end: secondaryStrandConnection.properties.end,
            onParentPosition: {
              start:
                secondaryStrandConnection.node.parentConnection.edges[0]
                  ?.properties.start,
              end: secondaryStrandConnection.node.parentConnection.edges[0]
                ?.properties.end
            }
          },
          index: duplex.index
        }
      })
      if (!duplexes.every(isDefined)) return undefined
      return {
        guide: {
          id: guide.value.id,
          name: guide.value.name || undefined,
          class: guide.value.class,
          subclass_label: guide.value.subclass_label
        },
        target: {
          id: interaction.target.id,
          name: interaction.target.name || undefined,
          class: interaction.target.class || SequenceClass.Other,
          unit: interaction.target.unit || undefined
        },
        modification: {
          id: interaction.modification.id,
          name: interaction.modification.name,
          type: interaction.modification.type || undefined,
          type_short_label:
            interaction.modification.type_short_label || undefined,
          position: interaction.modification.position,
          symbol: interaction.modification.symbol,
          symbol_label: interaction.modification.symbol_label
        },
        duplexes
      }
    })
    .filter(isDefined)
})

/**
 * Guide modifications, filtered by keeping only ones with positive position, relative to the guide sequence.
 */
const filteredFacingModifications = computed(() =>
  _uniq(
    interactionList.value
      ?.map((interaction) => {
        // Start coordinate of the guide fragment on the guide
        const guideFragmentStart =
          interaction.duplexes[0]?.primaryFragment.onParentPosition?.start ||
          NaN
        // End coordinate of the target fragment on the target
        const targetFragmentEnd =
          interaction.duplexes[0]?.secondaryFragment.onParentPosition?.end ||
          NaN
        // Start coordinate of the binding on the guide fragment
        const bindingGuideStart =
          interaction.duplexes[0]?.primaryFragment.start || NaN
        // End coordinate of the binding on the target fragment
        const bindingTargetEnd =
          interaction.duplexes[0]?.secondaryFragment.end || NaN
        // Length of the target fragment
        const targetFragmentLength =
          interaction.duplexes[0]?.secondaryFragment.seq.length || NaN

        const position =
          guideFragmentStart +
          (bindingGuideStart - 1) +
          (targetFragmentEnd -
            (targetFragmentLength - bindingTargetEnd) -
            interaction.modification.position)
        return position && position >= 0
          ? { ...interaction.modification, facingPosition: position }
          : undefined
      })
      .filter(isDefined)
  )
)

/**
 * Objects to highlight on the sequence of the guide.
 */
const sequenceObjects = computed(() => ({
  ...guide.value?.boxConnections.edges.reduce<{
    [groupId: string]: objectModel
  }>((boxes, boxConnection) => {
    return boxConnection.node.annotation
      ? {
          ...boxes,
          [boxConnection.node.id]: {
            start: Math.max(boxConnection.properties.start, 1),
            end: Math.min(
              boxConnection.properties.end,
              guide.value?.length || boxConnection.properties.end
            ),
            color: getBoxColor(boxConnection.node.annotation),
            type: boxConnection.node.annotation,
            shouldTooltip: true
          }
        }
      : boxes
  }, {}),
  ...filteredFacingModifications.value?.reduce<{
    [groupId: string]: objectModel
  }>(
    (sequenceObjects, facingModification) => ({
      ...sequenceObjects,
      [facingModification.id]: {
        start: facingModification.facingPosition,
        end: facingModification.facingPosition,
        color: 'slate' as TailwindDefaultColorNameModel,
        name: facingModification.name,
        type: facingModification.type,
        link: {
          name: 'modificationDetails',
          query: {
            id: facingModification.id
          }
        },
        shouldTooltip: true
      }
    }),
    {}
  )
}))

/**
 * A list of the different types of the objects on the guide's sequence.
 */
const sequenceObjectTypes = computed(() =>
  _uniq(
    Object.values(sequenceObjects.value).map(
      (object) => object.type || AnnotationType.Other
    )
  )
)

/**
 * The legend to display on the sequence board.
 */
const sequenceLegendItems = computed(() =>
  sequenceObjectTypes.value
    .map<LegendItemModel>((sequenceObjectType) => ({
      id: isInEnum(sequenceObjectType, ModifType)
        ? 'ModFacing'
        : sequenceObjectType,
      title: isInEnum(sequenceObjectType, ModifType)
        ? 'Facing the modification'
        : formatAnnotationType(
            isInEnum(sequenceObjectType, AnnotationType)
              ? sequenceObjectType
              : AnnotationType.Other
          ),
      color: isInEnum(sequenceObjectType, AnnotationType)
        ? getBoxColor(sequenceObjectType)
        : 'slate',
      description: isInEnum(sequenceObjectType, ModifType)
        ? 'In the duplex, the nucleotide which is in front of the modified one on the target.'
        : isInEnum(sequenceObjectType, AnnotationType)
          ? undefined
          : 'Annotation of a different type than those listed above.'
    }))
    .sort(
      (legendItemA, legendItemB) =>
        (legendItemA.id === 'Other' && 1) ||
        (legendItemA.id === 'ModFacing' && 1) ||
        (legendItemA.title > legendItemB.title && 1) ||
        -1
    )
)

/**
 * Number of modifications linked to the guide,
 * equals to `'...'` if not fetched yet.
 */
const linkedModificationsCount = computed(() =>
  guide.value?.modificationsAggregate == null
    ? '...'
    : guide.value.modificationsAggregate.count
)

/**
 * Name of "Guided modification(s)" field in "Informations" panel (potentially
 * plural).
 */
const linkedModificationsFieldName = computed(
  () =>
    `Guided modification${
      typeof linkedModificationsCount.value === 'number' &&
      linkedModificationsCount.value > 1
        ? 's'
        : ''
    }`
)

/**
 * Number of targets linked to the guide, equals to `'...'` if not fetched yet.
 */
const linkedTargetsCount = computed(() =>
  linkedTargets.value === undefined ? '...' : linkedTargets.value.length
)

/**
 * Name of "Target(s)" field in "Informations" panel (potentially plural).
 */
const linkedTargetsFieldName = computed(
  () =>
    `Target${
      typeof linkedTargetsCount.value === 'number' &&
      linkedTargetsCount.value > 1
        ? 's'
        : ''
    }`
)

/**
 * List of isoforms of the guide, the guide itself being removed from the list.
 */
const guideIsoforms = computed(() =>
  guide.value?.isoform?.guides.filter(
    (isoform) => isoform.id !== guide.value?.id
  )
)

/**
 * Number of guides which are isoforms of to the guide,
 * equals to `'...'` if not fetched yet.
 */
const isoformsCount = computed(() =>
  guide.value?.isoformAggregate?.count === 0
    ? 0
    : guide.value?.isoform?.guidesAggregate == null
      ? '...'
      : guide.value?.isoform?.guidesAggregate?.count - 1
)

/**
 * Name of "Isoform(s)" field in "Informations" panel (potentially plural).
 */
const isoformsFieldName = computed(
  () =>
    `Isoform${
      typeof isoformsCount.value === 'number' && isoformsCount.value > 1
        ? 's'
        : ''
    }`
)

/**
 * Link between the guide and its parent chromosome, also containing the
 * chromosome object.
 */
const guideParentChromosomeConnection = computed(() =>
  guide.value?.chromosomeConnection.edges.find(
    (chromosomeEdge) =>
      chromosomeEdge.node.graphql_type === GraphQlType.Chromosome
  )
)

/**
 * The guide parent chromosome.
 */
const guideParentChromosome = computed(
  () => guideParentChromosomeConnection.value?.node
)

/**
 * The location of the guide on its parent chromosome.
 */
const guideParentChromosomeLocation = computed(
  () => guideParentChromosomeConnection.value?.properties
)

/**
 * The various ontology IDs, cleaned from their prefix.
 */
const ontologyIdsCleaned = computed(() => ({
  SO: guide.value?.so_id?.match(/SO_(\d+)/)?.[1],
  ChEBI: guide.value?.chebi_id?.match(/CHEBI_(\d+)/)?.[1]
}))

/**
 * The various ontology links.
 */
const ontologyLinks = computed(() => ({
  SO:
    ontologyIdsCleaned.value.SO &&
    `http://www.sequenceontology.org/browser/current_release/term/SO:${ontologyIdsCleaned.value.SO}`,
  ChEBI:
    ontologyIdsCleaned.value.ChEBI &&
    `https://www.ebi.ac.uk/chebi/searchId.do?chebiId=${ontologyIdsCleaned.value.ChEBI}`
}))

/**
 * The selected tab in the 'Graphics' panel.
 */
const selectedGraphicsTab = ref(GraphicsTabEnum[props.initialGraphicsPanelTab])
</script>

<template>
  <MainLayout padded>
    <div class="relative hidden md:block">
      <RouterLink
        v-if="guide"
        :to="{ name: 'table', query: { guideId: guide.id } }"
      >
        <Button
          severity="secondary"
          outlined
          class="!absolute flex gap-2 !shadow-none top-0 right-8"
        >
          <icon-fa6-solid-table class="text-xl" />
          View table for {{ guide.name }}
        </Button>
      </RouterLink>
    </div>

    <h1 class="mb-4 text-center text-3xl font-semibold text-slate-700">
      {{ guide?.name }}
    </h1>
    <h2 class="mb-8 text-center text-2xl text-slate-400">
      Guide • <span class="font-mono">{{ guide?.id }}</span>
    </h2>

    <Card
      v-if="guide?.description || guide?.altnames?.length"
      class="mx-auto mb-8 max-w-4xl border text-center !text-lg text-slate-700 !shadow-none 2xl:max-w-5xl"
      :pt="{
        content: {
          style: {
            padding: 0,
            textAlign: 'justify'
          }
        }
      }"
    >
      <template v-if="guide?.description" #title>
        <span>
          <icon-fa6-solid-file-lines class="mb-1 mr-2 inline-block text-xl" />
          <span>Description</span>
        </span>
      </template>

      <template #content>
        <BaseRenderedMarkdown
          v-if="guide?.description"
          :stringified-markdown="guide.description"
          class="flex justify-center"
        />

        <div
          v-if="guide?.altnames?.length"
          :class="[
            'text-center italic text-slate-400',
            { 'mt-4': guide?.description }
          ]"
        >
          Alternative names:
          {{ guide.altnames.join(', ') }}
        </div>
      </template>
    </Card>

    <Panel
      toggleable
      class="mx-auto mb-16 max-w-6xl text-center 2xl:max-w-7xl"
      :pt="{
        header: { class: '!bg-slate-50' },
        toggler: { class: 'hover:!bg-slate-100' }
      }"
    >
      <template #header="scope">
        <icon-fa6-solid-circle-info :class="scope.class" />
        <span :class="scope.class">Informations</span>
      </template>

      <div class="grid grid-cols-3 gap-x-4">
        <div
          class="relative my-auto italic text-slate-400 after:absolute after:-right-[1px] after:top-0 after:-mr-2 after:h-full after:rounded after:border-l-[1px] after:border-slate-200"
        >
          Guide class:
          <em class="text-lg font-bold italic text-slate-700">
            {{ guide?.class }}
          </em>
        </div>

        <div
          class="relative my-auto italic text-slate-400 after:absolute after:-right-[1px] after:top-0 after:-mr-2 after:h-full after:rounded after:border-l-[1px] after:border-slate-200"
        >
          Guide subclass:
          <em class="whitespace-nowrap text-lg font-bold italic text-slate-700">
            <BaseRenderedMarkdown
              :stringified-markdown="guide?.subclass_label || 'Other'"
              inline-content
            />
          </em>
        </div>

        <div class="my-auto italic text-slate-400">
          Organism:
          <RouterLink
            v-if="guide?.genome?.organism?.label"
            :to="{
              name: 'organism',
              query: { id: guide.genome.organism.id }
            }"
            class="text-lg font-bold not-italic text-slate-700 underline transition-all duration-300 ease-in-out hover:text-indigo-600"
          >
            <BaseRenderedMarkdown
              :stringified-markdown="guide.genome.organism.label"
            />
          </RouterLink>
        </div>

        <Divider class="col-span-3" />

        <span class="my-auto text-left italic text-slate-400"> Location: </span>
        <div
          :class="[
            guide?.cluster
              ? 'relative after:absolute after:-right-[1px] after:top-0 after:-mr-2 after:h-full after:rounded after:border-l-[1px] after:border-slate-200'
              : 'col-span-2',
            'my-auto text-left text-slate-700'
          ]"
        >
          <span v-if="guideParentChromosome && guideParentChromosomeLocation">
            <em
              v-tooltip.top="`Chromosome ${guideParentChromosome.name}`"
              class="text-lg font-bold not-italic"
            >
              {{ guideParentChromosome.name }} </em
            >:<em
              v-tooltip.top="
                `Start position: ${separateThousands(
                  guideParentChromosomeLocation.start
                )}`
              "
              class="text-lg font-bold not-italic"
            >
              {{ separateThousands(guideParentChromosomeLocation.start) }} </em
            >-<em
              v-tooltip.top="
                `End position: ${separateThousands(
                  guideParentChromosomeLocation.end
                )}`
              "
              class="text-lg font-bold not-italic"
            >
              {{ separateThousands(guideParentChromosomeLocation.end) }}
            </em>
            (strand:
            <em class="inline-block font-bold not-italic">
              <span class="flex items-center gap-2">
                {{ guideParentChromosomeLocation.strand }}
                <component
                  :is="
                    STRAND_CODE_TO_ICON_COMPONENT[
                      guideParentChromosomeLocation.strand || 'UNKNOWN'
                    ]
                  "
                  class="text-xl font-extrabold"
                />
              </span> </em
            >)
          </span>
          <em v-else class="font-bold italic">Unknown</em>
        </div>

        <div v-if="guide?.cluster" class="my-auto italic text-slate-400">
          Cluster:
          <RouterLink
            :key="guide.cluster.id"
            :to="{
              name: 'clusterDetails',
              query: { id: guide.cluster.id }
            }"
          >
            <Chip
              class="border-2 !border-slate-600 !bg-slate-100 !font-semibold !text-slate-600"
            >
              {{ guide.cluster.id }}
            </Chip>
          </RouterLink>
        </div>

        <template v-if="guide?.host_genes.length">
          <Divider class="col-span-3" />

          <span class="my-auto text-left italic text-slate-400">
            Host {{ composePlural(guide.host_genes.length, 'gene') }}:
          </span>
          <div
            class="col-span-2 my-auto flex flex-wrap justify-between gap-x-2 gap-y-1"
          >
            <Chip
              v-for="(gene, geneIndex) in guide?.host_genes"
              :key="geneIndex"
              class="border-2 !border-slate-600 !bg-slate-100 !font-semibold !text-slate-600"
            >
              {{ gene }}
            </Chip>
            <!-- The following div is here to ensure that Chips stay left-aligned
      in the flex div -->
            <div class="flex-1" />
          </div>
        </template>

        <template v-if="false">
          <Divider class="col-span-3" />

          <span class="my-auto text-left italic text-slate-400">
            Reference genome:
          </span>
          <em
            class="col-span-2 my-auto text-left text-lg font-bold italic text-slate-700"
          >
            None
          </em>
        </template>

        <Divider class="col-span-3" />

        <span class="my-auto text-left italic text-slate-400">
          {{ linkedModificationsFieldName }} ({{ linkedModificationsCount }}):
        </span>
        <div
          class="col-span-2 my-auto flex flex-wrap justify-between gap-x-2 gap-y-1"
        >
          <em
            v-if="linkedModificationsCount === 0"
            class="font-bold italic text-slate-700"
          >
            None
          </em>
          <RouterLink
            v-for="modification in guide?.modifications"
            :key="modification.id"
            :to="{
              name: 'modificationDetails',
              query: { id: modification.id }
            }"
          >
            <Chip
              :class="`border-2 !border-${modification.tailwindColor}-600 !bg-${modification.tailwindColor}-100 !font-semibold !text-${modification.tailwindColor}-600`"
            >
              {{ modification.name }}
            </Chip>
          </RouterLink>
          <!-- The following div is here to ensure that Chips stay left-aligned
              in the flex div -->
          <div class="flex-1" />
        </div>

        <Divider class="col-span-3" />

        <span class="my-auto text-left italic text-slate-400">
          {{ linkedTargetsFieldName }} ({{ linkedTargetsCount }}):
        </span>
        <div
          class="col-span-2 my-auto flex flex-wrap justify-between gap-x-2 gap-y-1"
        >
          <em
            v-if="linkedTargetsCount === 0"
            class="font-bold italic text-slate-700"
            >None</em
          >
          <RouterLink
            v-for="target in linkedTargets"
            :key="target.id"
            :to="{
              name: 'targetDetails',
              query: { id: target.id }
            }"
          >
            <Chip
              class="border-2 !border-amber-600 !bg-amber-100 !font-semibold !text-amber-600"
            >
              {{ target.name }} -
              {{ (target.unit ? `${target.unit} ` : '') + target.class }}
            </Chip>
          </RouterLink>
          <!-- The following div is here to ensure that Chips stay left-aligned
              in the flex div -->
          <div class="flex-1" />
        </div>

        <Divider class="col-span-3" />

        <span class="my-auto text-left italic text-slate-400">
          {{ isoformsFieldName }} ({{ isoformsCount }}):
        </span>
        <div
          class="col-span-2 my-auto flex flex-wrap justify-between gap-x-2 gap-y-1"
        >
          <em v-if="isoformsCount === 0" class="font-bold italic text-slate-700"
            >None</em
          >
          <RouterLink
            v-for="isoformGuide in guideIsoforms"
            :key="isoformGuide.id"
            :to="{
              name: 'guideDetails',
              query: { id: isoformGuide.id }
            }"
          >
            <Chip
              class="border-2 !border-lime-600 !bg-lime-100 !font-semibold !text-lime-600"
            >
              {{ isoformGuide.name }}
              <!-- - {{ isoformGuide.type }} -->
            </Chip>
          </RouterLink>
          <!-- The following div is here to ensure that Chips stay left-aligned
              in the flex div -->
          <div class="flex-1" />
        </div>

        <template v-if="ontologyLinks.SO || ontologyLinks.ChEBI">
          <Divider class="col-span-3" />

          <span class="my-auto text-left italic text-slate-400">
            Ontologies:
          </span>
          <div
            class="col-span-2 my-auto flex flex-wrap justify-between gap-x-2 gap-y-1"
          >
            <a v-if="ontologyLinks.SO" :href="ontologyLinks.SO">
              <BaseDescribedChip
                :label="ontologyIdsCleaned.SO"
                description="SO"
                color="red"
              />
            </a>
            <a v-if="ontologyLinks.ChEBI" :href="ontologyLinks.ChEBI">
              <BaseDescribedChip
                :label="ontologyIdsCleaned.ChEBI"
                description="ChEBI"
                color="red"
              />
            </a>
            <!-- The following div is here to ensure that Chips stay left-aligned
              in the flex div -->
            <div class="flex-1" />
          </div>
        </template>
      </div>
    </Panel>

    <Panel
      id="sequence-panel"
      toggleable
      class="mx-auto mb-16 max-w-7xl break-words 2xl:max-w-[100rem]"
      :pt="{
        header: { class: '!bg-slate-50' },
        toggler: { class: 'hover:!bg-slate-100' }
      }"
    >
      <template #header="scope">
        <icon-fa6-solid-dna :class="scope.class" />
        <span :class="scope.class">Guide sequence</span>
      </template>
      <SequenceBoard
        v-if="guide?.seq"
        :sequence="guide.seq.replace(/T/g, 'U')"
        :sequence-id="guide.id"
        :objects="sequenceObjects"
        :legend-items="sequenceLegendItems"
      >
        <template #tooltip-item="{ object: group }">
          <Chip
            v-if="group.type && isInEnum(group.type, ModifType)"
            :class="[
              'border-2 !font-semibold',
              `!border-${getModificationColor(
                group.type
              )}-600 !bg-${getModificationColor(
                group.type
              )}-100 !text-${getModificationColor(group.type)}-600`
            ]"
          >
            {{ group.name }}
          </Chip>
          <span
            v-else-if="group.type === AnnotationType.CBox"
            :class="[
              'whitespace-nowrap font-bold not-italic',
              `text-${getBoxColor(group.type)}-600`
            ]"
          >
            <em>C</em> box
          </span>
          <span
            v-else-if="group.type === AnnotationType.DBox"
            :class="[
              'whitespace-nowrap font-bold not-italic',
              `text-${getBoxColor(group.type)}-600`
            ]"
          >
            <em>D</em> box
          </span>
        </template>
      </SequenceBoard>
    </Panel>

    <Panel
      id="graphics-panel"
      toggleable
      class="mx-auto mb-16 max-w-7xl break-words 2xl:max-w-[100rem]"
      :pt="{
        header: { class: '!bg-slate-50' },
        toggler: { class: 'hover:!bg-slate-100' }
      }"
    >
      <template #header="scope">
        <icon-fa6-solid-draw-polygon :class="scope.class" />
        <span :class="scope.class">Graphics</span>
      </template>

      <TabView v-model:active-index="selectedGraphicsTab">
        <TabPanel header="Local interaction">
          <!-- v-if is to ensure we can access and map `guide.interactions` -->
          <InteractionBoard
            v-if="interactionList"
            :interaction-list="interactionList"
            selection-field-path1="target.id"
            selection-field-legend1="Targets"
            selection-field-path2="modification.id"
            selection-field-legend2="Modifications"
          >
            <template #item-label-1="{ currentValue }">
              <strong>{{ currentValue.target.name }}</strong>
              <em class="italic text-slate-400">
                -
                {{
                  (currentValue.target.unit
                    ? `${currentValue.target.unit} `
                    : '') + currentValue.target.class
                }}
              </em>
            </template>

            <template #item-label-2="{ currentValue }">
              <strong>{{ currentValue.modification.name }}</strong>
              <em
                v-if="currentValue.modification.type_short_label"
                class="italic text-slate-400"
              >
                -
                <BaseRenderedMarkdown
                  :stringified-markdown="
                    currentValue.modification.type_short_label
                  "
                  inline-content
                />
              </em>
            </template>

            <template #selection-incomplete-message>
              Please select a target & modification on the left to visualise the
              interaction.
            </template>
          </InteractionBoard>
        </TabPanel>
      </TabView>
    </Panel>
  </MainLayout>
</template>

<style></style>
