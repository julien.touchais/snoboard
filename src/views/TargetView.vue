<script setup lang="ts">
/**
 * Vue imports
 */
import { computed, onMounted, ref, toRef } from 'vue'
/**
 * Components imports
 */
import MainLayout from '@/layouts/MainLayout.vue'
import SequenceBoard from '@/components/SequenceBoard.vue'
import InteractionBoard from '@/components/InteractionBoard.vue'
import FormattedModificationType from '@/components/FormattedModificationType.vue'
import SecondaryStructure from '@/components/SecondaryStructure.vue'
import BaseDescribedChip from '@/components/BaseDescribedChip.vue'
import BaseRenderedMarkdown from '@/components/BaseRenderedMarkdown.vue'
import TabPanel from 'primevue/tabpanel'
import TabView from 'primevue/tabview'
import Divider from 'primevue/divider'
import Chip from 'primevue/chip'
import Card from 'primevue/card'
import Panel from 'primevue/panel'
import Button from 'primevue/button'
import IconFa6SolidFileLines from '~icons/fa6-solid/file-lines'
import IconFa6SolidCircleInfo from '~icons/fa6-solid/circle-info'
import IconFa6SolidDna from '~icons/fa6-solid/dna'
import IconFa6SolidDrawPolygon from '~icons/fa6-solid/draw-polygon'
import IconFa6SolidCircleMinus from '~icons/fa6-solid/circle-minus'
import IconFa6SolidCirclePlus from '~icons/fa6-solid/circle-plus'
import IconFa6SolidCircleQuestion from '~icons/fa6-solid/circle-question'
import IconFa6SolidCircleXmark from '~icons/fa6-solid/circle-xmark'
import IconFa6SolidTable from '~icons/fa6-solid/table'
/**
 * Composables imports
 */
import { useQuery } from '@urql/vue'
import { useFetch, useTitle } from '@vueuse/core'
/**
 * Other 3rd-party imports
 */
import {
  find as _find,
  sortBy as _sortBy,
  mapValues as _mapValues,
  uniq as _uniq
} from 'lodash-es'
/**
 * Types import
 */
import type { TailwindDefaultColorNameModel } from '@/typings/styleTypes'
import type { InteractionCardModel } from '@/components/InteractionCard.vue'
import type { C4GGraphModel } from '@/typings/Codev4GraphFormat'
import type { objectModel } from '@/components/SequenceBoard.vue'
import {
  GraphQlType,
  ModifType,
  SequenceClass,
  Strand
} from '@/gql/codegen/graphql'
/**
 * Utils imports
 */
import { separateThousands } from '@/utils/textFormatting'
import { targetByIdQuery } from '@/gql/queries'
import { isDefined, isInEnum } from '@/typings/typeUtils'
import { getModificationColor } from '@/utils/colors'
import type { LegendItemModel } from '@/components/BaseLegendButtonOverlay.vue'

/**
 * Enum to represent the tabs in the 'Graphics' panel.
 */
enum GraphicsTabEnum {
  interaction = 0,
  '2d-struct'
}

/**
 * Utility constant to get the icon component corresponding to each strand value
 */
const STRAND_CODE_TO_ICON_COMPONENT = {
  [Strand.Negative]: IconFa6SolidCircleMinus,
  [Strand.Positive]: IconFa6SolidCirclePlus,
  [Strand.Unknown]: IconFa6SolidCircleQuestion,
  [Strand.NotStranded]: IconFa6SolidCircleXmark
}

/**
 * Component props
 */
const props = defineProps<{
  /** The ID of the target to display */
  targetId: string
  /** The tab to display initially in the 'Graphics' panel */
  initialGraphicsPanelTab: keyof typeof GraphicsTabEnum
}>()

/**
 * Reactive urql GraphQL query object, updated with query state & response
 */
const gqlQuery = useQuery({
  query: targetByIdQuery,
  variables: toRef(() => ({
    id: props.targetId
  }))
})

/**
 * The target to display, reactively updated when fetched
 */
const target = computed(
  () =>
    gqlQuery.data.value?.targets[0] && {
      ...gqlQuery.data.value?.targets[0],
      modifications: gqlQuery.data.value?.targets[0].modifications.map(
        (modification) => ({
          ...modification,
          tailwindColor: modification.type
            ? getModificationColor(modification.type)
            : 'slate'
        })
      )
    }
)

/**
 * The list of the guides interacting with the target,
 * reactively updated when fetched
 */
const linkedGuides = computed(() =>
  _sortBy(gqlQuery.data.value?.guides, 'chromosome.name')
)
/**
 * URL where to find static asset for secondary structure
 */
const secondaryStructureUrl = computed(() =>
  target.value?.secondary_struct_file
    ? import.meta.env.VITE_STATIC_URL + target.value?.secondary_struct_file
    : ''
)

/**
 * Reactive secondary structure query object, updated with query state & response
 */
const secondaryStructureQuery = useFetch(secondaryStructureUrl, {
  refetch: true
}).json<C4GGraphModel>()

/**
 * The secondary structure graph representation of the target,
 * reactively updated when fetched
 */
const secondaryStructure = computed(() => secondaryStructureQuery.data.value)

/**
 * The secondary structure graph representation of the target,
 * enriched with modification information on appropriate nodes,
 * reactively updated when fetched
 */
const secondaryStructureWithModificationsMetadata =
  computed<C4GGraphModel | null>(
    () =>
      secondaryStructure.value && {
        graph: {
          ...secondaryStructure.value?.graph,
          nodes: _mapValues(secondaryStructure.value?.graph.nodes, (node) => {
            const nodeModification = _find(target.value?.modifications, [
              'position',
              _find(node.metadata?.data, ['label', 'nucleotidePosition'])?.value
            ])
            return nodeModification
              ? {
                  ...node,
                  metadata: {
                    ...node.metadata,
                    data: [
                      ...(node.metadata?.data || []),
                      {
                        label: 'modification',
                        type: 'number',
                        value: nodeModification
                      }
                    ]
                  }
                }
              : node
          })
        }
      }
  )

/**
 * Title of the page, reactively updated when data is fetched
 */
const pageTitle = computed(() =>
  target.value
    ? `${target.value.name} • Target | snoBoard`
    : 'Target | snoBoard'
)
// Bind actual page title to computed one
onMounted(() => useTitle(pageTitle))

/**
 * The list of the interaction, formatted for display. If some mandatory fields
 * for display (mainly sequences) are missing, the corresponding interaction
 * will not be present in the list (and thus not available for display).
 */
const interactionList = computed<InteractionCardModel[] | undefined>(() => {
  if (!target.value) return undefined
  return target.value.interactions
    .map<InteractionCardModel | undefined>((interaction) => {
      if (!target.value) return undefined
      const duplexes = interaction.duplexes.map<
        InteractionCardModel['duplexes'][0] | undefined
      >((duplex) => {
        const primaryStrandConnection = duplex.primaryStrandsConnection.edges[0]
        const secondaryStrandConnection =
          duplex.secondaryStrandsConnection.edges[0]
        if (
          !primaryStrandConnection?.node.seq ||
          !secondaryStrandConnection?.node.seq
        )
          return undefined
        return {
          primaryFragment: {
            seq: primaryStrandConnection.node.seq,
            start: primaryStrandConnection.properties.start,
            end: primaryStrandConnection.properties.end,
            onParentPosition: {
              start:
                primaryStrandConnection.node.parentConnection.edges[0]
                  ?.properties.start,
              end: primaryStrandConnection.node.parentConnection.edges[0]
                ?.properties.end
            }
          },
          secondaryFragment: {
            seq: secondaryStrandConnection.node.seq,
            start: secondaryStrandConnection.properties.start,
            end: secondaryStrandConnection.properties.end,
            onParentPosition: {
              start:
                secondaryStrandConnection.node.parentConnection.edges[0]
                  ?.properties.start,
              end: secondaryStrandConnection.node.parentConnection.edges[0]
                ?.properties.end
            }
          },
          index: duplex.index
        }
      })
      if (!duplexes.every(isDefined)) return undefined
      return {
        guide: {
          id: interaction.guide.id,
          name: interaction.guide.name || undefined,
          class: interaction.guide.class,
          subclass_label: interaction.guide.subclass_label
        },
        target: {
          id: target.value.id,
          name: target.value.name || undefined,
          class: target.value.class || SequenceClass.Other,
          unit: target.value.unit || undefined
        },
        modification: {
          id: interaction.modification.id,
          name: interaction.modification.name,
          type: interaction.modification.type || undefined,
          type_short_label:
            interaction.modification.type_short_label || undefined,
          position: interaction.modification.position,
          symbol: interaction.modification.symbol,
          symbol_label: interaction.modification.symbol_label
        },
        duplexes
      }
    })
    .filter(isDefined)
})

/**
 * Target modifications, filtered by keeping only ones with positive position
 */
const filteredModifications = computed(
  () =>
    target.value?.modifications.filter(
      (modification) => modification.position >= 0
    ) || []
)

/**
 * Objects to highlight on the sequence of the target
 */
const sequenceObjects = computed(() =>
  filteredModifications.value.reduce<{
    [groupId: string]: objectModel
  }>(
    (sequenceObjects, modification) => ({
      ...sequenceObjects,
      [modification.id]: {
        start: modification.position,
        end: modification.position,
        color: modification.type
          ? getModificationColor(modification.type)
          : ('slate' as TailwindDefaultColorNameModel),
        name: modification.name,
        type: modification.type || undefined,
        link: {
          name: 'modificationDetails',
          query: {
            id: modification.id
          }
        },
        shouldTooltip: true
      }
    }),
    {}
  )
)

/**
 * A list of the different types of the modifications of the target.
 */
const modificationTypes = computed(() =>
  _uniq(
    filteredModifications.value.map(
      (modification) => modification.type || ModifType.Other
    )
  )
)

/**
 * The legend to display on the sequence board.
 */
const sequenceLegendItems = computed(() =>
  modificationTypes.value
    .map<LegendItemModel>((modificationType) => ({
      id: modificationType,
      title: modificationType,
      color: getModificationColor(modificationType),
      description:
        modificationType === ModifType.Other
          ? 'Modification of a different type than those listed above.'
          : undefined
    }))
    .sort(
      (legendItemA, legendItemB) =>
        (legendItemA.id === 'Other' && 1) ||
        (legendItemA.title > legendItemB.title && 1) ||
        -1
    )
)

/**
 * Number of modifications linked to the target, equals to `'...'` if not fetched yet
 */
const linkedModificationsCount = computed(() =>
  target.value?.modificationsAggregate == null
    ? '...'
    : target.value.modificationsAggregate.count
)

/**
 * Name of "Guided modification(s)" field in "Informations" panel (potentially
 * plural).
 */
const linkedModificationsFieldName = computed(
  () =>
    `Guided modification${
      typeof linkedModificationsCount.value === 'number' &&
      linkedModificationsCount.value > 1
        ? 's'
        : ''
    }`
)

/**
 * Number of guides linked to the target, equals to `'...'` if not fetched yet
 */
const linkedGuidesCount = computed(() =>
  linkedGuides.value === undefined ? '...' : linkedGuides.value.length
)

/**
 * Name of "Guide(s)" field in "Informations" panel (potentially plural).
 */
const linkedGuidesFieldName = computed(
  () =>
    `Guide${
      typeof linkedGuidesCount.value === 'number' && linkedGuidesCount.value > 1
        ? 's'
        : ''
    }`
)

/**
 * Link between the target and its parent chromosome, also containing the
 * chromosome object
 */
const targetParentChromosomeConnection = computed(() =>
  target.value?.chromosomeConnection.edges.find(
    (chromosomeEdge) =>
      chromosomeEdge.node.graphql_type === GraphQlType.Chromosome
  )
)

/**
 * The target parent chromosome
 */
const targetParentChromosome = computed(
  () => targetParentChromosomeConnection.value?.node
)

/**
 * The location of the target on its parent chromosome
 */
const targetParentChromosomeLocation = computed(
  () => targetParentChromosomeConnection.value?.properties
)

/**
 * The various ontology IDs, cleaned from their prefix
 */
const ontologyIdsCleaned = computed(() => ({
  SO: target.value?.so_id?.match(/SO_(\d+)/)?.[1],
  ChEBI: target.value?.chebi_id?.match(/CHEBI_(\d+)/)?.[1]
}))

/**
 * The various ontology links
 */
const ontologyLinks = computed(() => ({
  SO:
    ontologyIdsCleaned.value.SO &&
    `http://www.sequenceontology.org/browser/current_release/term/SO:${ontologyIdsCleaned.value.SO}`,
  ChEBI:
    ontologyIdsCleaned.value.ChEBI &&
    `https://www.ebi.ac.uk/chebi/searchId.do?chebiId=${ontologyIdsCleaned.value.ChEBI}`
}))

/**
 * The selected tab in the 'Graphics' panel.
 */
const selectedGraphicsTab = ref(GraphicsTabEnum[props.initialGraphicsPanelTab])
</script>

<template>
  <MainLayout padded>
    <div class="relative hidden md:block">
      <RouterLink
        v-if="target"
        :to="{ name: 'table', query: { targetId: target.id } }"
      >
        <Button
          severity="secondary"
          outlined
          class="!absolute flex gap-2 !shadow-none top-0 right-8"
        >
          <icon-fa6-solid-table class="text-xl" />
          View table for {{ target.name }}
        </Button>
      </RouterLink>
    </div>

    <h1 class="mb-4 text-center text-3xl font-semibold text-slate-700">
      {{ target?.name }}
    </h1>
    <h2 class="mb-8 text-center text-2xl text-slate-400">
      Target • <span class="font-mono">{{ target?.id }}</span>
    </h2>

    <Card
      v-if="target?.description || target?.altnames?.length"
      class="mx-auto mb-8 max-w-4xl border text-center !text-lg text-slate-700 !shadow-none 2xl:max-w-5xl"
      :pt="{
        content: {
          style: {
            padding: 0,
            textAlign: 'justify'
          }
        }
      }"
    >
      <template v-if="target?.description" #title>
        <span>
          <icon-fa6-solid-file-lines class="mb-1 mr-2 inline-block text-xl" />
          <span class="font-semibold">Description</span>
        </span>
      </template>

      <template #content>
        <BaseRenderedMarkdown
          v-if="target?.description"
          :stringified-markdown="target.description"
          class="flex justify-center"
        />

        <div
          v-if="target?.altnames?.length"
          :class="[
            'text-center italic text-slate-400',
            { 'mt-4': target?.description }
          ]"
        >
          Alternative names:
          {{ target.altnames.join(', ') }}
        </div>
      </template>
    </Card>

    <Panel
      toggleable
      class="mx-auto mb-16 max-w-6xl text-center 2xl:max-w-7xl"
      :pt="{
        header: { class: '!bg-slate-50' },
        toggler: { class: 'hover:!bg-slate-100' }
      }"
    >
      <template #header="scope">
        <icon-fa6-solid-circle-info :class="scope.class" />
        <span :class="scope.class">Informations</span>
      </template>
      <div class="grid grid-cols-3 gap-x-4">
        <div
          class="relative my-auto italic text-slate-400 after:absolute after:-right-[1px] after:top-0 after:-mr-2 after:h-full after:rounded after:border-l-[1px] after:border-slate-200"
        >
          Target type:
          <em class="text-lg font-bold not-italic text-slate-700">
            {{ (target?.unit ? `${target.unit} ` : '') + target?.class }}
          </em>
        </div>

        <div
          class="relative my-auto italic text-slate-400 after:absolute after:-right-[1px] after:top-0 after:-mr-2 after:h-full after:rounded after:border-l-[1px] after:border-slate-200"
        >
          Target length:
          <em class="text-lg font-bold not-italic text-slate-700">
            {{ target?.length }}
          </em>
        </div>

        <div class="my-auto italic text-slate-400">
          Organism:
          <RouterLink
            v-if="target?.genome?.organism?.label"
            :to="{
              name: 'organism',
              query: { id: target.genome?.organism?.id }
            }"
            class="text-lg font-bold not-italic text-slate-700 underline transition-all duration-300 ease-in-out hover:text-indigo-600"
          >
            <BaseRenderedMarkdown
              :stringified-markdown="target.genome.organism.label"
            />
          </RouterLink>
        </div>

        <Divider class="col-span-3" />

        <span class="my-auto text-left italic text-slate-400"> Location: </span>
        <div class="col-span-2 my-auto text-left text-slate-700">
          <span v-if="targetParentChromosome && targetParentChromosomeLocation">
            <em
              v-tooltip.top="`Chromosome ${targetParentChromosome.name}`"
              class="text-lg font-bold not-italic"
            >
              {{ targetParentChromosome.name }} </em
            >:<em
              v-tooltip.top="
                `Start position: ${separateThousands(
                  targetParentChromosomeLocation.start
                )}`
              "
              class="text-lg font-bold not-italic"
            >
              {{ separateThousands(targetParentChromosomeLocation.start) }} </em
            >–<em
              v-tooltip.top="
                `End position: ${separateThousands(
                  targetParentChromosomeLocation.end
                )}`
              "
              class="text-lg font-bold not-italic"
            >
              {{ separateThousands(targetParentChromosomeLocation.end) }}
            </em>
            (strand:
            <em class="inline-block font-bold not-italic">
              <span class="flex items-center gap-2">
                {{ targetParentChromosomeLocation.strand }}
                <component
                  :is="
                    STRAND_CODE_TO_ICON_COMPONENT[
                      targetParentChromosomeLocation.strand || 'UNKNOWN'
                    ]
                  "
                  class="text-xl font-extrabold"
                /> </span></em
            >)
          </span>
          <em v-else class="font-bold italic">Unknown</em>
        </div>

        <template v-if="false">
          <Divider class="col-span-3" />

          <span class="my-auto text-left italic text-slate-400">
            Reference genome:
          </span>
          <em
            class="col-span-2 my-auto text-left text-lg font-bold italic text-slate-700"
          >
            None
          </em>
        </template>

        <Divider class="col-span-3" />

        <span class="my-auto text-left italic text-slate-400">
          {{ linkedModificationsFieldName }} ({{ linkedModificationsCount }}):
        </span>
        <div
          class="col-span-2 my-auto flex flex-wrap justify-between gap-x-2 gap-y-1"
        >
          <em
            v-if="linkedModificationsCount === 0"
            class="font-bold italic text-slate-700"
            >None</em
          >
          <RouterLink
            v-for="modification in target?.modifications"
            :key="modification.id"
            :to="{
              name: 'modificationDetails',
              query: { id: modification.id }
            }"
          >
            <Chip
              :class="`border-2 !border-${modification.tailwindColor}-600 !bg-${modification.tailwindColor}-100 !font-semibold !text-${modification.tailwindColor}-600`"
            >
              {{ modification.name }}
            </Chip>
          </RouterLink>
          <!-- The following div is here to ensure that Chips stay left-aligned
              in the flex div -->
          <div class="flex-1" />
        </div>

        <Divider class="col-span-3" />

        <span class="my-auto text-left italic text-slate-400">
          {{ linkedGuidesFieldName }} ({{ linkedGuidesCount }}):
        </span>
        <div
          class="col-span-2 my-auto flex flex-wrap justify-between gap-x-2 gap-y-1"
        >
          <em
            v-if="linkedGuidesCount === 0"
            class="font-bold italic text-slate-700"
            >None</em
          >
          <RouterLink
            v-for="linkedGuide in linkedGuides"
            :key="linkedGuide.id"
            :to="{
              name: 'guideDetails',
              query: { id: linkedGuide.id }
            }"
          >
            <Chip
              class="border-2 !border-lime-600 !bg-lime-100 !font-semibold !text-lime-600"
            >
              {{ linkedGuide.name }} -
              {{ linkedGuide.class }}
            </Chip>
          </RouterLink>
          <!-- The following div is here to ensure that Chips stay left-aligned
              in the flex div -->
          <div class="flex-1" />
        </div>

        <template v-if="ontologyLinks.SO || ontologyLinks.ChEBI">
          <Divider class="col-span-3" />

          <span class="my-auto text-left italic text-slate-400">
            Ontologies:
          </span>
          <div
            class="col-span-2 my-auto flex flex-wrap justify-between gap-x-2 gap-y-1"
          >
            <a v-if="ontologyLinks.SO" :href="ontologyLinks.SO">
              <BaseDescribedChip
                :label="ontologyIdsCleaned.SO"
                description="SO"
                color="red"
              />
            </a>
            <a v-if="ontologyLinks.ChEBI" :href="ontologyLinks.ChEBI">
              <BaseDescribedChip
                :label="ontologyIdsCleaned.ChEBI"
                description="ChEBI"
                color="red"
              />
            </a>
            <!-- The following div is here to ensure that Chips stay left-aligned
                in the flex div -->
            <div class="flex-1" />
          </div>
        </template>
      </div>
    </Panel>

    <Panel
      id="sequence-panel"
      toggleable
      class="mx-auto mb-16 max-w-7xl break-words 2xl:max-w-[100rem]"
      :pt="{
        header: { class: '!bg-slate-50' },
        toggler: { class: 'hover:!bg-slate-100' }
      }"
    >
      <template #header="scope">
        <icon-fa6-solid-dna :class="scope.class" />
        <span :class="scope.class">Target sequence</span>
      </template>
      <SequenceBoard
        v-if="target?.seq"
        :sequence="target.seq.replace(/T/g, 'U')"
        :sequence-id="target.id"
        :objects="sequenceObjects"
        :legend-items="sequenceLegendItems"
      >
        <template #legend-item-title="{ item }">
          <FormattedModificationType
            v-if="isInEnum(item.title, ModifType)"
            class="font-bold not-italic text-slate-600"
            :type-code="item.title"
            long-format
          />
        </template>

        <template #tooltip-item="{ object: group }">
          <Chip
            v-if="group.type && isInEnum(group.type, ModifType)"
            :class="[
              'border-2 !font-semibold',
              `!border-${getModificationColor(
                group.type
              )}-600 !bg-${getModificationColor(
                group.type
              )}-100 !text-${getModificationColor(group.type)}-600`
            ]"
          >
            {{ group.name }}
          </Chip>
        </template>
      </SequenceBoard>
    </Panel>

    <Panel
      id="graphics-panel"
      toggleable
      class="mx-auto mb-16 max-w-7xl break-words 2xl:max-w-[100rem]"
      :pt="{
        header: { class: '!bg-slate-50' },
        toggler: { class: 'hover:!bg-slate-100' }
      }"
    >
      <template #header="scope">
        <icon-fa6-solid-draw-polygon :class="scope.class" />
        <span :class="scope.class">Graphics</span>
      </template>

      <TabView v-model:active-index="selectedGraphicsTab" :lazy="true">
        <TabPanel header="Local interaction">
          <!-- v-if is to ensure we can access and map `target.interactions` -->
          <InteractionBoard
            v-if="interactionList"
            :interaction-list="interactionList"
            selection-field-path1="modification.id"
            selection-field-legend1="Modifications"
            selection-field-path2="guide.id"
            selection-field-legend2="Guides"
          >
            <template #item-label-1="{ currentValue }">
              <strong>{{ currentValue.modification.name }}</strong>
              <em
                v-if="currentValue.modification.type_short_label"
                class="italic text-slate-400"
              >
                -
                <BaseRenderedMarkdown
                  :stringified-markdown="
                    currentValue.modification.type_short_label
                  "
                  inline-content
                />
              </em>
            </template>

            <template #item-label-2="{ currentValue }">
              <strong>{{ currentValue.guide.name }}</strong>
              <em class="italic text-slate-400">
                -
                {{ currentValue.guide.class }}
              </em>
            </template>

            <template #selection-incomplete-message>
              Please select a modification & guide on the left to visualise the
              interaction.
            </template>
          </InteractionBoard>
        </TabPanel>

        <TabPanel
          v-if="secondaryStructureWithModificationsMetadata"
          header="Secondary structure"
        >
          <SecondaryStructure
            :structure="secondaryStructureWithModificationsMetadata"
          />
        </TabPanel>
      </TabView>
    </Panel>
  </MainLayout>
</template>
