<script setup lang="ts">
/**
 * Vue imports
 */
import { ref, computed } from 'vue'
/**
 * Components imports
 */
import MainLayout from '@/layouts/MainLayout.vue'
import Panel from 'primevue/panel'
import Divider from 'primevue/divider'
import Chart from 'primevue/chart'
import ToggleButton from 'primevue/togglebutton'
import IconFa6SolidPaw from '~icons/fa6-solid/paw'
import IconCarbonAccessibilityColor from '~icons/carbon/accessibility-color'
import IconCarbonAccessibilityColorFilled from '~icons/carbon/accessibility-color-filled'
import IconSnoboardModification from '~icons/snoboard/modification'
import IconSnoboardGuide from '~icons/snoboard/guide'
/**
 * Composables imports
 */
import { useQuery } from '@urql/vue'
/**
 * Other 3rd-party imports
 */
import { countBy as _countBy } from 'lodash-es'
import pattern from 'patternomaly'
/**
 * Types imports
 */
import { GuideClass, ModifType } from '@/gql/codegen/graphql'
import type { ChartOptions } from 'chart.js'
/**
 * Utils imports
 */
import {
  composePlural,
  formatGuideSubclass,
  formatModificationType
} from '@/utils/textFormatting'
import { databaseStatisticsQuery } from '@/gql/queries'
import {
  composeDivergentScale,
  getColorWithOptionalShade,
  getModificationColor
} from '@/utils/colors'

/**
 * The options for the doughnut chart (chart.js options).
 */
const DOUGHNUT_CHART_OPTIONS: ChartOptions = {
  plugins: {
    legend: {
      position: 'right',
      labels: {
        color: getColorWithOptionalShade('slate', '700'),
        font: {
          size: 18,
          weight: 700,
          style: 'italic'
        },
        padding: 16,
        filter: (legendItem, chartData) =>
          legendItem.index != null &&
          !!chartData.datasets[0]?.data[legendItem.index],
        pointStyle: 'rectRounded',
        usePointStyle: true
      }
    }
  },
  layout: {
    padding: 25
  },
  datasets: {
    doughnut: {
      borderRadius: 10,
      offset: 50,
      hoverOffset: 75
    }
  }
}

/**
 * Reactive urql GraphQL query object, updated with query state & response.
 */
const gqlQuery = useQuery({
  query: databaseStatisticsQuery
})

/**
 * Stats about organisms in the database, reactively updated when fetched.
 */
const organisms = computed(() => gqlQuery.data.value?.organismsAggregate)

/**
 * For each modification type represented at least once in the database, how many
 * modification of this type is there.
 */
const modificationCountByType = computed(() =>
  _countBy(gqlQuery.data.value?.allModifications, 'type')
)

/**
 * Stats about modifications in the database, reactively updated when fetched.
 */
const modifications = computed(() => ({
  totalCount: gqlQuery.data.value?.allModificationsCount.count,
  nonOrphanCount: gqlQuery.data.value?.nonOrphanModificationsCount.count,
  typePieChartData: {
    labels: Object.keys(modificationCountByType.value).map((modifTypeCode) =>
      formatModificationType(modifTypeCode as ModifType)
    ),
    datasets: [
      {
        label: 'Count',
        data: Object.values(modificationCountByType.value),
        backgroundColor: isColourBlindMode.value
          ? pattern.generate(
              Object.keys(modificationCountByType.value).map((modifTypeCode) =>
                getModificationColor(modifTypeCode as ModifType, '400')
              )
            )
          : Object.keys(modificationCountByType.value)
              .map((modifTypeCode) =>
                getModificationColor(modifTypeCode as ModifType, '400')
              )
              .map((hexCode) => `${hexCode}80`),
        hoverBackgroundColor: !isColourBlindMode.value
          ? Object.keys(modificationCountByType.value)
              .map((modifTypeCode) =>
                getModificationColor(modifTypeCode as ModifType, '400')
              )
              .map((hexCode) => `${hexCode}b0`)
          : undefined,
        borderColor: Object.keys(modificationCountByType.value).map(
          (modifTypeCode) =>
            getModificationColor(modifTypeCode as ModifType, '400')
        )
      }
    ]
  }
}))

/**
 * For each guide subclass represented at least once in the database, how many
 * guides of this subclass is there.
 */
const guideCountBySubclass = computed<{ [k in GuideClass]?: number }>(() =>
  _countBy(gqlQuery.data.value?.allGuides, 'subclass')
)

/**
 * Stats about guides in the database, reactively updated when fetched.
 */
const guides = computed(() => ({
  totalCount: gqlQuery.data.value?.allGuidesCount.count,
  nonOrphanCount: gqlQuery.data.value?.nonOrphanGuidesCount.count,
  subclassPieChartData: {
    labels: Object.keys(guideCountBySubclass.value).map((guideSubclassCode) =>
      formatGuideSubclass(guideSubclassCode as GuideClass)
    ),
    datasets: [
      {
        label: 'Count',
        data: Object.values(guideCountBySubclass.value),
        backgroundColor: isColourBlindMode.value
          ? pattern.generate(
              composeDivergentScale(
                '#65a30d',
                '#fbbf24',
                Object.values(guideCountBySubclass.value).length
              )
            )
          : composeDivergentScale(
              '#65a30d',
              '#fbbf24',
              Object.values(guideCountBySubclass.value).length
            ).map((hexCode) => `${hexCode}80`),
        hoverBackgroundColor: !isColourBlindMode.value
          ? composeDivergentScale(
              '#65a30d',
              '#fbbf24',
              Object.values(guideCountBySubclass.value).length
            ).map((hexCode) => `${hexCode}b0`)
          : undefined,
        borderColor: composeDivergentScale(
          '#65a30d',
          '#fbbf24',
          Object.values(guideCountBySubclass.value).length
        )
      }
    ]
  }
}))

/**
 * Coverages values (% of guides _(resp. modifications)_ which has at least one
 * modification _(resp.guide)_ linked to it), in percents.
 */
const coverages = computed(() => ({
  guide: (
    ((guides.value.nonOrphanCount || 0) / (guides.value.totalCount || 1)) *
    100
  ).toPrecision(3),
  modification: (
    ((modifications.value.nonOrphanCount || 0) /
      (modifications.value.totalCount || 1)) *
    100
  ).toPrecision(3)
}))

/**
 * Orphan counts (number of guides _(resp. modifications)_ which has no
 * modification _(resp.guide)_ linked to it).
 */
const orphanCounts = computed(() => ({
  guide: (guides.value.totalCount || 0) - (guides.value.nonOrphanCount || 0),
  modification:
    (modifications.value.totalCount || 0) -
    (modifications.value.nonOrphanCount || 0)
}))

/**
 * Wether to display the charts in colour-blind mode or not.
 */
const isColourBlindMode = ref(false)
</script>

<template>
  <MainLayout padded>
    <h1 class="mb-4 text-center text-3xl font-semibold italic text-slate-700">
      Database statistics
    </h1>
    <h2 class="mb-8 text-center text-2xl text-slate-400">
      This page compiles various statistics and numbers about the snoBoard's
      database
    </h2>

    <Panel
      toggleable
      class="mx-auto mb-8 max-w-6xl text-center 2xl:max-w-7xl"
      :pt="{
        header: { class: '!bg-slate-50' },
        toggler: { class: 'hover:!bg-slate-100' }
      }"
    >
      <template #header="scope">
        <icon-fa6-solid-paw :class="scope.class" />
        <span :class="scope.class">Organisms</span>
      </template>

      <div class="grid grid-cols-3 gap-x-4">
        <span class="my-auto text-left italic text-slate-400">
          Organisms in the database:
        </span>
        <em
          v-if="organisms?.count"
          class="col-span-2 my-auto text-left text-lg font-bold not-italic text-slate-700"
        >
          {{ organisms.count }}
        </em>
      </div>
    </Panel>

    <Panel
      toggleable
      class="mx-auto mb-8 max-w-6xl text-center 2xl:max-w-7xl"
      :pt="{
        header: { class: '!bg-slate-50' },
        toggler: { class: 'hover:!bg-slate-100' }
      }"
    >
      <template #header="scope">
        <icon-snoboard-modification :class="scope.class" />
        <span :class="scope.class">Modifications</span>
      </template>

      <div class="grid grid-cols-3 gap-x-4">
        <span class="my-auto text-left italic text-slate-400">
          Modifications in the database:
        </span>
        <em
          v-if="modifications.totalCount"
          class="col-span-2 my-auto text-left text-lg font-bold not-italic text-slate-700"
        >
          {{ modifications.totalCount }}
        </em>

        <Divider class="col-span-3" />

        <span class="my-auto text-left italic text-slate-400">
          <span class="block">Modification coverage:</span>
          <small class="text-sm text-slate-400">
            % of modifications which are associated with at least one guide
          </small>
        </span>
        <span class="col-span-2 my-auto text-left italic text-slate-700">
          <em class="text-lg font-bold not-italic">
            {{ coverages.modification }} %
          </em>
          (<em class="font-bold">{{ orphanCounts.modification }}</em>
          {{ composePlural(orphanCounts.modification, 'orphan modification') }})
        </span>

        <Divider class="col-span-3" />

        <span class="my-auto text-left italic text-slate-400">
          Repartition of the modifications by type:
        </span>
        <div class="relative col-span-2">
          <ToggleButton
            v-model="isColourBlindMode"
            on-label="Colour-blind mode"
            off-label="Colour-blind mode"
            class="!absolute right-0 top-0 z-10 gap-2"
          >
            <template #icon="{ value }">
              <icon-carbon-accessibility-color-filled
                v-if="value"
                class="text-2xl"
              />
              <icon-carbon-accessibility-color v-else class="text-2xl" />
            </template>
          </ToggleButton>
          <Chart
            type="doughnut"
            :data="modifications.typePieChartData"
            :options="DOUGHNUT_CHART_OPTIONS"
            class="mx-auto aspect-square w-full max-w-2xl"
          />
        </div>
      </div>
    </Panel>

    <Panel
      toggleable
      class="mx-auto mb-8 max-w-6xl text-center 2xl:max-w-7xl"
      :pt="{
        header: { class: '!bg-slate-50' },
        toggler: { class: 'hover:!bg-slate-100' }
      }"
    >
      <template #header="scope">
        <icon-snoboard-guide :class="scope.class" />
        <span :class="scope.class">Guides</span>
      </template>

      <div class="grid grid-cols-3 gap-x-4">
        <span class="my-auto text-left italic text-slate-400">
          Guides in the database:
        </span>
        <em
          v-if="guides.totalCount"
          class="col-span-2 my-auto text-left text-lg font-bold not-italic text-slate-700"
        >
          {{ guides.totalCount }}
        </em>

        <Divider class="col-span-3" />

        <span class="my-auto text-left italic text-slate-400">
          <span class="block">Guide coverage:</span>
          <small class="text-sm text-slate-400">
            % of guides which are associated with at least one modification
          </small>
        </span>
        <span class="col-span-2 my-auto text-left italic text-slate-700">
          <em class="text-lg font-bold not-italic">
            {{ coverages.guide }} %
          </em>
          (<em class="font-bold">{{ orphanCounts.guide }}</em>
          {{ composePlural(orphanCounts.guide, 'orphan guide') }})
        </span>

        <Divider class="col-span-3" />

        <span class="my-auto text-left italic text-slate-400">
          Repartition of the guides by type:
        </span>
        <div class="relative col-span-2">
          <ToggleButton
            v-model="isColourBlindMode"
            on-label="Colour-blind mode"
            off-label="Colour-blind mode"
            class="!absolute right-0 top-0 z-10 gap-2"
          >
            <template #icon="{ value }">
              <icon-carbon-accessibility-color-filled
                v-if="value"
                class="text-2xl"
              />
              <icon-carbon-accessibility-color v-else class="text-2xl" />
            </template>
          </ToggleButton>
          <Chart
            type="doughnut"
            :data="guides.subclassPieChartData"
            :options="DOUGHNUT_CHART_OPTIONS"
            class="mx-auto aspect-square w-full max-w-2xl"
          />
        </div>
      </div>
    </Panel>
  </MainLayout>
</template>
