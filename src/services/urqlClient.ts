import { Client, cacheExchange, fetchExchange } from '@urql/vue'

export default new Client({
  url: import.meta.env.VITE_API_URL,
  exchanges: [cacheExchange, fetchExchange]
})
