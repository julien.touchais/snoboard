export interface JGFNodeModel {
  label?: string
  metadata?: object
}

export interface JGFEdgeModel {
  id?: string
  source: string
  target: string
  relation?: string
  directed?: boolean
  label?: string
  metadata?: object
}

export interface JGFDirectedHyperedgeModel {
  id?: string
  source: string[]
  target: string[]
  relation?: string
  label?: string
  metadata?: object
}

export interface JGFUndirectedHyperedgeModel {
  id?: string
  nodes: string[]
  relation?: string
  label?: string
  metadata?: object
}

export interface JGFEdgeGraphModel {
  id?: string
  label?: string
  directed?: boolean
  type?: string
  metadata?: { [k: string]: unknown }
  nodes?: { [k: string]: JGFNodeModel }
  edges?: JGFEdgeModel[]
}

export interface JGFUndirectedHyperedgeGraphModel {
  id?: string
  label?: string
  directed: false
  type?: string
  metadata?: { [k: string]: unknown }
  nodes?: { [k: string]: JGFNodeModel }
  hyperedges?: JGFUndirectedHyperedgeModel[]
}

export interface JGFDirectedHyperedgeGraphModel {
  id?: string
  label?: string
  directed?: boolean
  type?: string
  metadata?: object
  nodes?: { [k: string]: JGFNodeModel }
  hyperedges?: JGFDirectedHyperedgeModel[]
}

export interface JGFGraphModel {
  graph:
    | JGFEdgeGraphModel
    | JGFUndirectedHyperedgeGraphModel
    | JGFDirectedHyperedgeGraphModel
}
