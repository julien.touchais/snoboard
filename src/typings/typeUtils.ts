export type HexDigitModel =
  | 0
  | 1
  | 2
  | 3
  | 4
  | 5
  | 6
  | 7
  | 8
  | 9
  | 'A'
  | 'B'
  | 'C'
  | 'D'
  | 'E'
  | 'F'
  | 'a'
  | 'b'
  | 'c'
  | 'd'
  | 'e'
  | 'f'

export type HexOctetModel = `${HexDigitModel}${HexDigitModel}`

export type DNANucleotideModel = 'A' | 'T' | 'C' | 'G'
export type RNANucleotideModel = 'A' | 'U' | 'C' | 'G'

export type DeepDefined<T> = T extends object
  ? Required<{
      [K in keyof T]: NonNullable<DeepDefined<T[K]>>
    }>
  : T

/**
 * Checks if a value is truthy (`!= false`), narrowing it.
 * @param value The value to check.
 */
// export const isTruthy = <T>(
//   value: T
// ): value is Exclude<T, false | 0 | '' | null | undefined> =>
//   !!value

/**
 * Checks if a value is not nullish (`!= null`), narrowing it.
 * @param value The value to check.
 */
export const isNonNullish = <T>(value: T): value is NonNullable<T> =>
  value != null

/**
 * Checks if a value is defined, narrowing it.
 * @param value The value to check.
 */
export const isDefined = <T>(value: T): value is Exclude<T, undefined> =>
  typeof value !== 'undefined'

/**
 * Checks if a value is a key of an object, narrowing its type to object
 * properties' one if yes.
 * @param value The value to check if it is a key of the object.
 * @param object The object in which to check properties.
 */
export const isIn = <T extends object>(
  value: string | number | symbol,
  object: T
): value is keyof T => value in object

/**
 * Checks if a value is present in a string enum, narrowing its type to enum's
 * one if yes.
 * @param value The value to check if it is present in enum.
 * @param enumType The enum to check into.
 */
export function isInEnum<T extends Record<string, string>>(
  value: string,
  enumType: T
): value is T[keyof T]
/**
 * Checks if a value is present in a numeric enum, narrowing its type to enum's
 * one if yes.
 * @param value The value to check if it is present in enum.
 * @param enumType The enum to check into.
 */
export function isInEnum<T extends Record<string, string | number>>(
  value: string | number,
  enumType: T
): value is T[keyof T]
export function isInEnum<T extends Record<string, string | number>>(
  value: string | number,
  enumType: T
): value is T[keyof T] {
  if (typeof value === 'string') {
    return Object.values(enumType).includes(value)
  } else {
    return Object.values(enumType)
      .filter((v) => typeof v === 'number')
      .includes(value)
  }
}

/**
 * Pick, from a `BaseType`, only the fields of a type extending `ConditionType`
 * one.
 */
export type SubType<BaseType, ConditionType> = Pick<
  BaseType,
  {
    [Key in keyof BaseType]: BaseType[Key] extends ConditionType ? Key : never
  }[keyof BaseType]
>

/**
 * Checks if an array contains at least one element, narrowing its type.
 * @param array The array for which to check the length.
 */
export function isNotEmpty<T>(array: T[]): array is [T, ...T[]] {
  return array.length >= 1
}
