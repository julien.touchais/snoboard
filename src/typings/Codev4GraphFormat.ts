/**
 * Types imports
 */
import type {
  JGFEdgeGraphModel,
  JGFEdgeModel,
  JGFGraphModel,
  JGFNodeModel
} from './JSONGraphFormat'

export interface C4GNodeModel extends JGFNodeModel {
  metadata?: {
    position?: {
      x: number
      y: number
    }
    groups?: string[]
    classes?: string[]
    type?: string
    degree?: number
    data?: {
      label: string
      type: string
      value: any
    }[]
    // _links?:{
    //   TBD
    // }
  }
}

export interface C4GEdgeModel extends JGFEdgeModel {
  metadata?: {
    classes?: string[]
    data?: {
      label: string
      type: string
      value: any
    }[]
    // _links?:{
    //   TBD
    // }
  }
}

export interface C4GEdgeGraphModel extends JGFEdgeGraphModel {
  metadata?: {
    groups?: {
      [k: string]: {
        title: string
        [k: string]: unknown
      }
    }
    classes?: {
      [k: string]: {
        title: string
        style: {
          [k: string]: unknown
        }
        [k: string]: unknown
      }
    }
    [k: string]: unknown
  }
  nodes?: { [k: string]: C4GNodeModel }
  edges?: C4GEdgeModel[]
}

export interface C4GGraphModel extends JGFGraphModel {
  graph: C4GEdgeGraphModel
}
