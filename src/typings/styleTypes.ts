/**
 * Types imports
 */
import type { SubType } from './typeUtils'
/**
 * Assets imports
 */
import tailwindDefaultColors from 'tailwindcss/colors'

/**
 * Available color names in Tailwind.
 */
export type TailwindDefaultColorNameModel = keyof typeof tailwindDefaultColors

/**
 * Available color names able to be shaded in Tailwind.
 */
export type TailwindDefaultShadableColorNameModel = keyof SubType<
  typeof tailwindDefaultColors,
  object
>

/**
 * Available color shades in Tailwind.
 */
export type TailwindDefaultColorShadeModel =
  keyof typeof tailwindDefaultColors.pink

// type HexColorCodeModel = `#${HexOctetModel}${HexOctetModel}${HexOctetModel}` // Too much for TS :')
export type HexColorCodeModel = `#${string}`
export interface DoubleHexColorModel {
  background: HexColorCodeModel
  foreground: HexColorCodeModel
}
export interface TripleHexColorModel {
  background: HexColorCodeModel
  foreground: HexColorCodeModel
  border: HexColorCodeModel
}

export type TailwindOrHexColorModel =
  | TailwindDefaultColorNameModel
  | HexColorCodeModel
export type TailwindOrTripleHexColorModel =
  | TailwindDefaultColorNameModel
  | TripleHexColorModel
