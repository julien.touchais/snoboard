<script setup lang="ts">
/**
 * Vue imports
 */
import { ref, watch } from 'vue'
/**
 * Component imports
 */
import BaseRenderedMarkdown from './BaseRenderedMarkdown.vue'
import Toolbar from 'primevue/toolbar'
import Dropdown from 'primevue/dropdown'
import TabView from 'primevue/tabview'
import TabPanel from 'primevue/tabpanel'
import Slider, { type SliderSlideEndEvent } from 'primevue/slider'
import MultiSelect from 'primevue/multiselect'
import SelectButton from 'primevue/selectbutton'
import ToggleButton from 'primevue/togglebutton'
import IconFa6RegularCircleQuestion from '~icons/fa6-regular/circle-question'

/**
 * Types imports.
 */
import type { HexColorCodeModel } from '@/typings/styleTypes'
import {
  ConservationAnalysisTypesEnum,
  type ConservationAnalysisConfigModel
} from '@/composables/useConservationAnalysis'

/**
 * Configuration for the bases conservation colouring.
 */
export interface BasesConservationConfigModel {
  /** Wether the bases conservation colouring is enabled. */
  isEnabled: boolean
  /** Conservation rate thresholds for bases conservation colouring. */
  thresholds: {
    low: number
    high: number
  }
}

/**
 * Available options for conservation analysis type.
 */
const CONSERVATION_ANALYSIS_TYPES_OPTIONS = [
  {
    label: 'Specific',
    analysisType: ConservationAnalysisTypesEnum.Specific
  },
  {
    label: 'Common',
    analysisType: ConservationAnalysisTypesEnum.Common
  }
]

/**
 * Component props.
 */
const props = withDefaults(
  defineProps<{
    /** The selected nucleotide group size. */
    nucleotideGroupsSizeModel: number
    /** The config for the bases conservation colouring. */
    basesConservationConfigModel: BasesConservationConfigModel
    /** Colours to use for bases conservation colouring */
    basesConservationColours?: {
      low: HexColorCodeModel
      middle: HexColorCodeModel
      high: HexColorCodeModel
    }
    /** The config for the conservation analysis. */
    conservationAnalysisConfigModel: ConservationAnalysisConfigModel
    /** Available options for track selection for conservation analysis. */
    conservationAnalysisTrackOptions: { label: string; id: string }[]
    /** Available options for selection of the types of the objects present on
     * the sequences for conservation analysis. */
    conservationAnalysisObjectTypeOptions: { label?: string; type: string }[]
  }>(),
  {
    basesConservationColours: () => ({
      low: '#F87171' as HexColorCodeModel,
      middle: '#FCD34D' as HexColorCodeModel,
      high: '#D9EDC1' as HexColorCodeModel
    })
  }
)

const emit = defineEmits<{
  /** Event used to update the `v-model:nucleotideGroupsSizeModel` value. */
  'update:nucleotideGroupsSizeModel': [nucleotideGroupsSizeModel: number]
  /** Event used to update the `v-model:basesConservationConfigModel` value. */
  'update:basesConservationConfigModel': [
    basesConservationConfigModel: BasesConservationConfigModel
  ]
  /** Event used to update the `v-model:conservationAnalysisConfigModel` value. */
  'update:conservationAnalysisConfigModel': [
    conservationAnalysisConfigModel: ConservationAnalysisConfigModel
  ]
}>()

/**
 * The currently active tab.
 */
const activeTabIndex = ref(0)

/**
 * Clears the conservation analysis type if no track or modification type is
 * selected anymore for conservation analysis.
 */
const clearAnalysisTypeIfNeeded = () => {
  if (
    props.conservationAnalysisConfigModel.objectTypes.length === 0 ||
    props.conservationAnalysisConfigModel.trackIds.length === 0
  ) {
    emit('update:conservationAnalysisConfigModel', {
      ...props.conservationAnalysisConfigModel,
      analysisType: null
    })
  }
}

/**
 * Callbacks to use for events in template.
 */
const eventHandlers = {
  /** Callbacks for values related to bases conservation colouring. */
  basesConservation: {
    /** Callback for the toggling of bases conservation colouring. */
    isEnabled: (newValue: boolean) =>
      emit('update:basesConservationConfigModel', {
        ...props.basesConservationConfigModel,
        isEnabled: newValue
      }),
    /** Callback for bases conservation colouring low threshold update by slider. */
    lowThreshold: (slideEndEvent: SliderSlideEndEvent) => {
      emit('update:basesConservationConfigModel', {
        ...props.basesConservationConfigModel,
        thresholds: {
          ...props.basesConservationConfigModel.thresholds,
          low: slideEndEvent.value
        }
      })
    },
    /** Callback for bases conservation colouring high threshold update by slider. */
    highThreshold: (slideEndEvent: SliderSlideEndEvent) => {
      emit('update:basesConservationConfigModel', {
        ...props.basesConservationConfigModel,
        thresholds: {
          ...props.basesConservationConfigModel.thresholds,
          high: slideEndEvent.value
        }
      })
    }
  },
  /** Callbacks for values related to conservation analysis. */
  conservationAnalysis: {
    /** Callback for track IDs selection update in Multiselect. */
    trackIds: (newValue: string[]) => {
      clearAnalysisTypeIfNeeded()
      emit('update:conservationAnalysisConfigModel', {
        ...props.conservationAnalysisConfigModel,
        trackIds: newValue
      })
    },
    /** Callback for object types selection update in Multiselect. */
    objectTypes: (newValue: string[]) => {
      clearAnalysisTypeIfNeeded()
      emit('update:conservationAnalysisConfigModel', {
        ...props.conservationAnalysisConfigModel,
        objectTypes: newValue
      })
    },
    /** Callback for object types selection update in Multiselect. */
    analysisType: (newValue: ConservationAnalysisTypesEnum) => {
      emit('update:conservationAnalysisConfigModel', {
        ...props.conservationAnalysisConfigModel,
        analysisType: newValue
      })
    }
  }
}

/**
 * Local values of the thresholds for bases conservation colouring, to use as
 * `v-model` for the sliders.
 *
 * @description We use this instead of
 * {@link props.basesConservationConfigModel.thresholds} directly because
 * sliders update their value immediately, but we only wan't to update the
 * actual thresholds when mouse is released (on `slideend` event).
 * Thus, those intermediary values allow displaying the new threshold in real
 * time, but only updating the actual value when the mouse is released.
 */
const localBasesConservationThresholds = ref({
  ...props.basesConservationConfigModel.thresholds
})

/**
 * Watcher to update the local value of the thresholds for bases conservation
 * colouring when actual thresholds are updated using `v-model`.
 */
watch(
  () => props.basesConservationConfigModel.thresholds,
  () =>
    (localBasesConservationThresholds.value = {
      ...props.basesConservationConfigModel.thresholds
    })
)
</script>

<template>
  <Toolbar
    :pt="{
      start: {
        style: {
          gap: '2rem',
          alignItems: 'stretch'
        }
      }
    }"
    class="mx-auto w-5/6"
  >
    <template #start>
      <label class="flex flex-col gap-2 text-sm">
        Group length
        <div class="flex grow flex-col justify-center">
          <Dropdown
            :model-value="nucleotideGroupsSizeModel"
            :options="[5, 10, 20]"
            class="w-52"
            @update:model-value="
              (newValue: number) =>
                $emit('update:nucleotideGroupsSizeModel', newValue)
            "
          >
            <template #option="{ option }">{{ option }} nucleotides</template>
            <template #value="{ value }">{{ value }} nucleotides</template>
          </Dropdown>
        </div>
      </label>

      <!-- <div class="self-stretch rounded border border-slate-200" /> -->
    </template>

    <template #center>
      <TabView
        v-model:active-index="activeTabIndex"
        :pt="{
          nav: {
            style: {
              background: 'none',
              justifyContent: 'center'
            }
          },
          panelContainer: {
            style: {
              background: 'none'
            }
          }
        }"
      >
        <TabPanel
          header="Bases conservation"
          :pt="{
            headerAction: {
              style: {
                background: 'none'
              }
            }
          }"
        >
          <div class="flex flex-wrap gap-8">
            <div class="flex flex-col gap-2">
              <label
                for="sequence-conservation-colouring-switch"
                class="text-sm"
              >
                Conservation colouring
              </label>
              <div class="flex grow flex-col justify-center">
                <ToggleButton
                  :model-value="basesConservationConfigModel.isEnabled"
                  on-label="On"
                  off-label="Off"
                  class="w-full"
                  input-id="sequence-conservation-colouring-switch"
                  @update:model-value="
                    eventHandlers.basesConservation.isEnabled
                  "
                />
              </div>
            </div>

            <label class="flex flex-col gap-2 text-sm">
              Conservation thresholds
              <div class="flex grow flex-col justify-center">
                <div
                  v-tooltip.bottom="
                    !basesConservationConfigModel.isEnabled && {
                      value:
                        'To set the thresholds, first activate the colouring.',
                      autoHide: false,
                      pt: { text: 'text-xs text-center' }
                    }
                  "
                  class="flex gap-4 rounded-md border border-zinc-300 bg-white p-2 shadow-sm"
                >
                  <div
                    :class="{
                      'text-slate-400': !basesConservationConfigModel.isEnabled
                    }"
                  >
                    <label
                      id="low-conservation-threshold-label"
                      class="font-thin italic"
                    >
                      Little conserved
                      <span
                        v-tooltip.bottom="
                          basesConservationConfigModel.isEnabled && {
                            value:
                              'The conservation rate under which to color the nucleotides with the \'little conserved\' color.',
                            autoHide: false,
                            pt: { text: 'text-xs text-center' }
                          }
                        "
                      >
                        <icon-fa6-regular-circle-question
                          class="mb-0.5 ml-1 inline"
                        />
                      </span>
                    </label>
                    <Slider
                      v-model="localBasesConservationThresholds.low"
                      :pt="{
                        root: {
                          style: {
                            backgroundColor: basesConservationColours.middle
                          }
                        },
                        range: {
                          style: {
                            backgroundColor: basesConservationColours.low
                          }
                        }
                      }"
                      :disabled="!basesConservationConfigModel.isEnabled"
                      class="my-2 w-full"
                      :step="0.05"
                      :min="0"
                      :max="0.5"
                      aria-labelledby="low-conservation-threshold-label"
                      @slideend="eventHandlers.basesConservation.lowThreshold"
                    />
                    <div class="w-full text-center font-mono">
                      {{
                        Math.round(localBasesConservationThresholds.low * 100)
                      }}%
                    </div>
                  </div>
                  <div
                    :class="{
                      'text-slate-400': !basesConservationConfigModel.isEnabled
                    }"
                  >
                    <label
                      id="high-conservation-threshold-label"
                      class="font-thin italic"
                    >
                      Highly conserved
                      <span
                        v-tooltip.bottom="
                          basesConservationConfigModel.isEnabled && {
                            value:
                              'The conservation rate above which to color the nucleotides with the \'highly conserved\' color.',
                            autoHide: false,
                            pt: { text: 'text-xs text-center' }
                          }
                        "
                      >
                        <icon-fa6-regular-circle-question
                          class="mb-0.5 ml-1 inline"
                        />
                      </span>
                    </label>
                    <Slider
                      v-model="localBasesConservationThresholds.high"
                      :pt="{
                        root: {
                          style: {
                            backgroundColor: basesConservationColours.high
                          }
                        },
                        range: {
                          style: {
                            backgroundColor: basesConservationColours.middle
                          }
                        }
                      }"
                      :disabled="!basesConservationConfigModel.isEnabled"
                      class="my-2 w-full"
                      :step="0.05"
                      :min="0.5"
                      :max="1"
                      aria-labelledby="high-conservation-threshold-label"
                      @slideend="eventHandlers.basesConservation.highThreshold"
                    />
                    <div class="w-full text-center font-mono">
                      {{
                        Math.round(localBasesConservationThresholds.high * 100)
                      }}%
                    </div>
                  </div>
                </div>
              </div>
            </label>
          </div>
        </TabPanel>

        <TabPanel
          header="Modifications conservation"
          :pt="{
            headerAction: {
              style: {
                background: 'none'
              }
            }
          }"
        >
          <div class="flex flex-wrap gap-8">
            <div class="flex flex-col gap-2">
              <label for="track-selection" class="text-sm"> Tracks </label>
              <div class="flex grow flex-col justify-center">
                <MultiSelect
                  :model-value="conservationAnalysisConfigModel.trackIds"
                  :options="conservationAnalysisTrackOptions"
                  option-label="label"
                  option-value="id"
                  multiple
                  placeholder="Select tracks..."
                  input-id="track-selection"
                  @update:model-value="
                    eventHandlers.conservationAnalysis.trackIds
                  "
                />
              </div>
            </div>
            <div class="flex flex-col gap-2">
              <label for="object-types-selection" class="text-sm">
                Modification type
              </label>
              <div class="flex grow flex-col justify-center">
                <MultiSelect
                  :model-value="conservationAnalysisConfigModel.objectTypes"
                  :options="conservationAnalysisObjectTypeOptions"
                  option-label="label"
                  option-value="type"
                  multiple
                  placeholder="Select object types..."
                  input-id="object-types-selection"
                  @update:model-value="
                    eventHandlers.conservationAnalysis.objectTypes
                  "
                >
                  <template #option="{ option }">
                    <BaseRenderedMarkdown
                      :stringified-markdown="option.label"
                      inline-content
                    />
                  </template>
                  <template #value="{ value }">
                    <BaseRenderedMarkdown
                      v-if="value && value.length"
                      :stringified-markdown="value.join(', ')"
                      inline-content
                    />
                  </template>
                </MultiSelect>
              </div>
            </div>

            <div class="flex flex-col gap-2">
              <label for="analysis-type-selection" class="text-sm">
                Analysis type
                <span
                  v-tooltip.bottom="{
                    value: `• Specific: highlights positions modified on the selected sequences only (all of them).

                    • Common: highlights positions modified on the selected sequences at least (all of them).`,
                    autoHide: false,
                    pt: { text: 'text-xs w-[40ch]' }
                  }"
                >
                  <icon-fa6-regular-circle-question
                    class="mb-0.5 ml-1 inline"
                  />
                </span>
              </label>
              <div
                v-tooltip.bottom="
                  !(
                    conservationAnalysisConfigModel.objectTypes.length &&
                    props.conservationAnalysisConfigModel.objectTypes.length
                  ) && {
                    value:
                      'First select some sequences and modification types to chose a conservation analysis type.',
                    autoHide: false,
                    pt: { text: 'text-xs' }
                  }
                "
                class="flex grow flex-col justify-center"
              >
                <SelectButton
                  :model-value="conservationAnalysisConfigModel.analysisType"
                  :options="CONSERVATION_ANALYSIS_TYPES_OPTIONS"
                  option-label="label"
                  option-value="analysisType"
                  :disabled="
                    !(
                      conservationAnalysisConfigModel.objectTypes.length &&
                      props.conservationAnalysisConfigModel.objectTypes.length
                    )
                  "
                  input-id="analysis-type-selection"
                  @update:model-value="
                    eventHandlers.conservationAnalysis.analysisType
                  "
                />
              </div>
            </div>
          </div>
        </TabPanel>
      </TabView>
    </template>

    <!-- <template v-if="legendItems" #center>
      <BaseLegendButtonOverlay button-text="Legend" :items="legendItems">
        <template #item="{ item }">
          <slot name="legend-item" :item="item" />
        </template>
        <template #item-icon="{ item }">
          <slot name="legend-item-icon" :item="item" />
        </template>
        <template #item-title="{ item }">
          <slot name="legend-item-title" :item="item" />
        </template>
        <template #item-description="{ item }">
          <slot name="legend-item-description" :item="item" />
        </template>
      </BaseLegendButtonOverlay>
    </template> -->

    <!-- <template #end>
      <Button
        outlined
        severity="secondary"
        :class="[
          clipboardState === ClipboardStateModel.PostCopy &&
            '!bg-lime-100 !text-lime-700',
          'flex min-w-[18ch] justify-center'
        ]"
        :loading="clipboardState === ClipboardStateModel.Busy"
        @click="sequenceToClipboard"
      >
        <span
          v-if="clipboardState === ClipboardStateModel.Busy"
          class="mr-2 h-5 overflow-hidden text-xl"
        >
          <icon-fa6-solid-dna class="animate-[dnaSpin_.5s_linear_infinite]" />
          <icon-fa6-solid-dna class="animate-[dnaSpin_.5s_linear_infinite]" />
        </span>
        <icon-fa6-solid-circle-check
          v-else-if="clipboardState === ClipboardStateModel.PostCopy"
          class="mr-2 text-xl"
        />
        <icon-fa6-regular-copy v-else class="mr-2 text-xl" />
        {{
          clipboardState === ClipboardStateModel.Busy
            ? 'Copying...'
            : clipboardState === ClipboardStateModel.PostCopy
            ? 'Copied !'
            : 'Copy sequence'
        }}
      </Button>
    </template> -->
  </Toolbar>
</template>
