<script setup lang="ts">
/**
 * Vue imports
 */
import { computed, ref, toRef } from 'vue'
/**
 * Components imports
 */
import BaseRenderedMarkdown from '@/components/BaseRenderedMarkdown.vue'
import SelectButton from 'primevue/selectbutton'
import Chip from 'primevue/chip'
import MultiSelect from 'primevue/multiselect'
import Dropdown from 'primevue/dropdown'
/**
 * Composables imports
 */
import { useQuery } from '@urql/vue'
/**
 * Other 3rd-party imports
 */
import {
  uniqWith as _uniqWith,
  isEqual as _isEqual,
  groupBy as _groupBy,
  mapValues as _mapValues
} from 'lodash-es'
/**
 * Types imports
 */
import type { GuideClass } from '@/gql/codegen/graphql'
/**
 * Utils imports
 */
import { guideAlignmentQuery } from '@/gql/queries'

interface GuideAlignmentSelectionModel {
  /** Currently selected guide subclasses. */
  guideSubclasses: GuideClass[]
  /** Currently selected guide name. */
  guideName: string | undefined
  /** Currently selected organism IDs.*/
  organismIds: number[]
  /** Currently selected guide IDs for alignment. */
  guideIds: string[]
}

/**
 * Component props
 */
defineProps<{
  /** The currently selected guide IDs for alignment. */
  selectedGuideIdsModel?: string[]
}>()

/**
 * Component events.
 */
const emit = defineEmits<{
  /** Event used to update the `v-model:selectedGuideIdsModel` value. */
  'update:selectedGuideIdsModel': [selectedGuideIdsModel?: string[]]
}>()

/**
 * Current selection of parameters/filters for the guides to pick.
 */
const selection = ref<GuideAlignmentSelectionModel>({
  guideSubclasses: [],
  guideName: undefined,
  organismIds: [],
  guideIds: []
})

/**
 * Reactive urql GraphQL query object, updated with query state & response.
 */
const gqlQuery = useQuery({
  query: guideAlignmentQuery,
  variables: toRef(() => ({
    guideSubclasses: selection.value.guideSubclasses.length
      ? selection.value.guideSubclasses
      : undefined,
    guideName: selection.value.guideName || '',
    organismIds: selection.value.organismIds.length
      ? selection.value.organismIds
      : undefined
  }))
})

/**
 * Available options for guide subclass selection, independently of other fields'
 * selection.
 */
const guideSubclassOptions = computed(() =>
  _uniqWith(
    gqlQuery.data.value?.guideBase,
    (guideA, guideB) => guideA.subclass === guideB.subclass
  ).map((guide) => ({
    label: guide.subclass_label,
    value: guide.subclass
  }))
)

/**
 * Available options for guide name selection, independently of other fields'
 * selection.
 */
const guideNameOptionsBase = computed(() =>
  _uniqWith(
    gqlQuery.data.value?.guideBase,
    (guideA, guideB) => guideA.name === guideB.name
  ).map((guide) => ({
    label: guide.name,
    value: guide.name
  }))
)

/**
 * Available guide names, filtered based on other fields' selection (only guide
 * names which exists with selected options are present).
 */
const guideNamesFiltered = computed(() =>
  _uniqWith(
    gqlQuery.data.value?.guideNamesFilteredBySubclass,
    (guideA, guideB) => guideA.name === guideB.name
  ).map((guide) => guide.name)
)

/**
 * Available options for guide name selection, with a `isDisabled` field on
 * absence in `guideNamesFiltered`.
 */
const guideNameOptionsWithDisabling = computed(() =>
  guideNameOptionsBase.value.map((guideNameOption) => ({
    ...guideNameOption,
    isDisabled: !guideNamesFiltered.value.some(
      (guideNameFiltered) => guideNameFiltered === guideNameOption.value
    )
  }))
)

/**
 * Available options for organism ID selection, independently of other fields'
 * selection.
 */
const organismIdOptionsBase = computed(() =>
  _uniqWith(gqlQuery.data.value?.organismsBase, _isEqual).map((organism) => ({
    label: organism.label,
    value: organism.id
  }))
)

/**
 * For each guide name, an array of the IDs of the organisms which have at
 * least a guide of this name registered.
 */
const organismIdsByGuideNameBase = computed(() =>
  _mapValues(
    _groupBy(_uniqWith(gqlQuery.data.value?.guideBase, _isEqual), 'name'),
    (guides) => guides.map((guide) => guide.genome?.organism?.id)
  )
)

/**
 * Available organism IDs, filtered based on other fields' selection (only
 * organism IDs which exists with selected options are present).
 */
const organismIdsFiltered = computed(
  () =>
    (selection.value.guideName &&
      organismIdsByGuideNameBase.value[selection.value.guideName]) ||
    []
)

/**
 * Available options for organism ID selection, with a `isDisabled` field on
 * absence in `organismIdsFiltered`.
 */
const organismIdOptionsWithDisabling = computed(() =>
  organismIdOptionsBase.value.map((organismIdOption) => ({
    ...organismIdOption,
    isDisabled: !organismIdsFiltered.value.some(
      (organismIdFiltered) => organismIdFiltered === organismIdOption.value
    )
  }))
)

/**
 * Available options for guide ID selection for alignment given the other
 * fields selection.
 */
const guideIdOptions = computed(() =>
  gqlQuery.data.value?.selectableGuides.map((guide) => ({
    label: guide.id,
    optionLabel: `\`${guide.id}\` • ${guide.genome?.organism?.shortlabel}`,
    value: guide.id
  }))
)

/**
 * Clear the selected guide IDs.
 */
const clearGuideIds = () => {
  emit('update:selectedGuideIdsModel', (selection.value.guideIds = []))
}

/**
 * Remove an organism ID from the selected ones & clear the selected guide IDs.
 */
const removeOrganismAndClearGuideIds = (organismId: number) => {
  const organismIdIndex = selection.value.organismIds.findIndex(
    (currOrganismId) => currOrganismId === organismId
  )
  organismIdIndex !== -1 &&
    selection.value.organismIds.splice(organismIdIndex, 1)
  clearGuideIds()
}

/**
 * Clear the selected organisms & guide IDs.
 */
const clearOrganismsAndGuideIds = () => {
  selection.value.organismIds = selection.value.organismIds.filter(
    (organismId) =>
      selection.value.guideName &&
      organismIdsByGuideNameBase.value[selection.value.guideName]?.includes(
        organismId
      )
  )
  clearGuideIds()
}

/**
 * Clear the selection except the selected guide subclass.
 */
const clearSelectionExceptSubclass = () => {
  selection.value.guideName = undefined
  clearOrganismsAndGuideIds()
}
</script>

<template>
  <div class="flex max-w-max flex-wrap gap-8">
    <div class="flex flex-col gap-2">
      <h3 class="text-lg font-bold text-slate-700">Guide subclasses</h3>
      <SelectButton
        v-model="selection.guideSubclasses"
        :options="guideSubclassOptions"
        option-label="label"
        option-value="value"
        option-disabled="isDisabled"
        multiple
        @change="clearSelectionExceptSubclass"
      >
        <template #option="{ option }">
          <BaseRenderedMarkdown
            :stringified-markdown="option.label"
            class="font-medium"
          />
        </template>
      </SelectButton>
    </div>

    <div class="flex max-w-min flex-col gap-2">
      <h3 class="text-lg font-bold text-slate-700">Guides</h3>
      <Dropdown
        v-model="selection.guideName"
        :options="guideNameOptionsWithDisabling"
        option-label="label"
        option-value="value"
        option-disabled="isDisabled"
        placeholder="Select a guide..."
        filter
        @change="clearOrganismsAndGuideIds"
      />
    </div>

    <div
      v-tooltip.bottom="
        !selection.guideName && {
          value: 'First select a guide to list its organisms.',
          autoHide: false,
          pt: { text: { style: { textAlign: 'center' } } }
        }
      "
      class="flex max-w-min flex-col gap-2"
    >
      <h3 class="text-lg font-bold text-slate-700">Organisms</h3>
      <MultiSelect
        v-model="selection.organismIds"
        :options="organismIdOptionsWithDisabling"
        option-label="label"
        option-value="value"
        option-disabled="isDisabled"
        multiple
        placeholder="Select organisms..."
        :max-selected-labels="3"
        display="chip"
        filter
        :disabled="!selection.guideName"
        @change="clearGuideIds"
      >
        <template #value>
          <Chip
            v-for="organismId in selection.organismIds.slice(0, 3)"
            :key="organismId"
            class="mr-1"
            removable
            @remove.stop="removeOrganismAndClearGuideIds(organismId)"
          >
            <BaseRenderedMarkdown
              :stringified-markdown="
                organismIdOptionsBase.find(
                  (organismIdOption) => organismIdOption.value === organismId
                )?.label || ''
              "
              class="my-1.5"
            />
          </Chip>
          <template v-if="selection.organismIds.length > 3">
            +{{ selection.organismIds.length - 3 }} others...
          </template>
        </template>
        <template #option="{ option }">
          <BaseRenderedMarkdown :stringified-markdown="option.label" />
        </template>
      </MultiSelect>
    </div>

    <div
      v-tooltip.bottom="
        !selection.guideName && {
          value: 'First select a guide to list its sequences.',
          autoHide: false,
          pt: { text: { style: { textAlign: 'center' } } }
        }
      "
      class="flex max-w-min flex-col gap-2"
    >
      <h3 class="text-lg font-bold text-slate-700">Sequences</h3>
      <MultiSelect
        v-model="selection.guideIds"
        :options="guideIdOptions"
        option-label="label"
        option-value="value"
        multiple
        placeholder="Select sequences..."
        :max-selected-labels="3"
        filter
        :disabled="!selection.guideName"
        empty-message="First select a guide to list its sequences"
        @update:model-value="
          (selectedGuideIdsModel: string[]) =>
            $emit('update:selectedGuideIdsModel', selectedGuideIdsModel)
        "
      >
        <template #option="{ option }">
          <BaseRenderedMarkdown :stringified-markdown="option.optionLabel" />
        </template>
        <template #value="{ value }">
          <span v-if="value.length" class="font-mono">
            {{ value.join(', ') }}
          </span>
        </template>
      </MultiSelect>
    </div>
  </div>
</template>
