<script setup lang="ts">
/**
 * Vue imports
 */
import { ref, computed } from 'vue'
/**
 * Other 3rd-party imports
 */
import { isEqual as _isEqual, findLastIndex as _findLastIndex } from 'lodash-es'
/**
 * Types imports
 */
import { ModifType } from '@/gql/codegen/graphql'
/**
 * Utils imports
 */
import { getModificationColor } from '@/utils/colors'

export interface InteractionCDModel {
  /** Sequences of the strand fragments to display */
  fragments: {
    /** Guide fragment description */
    guide: {
      /** Fragment sequence, 5'-to-3' */
      seq: string
      /** Position of the fragment on the whole guide sequence */
      onParentPosition?: {
        start?: number
        end?: number
      }
    }
    /** Target fragment description */
    target: {
      /** Fragment sequence, 5'-to-3' */
      seq: string
      /** Position of the fragment on the whole target sequence. End position is
       * mandatory because used to compute the modification position in the
       * duplex reference
       */
      onParentPosition: {
        start?: number
        end: number
      }
    }
  }
  /** Representation of the duplex between the two strands */
  duplex: {
    /** Guide strand description */
    guide: {
      /** Start position of the actual binding on the fragment */
      start: number
      /** End position of the actual binding on the fragment */
      end: number
    }
    /** Target strand description */
    target: {
      /** Start position of the actual binding on the fragment */
      start: number
      /** End position of the actual binding on the fragment */
      end: number
    }
  }
  /** Informations on the guide */
  guide: {
    /** ID of the guide */
    id: string
    /** Name of the guide */
    name?: string
  }
  /** Informations on the target */
  target: {
    /** ID of the target */
    id: string
    /** Name of the target */
    name?: string
  }
  /** Informations on the modification */
  modification: {
    /** Position of the modification on the target */
    position: number
    /** Symbol corresponding to the result of nucleotide modification */
    symbol: string
  }
}

const props = defineProps<{
  /** The interaction to represent */
  interaction: InteractionCDModel
}>()

/**
 * Positions of the fragments on their parent, `undefined` if any of them is
 * `undefined`
 */
const fragmentsParentPositions = computed(() => {
  // Ensure every position is defined, otherwise return `undefined`
  return props.interaction.fragments.guide.onParentPosition?.start &&
    props.interaction.fragments.guide.onParentPosition?.end &&
    props.interaction.fragments.target.onParentPosition?.start
    ? {
        guide: {
          start: props.interaction.fragments.guide.onParentPosition.start,
          end: props.interaction.fragments.guide.onParentPosition.end
        },
        target: {
          start: props.interaction.fragments.target.onParentPosition.start,
          end: props.interaction.fragments.target.onParentPosition.end
        }
      }
    : undefined
})

/**
 * Target fragment, flipped to match the guide fragment
 */
const flippedTargetFragment = computed(() =>
  props.interaction.fragments.target.seq
    .split('')
    .reduceRight<string[]>(
      (reversedArray, element) => [...reversedArray, element],
      []
    )
    .join('')
)

/**
 * Lengths of the padding to insert at the start & end of the fragments
 */
const paddingLengths = computed(() => ({
  guide: {
    start: Math.max(
      flippedTargetFragment.value.length -
        props.interaction.duplex.target.end -
        (props.interaction.duplex.guide.start - 1),
      0
    ),
    end: Math.max(
      props.interaction.duplex.target.start -
        1 -
        (props.interaction.fragments.guide.seq.length -
          props.interaction.duplex.guide.end),
      0
    )
  },
  target: {
    start: Math.max(
      props.interaction.duplex.guide.start -
        1 -
        (flippedTargetFragment.value.length -
          props.interaction.duplex.target.end),
      0
    ),
    end: Math.max(
      props.interaction.fragments.guide.seq.length -
        props.interaction.duplex.guide.end -
        (props.interaction.duplex.target.start - 1),
      0
    )
  }
}))

/**
 * Guide & target fragments, padded with spaces (or '*' for the target)
 * to be of the same length
 */
const paddedFragments = computed(() => ({
  guide:
    ' '.repeat(paddingLengths.value.guide.start) +
    props.interaction.fragments.guide.seq.trim() +
    ' '.repeat(paddingLengths.value.guide.end),
  target:
    ' '.repeat(paddingLengths.value.target.start) +
    flippedTargetFragment.value.trim() +
    '*'.repeat(Math.min(4, paddingLengths.value.target.end)) +
    ' '.repeat(Math.max(paddingLengths.value.target.end - 4, 0))
}))

/**
 * Position of the modification in the duplex reference
 */
const modificationPositionInDuplex = computed(
  () =>
    Math.max(
      props.interaction.duplex.guide.start -
        1 -
        (flippedTargetFragment.value.length -
          props.interaction.duplex.target.end),
      0
    ) +
    (props.interaction.fragments.target.onParentPosition.end -
      props.interaction.modification.position)
)

/**
 * Position of the first real bond (G-C or A-U) in the duplex reference
 */
const firstBondPositionInDuplex = computed(() =>
  paddedFragments.value.guide
    .split('')
    .findIndex((nucleotide: string, index: number) =>
      /GC|CG|AU|UA/.test(nucleotide + paddedFragments.value.target[index])
    )
)

/**
 * Position of the last real bond (G-C or A-U) in the duplex reference
 */
const lastBondPositionInDuplex = computed(() =>
  _findLastIndex(
    paddedFragments.value.guide.split(''),
    (nucleotide: string, index: number) =>
      /GC|CG|AU|UA/.test(nucleotide + paddedFragments.value.target[index])
  )
)

/**
 * Text representing the bonds w/ their nature
 */
const bondsText = computed(() =>
  paddedFragments.value.guide
    .split('')
    .map((nucleotide, nucleotidePositionInDuplex) => {
      const basePair =
        nucleotide + paddedFragments.value.target[nucleotidePositionInDuplex] ||
        ''
      return nucleotidePositionInDuplex === modificationPositionInDuplex.value
        ? ' '
        : /GC|CG|AU|UA/.test(basePair)
          ? '|'
          : /GU|UG/.test(basePair) &&
              nucleotidePositionInDuplex > firstBondPositionInDuplex.value &&
              nucleotidePositionInDuplex < lastBondPositionInDuplex.value
            ? '∙'
            : /GA|AG/.test(basePair) &&
                nucleotidePositionInDuplex > firstBondPositionInDuplex.value &&
                nucleotidePositionInDuplex < lastBondPositionInDuplex.value
              ? '○'
              : ' '
    })
    .join('')
)

/**
 * SVG Element corresponding to the modification letter
 */
const modificationElement = ref()

/**
 * Size of the bounding box of the nucleotide letters
 */
const nucleotideLetterSize = computed(() =>
  modificationElement.value
    ? {
        width: Math.abs(modificationElement.value[0].getBBox().width),
        height: Math.abs(modificationElement.value[0].getBBox().height)
      }
    : {
        width: 0,
        height: 0
      }
)

/**
 * Position of the modification letter's SVG element in the SVG
 */
const modificationPositionRelToSVG = ref({ x: 0, y: 0 })

/**
 * Base unit for positioning SVG elements, = 1.25 * font size
 */
const interactionSVGBaseUnit = ref(0)

/**
 * Callback triggered when updating the SVG
 * @param interactionElement SVG DOM Element
 */
const interactionSVGUpdate = (interactionElement: any) => {
  const interactionSVGBaseUnitNew = interactionElement
    ? parseFloat(getComputedStyle(interactionElement).fontSize) * 1.25
    : 0
  const modificationPositionRelToSVGNew = modificationElement.value &&
    interactionElement && {
      x: modificationElement.value[0].getBBox().x,
      y: modificationElement.value[0].getBBox().y
    }

  if (
    interactionSVGBaseUnitNew &&
    interactionSVGBaseUnitNew !== interactionSVGBaseUnit.value
  ) {
    interactionSVGBaseUnit.value = interactionSVGBaseUnitNew
  }

  if (
    modificationPositionRelToSVGNew &&
    !_isEqual(
      modificationPositionRelToSVGNew,
      modificationPositionRelToSVG.value
    )
  ) {
    modificationPositionRelToSVG.value = modificationPositionRelToSVGNew
  }
}

/**
 * Lengths of the duplexe
 */
const fragmentLength = computed(() => ({
  duplex: paddedFragments.value.guide.length * interactionSVGBaseUnit.value
}))

/**
 * Description of the texts (nucleotides & bonds) of the duplexes.
 */
const textsDesc = computed(() => ({
  duplex: {
    seq1: {
      x: 2 * interactionSVGBaseUnit.value,
      y: 3.75 * interactionSVGBaseUnit.value,
      length: fragmentLength.value.duplex
    },
    bonds: {
      x: 2 * interactionSVGBaseUnit.value,
      y: 4.75 * interactionSVGBaseUnit.value,
      length: fragmentLength.value.duplex
    },
    seq2: {
      x: 2 * interactionSVGBaseUnit.value,
      y: 5.75 * interactionSVGBaseUnit.value,
      length: fragmentLength.value.duplex
    }
  }
}))

/**
 * Positions of the labels
 */
const labelsDesc = computed(() => ({
  guide: {
    name: {
      x: 2 * interactionSVGBaseUnit.value + fragmentLength.value.duplex / 2,
      y: 0
    },
    fivePrime: {
      x: 0,
      y: 3.75 * interactionSVGBaseUnit.value
    },
    threePrime: {
      x:
        2 * interactionSVGBaseUnit.value +
        fragmentLength.value.duplex +
        0.5 * interactionSVGBaseUnit.value,
      y: 3.75 * interactionSVGBaseUnit.value
    },
    startPosition: fragmentsParentPositions.value && {
      rect: {
        x:
          2 * interactionSVGBaseUnit.value +
          nucleotideLetterSize.value.width / 2 +
          paddingLengths.value.guide.start *
            (interactionSVGBaseUnit.value +
              (interactionSVGBaseUnit.value -
                nucleotideLetterSize.value.width) /
                paddedFragments.value.guide.length),
        y: 2.25 * interactionSVGBaseUnit.value,
        width:
          fragmentsParentPositions.value.guide.start.toString().length *
          nucleotideLetterSize.value.width,
        height: interactionSVGBaseUnit.value * 0.75,
        radius: interactionSVGBaseUnit.value / 5
      },
      text: {
        x:
          2 * interactionSVGBaseUnit.value +
          nucleotideLetterSize.value.width / 2 +
          paddingLengths.value.guide.start *
            (interactionSVGBaseUnit.value +
              (interactionSVGBaseUnit.value -
                nucleotideLetterSize.value.width) /
                paddedFragments.value.guide.length),
        y: 2.5 * interactionSVGBaseUnit.value
      },
      line: {
        x1:
          2 * interactionSVGBaseUnit.value +
          nucleotideLetterSize.value.width / 2 +
          paddingLengths.value.guide.start *
            (interactionSVGBaseUnit.value +
              (interactionSVGBaseUnit.value -
                nucleotideLetterSize.value.width) /
                paddedFragments.value.guide.length),
        y1: 3 * interactionSVGBaseUnit.value,
        x2:
          2 * interactionSVGBaseUnit.value +
          nucleotideLetterSize.value.width / 2 +
          paddingLengths.value.guide.start *
            (interactionSVGBaseUnit.value +
              (interactionSVGBaseUnit.value -
                nucleotideLetterSize.value.width) /
                paddedFragments.value.guide.length),
        y2: 3.25 * interactionSVGBaseUnit.value
      }
    },
    endPosition: fragmentsParentPositions.value && {
      rect: {
        x:
          2 * interactionSVGBaseUnit.value +
          fragmentLength.value.duplex -
          nucleotideLetterSize.value.width / 2 -
          paddingLengths.value.guide.end *
            (interactionSVGBaseUnit.value +
              (interactionSVGBaseUnit.value -
                nucleotideLetterSize.value.width) /
                paddedFragments.value.guide.length),
        y: 2.25 * interactionSVGBaseUnit.value,
        width:
          fragmentsParentPositions.value.guide.end.toString().length *
          nucleotideLetterSize.value.width,
        height: interactionSVGBaseUnit.value * 0.75,
        radius: interactionSVGBaseUnit.value / 5
      },
      text: {
        x:
          2 * interactionSVGBaseUnit.value +
          fragmentLength.value.duplex -
          nucleotideLetterSize.value.width / 2 -
          paddingLengths.value.guide.end *
            (interactionSVGBaseUnit.value +
              (interactionSVGBaseUnit.value -
                nucleotideLetterSize.value.width) /
                paddedFragments.value.guide.length),
        y: 2.5 * interactionSVGBaseUnit.value
      },
      line: {
        x1:
          2 * interactionSVGBaseUnit.value +
          fragmentLength.value.duplex -
          nucleotideLetterSize.value.width / 2 -
          paddingLengths.value.guide.end *
            (interactionSVGBaseUnit.value +
              (interactionSVGBaseUnit.value -
                nucleotideLetterSize.value.width) /
                paddedFragments.value.guide.length),
        y1: 3 * interactionSVGBaseUnit.value,
        x2:
          2 * interactionSVGBaseUnit.value +
          fragmentLength.value.duplex -
          nucleotideLetterSize.value.width / 2 -
          paddingLengths.value.guide.end *
            (interactionSVGBaseUnit.value +
              (interactionSVGBaseUnit.value -
                nucleotideLetterSize.value.width) /
                paddedFragments.value.guide.length),
        y2: 3.25 * interactionSVGBaseUnit.value
      }
    }
  },
  target: {
    name: {
      x: 2 * interactionSVGBaseUnit.value + fragmentLength.value.duplex / 2,
      y: 8.5 * interactionSVGBaseUnit.value
    },
    threePrime: {
      x: 0,
      y: 5.75 * interactionSVGBaseUnit.value
    },
    fivePrime: {
      x:
        2 * interactionSVGBaseUnit.value +
        fragmentLength.value.duplex +
        0.5 * interactionSVGBaseUnit.value,
      y: 5.75 * interactionSVGBaseUnit.value
    },
    startPosition: fragmentsParentPositions.value && {
      rect: {
        x:
          2 * interactionSVGBaseUnit.value +
          fragmentLength.value.duplex -
          nucleotideLetterSize.value.width / 2 -
          paddingLengths.value.target.end *
            (interactionSVGBaseUnit.value +
              (interactionSVGBaseUnit.value -
                nucleotideLetterSize.value.width) /
                paddedFragments.value.target.length),
        y: 6.75 * interactionSVGBaseUnit.value,
        width:
          fragmentsParentPositions.value.target.start.toString().length *
          nucleotideLetterSize.value.width,
        height: interactionSVGBaseUnit.value * 0.75,
        radius: interactionSVGBaseUnit.value / 5
      },
      text: {
        x:
          2 * interactionSVGBaseUnit.value +
          fragmentLength.value.duplex -
          nucleotideLetterSize.value.width / 2 -
          paddingLengths.value.target.end *
            (interactionSVGBaseUnit.value +
              (interactionSVGBaseUnit.value -
                nucleotideLetterSize.value.width) /
                paddedFragments.value.target.length),
        y: 7 * interactionSVGBaseUnit.value
      },
      line: {
        x1:
          2 * interactionSVGBaseUnit.value +
          fragmentLength.value.duplex -
          nucleotideLetterSize.value.width / 2 -
          paddingLengths.value.target.end *
            (interactionSVGBaseUnit.value +
              (interactionSVGBaseUnit.value -
                nucleotideLetterSize.value.width) /
                paddedFragments.value.target.length),
        y1: 6.5 * interactionSVGBaseUnit.value,
        x2:
          2 * interactionSVGBaseUnit.value +
          fragmentLength.value.duplex -
          nucleotideLetterSize.value.width / 2 -
          paddingLengths.value.target.end *
            (interactionSVGBaseUnit.value +
              (interactionSVGBaseUnit.value -
                nucleotideLetterSize.value.width) /
                paddedFragments.value.target.length),
        y2: 6.75 * interactionSVGBaseUnit.value
      }
    },
    endPosition: fragmentsParentPositions.value && {
      rect: {
        x:
          2 * interactionSVGBaseUnit.value +
          nucleotideLetterSize.value.width / 2 +
          paddingLengths.value.target.start *
            (interactionSVGBaseUnit.value +
              (interactionSVGBaseUnit.value -
                nucleotideLetterSize.value.width) /
                paddedFragments.value.target.length),
        y: 6.75 * interactionSVGBaseUnit.value,
        width:
          fragmentsParentPositions.value.target.end.toString().length *
          nucleotideLetterSize.value.width,
        height: interactionSVGBaseUnit.value * 0.75,
        radius: interactionSVGBaseUnit.value / 5
      },
      text: {
        x:
          2 * interactionSVGBaseUnit.value +
          nucleotideLetterSize.value.width / 2 +
          paddingLengths.value.target.start *
            (interactionSVGBaseUnit.value +
              (interactionSVGBaseUnit.value -
                nucleotideLetterSize.value.width) /
                paddedFragments.value.target.length),
        y: 7 * interactionSVGBaseUnit.value
      },
      line: {
        x1:
          2 * interactionSVGBaseUnit.value +
          nucleotideLetterSize.value.width / 2 +
          paddingLengths.value.target.start *
            (interactionSVGBaseUnit.value +
              (interactionSVGBaseUnit.value -
                nucleotideLetterSize.value.width) /
                paddedFragments.value.target.length),
        y1: 6.5 * interactionSVGBaseUnit.value,
        x2:
          2 * interactionSVGBaseUnit.value +
          nucleotideLetterSize.value.width / 2 +
          paddingLengths.value.target.start *
            (interactionSVGBaseUnit.value +
              (interactionSVGBaseUnit.value -
                nucleotideLetterSize.value.width) /
                paddedFragments.value.target.length),
        y2: 6.75 * interactionSVGBaseUnit.value
      }
    }
  }
}))

/**
 * Description of the shapes
 */
const shapesDesc = computed(() => ({
  guideBrace: {
    start: [0, 2 * interactionSVGBaseUnit.value],
    pathSequence: [
      [
        'A',
        0.25 * interactionSVGBaseUnit.value,
        0.25 * interactionSVGBaseUnit.value,
        90,
        0,
        1,
        0.25 * interactionSVGBaseUnit.value,
        1.75 * interactionSVGBaseUnit.value
      ],
      [
        'h',
        2 * interactionSVGBaseUnit.value +
          fragmentLength.value.duplex / 2 -
          0.5 * interactionSVGBaseUnit.value
      ],
      [
        'A',
        0.25 * interactionSVGBaseUnit.value,
        0.25 * interactionSVGBaseUnit.value,
        90,
        0,
        0,
        2 * interactionSVGBaseUnit.value + fragmentLength.value.duplex / 2,
        1.5 * interactionSVGBaseUnit.value
      ],
      [
        'A',
        0.25 * interactionSVGBaseUnit.value,
        0.25 * interactionSVGBaseUnit.value,
        90,
        0,
        0,
        2 * interactionSVGBaseUnit.value +
          fragmentLength.value.duplex / 2 +
          0.25 * interactionSVGBaseUnit.value,
        1.75 * interactionSVGBaseUnit.value
      ],
      [
        'h',
        2 * interactionSVGBaseUnit.value +
          fragmentLength.value.duplex / 2 -
          0.5 * interactionSVGBaseUnit.value
      ],
      [
        'A',
        0.25 * interactionSVGBaseUnit.value,
        0.25 * interactionSVGBaseUnit.value,
        90,
        0,
        1,
        2 * interactionSVGBaseUnit.value +
          fragmentLength.value.duplex +
          2 * interactionSVGBaseUnit.value,
        2 * interactionSVGBaseUnit.value
      ]
    ]
      .map((command) => command.join(' '))
      .join(' ')
  },
  targetBrace: {
    start: [0, 7.75 * interactionSVGBaseUnit.value],
    pathSequence: [
      [
        'A',
        0.25 * interactionSVGBaseUnit.value,
        0.25 * interactionSVGBaseUnit.value,
        90,
        0,
        0,
        0.25 * interactionSVGBaseUnit.value,
        8 * interactionSVGBaseUnit.value
      ],
      [
        'h',
        2 * interactionSVGBaseUnit.value +
          fragmentLength.value.duplex / 2 -
          0.5 * interactionSVGBaseUnit.value
      ],
      [
        'A',
        0.25 * interactionSVGBaseUnit.value,
        0.25 * interactionSVGBaseUnit.value,
        90,
        0,
        1,
        2 * interactionSVGBaseUnit.value + fragmentLength.value.duplex / 2,
        8.25 * interactionSVGBaseUnit.value
      ],
      [
        'A',
        0.25 * interactionSVGBaseUnit.value,
        0.25 * interactionSVGBaseUnit.value,
        90,
        0,
        1,
        2 * interactionSVGBaseUnit.value +
          fragmentLength.value.duplex / 2 +
          0.25 * interactionSVGBaseUnit.value,
        8 * interactionSVGBaseUnit.value
      ],
      [
        'h',
        2 * interactionSVGBaseUnit.value +
          fragmentLength.value.duplex / 2 -
          0.5 * interactionSVGBaseUnit.value
      ],
      [
        'A',
        0.25 * interactionSVGBaseUnit.value,
        0.25 * interactionSVGBaseUnit.value,
        90,
        0,
        0,
        2 * interactionSVGBaseUnit.value +
          fragmentLength.value.duplex +
          2 * interactionSVGBaseUnit.value,
        7.75 * interactionSVGBaseUnit.value
      ]
    ]
      .map((command) => command.join(' '))
      .join(' ')
  }
}))

/**
 * Dimensions of the viewbox of the SVG
 */
const SVGViewBox = computed(() => ({
  minX: -5, // Safety margin around the canvas
  minY: -5, // Idem
  width:
    2 * interactionSVGBaseUnit.value +
    fragmentLength.value.duplex +
    2 * interactionSVGBaseUnit.value +
    10,
  // '+ 10' = safety margin (5 on left + 5 on right)
  height: 9.5 * interactionSVGBaseUnit.value + 10
  // '+ 10' = safety margin (5 on top + 5 on bottom)
}))

/**
 * Dimensions of the SVG
 */
const SVGDimensions = computed(() => ({
  width: SVGViewBox.value.width,
  height: SVGViewBox.value.height
}))
</script>

<template>
  <div class="flex items-end">
    <svg
      :ref="interactionSVGUpdate"
      version="1.1"
      baseProfile="full"
      xmlns="http://www.w3.org/2000/svg"
      xmlns:xlink="http://www.w3.org/1999/xlink"
      xmlns:ev="http://www.w3.org/2001/xml-events"
      x="0px"
      y="0px"
      :width="SVGDimensions.width"
      :height="SVGDimensions.height"
      :viewBox="Object.values(SVGViewBox).join(' ')"
      class="font-mono font-bold"
    >
      <g id="labels" class="select-none fill-slate-400">
        <g id="guide-labels">
          <g id="guide-name-label">
            <text
              :x="labelsDesc.guide.name.x"
              :y="labelsDesc.guide.name.y"
              class="font-sans font-normal italic"
              style="transform: translate(-50%, 100%); transform-box: fill-box"
            >
              {{ interaction.guide.name || interaction.guide.id }}
            </text>
            <path
              class="stroke-slate-400"
              style="
                fill: none;
                stroke-width: 1.5;
                stroke-linecap: round;
                stroke-linejoin: round;
              "
              :d="`M ${shapesDesc.guideBrace.start} ${shapesDesc.guideBrace.pathSequence}`"
            />
          </g>
          <g id="guide-direction-labels">
            <text
              :x="labelsDesc.guide.fivePrime.x"
              :y="labelsDesc.guide.fivePrime.y"
              class="font-sans font-normal italic"
              style="transform: translateY(0.5rem)"
            >
              5' &rarr;
            </text>
            <text
              :x="labelsDesc.guide.threePrime.x"
              :y="labelsDesc.guide.threePrime.y"
              class="font-sans font-normal italic"
              style="transform: translateY(0.5rem)"
            >
              &rarr; 3'
            </text>
          </g>
          <g v-if="labelsDesc.guide.startPosition" id="guide-start-pos-label">
            <rect
              :x="labelsDesc.guide.startPosition.rect.x"
              :y="labelsDesc.guide.startPosition.rect.y"
              :width="labelsDesc.guide.startPosition.rect.width"
              :height="labelsDesc.guide.startPosition.rect.height"
              :rx="labelsDesc.guide.startPosition.rect.radius"
              style="transform: translateX(-50%); transform-box: fill-box"
            ></rect>
            <text
              :x="labelsDesc.guide.startPosition.text.x"
              :y="labelsDesc.guide.startPosition.text.y"
              class="fill-white text-sm"
              style="
                transform: translate(-50%, 0.5rem);
                transform-box: fill-box;
              "
            >
              {{ interaction.fragments.guide.onParentPosition?.start }}
            </text>
            <line
              :x1="labelsDesc.guide.startPosition.line.x1"
              :y1="labelsDesc.guide.startPosition.line.y1"
              :x2="labelsDesc.guide.startPosition.line.x2"
              :y2="labelsDesc.guide.startPosition.line.y2"
              class="stroke-slate-400 stroke-2"
            ></line>
          </g>
          <g v-if="labelsDesc.guide.endPosition" id="guide-end-pos-label">
            <rect
              :x="labelsDesc.guide.endPosition.rect.x"
              :y="labelsDesc.guide.endPosition.rect.y"
              :width="labelsDesc.guide.endPosition.rect.width"
              :height="labelsDesc.guide.endPosition.rect.height"
              :rx="labelsDesc.guide.endPosition.rect.radius"
              style="transform: translateX(-50%); transform-box: fill-box"
            ></rect>
            <text
              :x="labelsDesc.guide.endPosition.text.x"
              :y="labelsDesc.guide.endPosition.text.y"
              class="fill-white text-sm"
              style="
                transform: translate(-50%, 0.5rem);
                transform-box: fill-box;
              "
            >
              {{ interaction.fragments.guide.onParentPosition?.end }}
            </text>
            <line
              :x1="labelsDesc.guide.endPosition.line.x1"
              :y1="labelsDesc.guide.endPosition.line.y1"
              :x2="labelsDesc.guide.endPosition.line.x2"
              :y2="labelsDesc.guide.endPosition.line.y2"
              class="stroke-slate-400 stroke-2"
            ></line>
          </g>
        </g>
        <g id="target-labels">
          <g id="target-direction-labels">
            <text
              :x="labelsDesc.target.threePrime.x"
              :y="labelsDesc.target.threePrime.y"
              class="font-sans font-normal italic"
              style="transform: translateY(0.5rem)"
            >
              3' &larr;
            </text>
            <text
              :x="labelsDesc.target.fivePrime.x"
              :y="labelsDesc.target.fivePrime.y"
              class="font-sans font-normal italic"
              style="transform: translateY(0.5rem)"
            >
              &larr; 5'
            </text>
          </g>
          <g id="target-name-label">
            <text
              :x="labelsDesc.target.name.x"
              :y="labelsDesc.target.name.y"
              class="font-sans font-normal italic"
              style="transform: translate(-50%, 100%); transform-box: fill-box"
            >
              {{ interaction.target.name || interaction.target.id }}
            </text>
            <path
              class="stroke-slate-400"
              style="
                fill: none;
                stroke-width: 1.5;
                stroke-linecap: round;
                stroke-linejoin: round;
              "
              :d="`M ${shapesDesc.targetBrace.start} ${shapesDesc.targetBrace.pathSequence}`"
            />
          </g>
          <g v-if="labelsDesc.target.startPosition" id="target-start-pos-label">
            <rect
              :x="labelsDesc.target.startPosition.rect.x"
              :y="labelsDesc.target.startPosition.rect.y"
              :width="labelsDesc.target.startPosition.rect.width"
              :height="labelsDesc.target.startPosition.rect.height"
              :rx="labelsDesc.target.startPosition.rect.radius"
              style="transform: translateX(-50%); transform-box: fill-box"
            ></rect>
            <text
              :x="labelsDesc.target.startPosition.text.x"
              :y="labelsDesc.target.startPosition.text.y"
              class="fill-white text-sm"
              style="
                transform: translate(-50%, 0.5rem);
                transform-box: fill-box;
              "
            >
              {{ interaction.fragments.target.onParentPosition?.start }}
            </text>
            <line
              :x1="labelsDesc.target.startPosition.line.x1"
              :y1="labelsDesc.target.startPosition.line.y1"
              :x2="labelsDesc.target.startPosition.line.x2"
              :y2="labelsDesc.target.startPosition.line.y2"
              class="stroke-slate-400 stroke-2"
            ></line>
          </g>
          <g v-if="labelsDesc.target.endPosition" id="target-end-pos-label">
            <rect
              :x="labelsDesc.target.endPosition.rect.x"
              :y="labelsDesc.target.endPosition.rect.y"
              :width="labelsDesc.target.endPosition.rect.width"
              :height="labelsDesc.target.endPosition.rect.height"
              :rx="labelsDesc.target.endPosition.rect.radius"
              style="transform: translateX(-50%); transform-box: fill-box"
            ></rect>
            <text
              :x="labelsDesc.target.endPosition.text.x"
              :y="labelsDesc.target.endPosition.text.y"
              class="fill-white text-sm"
              style="
                transform: translate(-50%, 0.5rem);
                transform-box: fill-box;
              "
            >
              {{ interaction.fragments.target.onParentPosition?.end }}
            </text>
            <line
              :x1="labelsDesc.target.endPosition.line.x1"
              :y1="labelsDesc.target.endPosition.line.y1"
              :x2="labelsDesc.target.endPosition.line.x2"
              :y2="labelsDesc.target.endPosition.line.y2"
              class="stroke-slate-400 stroke-2"
            ></line>
          </g>
        </g>
      </g>
      <!-- Ordering does not respect duplex1, duplex2, duplex3, etc... to be able to
      select nucleotides in the good order -->
      <text
        id="guide"
        :x="textsDesc.duplex.seq1.x"
        :y="textsDesc.duplex.seq1.y"
        :textLength="textsDesc.duplex.seq1.length"
        style="transform: translateY(0.5rem)"
      >
        {{ paddedFragments.guide }}
      </text>
      <g id="target">
        <rect
          id="modif-highlight"
          :x="
            modificationPositionRelToSVG?.x -
            (3 * nucleotideLetterSize.width) / 4
          "
          :y="
            modificationPositionRelToSVG?.y - 0.25 * nucleotideLetterSize.height
          "
          width="2.5ch"
          height="1.5rem"
          rx="1ch"
          :class="[
            `fill-${getModificationColor(ModifType.Nm)}-100`,
            `stroke-${getModificationColor(ModifType.Nm)}-600`
          ]"
          stroke-width="2px"
          style="transform: translateY(0.65rem)"
        />
        <text
          :x="modificationPositionRelToSVG?.x - nucleotideLetterSize.width / 2"
          :y="textsDesc.duplex.seq2.y"
          style="transform: translateY(0.5rem)"
          :class="`fill-${getModificationColor(ModifType.Nm)}-600 select-none`"
        >
          {{ interaction.modification.symbol }}
        </text>
        <text
          :x="textsDesc.duplex.seq2.x"
          :y="textsDesc.duplex.seq2.y"
          :textLength="textsDesc.duplex.seq2.length"
          style="transform: translateY(0.5rem)"
        >
          <template
            v-for="(
              nucleotide, nucleotideIndex
            ) in paddedFragments.target.split('')"
            :key="nucleotideIndex"
          >
            <tspan
              v-if="nucleotideIndex === modificationPositionInDuplex"
              ref="modificationElement"
              class="fill-transparent"
            >
              {{ nucleotide }}
            </tspan>
            <tspan v-else>
              {{ nucleotide }}
            </tspan>
          </template>
        </text>
      </g>

      <text
        id="bonds"
        :x="textsDesc.duplex.bonds.x"
        :y="textsDesc.duplex.bonds.y"
        :textLength="textsDesc.duplex.bonds.length"
        style="transform: translateY(0.5rem)"
        class="select-none whitespace-pre"
      >
        {{ bondsText }}
      </text>
    </svg>
  </div>
</template>
