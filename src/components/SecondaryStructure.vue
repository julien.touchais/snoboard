<script setup lang="ts">
/**
 * Vue imports
 */
import { ref, onMounted, computed } from 'vue'
/**
 * Components imports
 */
import BaseDescribedChip from '@/components/BaseDescribedChip.vue'
import BaseLegendButtonOverlay from '@/components/BaseLegendButtonOverlay.vue'
import FormattedModificationType from '@/components/FormattedModificationType.vue'
import Button from 'primevue/button'
import SelectButton from 'primevue/selectbutton'
import Dialog from 'primevue/dialog'
import Image from 'primevue/image'
import Checkbox from 'primevue/checkbox'
import IconFluentScanCamera28Regular from '~icons/fluent/scan-camera-28-regular'
import IconFa6SolidCropSimple from '~icons/fa6-solid/crop-simple'
import IconFa6SolidExpand from '~icons/fa6-solid/expand'
import IconFa6SolidDownload from '~icons/fa6-solid/download'
/**
 * Composables imports
 */
import { useElementSize, useResizeObserver } from '@vueuse/core'
/**
 * Other 3rd-party imports
 */
import G6 from '@antv/g6'
import { find as _find, uniqBy as _uniqBy } from 'lodash-es'
import mime from 'mime'
import FileSaver from 'file-saver'
/**
 * Types imports
 */
import type { C4GGraphModel, C4GNodeModel } from '@/typings/Codev4GraphFormat'
import type {
  Graph as GraphG6,
  GraphData as GraphG6Data,
  EdgeConfig as EdgeG6Model,
  NodeConfig as NodeG6Model,
  GraphOptions as GraphG6Options,
  DataUrlType as G6DataUrlType
} from '@antv/g6'
import { ModifType } from '@/gql/codegen/graphql'
import type { LegendItemModel } from './BaseLegendButtonOverlay.vue'
/**
 * Utils imports
 */
import { getModificationColor } from '@/utils/colors'
import { composeNormalisedGraph } from '@/utils/normalise'
/**
 * Assets imports
 */
import tailwindColors from 'tailwindcss/colors'
import { isInEnum } from '@/typings/typeUtils'

/**
 * A base unit in pixels to use as reference for other dimensions in the graph
 */
const GRAPH_BASE_UNIT_PX = 1

/**
 * The length at which the edges should be normalised
 */
const NORMALISED_EDGE_LENGTH = 3.5 * GRAPH_BASE_UNIT_PX

/**
 * Options for graph capture
 */
const GRAPH_CAPTURE_OPTIONS = [
  {
    label: 'Full',
    value: 'full',
    iconComponent: IconFa6SolidExpand
  },
  {
    label: 'Visible',
    value: 'current',
    iconComponent: IconFa6SolidCropSimple
  }
]

/**
 * Format in which to download graph capture
 */
const GRAPH_DOWNLOAD_FORMAT: G6DataUrlType = 'image/png'

/**
 * Applies modifications to the node object based on the metadata of the
 * modification of the nucleotide represented by that node, and return a node
 * with the modifications applied. If the metadata object is not defined
 * (i.e. there is no modification of this nucl.), then the node is returned
 * unmodified.
 * @param node The node on which to apply the metadata
 * @param modificationMetadata The metadata of the node
 */
const applyNodeModificationMetadata = (
  node: NodeG6Model,
  modificationMetadata?: { symbol: string; type: ModifType }
): NodeG6Model => {
  const modificationType = modificationMetadata?.type || ModifType.Other
  return modificationMetadata
    ? {
        ...node,
        label: modificationMetadata.symbol,
        type: 'modification-nucleotide',
        size: 3 * GRAPH_BASE_UNIT_PX,
        style: {
          fill: getModificationColor(modificationType, '100'),
          stroke: getModificationColor(modificationType, '600'),
          radius: 0.5 * GRAPH_BASE_UNIT_PX
        },
        labelCfg: {
          style: {
            fill: getModificationColor(modificationType, '600'),
            fontSize: 1.2 * GRAPH_BASE_UNIT_PX,
            fontWeight: 700
          }
        },
        stateStyles: {
          selected: {
            lineWidth: 0.3 * GRAPH_BASE_UNIT_PX,
            fill: getModificationColor(modificationType, '100'),
            stroke: getModificationColor(modificationType, '600'),
            shadowColor: getModificationColor(modificationType, '600'),
            /** ! `labelCfg` doesn't work well in `stateStyles`:
             * - color and font size are inherited from unselected style
             * - but font weight isn't and can't be redefined here
             */
            labelCfg: {
              style: {
                fill: getModificationColor(modificationType, '600'),
                fontSize: 1.2 * GRAPH_BASE_UNIT_PX,
                fontWeight: 700
              }
            }
          }
        }
      }
    : node
}

/**
 * Converts a node in C4G Graph format to G6 format.
 * @param nodeId ID of the node
 * @param nodeC4GFormat Node object, in C4G format
 */
const nodeC4GToG6 = (
  nodeId: string,
  nodeC4GFormat: C4GNodeModel
): NodeG6Model =>
  applyNodeModificationMetadata(
    {
      id: nodeId,
      label: nodeC4GFormat.label,
      x: nodeC4GFormat.metadata?.position?.x,
      y: nodeC4GFormat.metadata?.position?.y,
      metadata: nodeC4GFormat.metadata?.data?.reduce<{ [k: string]: any }>(
        (metadata, { label, value }) => ({
          ...metadata,
          [label]: value
        }),
        {}
      )
    },
    _find(nodeC4GFormat.metadata?.data, ['label', 'modification'])?.value
  )

/**
 * Generates a graph with G6.
 * @param container The DOM element in which to generate the graph
 * @param graphOptions The G6 options of the graph
 */
const generateG6Graph = (
  G6Graph: GraphG6Data,
  container: HTMLElement,
  graphOptions: GraphG6Options
): GraphG6 => {
  console.info('Generate secondary structure.')

  const graph = new G6.Graph(graphOptions)

  graph.data(G6Graph)
  graph.render()

  useResizeObserver(container, (elements) => {
    if (!graph || graph.get('destroyed') || !elements[0]) return
    graph.changeSize(
      elements[0].contentRect.width,
      elements[0].contentRect.height
    )
  })

  return graph
}

/**
 * Component props
 */
const props = defineProps<{
  /**
   * The graph of the secondary structure to display
   */
  structure: C4GGraphModel
}>()

/**
 * Secondary structure graph container DOM element
 */
const container = ref<HTMLDivElement>()

/**
 * Size of the secondary structure graph container DOM element
 */
const containerSize = useElementSize(container)

/**
 * Graph object
 */
const G6Graph = ref<GraphG6>()

/**
 * Wether to show or not the graph capture dialog
 */
const isCaptureDialogShown = ref<boolean>()

/**
 * Wether to capture the whole graph or only the currently displayed part
 */
const graphCaptureType = ref<'current' | 'full'>('current')

/**
 * Background of the captured graph image
 */
const graphCaptureBackground = ref<'transparent' | 'white'>('white')

/**
 * Data URL of the graph image for export
 */
const graphDataURL = ref<string>()

/**
 * Secondary structure, normalised at the specified edge length
 */
const normalisedStructure = computed(() =>
  composeNormalisedGraph(props.structure, NORMALISED_EDGE_LENGTH, (graph) =>
    // Get the first edge of the `Covalent` `relation` (i.e. not a base pair)
    graph.graph.edges?.find((edge) => edge.relation === 'Covalent')
  )
)

/**
 * Graph metadata (model used, software name  & version...)
 */
const graphMetadata = computed(() => {
  const model = props.structure.graph.metadata?.model
  const software = props.structure.graph.metadata?.software
  return {
    model:
      typeof model === 'object' &&
      model !== null &&
      'source' in model &&
      'template' in model &&
      'family' in model
        ? model
        : undefined,
    software:
      typeof software === 'object' &&
      software !== null &&
      'name' in software &&
      'url' in software &&
      'version' in software &&
      'docker' in software &&
      'cms_version' in software
        ? software
        : undefined
  }
})

/**
 * The secondary structure graph in G6 format
 */
const structureG6Format = computed<GraphG6Data>(() => ({
  id: 'structure',
  nodes: Object.entries(normalisedStructure.value.graph.nodes || {}).map(
    ([nodeId, nodeJSONFormat]) => nodeC4GToG6(nodeId, nodeJSONFormat)
  ),
  edges:
    'edges' in normalisedStructure.value.graph
      ? normalisedStructure.value.graph.edges?.map<EdgeG6Model>((edge) => ({
          source: edge.source,
          target: edge.target
        }))
      : []
}))

/**
 * The options of the G6 graph viz
 */
const graphG6Options = computed<GraphG6Options>(() => ({
  container: 'container',
  width: containerSize.width.value,
  height: containerSize.height.value,
  fitView: true,
  linkCenter: true,
  defaultNode: {
    type: 'nucleotide',
    size: 2.5 * GRAPH_BASE_UNIT_PX,
    style: {
      fill: tailwindColors.sky[50],
      stroke: tailwindColors.sky[300],
      lineWidth: 0.3 * GRAPH_BASE_UNIT_PX
    },
    labelCfg: {
      style: {
        fill: tailwindColors.sky[300],
        fontSize: 1.2 * GRAPH_BASE_UNIT_PX,
        fontWeight: 700
      }
    },
    nucleotidePositionCfg: {
      style: {
        fill: tailwindColors.slate[600],
        stroke: tailwindColors.slate[400],
        fontSize: 1.2 * GRAPH_BASE_UNIT_PX,
        fontWeight: 700,
        lineWidth: 0.3 * GRAPH_BASE_UNIT_PX
      }
    },
    endPositionCfg: {
      style: {
        fill: tailwindColors.slate[400],
        fontSize: 1.7 * GRAPH_BASE_UNIT_PX,
        fontWeight: 900
      }
    }
  },
  nodeStateStyles: {
    selected: {
      fill: tailwindColors.sky[50],
      stroke: tailwindColors.sky[400],
      lineWidth: 0.3 * GRAPH_BASE_UNIT_PX,
      shadowBlur: 15 * GRAPH_BASE_UNIT_PX,
      /** ! `labelCfg` doesn't work well in `stateStyles`:
       * - color and font size are inherited from unselected style
       * - but font weight isn't and can't be redefined here
       */
      labelCfg: {
        style: {
          fontWeight: 700
        }
      }
    }
  },
  defaultEdge: {
    size: 0.1 * GRAPH_BASE_UNIT_PX,
    color: tailwindColors.slate[400]
  },
  modes: {
    default: [
      'drag-canvas',
      'click-select',
      {
        type: 'zoom-canvas',
        enableOptimize: true,
        optimizeZoom: 5 / GRAPH_BASE_UNIT_PX,
        sensitivity: 1 * GRAPH_BASE_UNIT_PX
      },
      {
        type: 'tooltip',
        formatText: (data: { [k: string]: any }) =>
          `<div class="tooltip-content"><div>Position: <span class="tooltip-content-value">${
            data.metadata.nucleotidePosition
          }</span></div>
            ${
              data.metadata.modification
                ? `<div>Modification: <span class="tooltip-content-value">${data.metadata.modification.name}</span></div></div>`
                : ''
            }`
      }
    ]
  }
}))

/**
 * The legend to display on the sequence board.
 */
const secondaryStructureLegendItems = computed(() =>
  _uniqBy(
    structureG6Format.value.nodes
      ?.map((node) => node.metadata)
      .filter(
        (nodeMetadata): nodeMetadata is { modification: { type: ModifType } } =>
          typeof nodeMetadata === 'object' &&
          nodeMetadata !== null &&
          'modification' in nodeMetadata &&
          typeof nodeMetadata.modification === 'object' &&
          nodeMetadata.modification !== null &&
          'type' in nodeMetadata.modification
      )
      .map<LegendItemModel>((nodeMetadata) => ({
        id: nodeMetadata.modification.type,
        title: nodeMetadata.modification.type,
        color: getModificationColor(nodeMetadata.modification.type),
        description:
          nodeMetadata.modification.type === ModifType.Other
            ? 'Modification of a different type than those listed above.'
            : undefined
      }))
      .sort(
        (legendItemA, legendItemB) =>
          (legendItemA.id === 'Other' && 1) ||
          (legendItemA.title > legendItemB.title && 1) ||
          -1
      ),
    'id'
  )
)

/**
 * Updates the graph data URL based on the desired capture type
 */
const updateGraphDataURL = () => {
  switch (graphCaptureType.value) {
    case 'current':
      graphDataURL.value = G6Graph.value?.toDataURL(
        GRAPH_DOWNLOAD_FORMAT,
        graphCaptureBackground.value
      )
      break
    case 'full':
      G6Graph.value?.toFullDataURL(
        (dataURL) => {
          graphDataURL.value = dataURL
        },
        GRAPH_DOWNLOAD_FORMAT,
        {
          backgroundColor: graphCaptureBackground.value,
          padding: 10
        }
      )
      break
  }
}

/**
 * Function called when clicking the "Capture" button
 */
const showCaptureDialog = () => {
  updateGraphDataURL()
  isCaptureDialogShown.value = true
}

/**
 * Trigger the download of the graph image to the client
 */
const downloadGraphImage = () => {
  if (!graphDataURL.value) {
    console.warn('No image to download')
    return
  }
  FileSaver.saveAs(
    graphDataURL.value,
    `${props.structure.graph.id}-2D-${graphMetadata.value.model?.source}-${
      graphMetadata.value.model?.template
    }.${mime.getExtension(GRAPH_DOWNLOAD_FORMAT)}`
  )
}

/**
 * Create a custom node for displaying the nucleotide letter AND the nucleotide
 * position (if needed)
 */
G6.registerNode(
  'nucleotide',
  {
    afterDraw: (cfg: { [k: string]: any } | undefined, group) => {
      if (cfg?.metadata.endPositionLabel) {
        group?.addShape('text', {
          attrs: {
            text: cfg.metadata.endPositionLabel.label,
            x: cfg.metadata.endPositionLabel.x - cfg.x,
            y: cfg.metadata.endPositionLabel.y - cfg.y,
            fontSize: cfg.endPositionCfg.style.fontSize,
            fontWeight: cfg.endPositionCfg.style.fontWeight,
            fill: cfg.endPositionCfg.style.fill,
            fontStyle: 'italic',
            textAlign: 'center',
            textBaseline: 'middle'
          },
          capture: false,
          // must be assigned in G6 3.3 and later versions. it can be any string you want, but should be unique in a custom item type
          name: 'end-position-label'
        })
      }
      if (cfg?.metadata.nucleotidePositionLabel) {
        group?.addShape('text', {
          attrs: {
            text: cfg.metadata.nucleotidePosition,
            x: cfg.metadata.nucleotidePositionLabel.x - cfg.x,
            y: cfg.metadata.nucleotidePositionLabel.y - cfg.y,
            fontSize: cfg.nucleotidePositionCfg.style.fontSize,
            fontWeight: cfg.nucleotidePositionCfg.style.fontWeight,
            fill: cfg.nucleotidePositionCfg.style.fill,
            textAlign: 'center',
            textBaseline: 'middle'
          },
          capture: false,
          // must be assigned in G6 3.3 and later versions. it can be any string you want, but should be unique in a custom item type
          name: 'nucleotide-position-text'
        })
      }
      if (cfg?.metadata.nucleotidePositionLine) {
        group?.addShape('path', {
          attrs: {
            path: [
              [
                'M',
                cfg.metadata.nucleotidePositionLine.x1 - cfg.x,
                cfg.metadata.nucleotidePositionLine.y1 - cfg.y
              ],
              [
                'L',
                cfg.metadata.nucleotidePositionLine.x2 - cfg.x,
                cfg.metadata.nucleotidePositionLine.y2 - cfg.y
              ]
            ],
            stroke: cfg.nucleotidePositionCfg.style.stroke,
            lineCap: 'round',
            lineWidth: cfg.nucleotidePositionCfg.style.lineWidth
          },
          capture: false,
          // must be assigned in G6 3.3 and later versions. it can be any string you want, but should be unique in a custom item type
          name: 'nucleotide-position-line'
        })
      }
    }
  },
  'circle'
)

/**
 * Create a custom node for displaying the nucleotide letter AND the nucleotide
 * position (if needed) - for modification nucleotides
 */
G6.registerNode(
  'modification-nucleotide',
  {
    afterDraw: (cfg: { [k: string]: any } | undefined, group) => {
      if (cfg?.metadata.endPositionLabel) {
        group?.addShape('text', {
          attrs: {
            text: cfg.metadata.endPositionLabel.label,
            x: cfg.metadata.endPositionLabel.x - cfg.x,
            y: cfg.metadata.endPositionLabel.y - cfg.y,
            fontSize: cfg.endPositionCfg.style.fontSize,
            fontWeight: cfg.endPositionCfg.style.fontWeight,
            fill: cfg.endPositionCfg.style.fill,
            fontStyle: 'italic',
            textAlign: 'center',
            textBaseline: 'middle'
          },
          capture: false,
          // must be assigned in G6 3.3 and later versions. it can be any string you want, but should be unique in a custom item type
          name: 'modification-end-position-label'
        })
      }
      if (cfg?.metadata.nucleotidePositionLabel) {
        group?.addShape('text', {
          attrs: {
            text: cfg.metadata.nucleotidePosition,
            x: cfg.metadata.nucleotidePositionLabel.x - cfg.x,
            y: cfg.metadata.nucleotidePositionLabel.y - cfg.y,
            fontSize: cfg.nucleotidePositionCfg.style.fontSize,
            fontWeight: cfg.nucleotidePositionCfg.style.fontWeight,
            fill: cfg.nucleotidePositionCfg.style.fill,
            textAlign: 'center',
            textBaseline: 'middle'
          },
          capture: false,
          // must be assigned in G6 3.3 and later versions. it can be any string you want, but should be unique in a custom item type
          name: 'modification-nucleotide-position-label'
        })
      }
      if (cfg?.metadata.nucleotidePositionLine) {
        group?.addShape('path', {
          attrs: {
            path: [
              [
                'M',
                cfg.metadata.nucleotidePositionLine.x1 - cfg.x,
                cfg.metadata.nucleotidePositionLine.y1 - cfg.y
              ],
              [
                'L',
                cfg.metadata.nucleotidePositionLine.x2 - cfg.x,
                cfg.metadata.nucleotidePositionLine.y2 - cfg.y
              ]
            ],
            stroke: cfg.nucleotidePositionCfg.style.stroke,
            lineCap: 'round',
            lineWidth: cfg.nucleotidePositionCfg.style.lineWidth
          },
          capture: false,
          // must be assigned in G6 3.3 and later versions. it can be any string you want, but should be unique in a custom item type
          name: 'modification-nucleotide-position-line'
        })
      }
    }
  },
  'rect'
)

/**
 * Mounted hook, generates the graph
 */
onMounted(() => {
  if (!container.value) return
  G6Graph.value = generateG6Graph(
    structureG6Format.value,
    container.value,
    graphG6Options.value
  )
})
</script>

<template>
  <div class="relative">
    <p
      v-if="graphMetadata.model?.template"
      class="mb-8 mt-4 text-center text-xl font-extralight text-slate-600"
    >
      Model used:
      <BaseDescribedChip
        :description="
          typeof graphMetadata.model?.source === 'string'
            ? graphMetadata.model.source
            : undefined
        "
        color="sky"
      >
        <span
          v-tooltip.top="`Family: ${graphMetadata.model?.family}`"
          class="underline decoration-dashed"
        >
          {{ graphMetadata.model?.template }}
        </span>
      </BaseDescribedChip>
    </p>
    <div class="relative">
      <div
        id="container"
        ref="container"
        class="relative overflow-hidden rounded-xl border shadow-md"
      ></div>
      <Button
        class="!absolute right-4 top-4"
        outlined
        severity="secondary"
        @click="showCaptureDialog"
      >
        <icon-fluent-scan-camera28-regular class="mr-2 text-3xl" />
        Capture
      </Button>
      <BaseLegendButtonOverlay
        button-text="Legend"
        :items="secondaryStructureLegendItems"
        class="!absolute left-4 top-4"
      >
        <template #item-icon="{ item }">
          <svg
            class="min-w-max"
            viewBox="0 0 32 32"
            width="1.25em"
            height="1.25em"
          >
            <rect
              :class="`fill-${item.color}-100 stroke-${item.color}-600`"
              x="2"
              y="2"
              width="24"
              height="24"
              rx="4"
              stroke-width="4"
            />
          </svg>
        </template>

        <template #item-title="{ item }">
          <FormattedModificationType
            v-if="isInEnum(item.title, ModifType)"
            class="font-bold not-italic text-slate-600"
            :type-code="item.title"
            long-format
          />
        </template>
      </BaseLegendButtonOverlay>
    </div>
    <p
      v-if="graphMetadata.software?.name"
      class="mr-8 mt-4 text-right italic text-slate-400"
    >
      Built with
      <a
        :href="
          typeof graphMetadata.software?.url === 'string'
            ? graphMetadata.software.url
            : undefined
        "
        class="font-bold underline transition-all hover:text-lime-600"
        >{{ graphMetadata.software.name }}</a
      >
      <span v-if="graphMetadata.software.version">
        version
        <em
          v-tooltip.top="{
            value: graphMetadata.software.docker,
            autoHide: false,
            pt: { text: 'text-xs' }
          }"
          class="underline decoration-dashed"
        >
          {{ graphMetadata.software.version }}
        </em>
        (CMS {{ graphMetadata.software.cms_version }})
      </span>
    </p>
    <Dialog
      v-model:visible="isCaptureDialogShown"
      modal
      dismissable-mask
      :draggable="false"
      class="h-[75%]"
    >
      <h4 class="mb-4 text-center text-xl italic text-slate-400">
        Take a screenshot of the currently visible part of the graph, or of the
        full graph
      </h4>
      <Image
        class="h-5/6 w-auto overflow-hidden rounded-xl border-2"
        :src="graphDataURL"
        preview
      />
      <div class="flex justify-center">
        <SelectButton
          v-model="graphCaptureType"
          :options="GRAPH_CAPTURE_OPTIONS"
          option-value="value"
          @change="updateGraphDataURL"
        >
          <template #option="{ option }">
            <component :is="option.iconComponent || ''" class="mr-2" />
            {{ option.label }}
          </template>
        </SelectButton>
      </div>
      <template #header>
        <h3 class="mx-auto text-2xl font-bold text-slate-600">Graph capture</h3>
      </template>
      <template #footer>
        <div class="flex justify-end">
          <div class="flex flex-col items-center gap-2">
            <div class="flex items-center gap-2">
              <Checkbox
                v-model="graphCaptureBackground"
                input-id="transparent-bg-checkbox"
                true-value="transparent"
                false-value="white"
                binary
                @change="updateGraphDataURL"
              />
              <label for="transparent-bg-checkbox">
                Transparent background
              </label>
            </div>
            <Button @click="downloadGraphImage">
              <icon-fa6-solid-download class="mr-2 text-xl" />
              Download image
            </Button>
          </div>
        </div>
      </template>
    </Dialog>
  </div>
</template>

<style lang="scss">
#container {
  min-height: 600px;
  min-width: 800px;
}

.g6-tooltip {
  padding: 1rem;
  color: #444;
  background-color: rgba(255, 255, 255, 0.9);
  border: 1px solid #e2e2e2;
  border-radius: 0.5rem;
  max-width: 50ch;

  .tooltip-content {
    display: flex;
    flex-direction: column;

    &-value {
      font-weight: 700;
    }
  }
}
</style>
