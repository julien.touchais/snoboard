<script setup lang="ts">
/**
 * Vue imports
 */
import { computed, onMounted, ref } from 'vue'
/**
 * Components imports
 */
import BaseLockableTooltip from '@/components/BaseLockableTooltip.vue'
/**
 * Composables imports
 */
import { useMouseInElement } from '@vueuse/core'
/**
 * Other 3rd-party imports
 */
import { G, Rect, SVG, type Svg } from '@svgdotjs/svg.js'
/**
 * Types imports
 */
import type { ChromosomeModel, GenomeObjectModel } from './KaryotypeBoard.vue'

/**
 * In pixels, the minimal unzoomed size at which to draw karyotype objects.
 */
const MIN_BASE_OBJECT_LENGTH_PX = 4

/**
 * Component props.
 */
const props = withDefaults(
  defineProps<{
    // The chromosome to display
    chromosome: ChromosomeModel
    // The DOM element corresponding to the chromosome
    chromosomeElement: HTMLElement | SVGElement
    // A list of objects to display on the chromosome
    objects?: GenomeObjectModel[]
    // The zoom factor to apply
    zoomFactor?: number
    // The orientation of the chromosome
    orientation?: 'horizontal' | 'vertical'
    // The radius of the 'magnifying glass'
    radius?: number
  }>(),
  {
    objects: () => [],
    zoomFactor: 10,
    orientation: 'horizontal',
    radius: 50
  }
)

/**
 * Dimensions of the base chromosome element, position of the mouse relative to
 * the top left of the client, and to the chromosome element.
 */
const { chromosomeDimensions, mouseClient, mouseChromosome } = (() => {
  const { elementHeight, elementWidth, elementX, elementY, isOutside, x, y } =
    useMouseInElement(props.chromosomeElement, {
      type: 'client'
    })
  return {
    chromosomeDimensions: {
      height: elementHeight,
      width: elementWidth
    },
    mouseClient: {
      x: x,
      y: y
    },
    mouseChromosome: {
      x: elementX,
      y: elementY,
      isOutside
    }
  }
})()

/**
 * Ref to the div containing the chromosome SVG.
 */
const magnifiedChromosome = ref<HTMLDivElement>()

/**
 * Position of the magnify div relative to the viewport.
 */
const magnifyElementPositions = computed(() => ({
  top: mouseClient.y.value,
  left: mouseClient.x.value
}))

/**
 * DOM elements of objects currently hovered, to display in the tooltip when
 * not locked.
 */
const hoveredObjectsElements = ref<SVGElement[]>([])

/**
 * DOM elements of objects to display in the tooltip when locked.
 */
const lockedTooltipObjectsElements = ref<SVGElement[]>()

/**
 * DOM elements of objects to display in the tooltip.
 */
const tooltipObjectsElements = computed(
  () => lockedTooltipObjectsElements.value || hoveredObjectsElements.value
)

/**
 * Ref to the tooltip component.
 */
const tooltipComponent = ref<InstanceType<typeof BaseLockableTooltip>>()

/**
 * Locks the tooltip and the values to display in it.
 */
const lockTooltip = () => {
  lockedTooltipObjectsElements.value = hoveredObjectsElements.value
  tooltipComponent.value?.lockTooltipIfUnlocked()
}

/**
 * Normalised position of the mouse on the chromosome (between 0 & 1).
 */
const mousePositionNormalised = computed(() => ({
  x: mouseChromosome.x.value / chromosomeDimensions.width.value,
  y: mouseChromosome.y.value / chromosomeDimensions.height.value
}))

/**
 * Dimensions (in px) of the entire magnified chromosome.
 */
const magnifiedChromosomeDimensionsPX = computed(() => ({
  length: chromosomeDimensions.width.value * props.zoomFactor,
  thickness: 1.5 * props.radius
}))

/**
 * Translation applied to the magnified chromosome.
 */
const magnifiedChromosomeTranslate = computed(() => ({
  x:
    -mousePositionNormalised.value.x *
      magnifiedChromosomeDimensionsPX.value.length +
    props.radius,
  y:
    (0.5 - mousePositionNormalised.value.y + 0.25) *
    magnifiedChromosomeDimensionsPX.value.thickness
}))

/**
 * Ref to the SVG representation of the chromosome.
 */
const draw = ref<Svg>()

/**
 * Creates the elements forming the chromosome SVG.
 */
const createChromosomeSVG = () => {
  if (!magnifiedChromosome.value) {
    console.warn('Element not present')
    return
  }

  draw.value = SVG().addTo(magnifiedChromosome.value)

  // Create the body of the chromosome, made of the 2 arms
  const mainGroup = draw.value
    .group()
    .fill(props.chromosome.color || '#84b650')
    .addClass('zoom-chr-svg-main')
  mainGroup.rect().addClass('zoom-chr-svg-main-short-arm')
  mainGroup.rect().addClass('zoom-chr-svg-main-long-arm')

  // Create and fill a group with the objects present on the chromosome
  const objectsGroup = draw.value
    .group()
    .addClass('fill-[#abcf74]')
    .addClass('zoom-chr-svg-objects')
  props.objects.forEach((object) => {
    const rect = objectsGroup.rect().data(object)
    if (object.color) {
      rect.fill(object.color)
    }
    rect.addClass('cursor-pointer')
  })
  objectsGroup.mousemove((e: MouseEvent) => {
    // Get current hovered object elements
    const hoveredObjectsElementsNew = document
      .elementsFromPoint(e.clientX, e.clientY)
      .filter((objectElement) =>
        objectElement.parentElement?.classList.contains('zoom-chr-svg-objects')
      ) as SVGElement[]

    // Remove highlight on no longer hovered elements
    hoveredObjectsElements.value.forEach((objectElement) => {
      if (
        !hoveredObjectsElementsNew.find(
          (objectElementNew) =>
            objectElementNew.dataset.id === objectElement.dataset.id
        )
      ) {
        objectElement.setAttribute(
          'class',
          (objectElement.getAttribute('class') || '').replace(
            ' brightness-90',
            ''
          )
        )
      }
    })
    // Add highlight on newly hovered elements
    hoveredObjectsElementsNew.forEach((objectElementNew) => {
      if (
        !hoveredObjectsElements.value.find(
          (objectElement) =>
            objectElement.dataset.id === objectElementNew.dataset.id
        )
      ) {
        objectElementNew.setAttribute(
          'class',
          `${objectElementNew.getAttribute('class') || ''} brightness-90`
        )
      }
    })
    hoveredObjectsElements.value = hoveredObjectsElementsNew
  })

  // Clear style of no-longer-hovered elements
  objectsGroup.mouseleave(() => {
    hoveredObjectsElements.value.forEach((objectElement) => {
      objectElement.setAttribute(
        'class',
        (objectElement.getAttribute('class') || '').replace(
          ' brightness-90',
          ''
        )
      )
    })
    hoveredObjectsElements.value = []
  })

  // Lock the tooltip when clicking on any chromosome object
  objectsGroup.click(lockTooltip)

  // Create the clip of the object's group, by cloning the chromosome body,
  // and apply the clip
  const clipGroup = draw.value.clip()
  for (const path of mainGroup.clone().children()) {
    clipGroup.add(path)
  }
  objectsGroup.clipWith(clipGroup)
}

/**
 * Updates the size & position of the elements in the chromosome SVG.
 */
const updateChromosomeSVG = () => {
  if (!draw.value) {
    return
  }
  // 1. Compute dimensions
  const chromosomeDrawSize = {
    height: magnifiedChromosomeDimensionsPX.value.length,
    width: magnifiedChromosomeDimensionsPX.value.thickness
  }
  const ONE_BP_LENGTH_PX = chromosomeDrawSize.height / props.chromosome.length
  // A tenth of the width of the chromosomes is added to arm's length to allow
  // their superposition w/o losing the real length
  const shortArmHeight =
    ONE_BP_LENGTH_PX * (props.chromosome.centromereCenter || 0) +
    chromosomeDrawSize.width / 10
  const longArmHeight =
    ONE_BP_LENGTH_PX *
      (props.chromosome.length - (props.chromosome.centromereCenter || 0)) +
    chromosomeDrawSize.width / 10
  // If horizontal, chromosome will be rotated so the SVG height equals the
  // chromosome width and vice-versa
  const SVGSize =
    props.orientation === 'vertical'
      ? {
          height: chromosomeDrawSize.height,
          width: chromosomeDrawSize.width
        }
      : {
          height: chromosomeDrawSize.width,
          width: chromosomeDrawSize.height
        }
  // Resize the SVG
  draw.value.size(SVGSize.width, SVGSize.height)

  // 2. Retrieve SVG objects
  // Casting is needed for Typescript to work, so the notation is a bit verbose
  const chrMainGroup = draw.value.findOne('g.zoom-chr-svg-main') as G
  const shortArmRect = draw.value.findOne(
    'rect.zoom-chr-svg-main-short-arm'
  ) as Rect
  const longArmRect = draw.value.findOne(
    'rect.zoom-chr-svg-main-long-arm'
  ) as Rect
  const objectsGroup = draw.value.findOne('g.zoom-chr-svg-objects') as G
  const objectsGroupClipperShortArmRect = objectsGroup
    .clipper()
    .findOne('rect.zoom-chr-svg-main-short-arm') as Rect
  const objectsGroupClipperLongArmRect = objectsGroup
    .clipper()
    .findOne('rect.zoom-chr-svg-main-long-arm') as Rect

  // 3. Base operations (chromosome is vertical, centered, and text horizontal)
  // Chromosome arms
  shortArmRect
    .size(chromosomeDrawSize.width, shortArmHeight)
    .radius(chromosomeDrawSize.width / 2)
    .cx(SVGSize.width / 2)
  longArmRect
    .size(chromosomeDrawSize.width, longArmHeight)
    .radius(chromosomeDrawSize.width / 2)
    .cx(SVGSize.width / 2)
    .y(shortArmHeight - chromosomeDrawSize.width / 5)
  // Chromosome objects
  objectsGroup.children().forEach((objectRect) => {
    const objectSide = objectRect.data('side') || 'left'
    const objectLengthPx =
      (objectRect.data('end') - objectRect.data('start')) * ONE_BP_LENGTH_PX
    const isObjectTooSmall =
      objectLengthPx < MIN_BASE_OBJECT_LENGTH_PX * props.zoomFactor
    const objectDimensions = {
      width: chromosomeDrawSize.width / 2,
      height: isObjectTooSmall
        ? MIN_BASE_OBJECT_LENGTH_PX * props.zoomFactor
        : objectLengthPx,
      x:
        objectSide === 'left'
          ? SVGSize.width / 2 -
            chromosomeDrawSize.width / 2 -
            chromosomeDrawSize.width / 10
          : SVGSize.width / 2 + chromosomeDrawSize.width / 10,
      y:
        objectRect.data('start') * ONE_BP_LENGTH_PX -
        (isObjectTooSmall
          ? (MIN_BASE_OBJECT_LENGTH_PX * props.zoomFactor) / 2
          : 0)
    }
    ;(objectRect as Rect)
      .move(objectDimensions.x, objectDimensions.y)
      .size(objectDimensions.width, objectDimensions.height)
      .radius(2)
  })
  // Clip path
  objectsGroupClipperShortArmRect
    .size(chromosomeDrawSize.width, shortArmHeight)
    .radius(chromosomeDrawSize.width / 2)
    .cx(SVGSize.width / 2)
  objectsGroupClipperLongArmRect
    .size(chromosomeDrawSize.width, longArmHeight)
    .radius(chromosomeDrawSize.width / 2)
    .cx(SVGSize.width / 2)
    .y(shortArmHeight - chromosomeDrawSize.width / 5)

  // 4. Transforms (orientation-specific)
  const groupsTransforms =
    props.orientation === 'vertical'
      ? {
          rotate: 0,
          origin: { x: 0, y: 0 },
          translateX: 0
        }
      : {
          rotate: -90,
          origin: { x: SVGSize.width / 2, y: SVGSize.height / 2 },
          translateX: -SVGSize.width / 2 + SVGSize.height / 2
        }
  // Main group (chr arms)
  chrMainGroup.transform(groupsTransforms)
  // Objects group
  objectsGroup.transform(groupsTransforms)
}

/**
 * Creates and updates the SVG elements of the chromosome on mount.
 */
onMounted(() => {
  createChromosomeSVG()
  updateChromosomeSVG()
})
</script>

<template>
  <div
    v-show="!mouseChromosome.isOutside.value"
    class="fixed -translate-x-1/2 -translate-y-1/2 overflow-hidden rounded-full border bg-white shadow-lg"
    :style="{
      height: `${radius * 2}px`,
      width: `${radius * 2}px`,
      top: `${magnifyElementPositions.top}px`,
      left: `${magnifyElementPositions.left}px`
    }"
  >
    <div
      ref="magnifiedChromosome"
      :style="{
        transform: `translateY(${magnifiedChromosomeTranslate.y}px) translateX(${magnifiedChromosomeTranslate.x}px)`
      }"
    ></div>
  </div>
  <BaseLockableTooltip
    ref="tooltipComponent"
    :show="!!hoveredObjectsElements.length"
    @unlock="lockedTooltipObjectsElements = undefined"
  >
    <div class="flex flex-col gap-2">
      <RouterLink
        v-for="objectElement in tooltipObjectsElements"
        :key="objectElement.dataset.id"
        :to="{
          name: 'guideDetails',
          query: { id: objectElement.dataset.id }
        }"
        class="font-bold underline hover:text-sky-600"
      >
        {{ objectElement.dataset.name }}
      </RouterLink>
    </div>
  </BaseLockableTooltip>
</template>
