<script setup lang="ts">
/**
 * Vue imports
 */
import { computed, onMounted, ref, toRef, watch } from 'vue'
/**
 * Components imports
 */
import SequenceAlignmentToolbar from './SequenceAlignmentToolbar.vue'
import SequenceAlignmentTrackNames from './SequenceAlignmentTrackNames.vue'
/**
 * Composables imports
 */
import { useConservationAnalysis } from '@/composables/useConservationAnalysis'
import { useElementBounding, useResizeObserver } from '@vueuse/core'
/**
 * Other 3rd-party imports
 */
import {
  inRange as _inRange,
  uniqBy as _uniqBy,
  mapValues as _mapValues
} from 'lodash-es'
/**
 * Types imports
 */
import type {
  HexColorCodeModel,
  TailwindDefaultColorNameModel
} from '@/typings/styleTypes'
import type { RouteLocationRaw } from 'vue-router'
import type { StyleValue } from 'vue'
import type { BasesConservationConfigModel } from './SequenceAlignmentToolbar.vue'
import type { ConservationAnalysisConfigModel } from '@/composables/useConservationAnalysis'
/**
 * Utils imports
 */
import { composeConservationRate } from '@/utils/sequences'
import { isDefined } from '@/typings/typeUtils'
import { range } from '@/utils/numbers'

/**
 * A legend item.
 */
export interface LegendItemModel {
  /** Item id. */
  id: string
  /** Item title. */
  title: string
  /** Item description. */
  description?: string
  /** Tailwind color name of the color to legend. */
  color?: TailwindDefaultColorNameModel
}

/**
 * An object on a track in an alignment.
 */
export interface AlignmentObjectModel {
  /** The name of the object. */
  name?: string
  /** The type of the object. */
  type?: string
  /** The start position of the object (in the alignment reference). */
  start: number
  /** The end position of the object (in the alignment reference). */
  end: number
  /** The color in which to represent the object. */
  color?: TailwindDefaultColorNameModel
  /** Link to go to when clicking on the object. */
  link?: RouteLocationRaw
}

/**
 * An object on a track in an alignment, with the ID of the track it is on.
 */
export interface AlignmentObjectWithTrackIdModel extends AlignmentObjectModel {
  /** The ID of track the object is present on. */
  trackId: string
}

/**
 * A track in an alignment.
 */
export interface AlignmentTrackModel {
  /** The ID of the track. */
  id: string
  /** The name of the track. */
  name?: string
  /** The short name of the track. */
  shortname?: string
  /** The sequence of the track. */
  sequence: string
  /** Wether the track is a reference sequence or not. */
  isReference?: boolean
  /** A list of object present on the track to display. */
  objects?: { [objectId: string]: AlignmentObjectModel }
  /** Wether the track is an information track. */
  isInformation?: boolean
}

/**
 * An alignment.
 */
export interface AlignmentModel {
  /** The tracks composing the alignment. */
  tracks: AlignmentTrackModel[]
}

/**
 * The colours for base conservation colouring.
 */
const BASE_CONSERVATION_COLOURS = {
  low: '#F87171' as HexColorCodeModel,
  middle: '#FCD34D' as HexColorCodeModel,
  high: '#D9EDC1' as HexColorCodeModel
}

/**
 * Component props.
 */
const props = defineProps<{
  /** The representation of the alignment. */
  alignment: AlignmentModel
  /** The items of the legend to display for the highlighted groups. */
  legendItems?: LegendItemModel[]
}>()

/**
 * Size (in term of nucl.) of the groups into which the sequences are divided.
 */
const nucleotideGroupsSize = ref(10)

/**
 * Number of groups in which the sequences are divided.
 */
const nucleotideGroupsCount = computed(() =>
  Math.ceil(
    (props.alignment.tracks[0]?.sequence.length || 1) /
      nucleotideGroupsSize.value
  )
)

/**
 * Number of nucleotides in the last group.
 */
const lastNucleotideGroupSize = computed(
  () =>
    (props.alignment.tracks[0]?.sequence.length || 1) %
    nucleotideGroupsSize.value
)

/**
 * The tracks, with an additional track for the conservation analysis added
 * at the end.
 */
const tracksWithConservationTrack = computed(() => [
  ...props.alignment.tracks,
  conservationAnalysis.track.value
])

/**
 * The sequences of the alignment, split and rearranged in the following nested
 * arrays structure:
 * ```ts
 *  [
 *    [ // Group 0
 *      [ // Nucleotide 0
 *        sequence1[0],
 *        sequence2[0],
 *        ...,
 *        sequence<#ofTracks>[0]
 *      ],
 *      [ // Nucleotide 1
 *        sequence1[1],
 *        sequence2[1],
 *        ...,
 *        sequence<#ofTracks>[1]
 *      ],
 *      ...,
 *      [ // Last nucleotide of group 0
 *        sequence1[nucleotideGroupsSize - 1],
 *        sequence2[nucleotideGroupsSize - 1],
 *        ...,
 *        sequence<#ofTracks>[nucleotideGroupsSize - 1]
 *      ],
 *    ],
 *    [ // Group 1
 *      [ // First nucleotide of group 1
 *        sequence1[nucleotideGroupsSize],
 *        sequence2[nucleotideGroupsSize],
 *        ...,
 *        sequence<#ofTracks>[nucleotideGroupsSize]
 *      ],
 *      [
 *        sequence1[nucleotideGroupsSize + 1],
 *        sequence2[nucleotideGroupsSize + 1],
 *        ...,
 *        sequence<#ofTracks>[nucleotideGroupsSize + 1]
 *      ],
 *      ...,
 *      [ // Last nucleotide of group 1
 *        sequence1[2 * nucleotideGroupsSize - 1],
 *        sequence2[2 * nucleotideGroupsSize - 1],
 *        ...,
 *        sequence<#ofTracks>[2 * nucleotideGroupsSize - 1]
 *      ],
 *    ],
 *    ...
 *  ]
 * ```
 */
const splitAlignmentSequences = computed(() =>
  // Top-level array, size = group count, one cell = one group
  Array.from({ length: nucleotideGroupsCount.value }, (_, groupIndex) =>
    // Second-level array, size = group size (if last group, special size), one
    // cell = one position in alignment
    Array.from(
      {
        length:
          groupIndex === nucleotideGroupsCount.value - 1
            ? lastNucleotideGroupSize.value
            : nucleotideGroupsSize.value
      },
      (_, nucleotideIndexInGroup) => {
        // Compute the position of the nucleotide
        const nucleotideIndex =
          groupIndex * nucleotideGroupsSize.value + nucleotideIndexInGroup
        // Third-level array, size = # of sequences, one cell = the nucleotide
        // at current position in the corresponding sequence
        return Array.from(
          { length: tracksWithConservationTrack.value.length },
          (_, sequenceIndex) => {
            const nucleotide =
              tracksWithConservationTrack.value[sequenceIndex]?.sequence[
                nucleotideIndex
              ]
            return nucleotide ? nucleotide : '-'
          }
        )
      }
    )
  )
)

/**
 * Wether a nucleotide is in a given object of a given track or not.
 * @param trackIndex The index of the track in the alignment.
 * @param position The position of the nucleotide to check.
 * @param objectId The id of the object to check for.
 * @returns `true` if position if in the given object, `false` otherwise.
 */
const isInObject = (
  trackIndex: number,
  position: number,
  objectId: string
): boolean => {
  const object =
    tracksWithConservationTrack.value[trackIndex]?.objects?.[objectId]
  return !!object && _inRange(position, object.start, object.end + 1)
}

/**
 * Gets the IDs of the objects of a given track containing a nucleotide.
 * @param trackIndex The index of the track in the alignment.
 * @param position The position of the nucleotide for which to get object IDs.
 * @returns A list of the IDs of the objects containing the nucleotide.
 */
const objectIdsContaining = (
  trackIndex: number,
  position: number
): string[] => {
  const trackObjects = tracksWithConservationTrack.value[trackIndex]?.objects
  return trackObjects
    ? Object.keys(trackObjects).filter((objectId) =>
        isInObject(trackIndex, position, objectId)
      )
    : []
}

/**
 * Whether a nucleotide is in any object of a given track.
 * @param trackIndex The index of the track in the alignment.
 * @param position The position of the nucleotide to check.
 * @returns `true` if the nucleotide is in any object, `false` otherwise.
 */
const isInAnyObject = (trackIndex: number, position: number): boolean =>
  !!objectIdsContaining(trackIndex, position).length

/**
 * For each track, a `Set` of the boundary positions (start & end) of all
 * objects. It is in the form of an array, indexed in the same order as the
 * tracks in the alignment.
 */
const objectsBoundaryPositions = computed(() =>
  tracksWithConservationTrack.value.map((track) => {
    // Objects present on the current track
    const trackObjects =
      track.isInformation && track.objects
        ? Object.values(track.objects)
        : referenceTrackModifications.value.filter(
            (modification) => modification.trackId === track.id
          )
    return trackObjects.reduce(
      (objectsBoundaryPositions, object) =>
        object.start && object.end
          ? objectsBoundaryPositions.add(object.start).add(object.end)
          : objectsBoundaryPositions,
      new Set<number>()
    )
  })
)

/**
 * Whether a nucleotide is a boundary position (start or end) of an object.
 * @param position The position of the nucleotide to check.
 * @returns `true` if the nucleotide is the start of an object, `false` otherwise.
 */
const isObjectBoundary = (trackIndex: number, position: number): boolean =>
  !!objectsBoundaryPositions.value[trackIndex]?.has(position)

/**
 * Composes the Tailwind color name in which to paint a nucleotide based on its
 * position.
 * @param trackIndex The index of the track in the alignment.
 * @param position The position for which to compose the color.
 * @returns `slate` if there is no or several objects at the given position, the
 * Tailwind color name of the object if there is only one.
 */
const composeInObjectPositionColor = (
  trackIndex: number,
  position: number
): TailwindDefaultColorNameModel => {
  const objectIds = objectIdsContaining(trackIndex, position)
  // If only one group, use its color, otherwise, use 'slate'
  return objectIds.length === 1 && objectIds[0]
    ? tracksWithConservationTrack.value[trackIndex]?.objects?.[objectIds[0]]
        ?.color || 'slate'
    : 'slate'
}

/**
 * Sequence container DOM `HTMLElement`.
 */
const sequenceContainerElement = ref<HTMLElement>()
/**
 * Split sequences groups DOM `HTMLElement` array.
 */
const sequenceGroupElements = ref<HTMLElement[]>([])
/**
 * First & last nucleotides of each box DOM `HTMLElement` array.
 */
const boxBoundaryElements = ref<HTMLElement[]>([])

/**
 * Bounding box of the first split sequences group DOM `HTMLElement`.
 */
const firstSequenceGroupElementBounding = useElementBounding(
  toRef(() => sequenceGroupElements.value[0])
)

/**
 * Bounding box of the split sequences groups DOM `HTMLElements`.
 */
const sequenceContainerElementBounding = useElementBounding(
  sequenceContainerElement
)

/**
 * Number of sequence lines.
 */
const lineCount = computed(() => {
  const firstSequenceGroupElement = sequenceGroupElements.value[0]
  return (
    (firstSequenceGroupElement &&
      Math.round(
        sequenceContainerElementBounding.height.value /
          (parseInt(
            window.getComputedStyle(firstSequenceGroupElement).marginTop
          ) +
            firstSequenceGroupElementBounding.height.value)
      )) ||
    1
  )
})

/**
 *  Height of a sequence line.
 */
const lineHeight = computed(
  () =>
    firstSequenceGroupElementBounding.bottom.value -
    sequenceContainerElementBounding.top.value
)

/**
 * Dictionary of the number of line on which each object spreads,
 * by group ID.
 */
const tracksObjectsLineCount = ref<
  ({ [groupId: string]: number | undefined } | undefined)[]
>([])

/**
 * Updates the number of line on which each object spreads.
 * @returns The updated number of line on which each object spreads.
 * @description Retrieves the line count by getting the difference between the
 * top of the last nucleotide of the box and the top of the first one, and
 * dividing by the line height.
 */
const updateObjectsLineCount = () =>
  (tracksObjectsLineCount.value = tracksWithConservationTrack.value.map(
    (track, trackIndex) =>
      _mapValues(track.objects, (trackObject) => {
        const objectStartNucleotideElementParent =
          boxBoundaryElements.value.find(
            (element) =>
              element.dataset.trackAndPosition ===
              `${trackIndex}:${trackObject.start}`
          )?.offsetParent

        const objectEndNucleotideElementParent = boxBoundaryElements.value.find(
          (element) =>
            element.dataset.trackAndPosition ===
            `${trackIndex}:${trackObject.end}`
        )?.offsetParent

        if (
          !(
            objectStartNucleotideElementParent instanceof HTMLElement &&
            objectEndNucleotideElementParent instanceof HTMLElement &&
            objectStartNucleotideElementParent.offsetParent instanceof
              HTMLElement &&
            objectEndNucleotideElementParent.offsetParent instanceof HTMLElement
          )
        ) {
          return undefined
        }

        return (
          lineHeight.value &&
          Math.round(
            (objectEndNucleotideElementParent.offsetParent.offsetTop -
              objectStartNucleotideElementParent.offsetParent.offsetTop) /
              lineHeight.value
          ) + 1
        )
      })
  ))

/**
 * Width of the sequence container.
 */
const sequenceContainerContentWidth = ref<number>()

/**
 * Updates the width of the sequence container.
 */
const updateSequenceContainerContentWidth =
  (): typeof sequenceContainerContentWidth.value => {
    // First sequence group DOM element
    const firstSequenceGroupElement = sequenceGroupElements.value[0]
    if (!firstSequenceGroupElement) return

    // Size of the right margin of a sequence group DOM element
    const sequenceGroupElementRightMargin = parseInt(
      window.getComputedStyle(firstSequenceGroupElement).marginRight
    )
    if (!sequenceGroupElementRightMargin) return

    // Width of a sequence group DOM element
    const sequenceGroupElementWidth =
      firstSequenceGroupElement.offsetWidth + sequenceGroupElementRightMargin
    if (!sequenceGroupElementWidth) return

    // Width of the sequences container DOM element
    const sequenceContainerElementWidth =
      sequenceContainerElement.value?.offsetWidth
    if (!sequenceContainerElementWidth) return

    // Sequence content width = container width - (space not actually occupied by
    // sequence + 1 * right margin of a sequence group)
    return (sequenceContainerContentWidth.value =
      sequenceContainerElementWidth -
      ((sequenceContainerElementWidth % sequenceGroupElementWidth) +
        sequenceGroupElementRightMargin))
  }

/**
 * Updates all non-reactive values.
 */
const update = (): void => {
  updateObjectsLineCount()
  updateSequenceContainerContentWidth()
}

/**
 * Sets hook to update non-reactive values on component mount.
 */
onMounted(update)

/**
 * Sets an observer to update on container resize.
 */
useResizeObserver(sequenceContainerElement, update)

/**
 * Reactive array, for each track, the style to apply to each fragment of each
 * of its objects.
 */
const boxStyles = computed(() =>
  // Map tracks
  tracksWithConservationTrack.value.map((track, trackIndex) => {
    // No object on current track
    if (!track.objects) {
      return {}
    }

    // Map track objects to the style of each of their fragment
    return _mapValues(track.objects, (trackObject, trackObjectId) => {
      // DOM element of the first nucleotide of the box
      const objectStartNucleotideElementParent = boxBoundaryElements.value.find(
        (element) =>
          element.dataset.trackAndPosition ===
          `${trackIndex}:${trackObject.start}`
      )?.offsetParent

      // DOM element of the last nucleotide of the box
      const objectEndNucleotideElementParent = boxBoundaryElements.value.find(
        (element) =>
          element.dataset.trackAndPosition ===
          `${trackIndex}:${trackObject.end}`
      )?.offsetParent

      if (
        !(
          objectStartNucleotideElementParent instanceof HTMLElement &&
          objectEndNucleotideElementParent instanceof HTMLElement &&
          objectStartNucleotideElementParent.offsetParent instanceof
            HTMLElement &&
          objectEndNucleotideElementParent.offsetParent instanceof HTMLElement
        )
      ) {
        return
      }

      // Absolute positions of the first and last nucleotide of the object DOM
      // elements
      const objectStartNucleotideElementTop =
        objectStartNucleotideElementParent.offsetParent.offsetTop +
        (objectStartNucleotideElementParent?.offsetTop || 0)
      const objectStartNucleotideElementLeft =
        objectStartNucleotideElementParent.offsetParent.offsetLeft +
        (objectStartNucleotideElementParent?.offsetLeft || 0)
      const objectEndNucleotideElementLeft =
        objectEndNucleotideElementParent.offsetParent.offsetLeft +
        (objectEndNucleotideElementParent?.offsetLeft || 0)
      const objectEndNucleotideElementRight =
        objectEndNucleotideElementLeft +
        (objectEndNucleotideElementParent?.offsetWidth || 0)

      // Number of fragment in which the current object is divided
      const boxFragmentCount =
        tracksObjectsLineCount.value?.[trackIndex]?.[trackObjectId] || 0

      // Array of length equals # of fragments, containing style for each of them
      return Array.from(
        { length: boxFragmentCount },
        (_, fragmentIndex): StyleValue => ({
          top: `${
            objectStartNucleotideElementTop +
            fragmentIndex * (lineHeight.value || 0)
          }px`,
          left:
            fragmentIndex === 0
              ? `calc(${objectStartNucleotideElementLeft}px - 0.125rem)`
              : '0',
          right: `calc(${
            (sequenceContainerElementBounding.width.value || 0) -
            (fragmentIndex === boxFragmentCount - 1
              ? objectEndNucleotideElementRight
              : sequenceContainerContentWidth.value || 0)
          }px - 0.125rem)`
        })
      )
    })
  })
)

/**
 * Compute the position of a nucleotide in the reference of a sequence from the
 * position on the same sequence gapped with `-` for alignment.
 * @param alignedSequence The sequence, gapped with `-` for alignment with other
 * sequences.
 * @param inAlignmentPosition The position in the reference of the alignment to
 * convert to the source sequence reference (starts at 1).
 */
const composePositionOnSource = (
  alignedSequence: string,
  inAlignmentPosition: number
): number | undefined =>
  alignedSequence[inAlignmentPosition - 1] === '-'
    ? undefined
    : inAlignmentPosition -
      (alignedSequence.slice(0, inAlignmentPosition).split('-').length - 1)

/**
 * Compute the position of a nucleotide on a sequence gapped with `-` for
 * alignment from the position in the source reference of the same sequence.
 * @param alignedSequence The sequence, gapped with `-` for alignment with other
 * sequences.
 * @param inSourcePosition The position in the source sequence reference to
 * convert to the reference of the alignment (starts at 1).
 * @returns The position of the nucleotide on the aligned sequence, -1 if the
 * requested position exceeded the actual length of the source sequence.
 */
const composePositionOnAlignment = (
  alignedSequence: string,
  inSourcePosition: number
): number => {
  const firstDashPositionOnAlignment = alignedSequence.indexOf('-') + 1

  // If the aligned sequence is shorter than the requested position, it means
  // that the latter doesn't exists.
  if (alignedSequence.length < inSourcePosition) {
    return -1
  }

  // If the requested position is closer to the start than the first dash (or if
  // there is no dash in it), then the position on the alignment is the same
  // than on the source sequence.
  if (
    inSourcePosition < firstDashPositionOnAlignment ||
    firstDashPositionOnAlignment === 0
  ) {
    return inSourcePosition
  }

  // Otherwise, remove the part to the first dash and compute the position on
  // the remaining aligned sequence (& handle case were the position doesn't
  // exists).
  const positionOnRemainingAlignment = composePositionOnAlignment(
    alignedSequence.slice(firstDashPositionOnAlignment),
    inSourcePosition - firstDashPositionOnAlignment - 1
  )
  return positionOnRemainingAlignment === -1
    ? -1
    : positionOnRemainingAlignment + firstDashPositionOnAlignment
}

/**
 * Maximum width of the names of the tracks DOM `HTMLElements`.
 */
const trackNamesWidth = ref<number>(0)

/**
 * Width of the container element of the track names, express as a ratio to
 * apply on the width of the sequence group elements (e.g., if it is 3, this
 * means that the container element of the track names should be 3 times wider
 * than a sequence group element).
 */
const trackNamesContainerWidthRatio = computed(() =>
  Math.ceil(
    trackNamesWidth.value / (firstSequenceGroupElementBounding.width.value || 1)
  )
)

/**
 * Configuration for the bases conservation colouring.
 */
const basesConservationConfig = ref<BasesConservationConfigModel>({
  isEnabled: false,
  thresholds: { low: 0.25, high: 0.75 }
})

/**
 * Computes the conservation rate of the reference nucleotide in the provided
 * nucl. list, and derivates from it the colour to use for the background.
 * @param nucleotides The list of nucleotides on which to compute the
 *                    conservation colour.
 * @param referenceNucleotide The nucleotide for which  to compute the
 *                            conservation colour.
 * @returns The colour in which to set the background according to the
 *          computed conservation rate.
 */
const composeConservationColour = (
  nucleotides: string[],
  referenceNucleotide: string
): string => {
  const conservationRate = composeConservationRate(
    nucleotides,
    referenceNucleotide
  )
  if (conservationRate > basesConservationConfig.value.thresholds.high) {
    return BASE_CONSERVATION_COLOURS.high
  }
  if (conservationRate > basesConservationConfig.value.thresholds.low) {
    return BASE_CONSERVATION_COLOURS.middle
  }
  return BASE_CONSERVATION_COLOURS.low
}

/**
 * All the modifications present on the reference track sequences.
 */
const referenceTrackModifications = computed<AlignmentObjectWithTrackIdModel[]>(
  () =>
    props.alignment.tracks
      .filter((track) => track.isReference)
      .map(
        (track) =>
          track.objects &&
          Object.values(track.objects).map((object) => ({
            ...object,
            trackId: track.id
          }))
      )
      .flat()
      .filter(isDefined)
)

/**
 * Available options for track selection for conservation analysis.
 */
const conservationAnalysisTrackOptions = computed(() =>
  props.alignment.tracks
    .filter((track) => track.isReference && !track.isInformation)
    .map((track) => ({
      label: track.name || track.id,
      id: track.id
    }))
)

/**
 * A list of the types of the objects present on the sequences of the alignment.
 */
const conservationAnalysisObjectTypeOptions = computed(() =>
  _uniqBy(
    Array.from(
      props.alignment.tracks
        .filter((track) =>
          conservationAnalysisConfig.value.trackIds.includes(track.id)
        )
        .map(
          (track) =>
            track.objects &&
            Object.values(track.objects).map((object) =>
              object.type === undefined
                ? undefined
                : { label: object.type, type: object.type }
            )
        )
        .flat()
        .filter(isDefined)
    ),
    'type'
  )
)

/**
 * Configuration for the conservation analysis.
 */
const conservationAnalysisConfig = ref<ConservationAnalysisConfigModel>({
  trackIds: [],
  objectTypes: [],
  analysisType: null
})

/**
 * Update non-reactive values when config changes.
 */
watch(conservationAnalysisConfig, () => {
  setTimeout(update, 100)
})

/**
 * Conservation analysis, reactively updated when analysis parameters change.
 */
const conservationAnalysis = useConservationAnalysis(
  props.alignment.tracks,
  conservationAnalysisConfig
)

/**
 * Wether a conservation analysis is displayed or not.
 */
const isConservationAnalysisDisplayed = computed(
  () => conservationAnalysisConfig.value.analysisType !== null
)

/**
 * The value to apply to the conservation analysis track `display` CSS property.
 */
const conservationAnalysisTrackDisplay = computed(() => ({
  trackLabel: isConservationAnalysisDisplayed.value ? 'flex' : 'none',
  trackSequence: isConservationAnalysisDisplayed.value ? undefined : 'none'
}))
</script>

<template>
  <SequenceAlignmentToolbar
    v-model:nucleotide-groups-size-model="nucleotideGroupsSize"
    v-model:bases-conservation-config-model="basesConservationConfig"
    v-model:conservation-analysis-config-model="conservationAnalysisConfig"
    :bases-conservation-colours="BASE_CONSERVATION_COLOURS"
    :conservation-analysis-track-options="conservationAnalysisTrackOptions"
    :conservation-analysis-object-type-options="
      conservationAnalysisObjectTypeOptions
    "
  />

  <div
    class="mt-8 grid gap-4 text-2xl leading-[normal]"
    :style="{
      gridTemplateColumns: `${
        trackNamesContainerWidthRatio * nucleotideGroupsSize * 1.5
      }rem 1fr`
    }"
  >
    <!-- Track names -->
    <div>
      <SequenceAlignmentTrackNames
        v-model:track-names-width-model="trackNamesWidth"
        :tracks="tracksWithConservationTrack"
      />
      <SequenceAlignmentTrackNames
        v-for="line in range(lineCount - 1)"
        :key="line"
        :tracks="tracksWithConservationTrack"
      />
    </div>

    <div ref="sequenceContainerElement" class="relative">
      <!-- Track content -->
      <span
        v-for="(sequenceGroup, groupIndex) in splitAlignmentSequences"
        :key="groupIndex"
        ref="sequenceGroupElements"
        :data-group-index="groupIndex"
        class="relative z-10 mr-4 mt-8 inline-block select-none whitespace-nowrap font-mono"
        :style="{ width: `${nucleotideGroupsSize * 1.5}rem` }"
      >
        <span class="absolute -top-4 left-0 select-none text-sm text-slate-400">
          {{ groupIndex * nucleotideGroupsSize + 1 }}
        </span>
        <span
          v-for="(nucleotide, nucleotideIndexInGroup) in sequenceGroup"
          :key="groupIndex * nucleotideGroupsSize + nucleotideIndexInGroup"
          :data-nucleotide-index="
            groupIndex * nucleotideGroupsSize + nucleotideIndexInGroup
          "
          class="inline-flex flex-col"
        >
          <span
            v-for="(trackNucleotide, trackIndex) in nucleotide"
            :key="`${trackIndex}:${
              groupIndex * nucleotideGroupsSize + nucleotideIndexInGroup
            }`"
            v-tooltip.top="{
              value:
                composePositionOnSource(
                  alignment.tracks[trackIndex]?.sequence || '',
                  groupIndex * nucleotideGroupsSize + nucleotideIndexInGroup + 1
                )?.toString() || '<em>N/A</em>',
              pt: {
                text: {
                  style: { textAlign: 'center', fontWeight: '600' }
                }
              },
              escape: true,
              autoHide: false
            }"
            :class="[
              'relative mx-[.0625rem] cursor-help border-2 border-transparent px-0.5 pt-0.5',
              {
                'rounded-t-md': trackIndex === 0,
                'rounded-b-md':
                  trackIndex === props.alignment.tracks.length - 1,
                'conservation-track-sequence':
                  tracksWithConservationTrack[trackIndex]?.isInformation
              },
              isInAnyObject(
                trackIndex,
                groupIndex * nucleotideGroupsSize + nucleotideIndexInGroup + 1
              ) && [
                `text-${composeInObjectPositionColor(
                  trackIndex,
                  groupIndex * nucleotideGroupsSize + nucleotideIndexInGroup + 1
                )}-600`,
                'modification font-semibold'
              ]
            ]"
            :style="
              (basesConservationConfig.isEnabled &&
                trackNucleotide !== '-' &&
                !tracksWithConservationTrack[trackIndex]?.isInformation && {
                  backgroundColor: composeConservationColour(
                    nucleotide,
                    trackNucleotide
                  )
                }) ||
              undefined
            "
          >
            <span
              v-if="
                isObjectBoundary(
                  trackIndex,
                  groupIndex * nucleotideGroupsSize + nucleotideIndexInGroup + 1
                )
              "
              ref="boxBoundaryElements"
              :data-track-and-position="`${trackIndex}:${
                groupIndex * nucleotideGroupsSize + nucleotideIndexInGroup + 1
              }`"
            >
              {{ trackNucleotide }}
            </span>
            <span v-else>
              {{ trackNucleotide }}
            </span>
          </span>
        </span>
      </span>

      <!-- Object boxes -->
      <span
        v-for="(track, trackIndex) in tracksWithConservationTrack"
        :key="track.id"
      >
        <span
          v-for="[objectId, object] in Object.entries(track.objects || {})"
          :key="objectId"
          class="object"
        >
          <span
            v-for="fragmentIndex in range(
              tracksObjectsLineCount?.[trackIndex]?.[objectId] || 0
            )"
            :key="fragmentIndex"
            :style="boxStyles?.[trackIndex]?.[objectId]?.[fragmentIndex]"
            :class="[
              'object-fragment absolute z-10 h-8 border-y-2 bg-opacity-50 px-0.5 text-2xl mix-blend-multiply',
              [`!border-${object.color}-600`, `bg-${object.color}-100`],
              {
                'rounded-l-xl border-l-2': fragmentIndex === 0,
                'rounded-r-xl border-r-2':
                  tracksObjectsLineCount &&
                  fragmentIndex + 1 ===
                    tracksObjectsLineCount[trackIndex]?.[objectId]
              }
            ]"
          />
        </span>
      </span>
    </div>
  </div>
</template>

<style lang="scss">
.conservation-track-label {
  display: v-bind('conservationAnalysisTrackDisplay.trackLabel');
}

.conservation-track-sequence {
  display: v-bind('conservationAnalysisTrackDisplay.trackSequence');
}

.track-name-arrow {
  &::before {
    // Custom dashed border
    border-image: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAACCAYAAABllJ3tAAAATUlEQVQIHQFCAL3/AZSjuP8AAAAAAAAAAAAAAABsXUgBAAAAAAAAAAAAAAAAAgAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA7aYEBI+Q3/YAAAAASUVORK5CYII=')
      2 0 0 / 1 1 0 repeat;
  }
  &::after {
    background: rgb(255, 255, 255);
    background: linear-gradient(
      45deg,
      rgba(255, 255, 255, 0) 0%,
      rgba(255, 255, 255, 0) 50%,
      rgba(255, 255, 255, 1) 50%,
      rgba(255, 255, 255, 1) 100%
    );
  }
}
</style>
