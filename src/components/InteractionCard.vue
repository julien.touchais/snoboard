<script setup lang="ts">
/**
 * Vue imports
 */
import { computed } from 'vue'
/**
 * Components imports
 */
import InteractionCardCD from './InteractionCardCD.vue'
import InteractionCardHACA from './InteractionCardHACA.vue'
import Card from 'primevue/card'
/**
 * Types imports
 */
import { type InteractionCDModel } from './InteractionCardCD.vue'
import { type InteractionHACAModel } from './InteractionCardHACA.vue'
import { ModifType, SequenceClass } from '@/gql/codegen/graphql'

export interface InteractionDuplexFragmentModel {
  /** Sequence of the fragment */
  seq: string
  /** Start position of the fragment's binding in the duplex */
  start: number
  /** End position of the fragment's binding in the duplex */
  end: number
  /** Position of the fragment on the whole strand sequence */
  onParentPosition?: {
    start?: number
    end?: number
  }
}

export interface InteractionCardModel {
  /** The guide of the interaction */
  guide: {
    /** Guide ID */
    id: string
    /** Guide name */
    name?: string
    /** Guide subclass (e.g. CD-box or H/ACA-box) label, with markdown */
    subclass_label?: string
    /** Guide class, e.g. snoRNA or snRNA */
    class?: SequenceClass
  }
  /** The target of the interaction */
  target: {
    /** Target ID */
    id: string
    /** Target name */
    name?: string
    /** Target class, e.g. rRNA */
    class?: SequenceClass
    /** Target unit, e.g. LSU/SSU */
    unit?: string
  }
  /** The modification of the target caused by the interaction */
  modification: {
    /** Modification ID */
    id: string
    /** Modification name */
    name?: string
    /** Modification position on target */
    position: number
    /** Modified nucl. symbol after modification */
    symbol: string
    /** Modified nucl. symbol label after modification, with markdown */
    symbol_label: string
    /** Modification type, e.g. Psi or 2'-O-me */
    type?: ModifType
    /** Modification type short label, with markdown */
    type_short_label?: string
  }
  /** Duplexes formed in the interaction */
  duplexes: {
    /** First fragment of the duplex */
    primaryFragment: InteractionDuplexFragmentModel
    /** Second fragment of the duplex */
    secondaryFragment: InteractionDuplexFragmentModel
    /** Index of the duplex in the interaction */
    index: number
  }[]
}

/**
 * Component props.
 */
const props = defineProps<{
  /** The interaction to visualise. */
  interaction: InteractionCardModel | undefined
}>()

/**
 * Component slots.
 */
defineSlots<{
  /** Custom card title. */
  title: () => void
  /** Custom card subtitle. */
  subtitle: () => void
  /** Custom message to show when `interaction` prop is not defined. */
  'undefined-interaction-message': () => void
}>()

const CDFormattedInteraction = computed<InteractionCDModel | undefined>(() => {
  const duplex = props.interaction?.duplexes[0]

  if (!duplex?.secondaryFragment.onParentPosition?.end) return undefined

  return props.interaction?.modification.type === ModifType.Nm
    ? {
        guide: props.interaction.guide,
        target: props.interaction.target,
        modification: props.interaction.modification,
        fragments: {
          guide: {
            seq: duplex.primaryFragment.seq,
            onParentPosition: duplex.primaryFragment.onParentPosition
          },
          target: {
            seq: duplex.secondaryFragment.seq,
            onParentPosition: {
              start: duplex.secondaryFragment.onParentPosition.start,
              end: duplex.secondaryFragment.onParentPosition.end
            }
          }
        },
        duplex: {
          guide: {
            start: duplex.primaryFragment.start,
            end: duplex.primaryFragment.end
          },
          target: {
            start: duplex.secondaryFragment.start,
            end: duplex.secondaryFragment.end
          }
        }
      }
    : undefined
})

const HACAFormattedInteraction = computed<InteractionHACAModel | undefined>(
  () => {
    if (props.interaction?.modification.type === ModifType.Psi) {
      const duplexes = {
        before: props.interaction.duplexes.find((duplex) => duplex.index === 0),
        middle: props.interaction.duplexes.find((duplex) => duplex.index === 1),
        after: props.interaction.duplexes.find((duplex) => duplex.index === 2)
      }

      if (
        !duplexes.before?.secondaryFragment.onParentPosition?.start ||
        !duplexes.middle ||
        !duplexes.after
      )
        return undefined

      return {
        guide: props.interaction.guide,
        target: props.interaction.target,
        modification: props.interaction.modification,
        fragments: {
          guideFirst: {
            seq: duplexes.before.primaryFragment.seq,
            onParentPosition: duplexes.before.primaryFragment.onParentPosition
          },
          guideSecond: {
            seq: duplexes.after.primaryFragment.seq,
            onParentPosition: duplexes.after.primaryFragment.onParentPosition
          },
          target: {
            seq: duplexes.before.secondaryFragment.seq,
            onParentPosition: {
              start: duplexes.before.secondaryFragment.onParentPosition.start,
              end: duplexes.before.secondaryFragment.onParentPosition.end
            }
          }
        },
        duplexes: {
          before: {
            guide: {
              start: duplexes.before.primaryFragment.start,
              end: duplexes.before.primaryFragment.end
            },
            target: {
              start: duplexes.before.secondaryFragment.start,
              end: duplexes.before.secondaryFragment.end
            }
          },
          middle: {
            guideFirst: {
              start: duplexes.middle.primaryFragment.start,
              end: duplexes.middle.primaryFragment.end
            },
            guideSecond: {
              start: duplexes.middle.secondaryFragment.start,
              end: duplexes.middle.secondaryFragment.end
            }
          },
          after: {
            guide: {
              start: duplexes.after.primaryFragment.start,
              end: duplexes.after.primaryFragment.end
            },
            target: {
              start: duplexes.after.secondaryFragment.start,
              end: duplexes.after.secondaryFragment.end
            }
          }
        }
      }
    } else {
      return undefined
    }
  }
)
</script>

<template>
  <Card>
    <template v-if="$slots['title']" #title>
      <slot name="title"></slot>
    </template>
    <template v-if="$slots['subtitle']" #subtitle>
      <slot name="subtitle"></slot>
    </template>
    <template #content>
      <template v-if="interaction">
        <InteractionCardCD
          v-if="CDFormattedInteraction"
          :interaction="CDFormattedInteraction"
        />
        <InteractionCardHACA
          v-else-if="HACAFormattedInteraction"
          :interaction="HACAFormattedInteraction"
        />
        <div
          v-else
          class="rounded-lg border-2 border-dashed border-slate-300 p-4 italic text-slate-400"
        >
          Not enough information to compute the duplex.
        </div>
      </template>
      <template v-else>
        <slot name="undefined-interaction-message">
          Provide a guide, modification & interaction to visualise it.
        </slot>
      </template>
    </template>
  </Card>
</template>
