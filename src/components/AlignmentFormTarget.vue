<script setup lang="ts">
/**
 * Vue imports
 */
import { computed, ref, toRef } from 'vue'
/**
 * Components imports
 */
import BaseRenderedMarkdown from '@/components/BaseRenderedMarkdown.vue'
import Chip from 'primevue/chip'
import MultiSelect from 'primevue/multiselect'
import Dropdown from 'primevue/dropdown'
/**
 * Composables imports
 */
import { useQuery } from '@urql/vue'
/**
 * Other 3rd-party imports
 */
import {
  uniqWith as _uniqWith,
  isEqual as _isEqual,
  groupBy as _groupBy,
  mapValues as _mapValues
} from 'lodash-es'
/**
 * Utils imports
 */
import { targetAlignmentQuery } from '@/gql/queries'

interface TargetAlignmentSelectionModel {
  /** Currently selected target name. */
  targetName: string | undefined
  /** Currently selected organism IDs.*/
  organismIds: number[]
  /** Currently selected target IDs for alignment. */
  targetIds: string[]
}

/**
 * Component props
 */
defineProps<{
  /** The currently selected target IDs for alignment. */
  selectedTargetIdsModel?: string[]
}>()

/**
 * Component events.
 */
const emit = defineEmits<{
  /** Event used to update the `v-model:selectedTargetIdsModel` value. */
  'update:selectedTargetIdsModel': [selectedTargetIdsModel?: string[]]
}>()

/**
 * Current selection of parameters/filters for the target sequences to pick.
 */
const selection = ref<TargetAlignmentSelectionModel>({
  targetName: undefined,
  organismIds: [],
  targetIds: []
})

/**
 * Reactive urql GraphQL query object, updated with query state & response.
 */
const gqlQuery = useQuery({
  query: targetAlignmentQuery,
  variables: toRef(() => ({
    targetName: selection.value.targetName || '',
    organismIds: selection.value.organismIds.length
      ? selection.value.organismIds
      : undefined
  }))
})

/**
 * Available options for target name selection, independently of other fields'
 * selection.
 */
const targetNameOptionsGroups = computed(() =>
  Object.entries(
    _groupBy(
      _uniqWith(
        gqlQuery.data.value?.targetBase,
        (targetA, targetB) => targetA.name === targetB.name
      ).map((target) => ({
        label: target.name,
        value: target.name,
        groupLabel: target.unit || 'Other'
      })),
      'groupLabel'
    )
  ).map(([targetUnit, targets]) => ({ label: targetUnit, children: targets }))
)

/**
 * Available options for organism ID selection, independently of other fields'
 * selection.
 */
const organismIdOptionsBase = computed(() =>
  _uniqWith(gqlQuery.data.value?.organismsBase, _isEqual).map((organism) => ({
    label: organism.label,
    value: organism.id
  }))
)

/**
 * For each target name, an array of the IDs of the organisms which have at
 * least a target of this name registered.
 */
const organismIdsByTargetNameBase = computed(() =>
  _mapValues(
    _groupBy(_uniqWith(gqlQuery.data.value?.targetBase, _isEqual), 'name'),
    (targets) => targets.map((target) => target.genome?.organism?.id)
  )
)

/**
 * Available organism IDs, filtered based on other fields' selection (only
 * organism IDs which exists with selected options are present).
 */
const organismIdsFiltered = computed(
  () =>
    (selection.value.targetName &&
      organismIdsByTargetNameBase.value[selection.value.targetName]) ||
    []
)

/**
 * Available options for organism ID selection, with a `isDisabled` field on
 * absence in `organismIdsFiltered`.
 */
const organismIdOptionsWithDisabling = computed(() =>
  organismIdOptionsBase.value.map((organismIdOption) => ({
    ...organismIdOption,
    isDisabled: !organismIdsFiltered.value.some(
      (organismIdFiltered) => organismIdFiltered === organismIdOption.value
    )
  }))
)

/**
 * Available options for target ID selection for alignment given the other
 * fields selection.
 */
const targetIdOptions = computed(() =>
  gqlQuery.data.value?.selectableTargets.map((target) => ({
    label: target.id,
    optionLabel: `\`${target.id}\` • ${target.genome?.organism?.shortlabel}`,
    value: target.id
  }))
)

/**
 * Clear the selected target IDs.
 */
const clearTargetIds = () => {
  emit('update:selectedTargetIdsModel', (selection.value.targetIds = []))
}

/**
 * Remove an organism ID from the selected ones & clear the selected target IDs.
 */
const removeOrganismAndClearTargetIds = (organismId: number) => {
  const organismIdIndex = selection.value.organismIds.findIndex(
    (currOrganismId) => currOrganismId === organismId
  )
  organismIdIndex !== -1 &&
    selection.value.organismIds.splice(organismIdIndex, 1)
  clearTargetIds()
}

/**
 * Clear the selected organisms & target IDs.
 */
const clearOrganismsAndTargetIds = () => {
  selection.value.organismIds = selection.value.organismIds.filter(
    (organismId) =>
      selection.value.targetName &&
      organismIdsByTargetNameBase.value[selection.value.targetName]?.includes(
        organismId
      )
  )
  clearTargetIds()
}
</script>

<template>
  <div class="flex max-w-max flex-wrap gap-8">
    <div class="flex max-w-min flex-col gap-2">
      <h3 class="text-lg font-bold text-slate-700">Targets</h3>
      <Dropdown
        v-model="selection.targetName"
        :options="targetNameOptionsGroups"
        option-label="label"
        option-value="value"
        option-group-label="label"
        option-group-children="children"
        placeholder="Select a target..."
        filter
        :pt="{
          item: {
            style: {
              marginLeft: '1rem'
            }
          }
        }"
        @change="clearOrganismsAndTargetIds"
      />
    </div>

    <div
      v-tooltip.bottom="
        !selection.targetName && {
          value: 'First select a target to list its organisms.',
          autoHide: false,
          pt: { text: { style: { textAlign: 'center' } } }
        }
      "
      class="flex max-w-min flex-col gap-2"
    >
      <h3 class="text-lg font-bold text-slate-700">Organisms</h3>
      <MultiSelect
        v-model="selection.organismIds"
        :options="organismIdOptionsWithDisabling"
        option-label="label"
        option-value="value"
        option-disabled="isDisabled"
        multiple
        placeholder="Select organisms..."
        :max-selected-labels="3"
        display="chip"
        filter
        :disabled="!selection.targetName"
        @change="clearTargetIds"
      >
        <template #value>
          <Chip
            v-for="organismId in selection.organismIds.slice(0, 3)"
            :key="organismId"
            class="mr-1"
            removable
            @remove.stop="removeOrganismAndClearTargetIds(organismId)"
          >
            <BaseRenderedMarkdown
              :stringified-markdown="
                organismIdOptionsBase.find(
                  (organismIdOption) => organismIdOption.value === organismId
                )?.label || ''
              "
              class="my-1.5"
            />
          </Chip>
          <template v-if="selection.organismIds.length > 3">
            +{{ selection.organismIds.length - 3 }} others...
          </template>
        </template>
        <template #option="{ option }">
          <BaseRenderedMarkdown :stringified-markdown="option.label" />
        </template>
      </MultiSelect>
    </div>

    <div
      v-tooltip.bottom="
        !selection.targetName && {
          value: 'First select a target to list its sequences.',
          autoHide: false,
          pt: { text: { style: { textAlign: 'center' } } }
        }
      "
      class="flex max-w-min flex-col gap-2"
    >
      <h3 class="text-lg font-bold text-slate-700">Sequences</h3>
      <MultiSelect
        v-model="selection.targetIds"
        :options="targetIdOptions"
        option-label="label"
        option-value="value"
        multiple
        placeholder="Select sequences..."
        :max-selected-labels="3"
        filter
        :disabled="!selection.targetName"
        empty-message="First select a target to list its sequences"
        @update:model-value="
          (selectedTargetIds: string[]) =>
            $emit('update:selectedTargetIdsModel', selectedTargetIds)
        "
      >
        <template #option="{ option }">
          <BaseRenderedMarkdown :stringified-markdown="option.optionLabel" />
        </template>
        <template #value="{ value }">
          <span v-if="value.length" class="font-mono">
            {{ value.join(', ') }}
          </span>
        </template>
      </MultiSelect>
    </div>
  </div>
</template>
