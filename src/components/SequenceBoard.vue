<script setup lang="ts">
/**
 * Vue imports
 */
import { ref, computed, onMounted, watch } from 'vue'
/**
 * Components imports
 */
import BaseLegendButtonOverlay from '@/components/BaseLegendButtonOverlay.vue'
import BaseLockableTooltip from '@/components/BaseLockableTooltip.vue'
import Dropdown from 'primevue/dropdown'
import Button from 'primevue/button'
import Toolbar from 'primevue/toolbar'
import IconFa6SolidDna from '~icons/fa6-solid/dna'
import IconFa6SolidCircleCheck from '~icons/fa6-solid/circle-check'
import IconFa6RegularCopy from '~icons/fa6-regular/copy'
/**
 * Composables imports
 */
import {
  useElementBounding,
  useElementVisibility,
  useMutationObserver,
  useResizeObserver
} from '@vueuse/core'
/**
 * Other 3rd-party imports
 */
import { inRange as _inRange, mapValues as _mapValues } from 'lodash-es'
/**
 * Types imports
 */
import type { TailwindDefaultColorNameModel } from '@/typings/styleTypes'
import type { LegendItemModel } from '@/components/BaseLegendButtonOverlay.vue'
import type { RouteLocationRaw } from 'vue-router'
import type { StyleValue } from 'vue'
/**
 * Utils imports
 */
import { promisedWait } from '@/utils/promise'
import { range } from '@/utils/numbers'

/**
 * An object to highlight on the sequence.
 */
export interface objectModel {
  /** Start position of the object (starts at 1). */
  start: number
  /** End position of the object (included). */
  end: number
  /** Color(s) to use to highlight the object. */
  color: TailwindDefaultColorNameModel
  /** Object name. */
  name?: string
  /** Object type. */
  type?: string
  /** Link to go to when clicking on the object. */
  link?: RouteLocationRaw
  /** Wether to show a tooltip on hover on this object. */
  shouldTooltip?: boolean
}

/**
 * The state of the clipboard.
 */
enum ClipboardStateModel {
  /** Clipboard is idle. */
  Idle,
  /** Something is being copied into the clipboard. */
  Busy,
  /** Post-copy tasks are running before releasing the clipboard. */
  PostCopy
}

/**
 * Component props.
 */
const props = defineProps<{
  /** An identifier for the sequence to represent. */
  sequenceId: string
  /** The sequence to represent. */
  sequence: string
  /** Objects to highlight on the sequence. */
  objects?: { [objectId: string]: objectModel }
  /** The items of the legend to display for the objects. */
  legendItems?: LegendItemModel[]
}>()

/**
 * Component slots.
 */
defineSlots<{
  /** Custom whole legend items template. */
  'legend-item': (props: {
    /** The item to customise. */
    item: LegendItemModel
  }) => unknown
  /** Custom legend items icon template (what is being captioned). */
  'legend-item-icon': (props: {
    /** The item of which to customise the icon. */
    item: LegendItemModel
  }) => unknown
  /** Custom legend items title template. */
  'legend-item-title': (props: {
    /** The item of which to customise the title. */ item: LegendItemModel
  }) => unknown
  /** Custom legend items title template. */
  'legend-item-description': (props: {
    /** The item of which to customise the description. */ item: LegendItemModel
  }) => unknown
  /** Custom tooltip object items template */
  'tooltip-item': (props: {
    /** The object of the item to customise */
    object: objectModel
    /** The ID of the object of the item to customise */
    objectId: string
  }) => unknown
}>()

/**
 * Current state of the clipboard availability.
 */
const clipboardState = ref<ClipboardStateModel>(ClipboardStateModel.Idle)

/**
 * Copies the sequence to the clipboard, and handles the different states of it.
 */
const sequenceToClipboard = () => {
  clipboardState.value = ClipboardStateModel.Busy
  // Wait that the sequence is written to clipboard, or at least 400ms if shorter
  Promise.all([
    promisedWait(400),
    navigator.clipboard.writeText(props.sequence)
  ]).then(() => {
    clipboardState.value = ClipboardStateModel.PostCopy
    setTimeout(() => {
      clipboardState.value = ClipboardStateModel.Idle
    }, 1000)
  })
}

/**
 * Size (in term of nucl.) of the groups into which the sequence is divided.
 */
const nucleotideGroupsSize = ref(10)

/**
 * Number of groups in which the sequence is divided.
 */
const nucleotideGroupsCount = computed(() =>
  Math.ceil(props.sequence.length / nucleotideGroupsSize.value)
)

/**
 * The sequence, split in groups of selected size, as an array.
 */
const splitSequence = computed(() =>
  Array.from({ length: nucleotideGroupsCount.value }, (_, i) =>
    props.sequence.slice(
      i * nucleotideGroupsSize.value,
      (i + 1) * nucleotideGroupsSize.value
    )
  )
)

/**
 * Wether a nucleotide is in a given object or not.
 * @param position The position of the nucleotide to check.
 * @param objectId The id of the object to check for.
 * @returns `true` if position if in the given object, `false` otherwise.
 */
const isInObject = (position: number, objectId: string): boolean => {
  const object = props.objects?.[objectId]
  return !!object && _inRange(position, object.start, object.end + 1)
}

/**
 * Gets the IDs of the objects containing a nucleotide.
 * @param position The position of the nucleotide for which to get object IDs.
 * @returns A list of the IDs of the objects containing the nucleotide.
 */
const objectIdsContaining = (position: number): string[] =>
  props.objects
    ? Object.keys(props.objects).filter((objectId) =>
        isInObject(position, objectId)
      )
    : []

/**
 * Gets the objects containing a nucleotide.
 * @param position The position of the nucleotide for which to get objects.
 * @returns A list of tuples, `[object ID, object]` for each of the objects
 * containing the nucleotide.
 */
const objectsContaining = (position: number): [string, objectModel][] =>
  props.objects
    ? Object.entries(props.objects).filter(([objectId]) =>
        isInObject(position, objectId)
      )
    : []

/**
 * Whether a nucleotide is in any object.
 * @param position The position of the nucleotide to check.
 * @returns `true` if the nucleotide is in any object, `false` otherwise.
 */
const isInAnyObject = (position: number): boolean =>
  !!objectIdsContaining(position).length

/**
 * Set of all the boundary positions (start & end) of all objects on
 * the sequence.
 */
const objectsBoundaryPositions = computed(
  () =>
    props.objects &&
    Object.values(props.objects).reduce(
      (objectsBoundaryPositions, object) =>
        objectsBoundaryPositions.add(object.start).add(object.end),
      new Set<number>()
    )
)

/**
 * Whether a nucleotide is a boundary position (start or end) of an object.
 * @param position The position of the nucleotide to check.
 * @returns `true` if the nucleotide is the start of an object, `false` otherwise.
 */
const isObjectBoundary = (position: number): boolean =>
  !!objectsBoundaryPositions.value?.has(position)

/**
 * Composes the Tailwind color name in which to paint a nucleotide based on its
 * position.
 * @param position The position for which to compose the color.
 * @returns `slate` if there is no or several object at the given position, the
 * Tailwind color name of the object if there is only one.
 */
const composeInObjectPositionColor = (
  position: number
): TailwindDefaultColorNameModel => {
  const objectIds = objectIdsContaining(position)
  // If only one object, use its color, otherwise, use 'slate'
  return objectIds.length === 1 && objectIds[0]
    ? props.objects?.[objectIds[0]]?.color || 'slate'
    : 'slate'
}

/**
 * Sequence & the directions labels container DOM `HTMLElement`.
 */
const sequenceAndLabelsContainerElement = ref<HTMLElement>()
/**
 * Sequence container DOM `HTMLElement`.
 */
const sequenceContainerElement = ref<HTMLElement>()
/**
 * Split sequence groups DOM `HTMLElement` array.
 */
const sequenceGroupElements = ref<HTMLElement[]>([])
/**
 * First & last nucleotides of each object DOM `HTMLElement` array.
 */
const objectBoundaryElements = ref<HTMLElement[]>([])

/**
 * Last split sequence group DOM `HTMLElement`.
 */
const lastSequenceGroupElement = computed(() =>
  sequenceGroupElements.value.length
    ? sequenceGroupElements.value[sequenceGroupElements.value.length - 1]
    : undefined
)

/**
 * Last nucleotide DOM `HTMLElement`.
 */
const lastNucleotideElement = computed(
  () =>
    lastSequenceGroupElement.value?.lastElementChild as
      | HTMLElement
      | null
      | undefined
)

/**
 * Bounding box of last nucleotide DOM `HTMLElement`.
 */
const lastNucleotideElementBounding = useElementBounding(lastNucleotideElement)

/**
 * Bounding box of sequence & the directions labels container DOM `HTMLElement`.
 */
const sequenceAndLabelsContainerElementBounding = useElementBounding(
  sequenceAndLabelsContainerElement
)

/**
 * Inline style of the `3'` label.
 */
const threePrimeLabelStyle = computed(() => ({
  left: `calc(${
    lastNucleotideElementBounding.left.value -
    sequenceAndLabelsContainerElementBounding.left.value +
    lastNucleotideElementBounding.width.value
  }px + 0.5rem)`,
  top: `calc(${
    sequenceAndLabelsContainerElementBounding.height.value -
    lastNucleotideElementBounding.height.value
  }px + 0.0rem)`
}))

/**
 *  Height of a line of sequence, adapted to both Chromium & Firefox.
 */
const lineHeight = computed(
  () =>
    lastSequenceGroupElement.value &&
    Math.max(
      parseInt(
        window.getComputedStyle(lastSequenceGroupElement.value).lineHeight
      ) || 0,
      lastSequenceGroupElement.value?.offsetHeight +
        parseInt(
          window.getComputedStyle(lastSequenceGroupElement.value).marginTop
        )
    )
)

/**
 * Dictionary of the number of line on which each object spreads, by object ID.
 */
const objectsLineCount = ref<{
  [objectId: string]: number | undefined
}>()

/**
 * Updates the number of line on which each object spreads.
 * @returns The updated number of line on which each object spreads.
 * @description Retrieves the line count by getting the difference between the
 * top of the last nucleotide of the object and the top of the first one, and
 * dividing by the line height.
 */
const updateObjectsLineCount = () =>
  (objectsLineCount.value = _mapValues(props.objects, (object) => {
    // Parent DOM elements of the first and last nucleotide of the object
    const objectStartNucleotideElementParent =
      objectBoundaryElements.value.find(
        (element) => element.dataset.position === object.start.toString()
      )?.offsetParent
    const objectEndNucleotideElementParent = objectBoundaryElements.value.find(
      (element) => element.dataset.position === object.end.toString()
    )?.offsetParent

    if (
      !(
        objectStartNucleotideElementParent instanceof HTMLElement &&
        objectEndNucleotideElementParent instanceof HTMLElement
      )
    ) {
      return undefined
    }

    return (
      lineHeight.value &&
      Math.round(
        (objectEndNucleotideElementParent.offsetTop -
          objectStartNucleotideElementParent.offsetTop) /
          lineHeight.value
      ) + 1
    )
  }))

/**
 * Width of the sequence container.
 */
const sequenceContainerContentWidth = ref<number>()

useMutationObserver(
  sequenceContainerElement,
  (mutations) => {
    if (mutations[0]) {
      // console.log(mutations[0])
    }
  },
  {
    childList: true
  }
)

/**
 * Updates the width of the sequence container.
 */
const updateSequenceContainerContentWidth =
  (): typeof sequenceContainerContentWidth.value => {
    const firstSequenceGroupElement = sequenceGroupElements.value[0]
    if (!firstSequenceGroupElement) return

    const sequenceContainerElementWidth =
      sequenceContainerElement.value?.offsetWidth
    if (!sequenceContainerElementWidth) return

    const sequenceGroupElementRightMargin = parseInt(
      window.getComputedStyle(firstSequenceGroupElement).marginRight
    )
    if (!sequenceGroupElementRightMargin) return

    const sequenceGroupElementWidth =
      firstSequenceGroupElement.offsetWidth + sequenceGroupElementRightMargin
    if (!sequenceGroupElementWidth) return

    return (sequenceContainerContentWidth.value =
      sequenceContainerElementWidth -
      ((sequenceContainerElementWidth % sequenceGroupElementWidth) +
        sequenceGroupElementRightMargin))
  }

/**
 * Updates all non-reactive values.
 */
const update = (): void => {
  updateObjectsLineCount()
  updateSequenceContainerContentWidth()
}

/**
 * Exposes methods to manually trigger updates.
 */
defineExpose({ update })

/**
 * Whether the sequence is visible or not.
 */
const isSequenceContainerElementVisible = useElementVisibility(
  sequenceContainerElement
)

/**
 * Sets hook to update on component mount.
 */
onMounted(update)

/**
 * Sets an observer to update on container resize.
 */
useResizeObserver(sequenceContainerElement, () => {
  update()
  setTimeout(() => {
    update()
  }, 100)
})

/**
 * Sets a watcher to update when resuming sequence visibility.
 */
watch(
  [isSequenceContainerElementVisible, () => props.sequenceId],
  ([isSequenceContainerElementVisible, newSequenceId], [, oldSequenceId]) => {
    if (isSequenceContainerElementVisible || newSequenceId !== oldSequenceId) {
      setTimeout(() => {
        update()
      }, 100)
    }
  }
)

/**
 * Bounding box of sequence container DOM `HTMLElement`.
 */
const sequenceContainerElementBounding = useElementBounding(
  sequenceContainerElement
)

/**
 * For each sequence group first position label, the bounding box of its
 * reference sequence group DOM element.
 */
const groupNumberReferenceSequenceGroupElementsBounding = computed(() =>
  Array.from({ length: nucleotideGroupsCount.value }, (_, groupNumberIndex) =>
    useElementBounding(
      sequenceGroupElements.value.find(
        (sequenceGroupElement) =>
          sequenceGroupElement.dataset.groupIndex ===
          groupNumberIndex.toString()
      )
    )
  )
)

/**
 * The style to apply to the sequence group first position label DOM element.
 */
const groupNumberStyles = computed(() =>
  groupNumberReferenceSequenceGroupElementsBounding.value.map<StyleValue>(
    (groupNumberReferenceSequenceGroupElementBounding) => ({
      left: `${
        groupNumberReferenceSequenceGroupElementBounding.left.value -
        sequenceContainerElementBounding.left.value
      }px`,
      top: `calc(${
        groupNumberReferenceSequenceGroupElementBounding.top.value -
        sequenceContainerElementBounding.top.value
      }px - 1.25rem)`
    })
  )
)

/**
 * The style to apply to each fragment of each of the object boxes.
 */
const objectBoxStyles = computed(() =>
  // Map objects to the styles of each of their fragment
  _mapValues(props.objects, (object, objectId) => {
    // DOM element of the first and last nucleotides of the object
    const objectStartNucleotideElement = objectBoundaryElements.value.find(
      (element) => element.dataset.position === object.start.toString()
    )
    const objectEndNucleotideElement = objectBoundaryElements.value.find(
      (element) => element.dataset.position === object.end.toString()
    )

    if (
      !(
        objectStartNucleotideElement?.offsetParent instanceof HTMLElement &&
        objectEndNucleotideElement?.offsetParent instanceof HTMLElement
      )
    ) {
      return
    }

    // Absolute positions of the DOM elements of the first and last nucleotide
    // of the object
    const objectStartNucleotideElementTop =
      objectStartNucleotideElement.offsetParent.offsetTop +
      (objectStartNucleotideElement?.offsetTop || 0)
    const objectStartNucleotideElementLeft =
      objectStartNucleotideElement.offsetParent.offsetLeft +
      (objectStartNucleotideElement?.offsetLeft || 0)
    const objectEndNucleotideElementLeft =
      objectEndNucleotideElement.offsetParent.offsetLeft +
      (objectEndNucleotideElement.offsetLeft || 0)
    const objectEndNucleotideElementRight =
      objectEndNucleotideElementLeft +
      (objectEndNucleotideElement.offsetWidth || 0)

    // Number of fragment in which the current object box is divided
    const objectBoxFragmentsCount = objectsLineCount.value?.[objectId] || 0

    // Array of length equals # of fragments, containing style for each of them
    return Array.from(
      { length: objectBoxFragmentsCount },
      (_, fragmentIndex): StyleValue => ({
        top: `${
          objectStartNucleotideElementTop +
          fragmentIndex * (lineHeight.value || 0)
        }px`,
        left:
          fragmentIndex === 0
            ? `calc(${objectStartNucleotideElementLeft}px - 0.125rem)`
            : '0',
        right: `calc(${
          (sequenceContainerElementBounding.width.value || 0) -
          (fragmentIndex === objectBoxFragmentsCount - 1
            ? objectEndNucleotideElementRight
            : sequenceContainerContentWidth.value || 0)
        }px - 0.125rem)`
      })
    )
  })
)

/**
 * The objects currently being hovered.
 */
const hoveredObjects = ref(new Set<[string, objectModel]>())

/**
 * The objects currently being hovered.
 */
const lockedTooltipObjects = ref<Set<[string, objectModel]>>()

/**
 * DOM elements of objects to display in the tooltip.
 */
const tooltipObjects = computed(
  () => lockedTooltipObjects.value || hoveredObjects.value
)

/**
 * Lockable tooltip component.
 */
const tooltipComponent = ref<InstanceType<typeof BaseLockableTooltip>>()

/**
 * Locks the tooltip and the values to display in it.
 */
const lockTooltip = () => {
  lockedTooltipObjects.value = new Set(hoveredObjects.value)
  tooltipComponent.value?.lockTooltipIfUnlocked()
}
</script>

<template>
  <Toolbar>
    <template #start>
      <label class="flex flex-col items-start text-sm">
        Group length
        <Dropdown
          v-model="nucleotideGroupsSize"
          :options="[5, 10, 20, 50, 100]"
          class="mt-2 w-52"
          @hide="update"
        >
          <template #option="{ option }">{{ option }} nucleotides</template>
          <template #value="{ value }">{{ value }} nucleotides</template>
        </Dropdown>
      </label>
    </template>

    <template v-if="legendItems" #center>
      <div class="relative flex flex-col items-center gap-2">
        <BaseLegendButtonOverlay button-text="Legend" :items="legendItems">
          <template #item="{ item }">
            <slot name="legend-item" :item="item" />
          </template>
          <template #item-icon="{ item }">
            <slot name="legend-item-icon" :item="item" />
          </template>
          <template #item-title="{ item }">
            <slot name="legend-item-title" :item="item" />
          </template>
          <template #item-description="{ item }">
            <slot name="legend-item-description" :item="item" />
          </template>
        </BaseLegendButtonOverlay>
        <span class="text-sm italic text-slate-600">
          Click on an object to lock its tooltip in place.
        </span>
      </div>
    </template>

    <template #end>
      <Button
        outlined
        severity="secondary"
        :class="[
          clipboardState === ClipboardStateModel.PostCopy &&
            '!bg-lime-100 !text-lime-700',
          'flex min-w-[18ch] justify-center'
        ]"
        :loading="clipboardState === ClipboardStateModel.Busy"
        @click="sequenceToClipboard"
      >
        <span
          v-if="clipboardState === ClipboardStateModel.Busy"
          class="mr-2 h-5 overflow-hidden text-xl"
        >
          <icon-fa6-solid-dna class="animate-[dnaSpin_.5s_linear_infinite]" />
          <icon-fa6-solid-dna class="animate-[dnaSpin_.5s_linear_infinite]" />
        </span>
        <icon-fa6-solid-circle-check
          v-else-if="clipboardState === ClipboardStateModel.PostCopy"
          class="mr-2 text-xl"
        />
        <icon-fa6-regular-copy v-else class="mr-2 text-xl" />
        {{
          clipboardState === ClipboardStateModel.Busy
            ? 'Copying...'
            : clipboardState === ClipboardStateModel.PostCopy
              ? 'Copied !'
              : 'Copy sequence'
        }}
      </Button>
    </template>
  </Toolbar>

  <div ref="sequenceAndLabelsContainerElement" class="relative px-8">
    <span class="absolute left-0 top-8 italic text-slate-400">5' &rarr;</span>
    <div
      ref="sequenceContainerElement"
      class="sequence-parent relative mx-2 overflow-x-auto pb-2 pt-4 font-mono text-2xl leading-[4.25rem]"
    >
      <span
        v-for="(sequenceGroup, groupIndex) in splitSequence"
        ref="sequenceGroupElements"
        :key="groupIndex"
        :data-group-index="groupIndex"
        class="relative z-10 mr-4 mt-8 whitespace-nowrap"
        :style="{ width: `${nucleotideGroupsSize * 1.5}rem` }"
      >
        <template
          v-for="(nucleotide, nucleotideIndex) in sequenceGroup"
          :key="nucleotideIndex"
        >
          <span
            v-if="
              isObjectBoundary(
                nucleotideIndex + groupIndex * nucleotideGroupsSize + 1
              )
            "
            ref="objectBoundaryElements"
            :data-position="
              nucleotideIndex + groupIndex * nucleotideGroupsSize + 1
            "
            :class="[
              'relative mx-[.0625rem] border-2 border-transparent px-0.5 pt-0.5',
              [
                `text-${composeInObjectPositionColor(
                  nucleotideIndex + groupIndex * nucleotideGroupsSize + 1
                )}-600`,
                'font-semibold'
              ]
            ]"
          >
            <span
              v-if="
                objectsContaining(
                  nucleotideIndex + groupIndex * nucleotideGroupsSize + 1
                ).find(([, object]) => object.shouldTooltip)
              "
              :class="[
                'absolute -bottom-0.5 -left-[0.1875rem] -right-[0.1875rem] -top-0.5',
                objectsContaining(
                  nucleotideIndex + groupIndex * nucleotideGroupsSize + 1
                ).find(([, object]) => object.link)
                  ? 'cursor-pointer'
                  : 'cursor-help'
              ]"
              @mouseenter="
                objectsContaining(
                  nucleotideIndex + groupIndex * nucleotideGroupsSize + 1
                ).forEach(
                  ([objectId, object]) =>
                    object.shouldTooltip &&
                    hoveredObjects.add([objectId, object])
                )
              "
              @mouseleave="hoveredObjects.clear()"
              @click="lockTooltip"
            />{{ nucleotide }}
          </span>
          <span
            v-else
            :class="[
              'relative mx-[.0625rem] border-2 border-transparent px-0.5 pt-0.5',
              isInAnyObject(
                nucleotideIndex + groupIndex * nucleotideGroupsSize + 1
              ) && [
                `text-${composeInObjectPositionColor(
                  nucleotideIndex + groupIndex * nucleotideGroupsSize + 1
                )}-600`,
                'font-semibold'
              ]
            ]"
          >
            <span
              v-if="
                objectsContaining(
                  nucleotideIndex + groupIndex * nucleotideGroupsSize + 1
                ).find(([, object]) => object.shouldTooltip)
              "
              :class="[
                'absolute -bottom-0.5 -left-[0.1875rem] -right-[0.1875rem] -top-0.5',
                objectsContaining(
                  nucleotideIndex + groupIndex * nucleotideGroupsSize + 1
                ).find(([, object]) => object.link)
                  ? 'cursor-pointer'
                  : 'cursor-help'
              ]"
              @mouseenter="
                objectsContaining(
                  nucleotideIndex + groupIndex * nucleotideGroupsSize + 1
                ).forEach(
                  ([objectId, object]) =>
                    object.shouldTooltip &&
                    hoveredObjects.add([objectId, object])
                )
              "
              @mouseleave="hoveredObjects.clear()"
              @click="lockTooltip"
            />{{ nucleotide }}
          </span>
        </template>
      </span>

      <span
        v-for="(_sequenceGroup, groupIndex) in splitSequence"
        :key="groupIndex"
        class="absolute select-none text-sm text-slate-400"
        :style="groupNumberStyles[groupIndex]"
      >
        {{ groupIndex * nucleotideGroupsSize + 1 }}
      </span>

      <span
        v-for="[objectId, object] in Object.entries(objects || {})"
        :key="objectId"
        class="object"
      >
        <span
          v-for="fragmentIndex in range(objectsLineCount?.[objectId] || 0)"
          :key="fragmentIndex"
          :style="objectBoxStyles?.[objectId]?.[fragmentIndex]"
          :class="[
            'object-fragment absolute h-8 border-y-2 bg-opacity-50 px-0.5 text-2xl mix-blend-multiply',
            [`!border-${object.color}-600`, `bg-${object.color}-100`],
            {
              'rounded-l-xl border-l-2': fragmentIndex === 0,
              'rounded-r-xl border-r-2':
                objectsLineCount &&
                fragmentIndex + 1 === objectsLineCount[objectId]
            },
            object.shouldTooltip &&
              (object.link ? 'cursor-pointer' : 'cursor-help')
          ]"
        />
      </span>

      <BaseLockableTooltip
        ref="tooltipComponent"
        :show="!!hoveredObjects.size"
        class="z-20 max-w-min font-sans text-base shadow-xl"
        @unlock="lockedTooltipObjects = undefined"
      >
        <ul class="flex flex-col gap-1">
          <li v-for="(object, index) in tooltipObjects" :key="index">
            <RouterLink
              v-if="object[1].link"
              :to="object[1].link"
              :class="[`text-${object[1].color}-600`, 'whitespace-nowrap']"
            >
              <slot
                name="tooltip-item"
                :object="object[1]"
                :object-id="object[0]"
              >
                {{ object[1].name || object[1].link }}
                {{ object[1].type && ` - ${object[1].type}` }}
              </slot>
            </RouterLink>
            <span v-else>
              <slot
                name="tooltip-item"
                :object="object[1]"
                :object-id="object[0]"
              >
                {{ object[1].name || object[1].link }}
                {{ object[1].type && ` - ${object[1].type}` }}
              </slot>
            </span>
          </li>
        </ul>
      </BaseLockableTooltip>
    </div>
    <span class="absolute italic text-slate-400" :style="threePrimeLabelStyle"
      >&rarr; 3'</span
    >
  </div>
</template>

<style lang="scss">
// .sequence-parent:has(.object:hover) {
//   // color: var(--gray-300);
//
//   .object-fragment {
//     border-color: var(--gray-300);
//     background-color: var(--gray-100);
//   }
// }

@-moz-document url-prefix() {
  .sequence-parent {
    line-height: normal;
    padding-top: 0;
    > span {
      display: inline-block;
    }
  }
}
</style>
