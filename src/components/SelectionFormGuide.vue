<script setup lang="ts">
/**
 * Vue imports
 */
import { computed, ref, toRef } from 'vue'
/**
 * Components imports
 */
import BaseRenderedMarkdown from './BaseRenderedMarkdown.vue'
import Chip from 'primevue/chip'
import SelectButton from 'primevue/selectbutton'
import MultiSelect from 'primevue/multiselect'
import IconFa6SolidCircleQuestion from '~icons/fa6-solid/circle-question'
/**
 * Composables imports
 */
import { useQuery } from '@urql/vue'
/**
 * Other 3rd-party imports
 */
import { uniqWith as _uniqWith, isEqual as _isEqual } from 'lodash-es'
/**
 * Types imports
 */
import { GuideClass, ModifType } from '@/gql/codegen/graphql'
/**
 * Utils imports
 */
import { guideSelectionQuery } from '@/gql/queries'
import { isNonNullish } from '@/typings/typeUtils'

/**
 * A guide selection.
 */
export interface GuideSelectionModel {
  /** Currently selected guide subclasses. */
  guideSubclasses: GuideClass[]
  /** Currently selected guide subclass labels. */
  guideSubclassLabels: string[]
  /** Currently selected target names. */
  targetNames: string[]
  /** Currently selected modification types. */
  modificationTypes: ModifType[]
  /** Currently selected organism IDs.*/
  organismIds: number[]
}

/**
 * Component props
 */
defineProps<{
  /** The current selection. */
  selectionModel?: GuideSelectionModel
}>()

/**
 * Component events.
 */
defineEmits<{
  /** Event used to update the `v-model:selectionModel` value. */
  'update:selectionModel': [selectionModel: GuideSelectionModel]
}>()

/**
 * Current selection of parameters/filters for the guides to pick.
 */
const selection = ref<GuideSelectionModel>({
  guideSubclasses: [],
  guideSubclassLabels: [],
  targetNames: [],
  modificationTypes: [],
  organismIds: []
})

/**
 * Reactive urql GraphQL query object, updated with query state & response.
 */
const gqlQuery = useQuery({
  query: guideSelectionQuery,
  variables: toRef(() => ({
    guideSubclasses: selection.value.guideSubclasses?.length
      ? selection.value.guideSubclasses
      : undefined,
    targetNames: selection.value.targetNames?.length
      ? selection.value.targetNames
      : undefined,
    modificationTypes: selection.value.modificationTypes?.length
      ? selection.value.modificationTypes
      : undefined,
    organismsIds: selection.value.organismIds?.length
      ? selection.value.organismIds
      : undefined
  }))
})

/**
 * Available options for guide subclass selection, independently of other fields'
 * selection.
 */
const guideSubclassOptionsBase = computed(() =>
  _uniqWith(gqlQuery.data.value?.guidesBase, _isEqual).map((guide) => ({
    label: guide.subclass_label,
    value: guide.subclass
  }))
)

/**
 * The guide subclasses, associated to their labels.
 */
const guideSubclassLabelBySubclass = computed<{
  [guideSubclass: string]: string
}>(() =>
  guideSubclassOptionsBase.value.reduce(
    (guideSubclassLabelBySubclass, guideSubclassOption) => ({
      ...guideSubclassLabelBySubclass,
      [guideSubclassOption.value]: guideSubclassOption.label
    }),
    {}
  )
)

/**
 * Available guide subclasses, filtered based on other fields' selection (only guide
 * subclasses which exists with selected options are present).
 */
const guideSubclassesFiltered = computed(() =>
  _uniqWith(gqlQuery.data.value?.guides, _isEqual).map(
    (guide) => guide.subclass
  )
)

/**
 * Available options for guide subclass selection, with a `isDisabled` field on
 * absence in `guideSubclassesFiltered`.
 */
const guideSubclassOptionsWithDisabling = computed(() =>
  guideSubclassOptionsBase.value.map((guideSubclassOption) => ({
    ...guideSubclassOption,
    isDisabled: !guideSubclassesFiltered.value.some(
      (guideSubclassFiltered) =>
        guideSubclassFiltered === guideSubclassOption.value
    )
  }))
)

/**
 * Available options for target name selection, independently of other fields'
 * selection.
 */
const targetNameOptionsBase = computed(() =>
  _uniqWith(gqlQuery.data.value?.targetsBase, _isEqual).map((target) => ({
    label: target.name,
    value: target.name,
    groupLabel: target.unit || 'Other'
  }))
)

/**
 * Available target names, filtered based on other fields' selection (only
 * target names which exists with selected options are present).
 */
const targetNamesFiltered = computed(() =>
  _uniqWith(gqlQuery.data.value?.targets, _isEqual).map((target) => target.name)
)

/**
 * Available options for target name selection, with a `isDisabled` field on
 * absence in `targetNamesFiltered`, and grouped.
 */
const targetNameOptionsWithDisablingGroups = computed(() =>
  targetNameOptionsBase.value.reduce(
    (targetNameOptionsWithDisablingGroups: any[], targetNameOption) => {
      const targetNameOptionGroupIndex =
        targetNameOptionsWithDisablingGroups.findIndex(
          (group) => group.label === targetNameOption.groupLabel
        )
      return targetNameOptionGroupIndex !== -1
        ? [
            ...targetNameOptionsWithDisablingGroups.slice(
              0,
              targetNameOptionGroupIndex
            ),
            {
              ...targetNameOptionsWithDisablingGroups[
                targetNameOptionGroupIndex
              ],
              children: [
                ...targetNameOptionsWithDisablingGroups[
                  targetNameOptionGroupIndex
                ].children,
                {
                  ...targetNameOption,
                  isDisabled: !targetNamesFiltered.value.some(
                    (targetNameFiltered) =>
                      targetNameFiltered === targetNameOption.value
                  )
                }
              ]
            },
            ...targetNameOptionsWithDisablingGroups.slice(
              targetNameOptionGroupIndex + 1
            )
          ]
        : [
            ...targetNameOptionsWithDisablingGroups,
            {
              label: targetNameOption.groupLabel,
              children: [
                {
                  ...targetNameOption,
                  isDisabled: !targetNamesFiltered.value.some(
                    (targetNameFiltered) =>
                      targetNameFiltered === targetNameOption.value
                  )
                }
              ]
            }
          ]
    },
    []
  )
)

/**
 * Available options for modification type selection, independently of other
 * fields' selection.
 */
const modificationTypeOptionsBase = computed(() =>
  _uniqWith(gqlQuery.data.value?.modificationsBase, _isEqual).map(
    (modification) => ({
      label: modification.type_label,
      value: modification.type
    })
  )
)

/**
 * Available modification types, filtered based on other fields' selection (only
 * modification types which exists with selected options are present).
 */
const modificationTypesFiltered = computed(() =>
  _uniqWith(gqlQuery.data.value?.modifications, _isEqual).map(
    (modification) => modification.type
  )
)

/**
 * Available options for modification type selection, with a `isDisabled` field
 * on absence in `modificationTypesFiltered`.
 */
const modificationTypeOptionsWithDisabling = computed(() =>
  modificationTypeOptionsBase.value.map((modificationTypeOption) => ({
    ...modificationTypeOption,
    isDisabled: !modificationTypesFiltered.value.some(
      (modificationTypeFiltered) =>
        modificationTypeFiltered === modificationTypeOption.value
    )
  }))
)

/**
 * Available options for organism ID selection, independently of other fields'
 * selection.
 */
const organismIdOptionsBase = computed(() =>
  _uniqWith(gqlQuery.data.value?.organismsBase, _isEqual).map((organism) => ({
    label: organism.label,
    value: organism.id
  }))
)

/**
 * Available organism IDs, filtered based on other fields' selection (only
 * organism IDs which exists with selected options are present).
 */
const organismIdsFiltered = computed(() =>
  _uniqWith(gqlQuery.data.value?.organisms, _isEqual).map(
    (organism) => organism.id
  )
)

/**
 * Available options for organism ID selection, with a `isDisabled` field on
 * absence in `organismIdsFiltered`.
 */
const organismIdOptionsWithDisabling = computed(() =>
  organismIdOptionsBase.value.map((organismIdOption) => ({
    ...organismIdOption,
    isDisabled: !organismIdsFiltered.value.some(
      (organismIdFiltered) => organismIdFiltered === organismIdOption.value
    )
  }))
)
</script>

<template>
  <div
    class="grid max-w-max grid-cols-[max-content_1fr] grid-rows-4 items-center gap-x-16 gap-y-8"
  >
    <h3 class="text-lg font-bold">
      Guide subclasses
      <span
        v-tooltip.right="{
          value: 'Only guides of the selected subclasses will be retained.',
          pt: { text: 'text-xs' }
        }"
        class="-mt-1 inline-block align-top text-sm text-slate-600"
      >
        <icon-fa6-solid-circle-question class="inline" />
      </span>
    </h3>

    <SelectButton
      v-model="selection.guideSubclasses"
      :options="guideSubclassOptionsWithDisabling"
      option-label="label"
      option-value="value"
      option-disabled="isDisabled"
      multiple
      class="mr-auto"
      @update:model-value="
        (selectedGuideSubclasses: GuideClass[]) => {
          selection.guideSubclassLabels = selectedGuideSubclasses
            .map((guideSubclass) => guideSubclassLabelBySubclass[guideSubclass])
            .filter(isNonNullish)
          $emit('update:selectionModel', {
            ...selection,
            guideSubclasses: selectedGuideSubclasses,
            guideSubclassLabels: selection.guideSubclassLabels
          })
        }
      "
    >
      <template #option="{ option }">
        <BaseRenderedMarkdown
          :stringified-markdown="option.label"
          class="font-medium"
        />
      </template>
    </SelectButton>

    <h3 class="text-lg font-bold">
      Targets
      <span
        v-tooltip.right="{
          value:
            'Only guides which interact with the selected targets will be retained.',
          pt: { text: 'text-xs' }
        }"
        class="-mt-1 inline-block align-top text-sm text-slate-600"
      >
        <icon-fa6-solid-circle-question class="inline" />
      </span>
    </h3>

    <MultiSelect
      v-model="selection.targetNames"
      :options="targetNameOptionsWithDisablingGroups"
      option-label="label"
      option-value="value"
      option-disabled="isDisabled"
      option-group-label="label"
      option-group-children="children"
      multiple
      class="mr-auto"
      placeholder="Select targets..."
      :max-selected-labels="3"
      display="chip"
      filter
      @update:model-value="
        (selectedTargetNames: string[]) =>
          $emit('update:selectionModel', {
            ...selection,
            targetNames: selectedTargetNames
          })
      "
    >
      <template #value>
        <Chip
          v-for="targetName in selection.targetNames.slice(0, 3)"
          :key="targetName"
          class="mr-1"
          :label="targetName"
          removable
          @remove.stop="
            $emit('update:selectionModel', {
              ...selection,
              targetNames: (selection.targetNames =
                selection.targetNames.filter(
                  (currTargetName) => currTargetName !== targetName
                ))
            })
          "
        />
        <template v-if="selection.targetNames.length > 3">
          +{{ selection.targetNames.length - 3 }} others...
        </template>
      </template>
    </MultiSelect>

    <h3 class="text-lg font-bold">
      Guided modification types
      <span
        v-tooltip.right="{
          value:
            'Only guides which guide at least a modification of the selected types will be retained.',
          pt: { text: 'text-xs' }
        }"
        class="-mt-1 inline-block align-top text-sm text-slate-600"
      >
        <icon-fa6-solid-circle-question class="inline" />
      </span>
    </h3>

    <SelectButton
      v-model="selection.modificationTypes"
      :options="modificationTypeOptionsWithDisabling"
      option-label="label"
      option-value="value"
      option-disabled="isDisabled"
      multiple
      class="mr-auto"
      @update:model-value="
        (selectedModificationTypes: ModifType[]) =>
          $emit('update:selectionModel', {
            ...selection,
            modificationTypes: selectedModificationTypes
          })
      "
    >
      <template #option="{ option }">
        <BaseRenderedMarkdown
          :stringified-markdown="option.label"
          class="font-medium"
        />
      </template>
    </SelectButton>

    <h3 class="text-lg font-bold">
      Organisms
      <span
        v-tooltip.right="{
          value: 'Only guides of the selected organisms will be retained.',
          pt: { text: 'text-xs' }
        }"
        class="-mt-1 inline-block align-top text-sm text-slate-600"
      >
        <icon-fa6-solid-circle-question class="inline" />
      </span>
    </h3>

    <MultiSelect
      v-model="selection.organismIds"
      :options="organismIdOptionsWithDisabling"
      option-label="label"
      option-value="value"
      option-disabled="isDisabled"
      multiple
      class="mr-auto"
      placeholder="Select organisms..."
      :max-selected-labels="3"
      display="chip"
      filter
      @update:model-value="
        (selectedOrganismIds: number[]) =>
          $emit('update:selectionModel', {
            ...selection,
            organismIds: selectedOrganismIds
          })
      "
    >
      <template #value>
        <Chip
          v-for="organismId in selection.organismIds.slice(0, 3)"
          :key="organismId"
          class="mr-1"
          removable
          @remove.stop="
            $emit('update:selectionModel', {
              ...selection,
              organismIds: (selection.organismIds =
                selection.organismIds.filter(
                  (currOrganismId) => currOrganismId !== organismId
                ))
            })
          "
        >
          <BaseRenderedMarkdown
            :stringified-markdown="
              organismIdOptionsBase.find(
                (organismIdOption) => organismIdOption.value === organismId
              )?.label || ''
            "
            class="my-1.5"
          />
        </Chip>

        <template v-if="selection.organismIds.length > 3">
          +{{ selection.organismIds.length - 3 }} others...
        </template>
      </template>
      <template #option="{ option }">
        <BaseRenderedMarkdown :stringified-markdown="option.label" />
      </template>
    </MultiSelect>
  </div>
</template>
