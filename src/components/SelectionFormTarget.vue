<script setup lang="ts">
/**
 * Vue imports
 */
import { computed, ref, toRef, watchEffect } from 'vue'
/**
 * Components imports
 */
import BaseRenderedMarkdown from './BaseRenderedMarkdown.vue'
import Chip from 'primevue/chip'
import SelectButton from 'primevue/selectbutton'
import MultiSelect from 'primevue/multiselect'
import IconFa6SolidCircleQuestion from '~icons/fa6-solid/circle-question'
/**
 * Composables imports
 */
import { useQuery } from '@urql/vue'
/**
 * Other 3rd-party imports
 */
import {
  uniqWith as _uniqWith,
  isEqual as _isEqual,
  remove as _remove
} from 'lodash-es'
/**
 * Types imports
 */
import { ModifType } from '@/gql/codegen/graphql'
/**
 * Utils imports
 */
import { modificationAndTargetSelectionQuery } from '@/gql/queries'

/**
 * A selection.
 */
export interface TargetSelectionModel {
  /** Currently selected target names. */
  targetNames: string[]
  /** Currently selected modification types. */
  modificationTypes: ModifType[]
  /** Currently selected organism IDs.*/
  organismIds: number[]
}

/**
 * Component props
 */
defineProps<{
  /** The current selection. */
  selectionModel?: TargetSelectionModel
  /** The ID of the target which matches the selection if it is unique (i.e. a
   * single target name & a single species is selected), `undefined` otherwise.
   */
  onlyTargetId?: string
}>()

/**
 * Component events.
 */
const emit = defineEmits<{
  /** Event used to update the `v-model:selectionModel` value. */
  'update:selectionModel': [selectionModel: TargetSelectionModel]
  /** Event used to update the `v-model:onlyTargetId` value. */
  'update:onlyTargetId': [onlyTargetId: string | undefined]
}>()

/**
 * Current selection of parameters/filters for the modifications to pick.
 */
const selection = ref<TargetSelectionModel>({
  modificationTypes: [],
  targetNames: [],
  organismIds: []
})

/**
 * Reactive urql GraphQL query object, updated with query state & response.
 */
const gqlQuery = useQuery({
  query: modificationAndTargetSelectionQuery,
  variables: toRef(() => ({
    targetNames: selection.value.targetNames?.length
      ? selection.value.targetNames
      : undefined,
    modificationTypes: selection.value.modificationTypes?.length
      ? selection.value.modificationTypes
      : undefined,
    organismIds: selection.value.organismIds?.length
      ? selection.value.organismIds
      : undefined
  }))
})

/**
 * Available options for modification type selection, independently of other
 * fields' selection.
 */
const modificationTypeOptionsBase = computed(() =>
  _uniqWith(gqlQuery.data.value?.modificationsBase, _isEqual).map(
    (modification) => ({
      label: modification.type_label,
      value: modification.type
    })
  )
)

/**
 * Available modification types, filtered based on other fields' selection (only
 * modification types which exists with selected options are present).
 */
const modificationTypesFiltered = computed(() =>
  _uniqWith(gqlQuery.data.value?.modifications, _isEqual).map(
    (modification) => modification.type
  )
)

/**
 * Available options for modification type selection, with a `isDisabled` field
 * on absence in `modificationTypesFiltered`.
 */
const modificationTypeOptionsWithDisabling = computed(() =>
  modificationTypeOptionsBase.value.map((modificationTypeOption) => ({
    ...modificationTypeOption,
    isDisabled: !modificationTypesFiltered.value.some(
      (modificationTypeFiltered) =>
        modificationTypeFiltered === modificationTypeOption.value
    )
  }))
)

/**
 * Available options for target name selection, independently of other fields'
 * selection.
 */
const targetNameOptionsBase = computed(() =>
  _uniqWith(gqlQuery.data.value?.targetsBase, _isEqual).map((target) => ({
    label: target.name,
    value: target.name,
    groupLabel: target.unit || 'Other'
  }))
)

/**
 * Available target names, filtered based on other fields' selection (only
 * target names which exists with selected options are present).
 */
const targetNamesFiltered = computed(() =>
  _uniqWith(gqlQuery.data.value?.targets, _isEqual).map((target) => target.name)
)

/**
 * Available options for target name selection, with a `isDisabled` field on
 * absence in `targetNamesFiltered`, and grouped.
 */
const targetNameOptionsWithDisablingGroups = computed(() =>
  targetNameOptionsBase.value.reduce<
    {
      label: string
      children: ((typeof targetNameOptionsBase.value)[0] & {
        isDisabled: boolean
      })[]
    }[]
  >((targetNameOptionsWithDisablingGroups, targetNameOption) => {
    const targetNameOptionGroupIndex =
      targetNameOptionsWithDisablingGroups.findIndex(
        (group) => group.label === targetNameOption.groupLabel
      )
    const targetNameOptionGroup =
      targetNameOptionsWithDisablingGroups[targetNameOptionGroupIndex]
    return targetNameOptionGroupIndex !== -1 && targetNameOptionGroup
      ? [
          ...targetNameOptionsWithDisablingGroups.slice(
            0,
            targetNameOptionGroupIndex
          ),
          {
            label: targetNameOptionGroup.label,
            children: [
              ...targetNameOptionGroup.children,
              {
                ...targetNameOption,
                isDisabled: !targetNamesFiltered.value.some(
                  (targetNameFiltered) =>
                    targetNameFiltered === targetNameOption.value
                )
              }
            ]
          },
          ...targetNameOptionsWithDisablingGroups.slice(
            targetNameOptionGroupIndex + 1
          )
        ]
      : [
          ...targetNameOptionsWithDisablingGroups,
          {
            label: targetNameOption.groupLabel,
            children: [
              {
                ...targetNameOption,
                isDisabled: !targetNamesFiltered.value.some(
                  (targetNameFiltered) =>
                    targetNameFiltered === targetNameOption.value
                )
              }
            ]
          }
        ]
  }, [])
)

/**
 * Available options for organism ID selection, independently of other fields'
 * selection.
 */
const organismIdOptionsBase = computed(() =>
  _uniqWith(gqlQuery.data.value?.organismsBase, _isEqual).map((organism) => ({
    label: organism.label,
    value: organism.id
  }))
)

/**
 * Available organism IDs, filtered based on other fields' selection (only
 * organism IDs which exists with selected options are present).
 */
const organismIdsFiltered = computed(() =>
  _uniqWith(gqlQuery.data.value?.organisms, _isEqual).map(
    (organism) => organism.id
  )
)

/**
 * Available options for organism ID selection, with a `isDisabled` field on
 * absence in `organismIdsFiltered`.
 */
const organismIdOptionsWithDisabling = computed(() =>
  organismIdOptionsBase.value.map((organismIdOption) => ({
    ...organismIdOption,
    isDisabled: !organismIdsFiltered.value.some(
      (organismIdFiltered) => organismIdFiltered === organismIdOption.value
    )
  }))
)

/**
 * Watcher to update the `v-model:onlyTargetId` value.
 */
watchEffect(() => {
  if (
    selection.value.organismIds.length === 1 &&
    selection.value.targetNames.length === 1
  ) {
    const onlyTargetId = gqlQuery.data.value?.targets.find(
      (target) => target.name === selection.value.targetNames[0]
    )?.id
    if (onlyTargetId) {
      emit('update:onlyTargetId', onlyTargetId)
      return
    }
  }
  emit('update:onlyTargetId', undefined)
})
</script>

<template>
  <div
    class="grid max-w-max grid-cols-[max-content_1fr] grid-rows-3 items-center gap-x-16 gap-y-8"
  >
    <h3 class="text-lg font-bold">
      Target families
      <span
        v-tooltip.right="{
          value: 'Only targets of the selected units will be retained.',
          pt: { text: 'text-xs' }
        }"
        class="-mt-1 inline-block align-top text-sm text-slate-600"
      >
        <icon-fa6-solid-circle-question class="inline" />
      </span>
    </h3>

    <MultiSelect
      v-model="selection.targetNames"
      :options="targetNameOptionsWithDisablingGroups"
      option-label="label"
      option-value="value"
      option-disabled="isDisabled"
      option-group-label="label"
      option-group-children="children"
      multiple
      class="mr-auto"
      placeholder="Select targets..."
      :max-selected-labels="3"
      display="chip"
      filter
      @update:model-value="
        (selectedTargetNames: string[]) =>
          $emit('update:selectionModel', {
            ...selection,
            targetNames: selectedTargetNames
          })
      "
    >
      <template #value>
        <Chip
          v-for="targetName in selection.targetNames.slice(0, 3)"
          :key="targetName"
          class="mr-1"
          :label="targetName"
          removable
          @remove.stop="
            $emit('update:selectionModel', {
              ...selection,
              targetNames: _remove(
                selection.targetNames,
                (currTargetName) => currTargetName === targetName
              )
            })
          "
        />
        <template v-if="selection.targetNames.length > 3">
          +{{ selection.targetNames.length - 3 }} others...
        </template>
      </template>
    </MultiSelect>

    <h3 class="text-lg font-bold">
      Modification types
      <span
        v-tooltip.right="{
          value:
            'Only targets on which exist modifications of the selected types will be retained.',
          pt: { text: 'text-xs' }
        }"
        class="-mt-1 inline-block align-top text-sm text-slate-600"
      >
        <icon-fa6-solid-circle-question class="inline" />
      </span>
    </h3>

    <SelectButton
      v-model="selection.modificationTypes"
      :options="modificationTypeOptionsWithDisabling"
      option-label="label"
      option-value="value"
      option-disabled="isDisabled"
      multiple
      class="mr-auto"
      @update:model-value="
        (selectedModificationTypes: ModifType[]) =>
          $emit('update:selectionModel', {
            ...selection,
            modificationTypes: selectedModificationTypes
          })
      "
    >
      <template #option="{ option }">
        <BaseRenderedMarkdown
          :stringified-markdown="option.label"
          class="font-medium"
        />
      </template>
    </SelectButton>

    <h3 class="text-lg font-bold">
      Organisms
      <span
        v-tooltip.right="{
          value: 'Only targets of the selected organisms will be retained.',
          pt: { text: 'text-xs' }
        }"
        class="-mt-1 inline-block align-top text-sm text-slate-600"
      >
        <icon-fa6-solid-circle-question class="inline" />
      </span>
    </h3>

    <MultiSelect
      v-model="selection.organismIds"
      :options="organismIdOptionsWithDisabling"
      option-label="label"
      option-value="value"
      option-disabled="isDisabled"
      multiple
      class="mr-auto"
      placeholder="Select organisms..."
      :max-selected-labels="3"
      display="chip"
      filter
      @update:model-value="
        (selectedOrganismIds: number[]) =>
          $emit('update:selectionModel', {
            ...selection,
            organismIds: selectedOrganismIds
          })
      "
    >
      <template #value>
        <Chip
          v-for="organismId in selection.organismIds.slice(0, 3)"
          :key="organismId"
          class="mr-1"
          removable
          @remove.stop="
            $emit('update:selectionModel', {
              ...selection,
              organismIds: _remove(
                selection.organismIds,
                (currOrganismId) => currOrganismId === organismId
              )
            })
          "
        >
          <BaseRenderedMarkdown
            class="my-1.5"
            :stringified-markdown="
              organismIdOptionsBase.find(
                (organismIdOption) => organismIdOption.value === organismId
              )?.label || ''
            "
          />
        </Chip>
        <template v-if="selection.organismIds.length > 3">
          +{{ selection.organismIds.length - 3 }} others...
        </template>
      </template>
      <template #option="{ option }">
        <BaseRenderedMarkdown :stringified-markdown="option.label" />
      </template>
    </MultiSelect>
  </div>
</template>
