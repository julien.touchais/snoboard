<script setup lang="ts">
/**
 * Vue imports
 */
import { computed, onMounted, ref } from 'vue'
/**
 * Components imports
 */
import Button from 'primevue/button'
import IconTablerCircleArrowLeft from '~icons/tabler/circle-arrow-left'
import IconTablerCircleArrowLeftFilled from '~icons/tabler/circle-arrow-left-filled'
/**
 * Composables imports
 */
import { useElementBounding, useWindowScroll } from '@vueuse/core'
/**
 * Other 3rd-party imports
 */
import { SVG, Svg } from '@svgdotjs/svg.js'
import { find as _find } from 'lodash-es'
/**
 * Types imports
 */
import type { G } from '@svgdotjs/svg.js'
import type { Rect } from '@svgdotjs/svg.js'
import type { Text } from '@svgdotjs/svg.js'
import type { HexColorCodeModel } from '@/typings/styleTypes'
import ChromosomeMagnify from './ChromosomeMagnify.vue'

/**
 * A chromosome in the karyotype.
 */
export interface ChromosomeModel {
  /** The name of the chromosome */
  name: string
  /** The ID of the chromosome */
  id: string
  /** The length (in bp) of the chromosome */
  length: number
  /** The position (in bp) at which to represent the centromere center */
  centromereCenter: number
  /** The color in which to fill the chromosome */
  color?: HexColorCodeModel
  /** The text displayed at the left of the chromosome */
  textLeft?: string
  /** The text displayed at the right of the chromosome */
  textRight?: string
  /** Additional metadata */
  metadata?: { [k: string]: unknown }
}

/**
 * An object placed on the genome, which will be represented on a chromosome
 * in the karyotype.
 */
export interface GenomeObjectModel {
  /** The name of the object */
  name: string
  /** The ID of the object */
  id: string
  /** The ID of the chromosome the object is placed on */
  chromosomeId: string
  /** The start position of the object on the chromosome */
  start: number
  /** The end position of the object on the chromosome */
  end: number
  /** The color in which to fill the object */
  color?: HexColorCodeModel
  /** The color in which to fill the object when it is highlighted */
  highlightColor?: HexColorCodeModel
  /** The side of the chromosome on which to place the object */
  side?: 'left' | 'right'
}

/** The dimensions of a chromosome drawn on the karyotype. */
interface ChromosomeDrawDimensionsModel {
  /** Thickness (in px) of the chromosome */
  thickness: number
  /** Length (in px) of the chromosome */
  length: number
}

/**
 * Component props
 */
const props = defineProps<{
  /** Chromosomes to be displayed */
  chromosomes: ChromosomeModel[]
  /** Objects to display on the chromosomes */
  objects?: GenomeObjectModel[]
}>()

const windowScroll = useWindowScroll({ behavior: 'smooth' })

const boardContent = ref<HTMLDivElement>()
const chromosomeElements = ref<{ [k: string]: SVGGElement }>({})
const boardContentBounding = useElementBounding(boardContent)
const remToPixelsRatio = computed(() =>
  parseInt(window.getComputedStyle(boardContent.value!).fontSize)
)

/**
 * In pixels, the minimal size at which to draw karyotype objects
 */
const MIN_OBJECT_LENGTH_PX = 4

/**
 * In pixels, length of the longest chromosome of the karyotype.
 */
const LONGEST_CHROMOSOME_LENGTH_PX = 300
/**
 * Length of the chromosome in expanded (horizontal) representation.
 */
const expandedChromosomeLength = computed(() =>
  Math.min(2 * LONGEST_CHROMOSOME_LENGTH_PX, boardContentBounding.width.value)
)

/**
 * The longest chromosome (in bp) of the karyotype.
 */
const longestChromosome = computed(() =>
  props.chromosomes.reduce(
    (longestChromosome, currChromosome) =>
      longestChromosome && currChromosome.length > longestChromosome.length
        ? currChromosome
        : longestChromosome,
    props.chromosomes[0]
  )
)

/**
 * A dictionary of objects lists indexed by karyotype's chromosome IDs.
 */
const objectsByChromosome = computed(() =>
  props.chromosomes.reduce<{ [k: string]: GenomeObjectModel[] }>(
    (objectsByChromosome, currChromosome) => ({
      ...objectsByChromosome,
      [currChromosome.id]: (props.objects || []).filter(
        (object) => object.chromosomeId === currChromosome.id
      )
    }),
    {}
  )
)

/**
 * Currently expanded chromosome, undefined if the full karyotype is displayed
 */
const expandedChromosome = ref<ChromosomeModel | undefined>(undefined)

/**
 * Collection containing the Objects representing the chromosome's SVGs.
 */
const draw = ref<{ [k: string]: Svg }>({})

/**
 * Fills the `draw` ref by creating the SVG corresponding to every chromosome.
 */
const createChromosomeSVGs = () => {
  props.chromosomes.forEach((chromosome) => {
    // Create the SVG
    const currChromosomeSVG = SVG().addTo(`#${chromosome.id}-svg`)

    // Create the body of the chromosome, made of the 2 arms
    const mainGroup = currChromosomeSVG
      .group()
      .fill(chromosome.color || '#84b650')
      .addClass('chr-svg-main')
    mainGroup.rect().addClass('chr-svg-main-short-arm')
    mainGroup.rect().addClass('chr-svg-main-long-arm')
    // Store DOM element corresponding to each chromosome in a ref
    chromosomeElements.value[chromosome.id] = mainGroup.node

    // Create and fill a group with the objects present on the chromosome
    const objectsGroup = currChromosomeSVG
      .group()
      .addClass('fill-[#abcf74]')
      .addClass('chr-svg-objects')
    for (const object of objectsByChromosome.value[chromosome.id] || []) {
      const rect = objectsGroup.rect().data(object)
      if (object.color) {
        rect.fill(object.color)
      }
    }

    // Create the clip of the object's group, by cloning the chromosome body,
    // and apply the clip
    const clipGroup = currChromosomeSVG.clip()
    for (const path of mainGroup.clone().children()) {
      clipGroup.add(path)
    }
    objectsGroup.clipWith(clipGroup)

    // Create a group for the texts
    const textGroup = currChromosomeSVG
      .group()
      .font({
        style: 'italic'
      })
      .addClass('fill-slate-700 chr-svg-text')

    // Create a group for side texts, and inserts those texts if provided
    const sideTextGroup = textGroup
      .group()
      .font({
        size: '1rem',
        weight: '500',
        anchor: 'middle'
      })
      .addClass('fill-slate-500 chr-svg-text-side')
    if (chromosome.textLeft) {
      sideTextGroup
        .plain(chromosome.textLeft)
        .addClass('chr-svg-text-side-left')
    }
    if (chromosome.textRight) {
      sideTextGroup
        .plain(chromosome.textRight)
        .addClass('chr-svg-text-side-right')
    }

    // Store the SVG
    draw.value[chromosome.id] = currChromosomeSVG
  })
}

/**
 * Updates the SVG of a given chromosome.
 * @param chromosome The chromosome to update SVG.
 * @param chromosomeDrawSize The dimensions at which to draw the chromosome.
 * @param orientation Whether to draw the chromosome horizontally or vertically.
 */
const updateChromosomeSVG = (
  chromosome: ChromosomeModel,
  chromosomeDrawSize: ChromosomeDrawDimensionsModel,
  orientation: 'vertical' | 'horizontal' = 'vertical'
) => {
  // 1. Compute dimensions
  const ONE_BP_LENGTH_PX = chromosomeDrawSize.length / chromosome.length
  // A tenth of the width of the chromosomes is added to arm's length to allow
  // their superposition w/o losing the real length
  const armsOverlap = chromosomeDrawSize.thickness / 5
  const shortArmLength =
    ONE_BP_LENGTH_PX * (chromosome.centromereCenter || 0) + armsOverlap / 2
  const longArmLength =
    ONE_BP_LENGTH_PX *
      (chromosome.length - (chromosome.centromereCenter || 0)) +
    armsOverlap / 2
  const SVGSize =
    orientation === 'vertical'
      ? {
          height: chromosomeDrawSize.length,
          width: chromosomeDrawSize.thickness + 2 * remToPixelsRatio.value + 15
        }
      : {
          height:
            chromosomeDrawSize.thickness + 2 * remToPixelsRatio.value + 15,
          width: chromosomeDrawSize.length
        }
  // Retrieve the processed chromosome's SVG in a dedicated object (mainly for
  // TS not being able to narrow on indexed values, so extracting ensure it is
  // defined)
  const chromosomeSVG = draw.value[chromosome.id]
  if (!chromosomeSVG) return

  // Resize the SVG
  chromosomeSVG.size(SVGSize.width, SVGSize.height)

  // 2. Retrieve SVG objects
  // Casting is needed for Typescript to work, so the notation is a bit verbose
  const chrMainGroup = chromosomeSVG.findOne('g.chr-svg-main') as G
  const shortArmRect = chromosomeSVG.findOne(
    'rect.chr-svg-main-short-arm'
  ) as Rect
  const longArmRect = chromosomeSVG.findOne(
    'rect.chr-svg-main-long-arm'
  ) as Rect
  const objectsGroup = chromosomeSVG.findOne('g.chr-svg-objects') as G
  const objectsGroupClipperShortArmRect = objectsGroup
    .clipper()
    .findOne('rect.chr-svg-main-short-arm') as Rect
  const objectsGroupClipperLongArmRect = objectsGroup
    .clipper()
    .findOne('rect.chr-svg-main-long-arm') as Rect
  const sideTextLeftText = chromosomeSVG.findOne(
    'text.chr-svg-text-side-left'
  ) as Text
  const sideTextRightText = chromosomeSVG.findOne(
    'text.chr-svg-text-side-right'
  ) as Text

  // 3. Base operations (chromosome is vertical, centered, and text horizontal)
  // Chromosome arms
  shortArmRect
    .size(chromosomeDrawSize.thickness, shortArmLength)
    .radius(chromosomeDrawSize.thickness / 2)
    .cx(SVGSize.width / 2)
  longArmRect
    .size(chromosomeDrawSize.thickness, longArmLength)
    .radius(chromosomeDrawSize.thickness / 2)
    .cx(SVGSize.width / 2)
    .y(shortArmLength - armsOverlap)
  // Chromosome objects
  objectsGroup.children().forEach((objectRect) => {
    const objectSide = objectRect.data('side') || 'left'
    const objectLengthPx =
      (objectRect.data('end') - objectRect.data('start')) * ONE_BP_LENGTH_PX
    const isObjectTooSmall = objectLengthPx < MIN_OBJECT_LENGTH_PX
    const objectDimensions = {
      width: chromosomeDrawSize.thickness / 2,
      height: isObjectTooSmall ? MIN_OBJECT_LENGTH_PX : objectLengthPx,
      x:
        objectSide === 'left'
          ? SVGSize.width / 2 -
            chromosomeDrawSize.thickness / 2 -
            chromosomeDrawSize.thickness / 10
          : SVGSize.width / 2 + chromosomeDrawSize.thickness / 10,
      y:
        objectRect.data('start') * ONE_BP_LENGTH_PX -
        (isObjectTooSmall ? MIN_OBJECT_LENGTH_PX / 2 : 0)
    }
    ;(objectRect as Rect)
      .move(objectDimensions.x, objectDimensions.y)
      .size(objectDimensions.width, objectDimensions.height)
      .radius(2)
  })
  // Clip path
  objectsGroupClipperShortArmRect
    .size(chromosomeDrawSize.thickness, shortArmLength)
    .radius(chromosomeDrawSize.thickness / 2)
    .cx(SVGSize.width / 2)
  objectsGroupClipperLongArmRect
    .size(chromosomeDrawSize.thickness, longArmLength)
    .radius(chromosomeDrawSize.thickness / 2)
    .cx(SVGSize.width / 2)
    .y(shortArmLength - chromosomeDrawSize.thickness / 5)
  // Texts
  if (chromosome.textLeft) {
    sideTextLeftText.center(SVGSize.width / 2, SVGSize.height / 2)
  }
  if (chromosome.textRight) {
    sideTextRightText.center(SVGSize.width / 2, SVGSize.height / 2)
  }

  // 4. Transforms (orientation-specific)
  const groupsTransforms =
    orientation === 'vertical'
      ? {
          rotate: 0,
          origin: { x: 0, y: 0 },
          translateX: 0
        }
      : {
          rotate: -90,
          origin: { x: SVGSize.width / 2, y: SVGSize.height / 2 },
          translateX: -SVGSize.width / 2 + SVGSize.height / 2
        }
  const textsTransforms =
    orientation === 'vertical'
      ? {
          left: {
            rotate: -90,
            translate: { x: -(chromosomeDrawSize.thickness / 2 + 15), y: 0 }
          },
          right: {
            rotate: 90,
            translate: { x: chromosomeDrawSize.thickness / 2 + 15, y: 0 }
          }
        }
      : {
          left: {
            rotate: 0,
            translate: { x: 0, y: chromosomeDrawSize.thickness / 2 + 15 }
          },
          right: {
            rotate: 0,
            translate: { x: 0, y: -(chromosomeDrawSize.thickness / 2 + 15) }
          }
        }
  // Main group (chr arms)
  chrMainGroup.transform(groupsTransforms)
  // Objects group
  objectsGroup.transform(groupsTransforms)
  // Texts
  if (chromosome.textLeft) {
    sideTextLeftText.transform(textsTransforms.left)
  }
  if (chromosome.textRight) {
    sideTextRightText.transform(textsTransforms.right)
  }
}

/**
 * Adds highlight on a given object.
 * @param chromosomeId The ID of the chromosome on which to highlight an object.
 * @param objectId The ID of the object to highlight on the provided chromosome.
 * @description This function is made available in component's slots.
 */
const highlightObjectOnChromosome = (
  chromosomeId: string,
  objectId: string
) => {
  // Retrieve the objects group on the provided chromosome
  const objectsGroup = (draw.value[chromosomeId]?.findOne(
    'g.chr-svg-objects'
  ) || undefined) as G | undefined
  // Retrieve the DOM Element corresponding to the requested object from the
  // objects group
  const objectRect = Array.from(objectsGroup?.children() || []).find(
    (objectRect) => objectRect.node.dataset.id === objectId
  )
  // Leave if no object found satisfying both provided IDs
  if (!objectRect) return

  // Retrieve the color in which to highlight the object
  const highlightColor = objectsByChromosome.value[chromosomeId]?.find(
    (object) => object.id === objectId
  )?.highlightColor
  // Apply highlight color
  if (highlightColor) {
    objectRect.fill(highlightColor)
  }
  // Change dimensions
  const objectWidth = parseInt(objectRect.width().toString())
  objectRect.width(objectWidth * 1.25).front()
}

/**
 * Removes highlight on a given object.
 * @param chromosomeId The ID of the chromosome on which to unhighlight an object.
 * @param objectId The ID of the object to unhighlight on the provided chromosome.
 * @description This function is made available in component's slots.
 */
const unhighlightObjectOnChromosome = (
  chromosomeId: string,
  objectId: string
) => {
  // Retrieve the objects group on the provided chromosome
  const objectsGroup = (draw.value[chromosomeId]?.findOne(
    'g.chr-svg-objects'
  ) || undefined) as G | undefined
  // Retrieve the DOM Element corresponding to the requested object from the
  // objects group
  const objectRect = Array.from(objectsGroup?.children() || []).find(
    (objectRect) => objectRect.node.dataset.id === objectId
  )
  // Leave if no object found satisfying both provided IDs
  if (!objectRect) return

  // Retrieve the base color of the object
  const color = objectsByChromosome.value[chromosomeId]?.find(
    (object) => object.id === objectId
  )?.color
  // Restore base color
  if (color) {
    objectRect.fill(color)
  }
  // Restore base dimensions
  const objectWidth = parseInt(objectRect.width().toString())
  objectRect.width(objectWidth * 0.8)
}

/**
 * Callback to expand a given chromosome.
 * @param chromosomeId The ID of the chromosome to expand.
 */
const expandChromosomeIfReduced = (chromosomeId: string) => {
  // Already an expanded chromosome: leave
  if (expandedChromosome.value) return
  // Update state
  expandedChromosome.value = _find(props.chromosomes, ['id', chromosomeId])!

  // Update UI
  updateChromosomeSVG(
    expandedChromosome.value,
    {
      length: expandedChromosomeLength.value,
      thickness: 24
    },
    'horizontal'
  )
  windowScroll.y.value = boardContentBounding.top.value
}

/**
 * Callback to go back to full karyotype.
 */
const reduceChromosome = () => {
  // No longest chromosome means no chromosome at all, so leave
  if (!longestChromosome.value) return

  // Use a size base shared between chromosomes for a representative relative sizing
  const ONE_BP_LENGTH_PX =
    LONGEST_CHROMOSOME_LENGTH_PX / longestChromosome.value.length

  if (expandedChromosome.value) {
    // Update UI
    updateChromosomeSVG(
      expandedChromosome.value,
      {
        length: ONE_BP_LENGTH_PX * expandedChromosome.value.length,
        thickness: 24
      },
      'vertical'
    )
  }
  // setTimeout(() => {
  windowScroll.y.value = boardContentBounding.top.value
  // }, 100)

  // Update state
  expandedChromosome.value = undefined
}

/**
 * Mounted hook, creates & init chromosome SVGs.
 */
onMounted(() => {
  // Create the SVG object for each chromosome
  createChromosomeSVGs()

  // No longest chromosome means no chromosome at all, so leave
  if (!longestChromosome.value) return

  // Use a size base shared between chromosomes for a representative relative sizing
  const ONE_BP_LENGTH_PX =
    LONGEST_CHROMOSOME_LENGTH_PX / longestChromosome.value.length

  props.chromosomes.forEach((chromosome) => {
    updateChromosomeSVG(chromosome, {
      length: ONE_BP_LENGTH_PX * chromosome.length,
      thickness: 24
    })
  })
})
</script>

<template>
  <Button
    v-if="!!expandedChromosome"
    class="group absolute cursor-pointer text-3xl text-slate-400 outline-none transition-colors duration-200 hover:text-slate-600 focus:text-slate-600"
    unstyled
  >
    <icon-tabler-circle-arrow-left
      class="absolute transition-all duration-200 group-hover:opacity-0 group-focus:opacity-0"
    />
    <icon-tabler-circle-arrow-left-filled
      class="absolute opacity-0 transition-all duration-200 group-hover:opacity-100 group-focus:opacity-100"
      @click="reduceChromosome()"
    />
  </Button>

  <div
    ref="boardContent"
    class="flex flex-wrap justify-around gap-y-8 text-base"
  >
    <div
      v-for="(chromosome, index) in chromosomes"
      :key="index"
      :class="[
        'mx-auto flex flex-col justify-between',
        expandedChromosome &&
          expandedChromosome.id !== chromosome.id &&
          'hidden'
      ]"
    >
      <div
        :class="[
          'relative mb-2 flex flex-col rounded-xl border-2 border-dashed border-transparent p-4 px-16 text-center outline-none',
          !expandedChromosome &&
            'transition-all duration-200 before:absolute before:inset-0 before:rounded-xl before:mix-blend-soft-light before:transition-all before:duration-200 hover:border-slate-300 hover:before:bg-white focus:border-slate-300 focus:before:bg-black',
          expandedChromosome && 'cursor-default'
        ]"
        role="button"
        :tabindex="expandedChromosome ? '' : '0'"
        @click="expandChromosomeIfReduced(chromosome.id)"
      >
        <div :class="['mb-8 text-2xl', expandedChromosome && 'mb-12']">
          <h2 class="font-semibold">
            {{
              (expandedChromosome ? 'Chromosome ' : '') + chromosome.name ||
              `chr${index}`
            }}
          </h2>
          <h3
            v-if="expandedChromosome"
            class="font-mono text-lg text-slate-400"
          >
            {{ chromosome.id }}
          </h3>
        </div>
        <div :id="`${chromosome.id}-svg`" class="mx-auto"></div>
        <!-- Using non-null type assertion for `chromosome-element` because it
          is checked in `v-if` and we have no other way to narrow it directly
          here in template -->
        <ChromosomeMagnify
          v-if="
            expandedChromosome?.id === chromosome.id &&
            chromosomeElements[chromosome.id]
          "
          :chromosome="chromosome"
          :chromosome-element="chromosomeElements[chromosome.id]!"
          :objects="objectsByChromosome[chromosome.id]"
        />
      </div>
      <slot
        v-if="!expandedChromosome"
        name="karyotypeChromosomeAppend"
        :chromosome="chromosome"
        :highlight-object="
          (objectId: string) => {
            highlightObjectOnChromosome(chromosome.id, objectId)
          }
        "
        :unhighlight-object="
          (objectId: string) => {
            unhighlightObjectOnChromosome(chromosome.id, objectId)
          }
        "
      ></slot>
      <slot
        v-else-if="expandedChromosome.id === chromosome.id"
        name="expandedChromosomeAppend"
        :chromosome="chromosome"
        :highlight-object="
          (objectId: string) => {
            highlightObjectOnChromosome(chromosome.id, objectId)
          }
        "
        :unhighlight-object="
          (objectId: string) => {
            unhighlightObjectOnChromosome(chromosome.id, objectId)
          }
        "
      ></slot>
    </div>
  </div>
</template>
