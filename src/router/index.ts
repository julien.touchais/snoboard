/**
 * Vue imports
 */
import { createRouter, createWebHistory } from 'vue-router'
/**
 * Components imports
 */
import HomeView from '@/views/HomeView.vue'
/**
 * Composables imports
 */
import { useTitle } from '@vueuse/core'
/**
 * Other 3rd-party imports
 */
import { isNonNullish } from '@/typings/typeUtils'
/**
 * Types imports
 */
import type { SelectionModesType } from '@/views/SelectionView.vue'
import type { AlignmentModesType } from '@/views/AlignmentView.vue'
/**
 * Utils imports
 */
import { ALIGNMENT_MODES, SELECTION_MODES } from '@/utils/constant'

declare module 'vue-router' {
  interface RouteMeta {
    title?: string
    managedTitle?: boolean
  }
}

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: HomeView,
      meta: {
        title: 'Home'
      }
    },
    {
      path: '/select',
      name: 'selection',
      component: () => import('@/views/SelectionView.vue'),
      props: (to) => ({
        initialMode: to.query.mode
      }),
      beforeEnter: (to) => {
        if (
          typeof to.query.mode !== 'string' ||
          !SELECTION_MODES.includes(to.query.mode as SelectionModesType)
        ) {
          router.replace({ name: 'selection', query: { mode: 'guide' } })
        }
      },
      meta: {
        managedTitle: true
      }
    },
    {
      path: '/table',
      name: 'table',
      component: () => import('@/views/DataTableView.vue'),
      props: (to) => ({
        initialColumnIds: Array.isArray(to.query.columns)
          ? to.query.columns
              .filter(isNonNullish)
              .map((column) => column?.split(','))
              .flat()
          : to.query.columns?.split(','),
        initialFilters: Object.entries(to.query).reduce(
          (initialFilters, [key, value]) =>
            key === 'columns' || !value
              ? initialFilters
              : {
                  ...initialFilters,
                  [key]: Array.isArray(value)
                    ? value
                        .filter(isNonNullish)
                        .map((filter) => filter?.split(','))
                        .flat()
                    : value.includes(',')
                      ? value.split(',')
                      : value
                },
          {}
        )
      }),
      meta: {
        title: 'Data table'
      }
    },
    {
      path: '/organism',
      name: 'organism',
      component: () => import('@/views/OrganismView.vue'),
      props: (to) => ({
        organismId:
          Array.isArray(to.query.id) || !to.query.id
            ? -1
            : parseInt(to.query.id)
      }),
      beforeEnter: (to) => {
        if (
          Array.isArray(to.query.id) ||
          !to.query.id ||
          !parseInt(to.query.id)
        ) {
          router.replace({ name: 'notFound' })
        }
      },
      meta: {
        managedTitle: true
      }
    },
    {
      path: '/details',
      children: [
        {
          path: '',
          redirect: {
            name: 'notFound'
          }
        },
        {
          path: 'guide',
          name: 'guideDetails',
          component: () => import('@/views/GuideView.vue'),
          props: (to) => ({
            guideId: to.query.id,
            initialGraphicsPanelTab: to.query.graphicsTab
          }),
          beforeEnter: (to) => {
            if (typeof to.query.id !== 'string') {
              router.replace({ name: 'notFound' })
            }
          },
          meta: {
            managedTitle: true
          }
        },
        {
          path: 'modification',
          name: 'modificationDetails',
          component: () => import('@/views/ModificationView.vue'),
          props: (to) => ({
            modificationId: to.query.id,
            initialGraphicsPanelTab: to.query.graphicsTab
          }),
          beforeEnter: (to) => {
            if (typeof to.query.id !== 'string') {
              router.replace({ name: 'notFound' })
            }
          },
          meta: {
            managedTitle: true
          }
        },
        {
          path: 'target',
          name: 'targetDetails',
          component: () => import('@/views/TargetView.vue'),
          props: (to) => ({
            targetId: to.query.id,
            initialGraphicsPanelTab: to.query.graphicsTab
          }),
          beforeEnter: (to) => {
            if (typeof to.query.id !== 'string') {
              router.replace({ name: 'notFound' })
            }
          },
          meta: {
            managedTitle: true
          }
        },
        {
          path: 'cluster',
          name: 'clusterDetails',
          component: () => import('@/views/ClusterView.vue'),
          props: (to) => ({ clusterId: to.query.id }),
          beforeEnter: (to) => {
            if (typeof to.query.id !== 'string') {
              router.replace({ name: 'notFound' })
            }
          },
          meta: {
            managedTitle: true
          }
        }
      ]
    },
    {
      path: '/alignment',
      name: 'alignment',
      component: () => import('@/views/AlignmentView.vue'),
      props: (to) => ({
        initialMode: to.query.mode
      }),
      beforeEnter: (to) => {
        if (
          typeof to.query.mode !== 'string' ||
          !ALIGNMENT_MODES.includes(to.query.mode as AlignmentModesType)
        ) {
          router.replace({ name: 'alignment', query: { mode: 'guide' } })
        }
      },
      meta: {
        managedTitle: true
      }
    },
    {
      path: '/statistics',
      name: 'statistics',
      component: () => import('@/views/StatisticsView.vue'),
      meta: {
        title: 'Database statistics'
      }
    },
    {
      path: '/api',
      name: 'api',
      component: () => import('@/views/APIView.vue'),
      meta: {
        title: 'API'
      }
    },
    {
      path: '/contact',
      name: 'contact',
      component: () => import('@/views/ContactView.vue'),
      meta: {
        title: 'Contact'
      }
    },
    {
      path: '/legal/:documentId',
      name: 'legal',
      meta: {
        title: 'Legal document'
      },
      props: true,
      component: () => import('@/views/LegalView.vue')
    },
    {
      path: '/:unknownPath(.*)*',
      name: 'notFound',
      component: () => import('@/views/404View.vue'),
      meta: {
        title: "You're lost !"
      },
      beforeEnter: (to) => {
        if (to.params) {
          console.warn(
            `Unknown route : ${import.meta.env.BASE_URL}${
              Array.isArray(to.params.unknownPath)
                ? to.params.unknownPath.join('/')
                : to.params.unknownPath
            }`
          )
        }
        if (to.query) {
          console.warn(`Route query :`, to.query)
        }
      }
    }
  ],
  scrollBehavior: (to, _from, savedPosition) =>
    // Scroll to anchor if present (smoothly), if not to saved position, and if
    // none to top
    to.hash
      ? new Promise((resolve) => {
          setTimeout(() => {
            resolve({
              el: to.hash,
              top: 20,
              behavior: 'smooth'
            })
          }, 500)
        })
      : savedPosition
        ? { ...savedPosition }
        : { top: 0 }
})

router.afterEach((to) => {
  if (!to.meta.managedTitle) {
    useTitle((to.meta.title ? `${to.meta.title} | ` : '') + 'snoBoard')
    console.info('Title changed.')
  }
})

export default router
