/**
 * Vue imports
 */
import { computed, toRef, type MaybeRef, type ComputedRef } from 'vue'
/**
 * Other 3rd-party imports
 */
import { groupBy as _groupBy, pick as _pick } from 'lodash-es'
/**
 * Types imports
 */
import type {
  AlignmentObjectWithTrackIdModel,
  AlignmentTrackModel
} from '@/components/SequenceAlignment.vue'
/**
 * Utils imports
 */
import { isDefined, isNotEmpty } from '@/typings/typeUtils'

/**
 * A conservation analysis type.
 */
export enum ConservationAnalysisTypesEnum {
  /**
   * Visualise modifications present **only** on **all** the selected
   * sequences
   */
  Specific = 0,
  /**
   * Visualise modifications present **at least** on **all** the selected
   * sequences
   */
  Common
}

/**
 * Configuration for the conservation analysis.
 */
export interface ConservationAnalysisConfigModel {
  /** The IDs of the tracks selected for conservation analysis. */
  trackIds: string[]
  /** The object types selected for conservation analysis. */
  objectTypes: string[]
  /** The type of conservation analysis to perform
   * See {@link ConservationAnalysisTypesEnum} for the description of each mode. */
  analysisType: ConservationAnalysisTypesEnum | null
}

/**
 * Reactive conservation analysis between sequences (mainly track)
 * @param tracks The tracks of the alignment on which to perform the
 * conservation analysis (contains selected AND unselected tracks, to be able to
 * perform the *Specific* analysis)
 * @param selectedAnalysisType
 */
export const useConservationAnalysis = (
  tracks: MaybeRef<AlignmentTrackModel[]>,
  config: MaybeRef<ConservationAnalysisConfigModel | null>
): {
  /** The track representing the conservation analysis in the alignment. */
  track: ComputedRef<AlignmentTrackModel>
} => {
  /**
   * All the modifications present on the reference track sequences.
   */
  const referenceTrackModifications = computed<
    AlignmentObjectWithTrackIdModel[]
  >(() =>
    toRef(tracks)
      .value.filter((track) => track.isReference)
      .map(
        (track) =>
          track.objects &&
          Object.values(track.objects).map((object) => ({
            ...object,
            trackId: track.id
          }))
      )
      .flat()
      .filter(isDefined)
  )

  /**
   * Modifications present on selected track sequences.
   */
  const selectedModifications = computed(() =>
    referenceTrackModifications.value.filter((modification) =>
      toRef(config).value?.trackIds.includes(modification.trackId)
    )
  )

  /**
   * Modifications present on selected track sequences, filtered according to the
   * currently selected mode (`selectedConservationAnalysisType`) & modification
   * type(`selectedConservationAnalysisObjectTypes`).
   *
   * See {@link ConservationAnalysisTypesEnum} for the description of each mode.
   */
  const filteredSelectedModifications = computed(() => {
    if (toRef(config).value?.analysisType == null) {
      return []
    }
    return (
      // Group modifications by position & type
      Object.values(
        _groupBy(
          selectedModifications.value,
          (selectedModification) =>
            `${selectedModification.start}-${selectedModification.end}:${selectedModification.type}`
        )
      )
        // Keep only modifications when present on every track selected for
        // analysis (i.e. the number of modifications on a position is the same
        // as the number of selected tracks)
        .filter(isNotEmpty)
        .filter(
          (selectedModificationsByPosition) =>
            selectedModificationsByPosition.length ===
            toRef(config).value?.trackIds.length
        )
        // Keep only the modification informations common to all selected tracks
        // for each modification (reduce modification arrays to a single one for
        // each)
        .map((selectedModificationsByPosition) => {
          return _pick<
            (typeof selectedModificationsByPosition)[0],
            'start' | 'end' | 'color' | 'type' | 'trackId'
          >(selectedModificationsByPosition[0], [
            'start',
            'end',
            'color',
            'type',
            'trackId'
          ])
        })
        // Keep only modifications of the selected types
        .filter(
          (selectedModification) =>
            selectedModification.type &&
            toRef(config).value?.objectTypes.includes(selectedModification.type)
        )
        // If in "specific" analysis mode, keep only modification if absent on
        // every non-selected track (i.e. there is no modification with same
        // positions on a non-selected track)
        .filter((selectedModification) =>
          toRef(config).value?.analysisType ===
          ConservationAnalysisTypesEnum.Specific
            ? !toRef(referenceTrackModifications).value.find(
                (referenceTrackModification) =>
                  // Using a cross-organism ID here would avoid matching 2 different
                  // modifications taking place at the same position
                  referenceTrackModification.start ===
                    selectedModification?.start &&
                  referenceTrackModification.end ===
                    selectedModification?.end &&
                  !toRef(config).value?.trackIds.includes(
                    referenceTrackModification.trackId
                  )
              )
            : selectedModification
        )
    )
  })

  /**
   * Selected tracks for conservation analysis.
   */
  const selectedTracks = computed<AlignmentTrackModel[]>(
    () =>
      toRef(config)
        .value?.trackIds.map((selectedTrackId) =>
          toRef(tracks).value.find((track) => track.id === selectedTrackId)
        )
        .filter(isDefined) || []
  )

  /**
   * If a conservation analysis is selected, the track to display to represent the
   * analysis.
   */
  const conservationAnalysisTrack = computed<AlignmentTrackModel>(() => {
    const objects: {
      [objectId: string]: (typeof filteredSelectedModifications.value)[number]
    } = {}

    const sequence = Array.from(
      {
        length: selectedTracks.value?.[0]?.sequence.length || 0
      },
      (_, index) => {
        const matchingModification = filteredSelectedModifications.value.find(
          (modification) => modification?.start === index + 1
        )

        // No conserved modification at current position
        if (!matchingModification) {
          return '-'
        }

        // Add modification to object list of conservation analysis track
        objects[`CONS_OBJ_${index}`] = {
          ...matchingModification,
          trackId: 'CONSERVATION_TRACK'
        }

        const modificationNucleotide = toRef(tracks).value.find(
          (track) => track.id === matchingModification.trackId
        )?.sequence[index]

        return modificationNucleotide || '↯'
      }
    ).join('')

    return {
      id: 'CONSERVATION_TRACK',
      name: 'Conservation analysis',
      shortname: 'Conservation',
      sequence,
      isInformation: true,
      objects
    }
  })

  return { track: conservationAnalysisTrack }
}
