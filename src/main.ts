import '@/assets/styles/main.css'
import '@/assets/styles/style.css'

// Vue
import { createApp } from 'vue'
import App from './App.vue'

// Pinia
import { createPinia } from 'pinia'

// vue-router
import router from './router'

// PrimeVue
import PrimeVue from 'primevue/config'
import 'primevue/resources/themes/tailwind-light/theme.css'
import Tooltip from 'primevue/tooltip'
// import '@/assets/styles/theme.css'

// urql
import urql from '@urql/vue'
import urqlClient from '@/services/urqlClient'

/* === App creation === */
const app = createApp(App)
// Dependencies
app
  .use(PrimeVue)
  .directive('tooltip', Tooltip)
  .use(createPinia())
  .use(router)
  .use(urql, urqlClient)
  // Mounting
  .mount('#app')
