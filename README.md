<table align="center"><tr><td align="center" width="9999">

<h1>
  <br>
  <a href="https://snoboard.org">
  
  ![snoBoard logo](src/assets/images/logo.svg){width=400}
  </a>
  snoBoard
  <br>
</h1>

![snoBoard usage demo](docs/assets/snoboard-demo.gif){width=100%}

<h3>A guided RNA modifications catalog.</h3>

<h5>

  _Note: this is the repository for the web client of **snoBoard**._
</h5>

<p>
  <a href="https://snoboard.org">
    <img alt="Website" src="https://img.shields.io/website?url=https%3A%2F%2Fsnoboard.org&up_message=%E2%9C%93%20UP&up_color=84cc16&down_message=%E2%9C%98%20DOWN&down_color=dc2626&style=flat-square&label=snoboard.org">
  </a>
  <a href="https://forgemia.inra.fr/metribo/snoboard-client/-/blob/master/LICENSE">
    <img alt="GitLab License" src="https://img.shields.io/gitlab/license/metribo%2Fsnoboard-client?gitlab_url=https%3A%2F%2Fforgemia.inra.fr&style=flat-square&label=License">
  </a>
  <a href="https://forgemia.inra.fr/metribo/snoboard-client/-/issues">
    <img alt="GitLab Issues" src="https://img.shields.io/gitlab/issues/open/metribo%2Fsnoboard-client?gitlab_url=https%3A%2F%2Fforgemia.inra.fr&style=flat-square&logo=gitlab&logoColor=white&label=Issues">
  </a>
  <!--
  <a href="https://doi.org/10./">
    <img alt="Citations" src="https://img.shields.io/endpoint?url=https%3A%2F%2Fapi.juleskreuer.eu%2Fcitation-badge.php%3Fshield%26doi%3D10.%2F&style=flat-square&logo=doi&logoColor=white">
  </a> -->

</p>

<p>
  <a href="#key-features">Key Features</a> •
  <a href="#credits">Credits</a> •
  <a href="#developer-guide">Do it yourself</a>
</p>

</td></tr></table>

## Key Features

- Visualize RNA modifications data from snoBoard's database
- Represent interaction between guide and target RNAs
- Represent target RNAs secondary structure
- Perform conservation analysis across species
  - Visualise sequence alignment
  - Perform bases & modifications conservation analysis on the alignment
- Show statistics about the organisms present in the database
- Explore snoBoard's API without leaving the website

## Credits

This software uses the following open source packages:

- [Vue.js](https://vuejs.org)
- [PrimeVue](https://primevue.org/)
- [Tailwind CSS](https://tailwindcss.com/)
- [Iconify](https://iconify.design/)
- [Lodash](https://lodash.com/)
- [urql](https://commerce.nearform.com/open-source/urql/)
- [Antv G6](https://g6.antv.antgroup.com/en/)
- [Prettier](https://prettier.io/)
- [ESLint](https://eslint.org/)
- [Vite](https://vitejs.dev/)
- [GraphQL codegen](https://the-guild.dev/graphql/codegen)

## Developer guide

Instructions on how to to work on and use snoBoard's client are available in [the project's wiki](../../wikis).